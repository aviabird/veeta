source 'https://rubygems.org'

ruby '2.3.1'

gem 'nokogiri'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2.0'

# Use postgresql as the database for Active Record
gem 'pg'

#########################################################################################################
######################################### Gems for WebDAV Access ########################################

gem 'net_dav'
gem 'curb'
gem 'carrierwave-webdav',  :require => 'carrierwave/webdav'

#########################################################################################################
######################################## Gems for Veeta Button #########################################

# oauth plugins
gem 'oauth'
gem 'oauth-plugin', '~> 0.5.1'

# bootstrap forms
gem 'bootstrap_form'

# sass
gem 'bootstrap-sass', '~> 3.3.5'
gem 'sass-rails', '>= 3.2'

#########################################################################################################
######################################## Bluemix deployment gems ########################################

# bluemix requirements
gem 'rb-readline'
gem "cf-autoconfig", "~> 0.2.1"
gem 'rails_12factor', group: :production

# process manager, starting procfile
gem 'foreman'

#########################################################################################################

# Use Rails-i18n for internationalization
gem 'rails-i18n', '~> 4.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# Use modificated (fixed) version of mandrill-api
gem 'mandrill-api', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/mandrill-api-ruby.git', branch: :master

# Use MandrillMailer for template supported mandrill mails
gem 'mandrill_mailer'

# Application Settings
gem 'settingslogic'

# Enumerize attributes with enumerize
#gem 'enumerize'

# Don't delete records in db, mark it as deleted with paranoia
#gem 'paranoia', '~> 2.0'

# State machine with AASM
#gem 'aasm'

# Extend model validations
#gem 'activevalidators'

# Extend model validations with date
#gem 'date_validator'

# Use kaminari for pagination
gem 'kaminari'

# Use ActiveModel Serializer for model serializations
gem 'active_model_serializers', '~> 0.8.2'

# Use Responders for rendering objects into JSON in our case
gem 'responders', '~> 2.0'

# Use RespondJSON to extend default JSON responses
gem 'respondjson', '=1.0.3', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/respondjson.git', branch: :develop
#gem 'respondjson', path: '/Users/guentr/work/talentsolutions/repos/respondjson'

# Authehticate users with Devise
#gem 'devise', '~> 3.4.1'

# Extend Devise with easy token authenthication
#gem 'simple_token_authentication'

# Extend Devise with advanced token handling for AngularJS
#gem 'devise_token_auth', github: 'lynndylanhurley/devise_token_auth', branch: '910596fa5787617e8b64460eb737f3606284a4cf'

# Send devise emails in the background
#gem 'devise-async'

# Use OmniAuth for 3rd party authentication
gem 'omniauth'

# Authorize users actions with CanCan (Community)
gem 'cancancan', '~> 1.9.0'

# Upload files with CarrierWave
#gem 'carrierwave'

# Use MiniMagick for low-memory image handling
#gem 'mini_magick'

# Use Resque for background jobs
gem 'resque', '1.25.2', git: 'https://github.com/resque/resque.git', branch: '1-x-stable'
gem 'resque-web', '0.0.6', require: 'resque_web'
gem 'resque_mailer', '2.2.7'
gem 'resque-scheduler'
gem 'rufus-scheduler', '< 3.0.0'

# Javascript engine
gem 'therubyracer'

# Bootstrap, older version for resque-web
gem 'twitter-bootstrap-rails', '2.2.8'

# concurrency support
gem 'celluloid', :git => 'https://github.com/celluloid/celluloid', :branch => '0-16-stable'

# Server monitoring with New Relic
gem 'newrelic_rpm'

# Use seedbank for mode advanced dummy/production data import
gem 'seedbank'

# Track user activity with PublicActivity
#gem 'public_activity'

# Use Savon for SOAP communication
gem 'savon'

# Use Nori for converting XML into hash
gem 'nori'

# 0.39.3 Causing error: NoMethodError: undefined method `valid_request_keys' for Excon::Utils:Module
#updated to 0.45: https://github.com/excon/excon/issues/482
gem 'excon', '~> 0.45.1'

# Add tags on models with 'acts as taggable on'
#gem 'acts-as-taggable-on'

# Enable CORS protection
gem 'rack-cors', require: 'rack/cors'

# Generate virtual model attributes with Virtus
gem 'virtus'

# Set default attributes on models with 'default value for'
gem 'default_value_for'

# Generate unique identification code of records with hash ids
gem 'hashids'

# Use LogEntries for tracking logs in cloud
gem 'le'

# Versioning
#gem 'amoeba'

# List of countries with ISO3166-1 and currencies ISO4217
gem 'iso_country_codes', github: 'talentsolutions/iso_country_codes'

# List of languages with ISO-639-1
gem 'language_list', '=1.3.0', github: 'talentsolutions/language_list'

# Administration panel
gem 'vadmin', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/vadmin.git' , branch: :master
#gem 'vadmin', path: '/Users/guentr/work/talentsolutions/repos/vadmin'

# Use Chewy for easier ElasticSearch
#gem 'chewy'

# Use official ElasticSearch Ruby gems for search
gem 'elasticsearch-model'

# Use official ElasticSearch Rails integration
gem 'elasticsearch-rails'

# improve performance
gem 'turbolinks'

# redis cache
gem 'redis-activesupport'

#gem 'myveeta_domain', path: '/Users/guentr/work/talentsolutions/repos/myveeta_domain'
#gem 'myveeta_domain',  '=0.1.5', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/myveeta_domain.git'
gem 'myveeta_domain', '~> 0.0.35', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/myveeta_domain.git', branch: :develop

# http://engineering.harrys.com/2014/07/29/hacking-bundler-groups.html
group :web do

  # wrapper for http requests
  gem 'http'

  #slack notifier
#  gem 'slack-notifier'

  # Passenger App server
  gem 'passenger', '5.0.21'

  # Dummy data generator with Forgery
  #gem 'forgery'

  # xing api wrapper
  gem 'xing_api'

  # linkedin api wrapper
  gem 'linkedin'


  # Use LocaleApp for getting translations
  gem 'localeapp'

  # Security gem for request filtering/blocking/tracking
  # https://github.com/kickstarter/rack-attack
  gem 'rack-attack'

  # https://github.com/HouseTrip/remote_lock
  gem 'remote_lock', "=1.2.0", github: 'HouseTrip/remote_lock'

  # calling intercom API
  gem 'intercom', "~> 3.4.0"
end

group :production, :staging do
  # Track errors with Sentry
  gem 'sentry-raven'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'pry'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Load env variables form file
  gem 'dotenv-rails'

  gem 'rspec-rails'
  gem 'simplecov'
  gem 'database_cleaner'
end

group :development do
  # Use Capistrano for deployment
  gem 'capistrano',  '3.3.4'
  gem 'capistrano-rails', '1.1.1'
  gem 'capistrano-bundler', '1.1.2'

  # Dump current table schema into file with annotate
  gem 'annotate'
end

group :test do
  # Use Spec with minitest
  gem 'minitest-spec-rails'
  gem 'minitest-reporters'

  # Use SimpleCov for code coverage
  #gem 'simplecov'

  # Fake web requests with webmock
  gem 'webmock', require: false

  # Stubs with objects and methods with mocha
  gem 'mocha'

  # Easy active model tests with shoulda
  gem 'shoulda'

  gem 'xing_api'

  gem 'intercom', "~> 3.4.0"

  gem 'remote_lock', "=1.2.0", github: 'HouseTrip/remote_lock'
  # Use FactoryGirl instead of fixtures
  #gem 'factory_girl_rails'

  # Clean up database records with database cleaner
  #gem 'database_cleaner'
end
