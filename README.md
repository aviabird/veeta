# Starting

Run application in virtual environment with every dependencies with just:

```
vagrant up
```

## Creating database

Before you begin, you need to create new development/test database. For this purpose is used PostgreSQL container by Sameersbn (https://github.com/sameersbn/docker-gitlab)

* Login to Vagrant

```
vagrant ssh
```

* Get password of root user (it's changed every build)

```
docker logs postgresql
```

* Create databases/users

```
CREATE ROLE veeta with LOGIN CREATEDB PASSWORD 'secret';
CREATE DATABASE veeta_development;
GRANT ALL PRIVILEGES ON DATABASE veeta_development to veeta;
```

# Develop
Rebuild the database and clear up the tmp
```
rake dev:build
```

Run seed.rb after rebuilt database
```
rake dev:rebuild
```

# Features (branches)

* `responders` - installed responders gem and created dummy testing for controller with json response 
* `mailer` - use Mandrill for sending emails, configured API and connected to Devise

# skip bundle local branch check
```
bundle config disable_local_branch_check true
```
## Autosuggestion

The endpoint for doing search queries on talents is available under */api/recruiter/autosuggestions*

Parameters are

+ params[:key] = *value* - is a string which contain field name like talent.work_experience.company
+ params[:text] = *value* - is a string which contain search word
+ params[:language] = *value* - is a string which contain language de or en

# feature get_referrers
The endpoint for getting list of referrers  *api/talent/referrers/get_referrers*
	
Job_id should be passed in params to get list of referrers of company to which job belongs.