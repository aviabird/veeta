# == Schema Information
#
# Table name: resume_templates
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  preview    :string
#  html       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_resume_templates_on_created_at  (created_at)
#  index_resume_templates_on_name        (name)
#

require "test_helper"

describe ResumeTemplate do
  let(:resume_template) { build_stubbed(:resume_template) }

  validate_presence_of(:name)

  have_db_index(:name)

  it "must be valid" do
    resume_template.must_be :valid?
  end
end
