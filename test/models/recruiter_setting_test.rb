# == Schema Information
#
# Table name: recruiter_settings
#
#  id                  :integer          not null, primary key
#  recruiter_id        :uuid
#  additional_settings :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_recruiter_settings_on_recruiter_id  (recruiter_id)
#

require "test_helper"

describe RecruiterSetting do
  let(:recruiter_setting) { build_stubbed(:recruiter_setting) }

  belong_to :recruiter

  have_db_index(:recruiter_id)

  validate_presence_of :recruiter

  it "must be valid" do
    recruiter_setting.must_be :valid?
  end
end
