# == Schema Information
#
# Table name: admins
#
#  id         :uuid             not null, primary key
#  first_name :string           not null
#  last_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_admins_on_created_at  (created_at)
#

require "test_helper"

describe Admin do
  let(:admin) { build_stubbed(:admin) }

  validate_presence_of(:first_name)
  validate_presence_of(:last_name)

  it "must be valid" do
    admin.must_be :valid?
  end

  it "should be admin" do
    admin.must_be :admin?
  end

  it "wont be talent or recruiter" do
    admin.wont_be :talent?
    admin.wont_be :recruiter?
  end

  it "must have right ability" do
    admin.ability.must_be_instance_of Ability::AdminAbility
  end
end
