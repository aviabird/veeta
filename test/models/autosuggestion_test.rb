# == Schema Information
#
# Table name: autosuggestions
#
#  id         :uuid             not null, primary key
#  key        :string
#  value      :string
#  text       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_autosuggestions_on_created_at  (created_at)
#

require "test_helper"

describe Autosuggestion do
  let(:autosuggestion) { build_stubbed(:autosuggestion) }



  it "must be valid" do
    autosuggestion.must_be :valid?
  end
end
