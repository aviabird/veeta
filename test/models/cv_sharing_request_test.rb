# == Schema Information
#
# Table name: cv_sharing_requests
#
#  id                :uuid             not null, primary key
#  company_id        :uuid             not null
#  recruiter_id      :uuid
#  pool_id           :uuid
#  language_id       :integer          not null
#  status            :string
#  blocking_notice   :boolean
#  contact_details   :text
#  ext_id            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  pool_welcome_text :string
#
# Indexes
#
#  index_cv_sharing_requests_on_company_id   (company_id)
#  index_cv_sharing_requests_on_ext_id       (ext_id)
#  index_cv_sharing_requests_on_language_id  (language_id)
#  index_cv_sharing_requests_on_pool_id      (pool_id)
#

require 'test_helper'

class CvSharingRequestTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
