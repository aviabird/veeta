# == Schema Information
#
# Table name: pools
#
#  id         :uuid             not null, primary key
#  company_id :uuid
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_pools_on_company_id  (company_id)
#  index_pools_on_created_at  (created_at)
#

require "test_helper"

describe Pool do
  let(:pool) { build_stubbed(:pool) }
  belong_to :company
  have_and_belong_to_many :talents

  it "must be valid" do
    pool.must_be :valid?
  end

  it "must have 3 talents in pool" do
    talents = [build_stubbed(:talent), build_stubbed(:talent), build_stubbed(:talent)]
    pool.stubs(:talents).returns(talents)
    pool.talents_count.must_equal 3
  end
end
