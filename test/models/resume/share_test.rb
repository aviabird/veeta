require 'test_helper'

class Resume::ShareTest < ActiveSupport::TestCase
  let(:resume) { build_stubbed(:resume) }
  let(:resume_sharing) { build_stubbed(:resume_sharing) }
  let(:sharing) { Resume::Share.new(resume, [1]) }
  let(:sharing_email_company) { Resume::Share.new(resume, [1], false) }

  it "should clone and share resume" do
    sharing.expects(:clone)
    sharing.expects(:share)
    sharing.call.must_be_instance_of Resume::Share
  end

  it "should clone and share resume with email company" do
    sharing_email_company.expects(:clone)
    sharing_email_company.expects(:share_with_email_application_company)
    sharing_email_company.call.must_be_instance_of Resume::Share

  end

  it "should clone ResumeClone service if is different" do
    # CloningService::ResumeClone.any_instance.stubs(:call)
    CloningService::ResumeClone.expects(:call).with(resume)
    sharing.stubs(:different?).returns(true)
    sharing.send(:clone)
  end

  it "should compare two resumes" do
    # CloningService::ResumeComparison.any_instance.stubs(:call)
    resume.stubs(:latest).returns(build_stubbed(:resume))
    CloningService::ResumeQuickComparison.expects(:call).with(resume, resume.latest)
    sharing.send(:compare)
  end

  it "should recognize if is different" do
    sharing.stubs(:compare).returns(false)
    sharing.must_be :different?
  end

  it "should recognize if is different" do
    resume.stubs(:latest).returns(resume)
    sharing.stubs(:compare).returns(true)
    sharing.wont_be :different?
  end

  it "should creat new sharing" do
    Company.stubs(:find).returns(build_stubbed(:company))
    ResumeSharing.expects(:find_or_initialize_by).once.returns(resume_sharing)
    #removed: mailing done via sidekiq
    #mailer = mock('mailer')
    #mailer.expects(:deliver)
    #JobApplicationAndUpdateMailer.stubs(:send_cv_update).returns(mailer)
    resume_sharing.stubs(:save!)
    sharing.send(:share)
  end
end
