# == Schema Information
#
# Table name: recruiter_talent_infos
#
#  id            :integer          not null, primary key
#  recruiter_id  :uuid
#  talent_id     :uuid
#  followed_at   :datetime
#  hidden_at     :datetime
#  in_pool_since :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_recruiter_talent_infos_on_followed_at    (followed_at)
#  index_recruiter_talent_infos_on_in_pool_since  (in_pool_since)
#  index_recruiter_talent_infos_on_recruiter_id   (recruiter_id)
#  index_recruiter_talent_infos_on_talent_id      (talent_id)
#

require "test_helper"

describe RecruiterTalentInfo do
  let(:recruiter_talent_info) { build_stubbed(:recruiter_talent_info) }

  have_db_index :recruiter_id
  have_db_index :talent_id
  have_db_index :followed_at
  have_db_index :in_pool_since

  belong_to :recruiter
  belong_to :talent

  validate_presence_of :recruiter
  validate_presence_of :talent

  it "must be valid" do
    recruiter_talent_info.must_be :valid?
  end
end
