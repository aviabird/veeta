require "test_helper"

describe Concerns::UserAbility do
  before do
    ABILITY = mock('ability') unless defined?(ABILITY)
    Customer = Struct.new(:name) do
      include Concerns::UserAbility
      
      def ability
        ABILITY
      end
    end unless defined?(Customer)
  end
  let(:customer) { Customer.new('John') }

  it "should delagate can to ability" do
    ABILITY.expects(:can?).with(:this)
    customer.can?(:this)
  end

  it "should delagate cannot to ability" do
    ABILITY.expects(:cannot?).with(:this)
    customer.cannot?(:this)
  end
end