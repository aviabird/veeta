require "test_helper"

module Dummy
  class Car
    include Concerns::Abilited

    ABILITIES = [:manage, :show]
  end
end

describe Concerns::Abilited do
  let (:car) { Dummy::Car.new }
  let (:current_user) { FactoryGirl::build_stubbed(:talent) }

  it "should have right class name" do 
    car.class_constantized.must_equal Dummy::Car
  end

  it "should respond to can" do
    car.respond_to?(:can).must_equal true
  end

  it "should have abilities defined" do
    car.abilities.must_equal(Dummy::Car::ABILITIES)
  end

  describe "define abilities" do
    before do
      current_user.stubs(:can?).with(:manage, car).returns(false)
      current_user.stubs(:can?).with(:show, car).returns(true)
    end

    it "should map abilities" do
      car.can(current_user).must_equal([:show])
    end
  end
end