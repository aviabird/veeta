require "test_helper"

describe Concerns::Compatible do 
  let(:resume) { FactoryGirl.build_stubbed(:resume) }

  it "should call required method" do 
    resume.expects(:active?)
    resume.is_active
  end

  it "should have same result" do
    resume.stubs(:active?).returns(true)
    resume.is_active.must_equal true
  end

  it "doesnt't affect anything else" do
    -> { resume.isnt_active }.must_raise NoMethodError
  end
end
