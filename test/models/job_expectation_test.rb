require 'test_helper'

describe JobExpectation do
  let(:setting) { FactoryGirl.build_stubbed(:talent_setting) }
  let(:talent) { FactoryGirl.build_stubbed(:talent, setting: setting) }
  let(:job_expectation) { JobExpectation.new(talent) }

  it "must delegate setting to user" do
    job_expectation.setting.must_equal setting
  end

  it "must delagate update and update! to setting" do
    setting.expects(:update)
    setting.expects(:update!)
    job_expectation.update
    job_expectation.update!
  end

  it "must delegate all of these thing" do
    setting.expects(:interested)
    setting.expects(:availability)
    setting.expects(:salary_exp_min)
    setting.expects(:salary_exp_max)
    setting.expects(:working_industries)
    setting.expects(:working_locations)
    setting.expects(:prefered_employment_type)
    job_expectation.interested
    job_expectation.availability
    job_expectation.salary_exp_min
    job_expectation.salary_exp_max
    job_expectation.working_industries
    job_expectation.working_locations
    job_expectation.prefered_employment_type
  end

  it "must have right attributes" do
    job_expectation.attributes.has_key?(:interested).must_equal true
    job_expectation.attributes.has_key?(:availability).must_equal true
    job_expectation.attributes.has_key?(:salary_exp_min).must_equal true
    job_expectation.attributes.has_key?(:salary_exp_max).must_equal true
    job_expectation.attributes.has_key?(:working_industries).must_equal true
    job_expectation.attributes.has_key?(:working_locations).must_equal true
    job_expectation.attributes.has_key?(:prefered_employment_type).must_equal true
  end
end
