# == Schema Information
#
# Table name: custom_autosuggestions
#
#  id           :uuid             not null, primary key
#  key          :string
#  scope        :string
#  value        :string
#  text         :string
#  talent_id    :uuid
#  recruiter_id :uuid
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_custom_autosuggestions_on_created_at  (created_at)
#

require "test_helper"

describe CustomAutosuggestion do
  let(:custom_autosuggestion) { build_stubbed(:custom_autosuggestion) }

  belong_to(:talent)
  belong_to(:recruiter)


  it "must be valid" do
    custom_autosuggestion.must_be :valid?
  end
end

