# == Schema Information
#
# Table name: email_applications
#
#  id              :uuid             not null, primary key
#  company_id      :uuid
#  recruiter_id    :uuid
#  company_name    :string
#  jobname         :string
#  recruiter_email :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  draft           :boolean          default(TRUE)
#  unsolicited     :boolean
#
# Indexes
#
#  index_email_applications_on_company_id       (company_id)
#  index_email_applications_on_created_at       (created_at)
#  index_email_applications_on_recruiter_email  (recruiter_email)
#  index_email_applications_on_recruiter_id     (recruiter_id)
#

require 'test_helper'

describe EmailApplication do

  let(:email_application) { build_stubbed(:email_application) }

  should have_many(:sharings).class_name('ResumeSharing')

  have_one(:jobapp)

  validate_presence_of(:jobname)

  have_db_index(:company_id)
  have_db_index(:recruiter_id)
  have_db_index(:recruiter_email)

  it "must be valid" do
    email_application.must_be :valid?
  end
end
