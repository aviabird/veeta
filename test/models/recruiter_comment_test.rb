# == Schema Information
#
# Table name: recruiter_comments
#
#  id             :integer          not null, primary key
#  company_id     :uuid             not null
#  commented_id   :integer          not null
#  commented_type :string           not null
#  recruiter_id   :uuid
#  content        :text
#  deleted_at     :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_recruiter_comments_on_commented_id    (commented_id)
#  index_recruiter_comments_on_commented_type  (commented_type)
#  index_recruiter_comments_on_company_id      (company_id)
#  index_recruiter_comments_on_deleted_at      (deleted_at)
#

require "test_helper"

describe RecruiterComment do
  let(:recruiter_comment) { build_stubbed(:recruiter_comment) }

  belong_to :company
  belong_to :recruiter
  belong_to :commented

  have_db_index :company_id
  have_db_index :commented_id
  have_db_index :commented_type
  have_db_index :deleted_at

  validate_presence_of :company
  validate_presence_of :commented

  it "must be valid" do
    recruiter_comment.must_be :valid?
  end
end
