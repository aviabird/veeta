# == Schema Information
#
# Table name: rs_publications
#
#  id               :uuid             not null, primary key
#  resume_id        :uuid
#  title            :string
#  publication_type :string
#  year             :datetime
#  description      :text
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_rs_publications_on_created_at  (created_at)
#  index_rs_publications_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Publication do
  let(:publication) { build_stubbed(:rs_publication) }
  validate_presence_of(:title)
  validate_presence_of(:publication_type)
  validate_presence_of(:year)

  it "must be valid" do
    publication.must_be :valid?
  end
end
