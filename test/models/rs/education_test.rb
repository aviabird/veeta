# == Schema Information
#
# Table name: rs_educations
#
#  id                :uuid             not null, primary key
#  resume_id         :uuid             not null
#  type_of_education :string
#  school_name       :string
#  degree            :string
#  subject           :string
#  country_id        :integer
#  from              :date
#  to                :date
#  description       :text
#  created_at        :datetime
#  updated_at        :datetime
#  completed         :boolean
#
# Indexes
#
#  index_rs_educations_on_country_id         (country_id)
#  index_rs_educations_on_created_at         (created_at)
#  index_rs_educations_on_from               (from)
#  index_rs_educations_on_resume_id          (resume_id)
#  index_rs_educations_on_school_name        (school_name)
#  index_rs_educations_on_subject            (subject)
#  index_rs_educations_on_to                 (to)
#  index_rs_educations_on_type_of_education  (type_of_education)
#

require "test_helper"

describe Rs::Education do
  let(:education) { build_stubbed(:rs_education) }

  validate_presence_of(:school)
  validate_presence_of(:country_id)
  validate_presence_of(:subject)
  validate_presence_of(:type_of_school)
  validate_presence_of(:from)

  it "must be valid" do
    education.must_be :valid?
  end

  it "must generate validated method" do
  	education.to = nil
  	education.to.must_be_nil
  	education.validated_to.wont_be_nil
  end

  it "cannot be valid with to is earlier than from" do
  	education.from = 2.years.ago
  	education.to = 5.years.ago
  	education.wont_be :valid?
  end

  it "could have nil as to" do
  	education.to = nil
  	education.must_be :valid?
  end

  it "should determinate if current is false" do
  	education.to = DateTime.now
  	education.current.must_equal false
  end

  it "should determinate if current is true" do
  	education.to = nil
  	education.current.must_equal true
  end
end
