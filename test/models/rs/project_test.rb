# == Schema Information
#
# Table name: rs_projects
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  name        :string
#  role        :string
#  link        :string
#  from        :datetime
#  to          :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_projects_on_created_at  (created_at)
#  index_rs_projects_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Project do
  let(:project) { build_stubbed(:rs_project) }
  validate_presence_of(:name)
  validate_presence_of(:role)

  it "must be valid" do
    project.must_be :valid?
  end
end
