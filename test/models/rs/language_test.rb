# == Schema Information
#
# Table name: rs_languages
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  language_id :integer
#  level       :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_languages_on_created_at   (created_at)
#  index_rs_languages_on_language_id  (language_id)
#  index_rs_languages_on_level        (level)
#  index_rs_languages_on_resume_id    (resume_id)
#

require "test_helper"

describe Rs::Language do
  let(:language) { build_stubbed(:rs_language, language: Language.find('en')) }

  validate_presence_of(:language)
  validate_presence_of(:level)

  it "must be valid" do
    language.must_be :valid?
  end
end
