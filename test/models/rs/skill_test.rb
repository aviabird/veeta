# == Schema Information
#
# Table name: rs_skills
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid
#  name       :string
#  level      :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_rs_skills_on_created_at  (created_at)
#  index_rs_skills_on_level       (level)
#  index_rs_skills_on_name        (name)
#  index_rs_skills_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Skill do
  let(:skill) { build_stubbed(:rs_skill) }

  validate_presence_of(:name)

  it "must be valid" do
    skill.must_be :valid?
  end
end
