require 'test_helper'

class Language::HelloTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  it "should get hello from iso code" do
    Language::Hello.by_iso('en').must_equal 'hello'
  end

  it "should get hello from language" do
    language = Language.find('en')
    Language::Hello.by_language(language).must_equal 'hello'
  end
end
