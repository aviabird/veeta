# == Schema Information
#
# Table name: companies
#
#  id                      :uuid             not null, primary key
#  owner_id                :uuid
#  name                    :string           not null
#  tnc_text                :text
#  tnc_link                :string
#  veeta_terms_accepted_at :datetime
#  veeta_terms_accepted_ip :string
#  veeta_terms_accepted_by :string
#  plan                    :string
#  deleted_at              :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  contact                 :text
#  logo                    :string
#  blocking_notice         :boolean          default(TRUE)
#  token                   :string
#  logo_meta               :text
#  short_name              :string
#
# Indexes
#
#  index_companies_on_created_at  (created_at)
#  index_companies_on_deleted_at  (deleted_at)
#  index_companies_on_name        (name)
#  index_companies_on_owner_id    (owner_id)
#  index_companies_on_short_name  (short_name) UNIQUE
#  index_companies_on_token       (token)
#

require "test_helper"

describe Company do
  let(:company) { build_stubbed(:company, owner: build_stubbed(:recruiter)) }
  let(:recruited_company) { build(:company) }

  before do
    recruited_company.owner = build(:recruiter, company: recruited_company)
  end

  let(:recruited_company) { build(:company) }

  have_one(:setting)
  have_many(:talent_infos).class_name('CompanyTalentInfo')
  have_many(:talents).through(:talent_infos)
  have_many(:jobs)
  have_many(:recruiters)
  have_many :recruiter_comments
  have_many :pools
  have_many(:sharings).class_name('ResumeSharing')

  validate_presence_of(:owner)
  validate_presence_of(:name)
  validate_presence_of(:veeta_terms_accepted_at)
  validate_presence_of(:veeta_terms_accepted_ip)
  validate_presence_of(:veeta_terms_accepted_by)
  have_many(:recruiters)
  belong_to(:owner)

  have_db_index(:owner_id)
  have_db_index(:name)
  have_db_index(:deleted_at)

  it "must be valid" do
    company.must_be :valid?
  end

  it "must set informations from current user" do
    company.accept_terms('my@email.com', '10.0.0.1')
    company.veeta_terms_accepted_at.to_s(:db).must_equal DateTime.now.to_s(:db)
    company.veeta_terms_accepted_by.must_equal 'my@email.com'
    company.veeta_terms_accepted_ip.must_equal '10.0.0.1'
  end
end
