# == Schema Information
#
# Table name: recruiter_auths
#
#  id                              :uuid             not null, primary key
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  reset_password_token            :string
#  reset_password_sent_at          :datetime
#  remember_created_at             :datetime
#  sign_in_count                   :integer          default(0), not null
#  current_sign_in_at              :datetime
#  last_sign_in_at                 :datetime
#  current_sign_in_ip              :string
#  last_sign_in_ip                 :string
#  confirmation_token              :string
#  confirmed_at                    :datetime
#  confirmation_sent_at            :datetime
#  unconfirmed_email               :string
#  tokens                          :text
#  provider                        :string
#  uid                             :string           default(""), not null
#  recruiter_id                    :uuid
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  authentication_token            :string
#  authentication_token_created_at :datetime
#  failed_attempts                 :integer          default(0)
#  unlock_token                    :string
#  locked_at                       :datetime
#
# Indexes
#
#  index_recruiter_auths_on_authentication_token  (authentication_token)
#  index_recruiter_auths_on_confirmation_token    (confirmation_token) UNIQUE
#  index_recruiter_auths_on_email                 (email) UNIQUE
#  index_recruiter_auths_on_recruiter_id          (recruiter_id) UNIQUE
#  index_recruiter_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_recruiter_auths_on_unlock_token          (unlock_token) UNIQUE
#

require "test_helper"

describe RecruiterAuth do
  let(:recruiter_auth) { build_stubbed(:recruiter_auth) }

  it "must be valid" do
    recruiter_auth.must_be :valid?
  end


  it "must know which kind of user is used" do
    recruiter_auth.respond_to?(:talent?).must_equal true
    recruiter_auth.respond_to?(:recruiter?).must_equal true
    recruiter_auth.respond_to?(:admin?).must_equal true
  end
end
