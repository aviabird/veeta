# == Schema Information
#
# Table name: logs
#
#  id                  :integer          not null, primary key
#  source              :string
#  source_user_id      :uuid
#  source_user_email   :string
#  source_ip           :string
#  source_referrer_url :string
#  priority            :string
#  key                 :string
#  resource_type       :string
#  resource_id         :uuid
#  status              :uuid
#  details             :string
#  data                :text
#  resume_id           :uuid
#  jobapp_id           :uuid
#  company_id          :uuid
#  talent_id           :uuid
#  recruiter_id        :uuid
#  admin_id            :uuid
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_logs_on_admin_id           (admin_id)
#  index_logs_on_company_id         (company_id)
#  index_logs_on_jobapp_id          (jobapp_id)
#  index_logs_on_key                (key)
#  index_logs_on_priority           (priority)
#  index_logs_on_recruiter_id       (recruiter_id)
#  index_logs_on_resource_id        (resource_id)
#  index_logs_on_resource_type      (resource_type)
#  index_logs_on_resume_id          (resume_id)
#  index_logs_on_source             (source)
#  index_logs_on_source_user_email  (source_user_email)
#  index_logs_on_source_user_id     (source_user_id)
#  index_logs_on_status             (status)
#  index_logs_on_talent_id          (talent_id)
#

require "test_helper"

describe Log do
  let(:log) { build_stubbed(:log) }

  it "must be valid" do
    log.must_be :valid?
  end
end
