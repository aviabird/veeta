# == Schema Information
#
# Table name: veeta_button_sharing_connections
#
#  id                            :uuid             not null, primary key
#  talent_id                     :uuid
#  client_application_id         :uuid
#  oauth_token_id                :integer
#  access_revoked_at             :datetime
#  connected_at                  :datetime
#  deleted_at                    :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  revoke_reason                 :string
#  third_party_data_wiped_out_at :datetime
#
# Indexes
#
#  index_veeta_button_sharing_connections_on_client_application_id  (client_application_id)
#  index_veeta_button_sharing_connections_on_oauth_token_id         (oauth_token_id)
#  index_veeta_button_sharing_connections_on_talent_id              (talent_id)
#

require 'test_helper'

class VeetaButtonSharingConnectionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
