# == Schema Information
#
# Table name: geo_states
#
#  id         :uuid             not null, primary key
#  country_id :integer          not null
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_geo_states_on_country_id  (country_id)
#  index_geo_states_on_created_at  (created_at)
#  index_geo_states_on_name        (name)
#

require "test_helper"

describe Geo::State do
  let(:state) { build(:state) }

  belong_to(:country)

  validate_presence_of(:country)
  validate_presence_of(:name)

  have_db_index(:country_id)
  have_db_index(:name)

  it "must be valid" do
    state.must_be :valid?
  end
end
