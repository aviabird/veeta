# == Schema Information
#
# Table name: geo_addresses
#
#  id             :uuid             not null, primary key
#  country_id     :integer
#  state_id       :uuid
#  zip            :string
#  city           :string
#  address_line   :string
#  address_line_2 :string
#  latitude       :float
#  longitude      :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_geo_addresses_on_address_line    (address_line)
#  index_geo_addresses_on_address_line_2  (address_line_2)
#  index_geo_addresses_on_city            (city)
#  index_geo_addresses_on_country_id      (country_id)
#  index_geo_addresses_on_created_at      (created_at)
#  index_geo_addresses_on_latitude        (latitude)
#  index_geo_addresses_on_longitude       (longitude)
#  index_geo_addresses_on_state_id        (state_id)
#  index_geo_addresses_on_zip             (zip)
#

require "test_helper"

describe Geo::Address do
  let(:address) { build(:address) }

  belong_to(:country)
  belong_to(:state)

  validate_presence_of(:zip)
  validate_presence_of(:city)
  validate_presence_of(:address_line)

  have_db_index(:country_id)
  have_db_index(:state_id)
  have_db_index(:zip)
  have_db_index(:city)
  have_db_index(:address_line)
  have_db_index(:address_line_2)

  have_db_index(:latitude)
  have_db_index(:longitude)

  it "must be valid" do
    address.must_be :valid?
  end
end
