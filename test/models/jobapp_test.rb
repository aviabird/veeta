# == Schema Information
#
# Table name: jobapps
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  job_id                          :uuid             not null
#  job_type                        :string           not null
#  language_id                     :integer
#  cover_letter                    :text
#  contact_preferences             :text
#  blocked_companies               :text
#  source                          :string
#  recruiter_rating                :integer
#  recruiter_note                  :text
#  talent_info_status              :string
#  added_by_recruiter              :boolean          default(FALSE), not null
#  read_at                         :datetime
#  resume_snapshot_id              :uuid
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  withdrawn_at                    :datetime
#  withdrawn_reason                :text
#  access_revoked_at               :datetime
#  hidden_at                       :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  draft                           :boolean          default(TRUE)
#  source_details                  :text
#  revoke_reason                   :string
#  submitted_at                    :datetime
#
# Indexes
#
#  index_jobapps_on_access_revoked_at   (access_revoked_at)
#  index_jobapps_on_added_by_recruiter  (added_by_recruiter)
#  index_jobapps_on_created_at          (created_at)
#  index_jobapps_on_job_id              (job_id)
#  index_jobapps_on_job_type            (job_type)
#  index_jobapps_on_language_id         (language_id)
#  index_jobapps_on_read_at             (read_at)
#  index_jobapps_on_recruiter_rating    (recruiter_rating)
#  index_jobapps_on_resume_id           (resume_id)
#  index_jobapps_on_talent_info_status  (talent_info_status)
#

require 'test_helper'

describe Jobapp do
  let(:jobapp) { build_stubbed(:jobapp) }

  belong_to(:resume)
  belong_to(:job)
  have_one(:expose)
  have_many(:recruiter_comments)

  validate_presence_of(:resume_id)
  validate_presence_of(:job_id)

  have_db_index(:resume_id)
  have_db_index(:job_id)
  have_db_index(:recruiter_rating)
  have_db_index(:talent_info_status)
  have_db_index(:added_by_recruiter)
  have_db_index(:read_at)
  have_db_index(:access_revoked_at)

  # it "must be valid" do
  #   lang = build_stubbed(:language)
  #   jobapp.language = lang
  #   jobapp.job = build_stubbed(:job, accepted_jobs_languages: [lang])
  #   jobapp.must_be :valid?
  # end

  it "could have company" do
    jobapp.applied_to.must_be_instance_of Company
  end

  it "could have responsible person" do
    jobapp.responsible_person.must_be_instance_of Recruiter
  end

  it "doesn't need to have a company" do
    job = build_stubbed(:job, company: nil)
    myapp = build_stubbed(:jobapp, job: job)
    myapp.applied_to.must_equal nil
  end

  it "must have talent who applied" do
    jobapp.applied_by.must_be_instance_of Talent
  end

  it "should update revoked at" do
    jobapp.expects(:save)
    jobapp.revoke
    jobapp.access_revoked_at.wont_equal nil
  end

  # it "should create new instance" do
  #   Jobapp.any_instance.expects(:check)
  #   Jobapp.prepare(build_stubbed(:job)).must_be_instance_of Jobapp
  # end

  it "should add error if job is no longer available" do
    jobapp.job.stubs(:available?).returns(false)
    jobapp.check
    jobapp.errors.wont_be :empty?
  end

  it "should create jobapp with email application" do
    jobapp_params = attributes_for(:jobapp)
    jobapp_params[:job_type] = 'EmailApplication'
    jobapp_params[:job_attributes] = attributes_for(:email_application)
    jobapp = Jobapp.create(jobapp_params)
    jobapp.job.must_be_instance_of EmailApplication
  end

  it "can't create jobapp without valid job" do
    jobapp_params = attributes_for(:jobapp)
    jobapp_params[:job_type] = 'EmailApplication'
    jobapp_params[:job_attributes] = attributes_for(:email_application, jobname: nil)
    jobapp = Jobapp.new(jobapp_params)
    jobapp.stubs(:language).returns(mock('lang'))
    jobapp.wont_be :valid?
  end

  it "should withdraw access" do
    jobapp.stubs(:save)
    jobapp.withdrawn 'Just like that'
    jobapp.withdrawn_reason.must_equal 'Just like that'
    jobapp.withdrawn_at.wont_equal nil
  end

  it "shouldn't update withdrawn date" do
    jobapp.stubs(:save)
    jobapp.withdrawn_at = 2.years.ago
    jobapp.withdrawn_reason = 'Any'
    jobapp.withdrawn 'Again'
    jobapp.errors.wont_be :empty?
  end

  it "should determinate and give dummy data" do
    talent = build_stubbed(:talent)
    talent.jobapps.stubs(:empty?).returns(true)

    Jobapp.expects(:dummy)
    Jobapp.my_or_dummy(talent)
  end

  it "should determinate and give users data" do
    talent = build_stubbed(:talent)
    talent.jobapps.stubs(:empty?).returns(false)

    Jobapp.expects(:my).with(talent)
    Jobapp.my_or_dummy(talent)
  end

  it "should whitelist attributes for email attributes" do
    attributes = { company_name: 'Apple', jobname: 'CEO', source: 'apple.com', recruiter_email: 'steve@apple.com', dummy: 'Something' }
    whitelisted = jobapp.job_attributes_whitelisted(attributes, 'EmailApplication')
    whitelisted.has_key?(:company_name).must_equal true
    whitelisted.has_key?(:jobname).must_equal true
    whitelisted.has_key?(:source).must_equal true
    whitelisted.has_key?(:recruiter_email).must_equal true
    whitelisted.has_key?(:dummy).must_equal false
  end

  it "shouldn't permit anything (but not false) if job" do
    attributes = { company_name: 'Apple', jobname: 'CEO', source: 'apple.com', recruiter_email: 'steve@apple.com', dummy: 'Something' }
    whitelisted = jobapp.job_attributes_whitelisted(attributes, 'Job')
    whitelisted.must_equal({})
  end

  it "should create connection between company and talent" do
    CompanyTalentInfo.expects(:find_or_create_by)
    jobapp.send(:create_connect)
  end

  it "should prepare email application when job id not specified" do
    Jobapp.expects(:prepare_email_application)
    Jobapp.prepare
  end

  it "should prepare job application when job id is specified" do
    Jobapp.expects(:prepare_job_application)
    Jobapp.prepare(job_id: 1)
  end

  it "should prepare new email application" do
    Jobapp.stubs(:save)
    jobapp = Jobapp.prepare_email_application
    jobapp.resume_id.must_be_nil
    jobapp.job.must_be_instance_of EmailApplication
  end

  it "should prepare new Job application" do
    Jobapp.stubs(:save)
    jobapp = Jobapp.prepare_job_application(1, nil)
    jobapp.resume_id.must_be_nil
    jobapp.job_id.must_equal 1
    jobapp.job_type.must_equal 'Job'
  end

  # it "should have errors" do
  #   job = build_stubbed(:job, language: 'en')
  #   resume = build_stubbed(:resume, language: 'de')
  #   new_job = build_stubbed(:jobapp, job: job, resume: resume)
  #   build_stubbed(:jobapp, job: job, resume: resume).wont_be :valid?
  # end

  # # Different language check
  # it "should validate language inclusion" do
  #   jobapp.language = build_stubbed(:language)
  #   jobapp.job = build_stubbed(:job, accepted_jobs_languages: [build_stubbed(:language)])
  #   jobapp.wont_be :valid?
  # end
end
