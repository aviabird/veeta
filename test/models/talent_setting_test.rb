# == Schema Information
#
# Table name: talent_settings
#
#  id                            :uuid             not null, primary key
#  talent_id                     :uuid             not null
#  additional_settings           :text
#  country_id                    :integer
#  professional_experience_level :integer
#  newsletter_locale             :string
#  newsletter_updates            :string
#  working_locations             :text
#  working_industries            :text
#  availability                  :string
#  job_seeker_status             :string           default("very_much")
#  prefered_employment_type      :string
#  salary_exp_min                :integer
#  salary_exp_max                :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
# Indexes
#
#  index_talent_settings_on_created_at  (created_at)
#  index_talent_settings_on_talent_id   (talent_id)
#

require "test_helper"

describe TalentSetting do
  let(:talent_setting) { build_stubbed(:talent_setting) }

  belong_to(:talent)

  have_db_index(:talent_id)

  it "must be valid" do
    talent_setting.must_be :valid?
  end

  it "must take locale from talent" do
    talent = build_stubbed(:talent, locale: 'cs')
    talent_setting.stubs(:talent).returns(talent)
    talent_setting.newsletter_locale = nil
    talent_setting.newsletter_locale.must_equal 'cs'
  end

  it "must update just one attribute" do
    talent_setting.expects(:update_attribute).with(:locale, 'cs')
    talent_setting.expects(:update).never
    talent_setting.smart_update(talent_setting: { locale: 'cs' })
  end

  it "must update object" do
    params = { talent_setting: { locale: 'cs', language: 'cz'} }
    talent_setting.expects(:update_attribute).never
    talent_setting.expects(:update).with(params)
    talent_setting.smart_update(params)
  end
end
