# == Schema Information
#
# Table name: cv_sharing_responses
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  cv_sharing_request_id           :uuid             not null
#  cv_sharing_request_type         :string           not null
#  blocked_companies               :text
#  read_at                         :datetime
#  draft                           :boolean          default(TRUE)
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  access_revoked_at               :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  update_only                     :boolean          default(FALSE)
#  revoke_reason                   :string
#  submitted_at                    :datetime
#
# Indexes
#
#  index_cv_sharing_responses_on_access_revoked_at      (access_revoked_at)
#  index_cv_sharing_responses_on_cv_sharing_request_id  (cv_sharing_request_id)
#  index_cv_sharing_responses_on_read_at                (read_at)
#  index_cv_sharing_responses_on_resume_id              (resume_id)
#

require 'test_helper'

class CvSharingResponseTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
