# == Schema Information
#
# Table name: company_talent_infos
#
#  id                    :uuid             not null, primary key
#  company_id            :uuid
#  talent_id             :uuid
#  rating                :integer
#  note                  :text
#  category              :string
#  sequential_id         :integer          not null
#  hashed_talent_id      :string
#  hashed_talent_id_salt :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_company_talent_infos_on_company_id                    (company_id)
#  index_company_talent_infos_on_hashed_talent_id              (hashed_talent_id)
#  index_company_talent_infos_on_sequential_id                 (sequential_id)
#  index_company_talent_infos_on_sequential_id_and_company_id  (sequential_id,company_id) UNIQUE
#  index_company_talent_infos_on_talent_id                     (talent_id)
#

require "test_helper"

describe CompanyTalentInfo do
  let(:company_talent_info) { build_stubbed(:company_talent_info) }

  validate_presence_of(:company)
  validate_presence_of(:talent)

  have_db_index(:company_id)
  have_db_index(:talent_id)

  it "must be valid" do
    company_talent_info.must_be :valid?
  end

end
