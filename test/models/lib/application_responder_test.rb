require "test_helper"

# Create new dummy module to avoid conflicts with our current application
module Dummy

  # Define new model for testing purposes
  class Talent
    def initialize name = 'John Doe'
      @name = name
    end
  end

  # Define new controller for testing
  class TalentsController < ApplicationController

    # Index returns multiple instances of resource
    def index
      @talents = [Talent.new, Talent.new]
      respond_with @talents
    end

    # Show returns just one instance, it creates new one each time
    def show
      @talent = Talent.new 'John Doe'
      respond_with @talent
    end
  end
end