require "test_helper"

describe GeneralMailHelper do
  GeneralMailHelperMailer = Struct.new(:deliver) do
    include GeneralMailHelper
  end

  let(:setting)   { build_stubbed(:talent_setting)}
  let(:talent)    { build_stubbed(:talent, setting: setting) }
  let(:resume)    { build_stubbed(:resume, talent: talent)}
  let(:recruiter) { build_stubbed(:recruiter) }
  let(:company)   { build_stubbed(:company, recruiters: [recruiter]) }
  let(:job)       { build_stubbed(:job, company: company) }
  let(:jobapp)    { build_stubbed(:jobapp, job: job, resume: resume) }
  let(:mailer)    { GeneralMailHelperMailer.new }

  it "must create pdf as a resume pdf" do
    mailer.stubs(:resume).returns(resume)
    PDFService::Resume.expects(:pdf).with(resume)
    mailer.send(:resume_pdf)
  end

  it "must get applicant name" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_name).must_equal talent.name
  end

  it "must get applicant first name" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_firstname).must_equal talent.first_name
  end

  it "must get applicant last name" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_lastname).must_equal talent.last_name
  end

  it "must get applicant age" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_age).must_equal talent.age
  end

  it "must get applicant email" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_email).must_equal talent.email
  end

  it "must get applicant phone" do
    mailer.stubs(:talent).returns(talent)
    mailer.send(:applicant_phone).must_equal talent.phone_number
  end

  it "must translate city and country code" do
    talent.address.city = 'Wien'
    talent.address.stubs(:country).returns(Country.find('AT'))
    mailer.stubs(:talent).returns(talent)
    I18n.expects(:t).with('recruiter.emails.general.city_and_ctrycode', {city: 'Wien', ctry_code: 'AT'})
    mailer.send(:applicant_city_country)
  end

  it "must get job title" do
    mailer.stubs(:job).returns(job)
    mailer.send(:job_title).must_equal job.name
  end

  it "must translate source" do
    jobapp.stubs(:source).returns('karriere')
    mailer.stubs(:jobapp).returns(jobapp)
    I18n.expects(:t).with('talent.data.referrer.karriere')
    mailer.send(:job_source)
  end

  it "must translate availability" do
    setting.stubs(:availability).returns('1m')
    mailer.stubs(:talent).returns(talent)
    I18n.expects(:t).with('talent.data.available.within_month')
    mailer.send(:availability)
  end

  it "must translate price range" do
    mailer.stubs(:talent).returns(talent)
    setting.stubs(:price_range_email_key).returns('talent.data.salary')

    I18n.expects(:t).with('recruiter.emails.notification.update.full.price_range_email_key', from: '1.000 €', to: '2.000 €')
    mailer.send(:price_range)
  end

  it "must get cover letter name" do
    mailer.stubs(:jobapp).returns(jobapp)
    mailer.send(:cover_letter).must_equal jobapp.cover_letter
  end

  it "must have admin note" do
    mailer.stubs(:jobapp).returns(jobapp)
    mailer.send(:admin_notes).must_equal jobapp.contact_preferences
  end

  it "must generate links to attachments with breaks" do
    pdf = build_stubbed(:rs_document, name: 'cv.pdf')
    portfolio = build_stubbed(:rs_document, name: 'portfolio.pdf')
    resume.stubs(:documents).returns([pdf, portfolio])
    mailer.stubs(:resume).returns(resume)
    I18n.stubs(:t).with('talent.data.document.reference').returns('Reference')
    I18n.expects(:t).with('recruiter.emails.resume.documents_BR', {doc_name: 'cv.pdf', doc_type: 'Reference', doc_url: pdf.url}).returns('-Resume-')
    I18n.expects(:t).with('recruiter.emails.resume.documents_BR', {doc_name: 'portfolio.pdf', doc_type: 'Reference', doc_url: portfolio.url}).returns('-Portfolio-')

    mailer.send(:attached_documents_br).must_equal '-Resume-<br/>-Portfolio-'
  end

  it "must generate links to attachments as a list" do
    pdf = build_stubbed(:rs_document, name: 'cv.pdf')
    portfolio = build_stubbed(:rs_document, name: 'portfolio.pdf')
    resume.stubs(:documents).returns([pdf, portfolio])
    mailer.stubs(:resume).returns(resume)
    I18n.stubs(:t).with('talent.data.document.reference').returns('Reference')
    I18n.expects(:t).with('recruiter.emails.resume.documents_UL', {doc_name: 'cv.pdf', doc_type: 'Reference', doc_url: pdf.url, li_css_class: ''}).returns('-Resume-')
    I18n.expects(:t).with('recruiter.emails.resume.documents_UL', {doc_name: 'portfolio.pdf', doc_type: 'Reference', doc_url: portfolio.url, li_css_class: ''}).returns('-Portfolio-')

    mailer.send(:attached_documents_ul).must_equal '<ul class="additional-documents"><li>-Resume-</li><li>-Portfolio-</li></ul>'
  end

  it "must get recruiter name" do
    mailer.stubs(:recruiter).returns(recruiter)
    mailer.send(:recruiter_name).must_equal recruiter.name
  end

  it "must get recruiter first name" do
    mailer.stubs(:recruiter).returns(recruiter)
    mailer.send(:recruiter_firstname).must_equal recruiter.first_name
  end

  it "must get recruiter last name" do
    mailer.stubs(:recruiter).returns(recruiter)
    mailer.send(:recruiter_lastname).must_equal recruiter.last_name
  end

  it "must get recruiter email" do
    mailer.stubs(:recruiter).returns(recruiter)
    mailer.send(:recruiter_email).must_equal recruiter.email
  end

  it "must get recruiter phone" do
    mailer.stubs(:recruiter).returns(recruiter)
    mailer.send(:recruiter_phone).must_equal recruiter.phone_number
  end

  it "must get company name" do
    mailer.stubs(:company).returns(company)
    mailer.send(:company_name).must_equal company.name
  end

  it "must merge working industries with comma" do
    mailer.stubs(:talent).returns(talent)
    setting.stubs(:working_industries).returns(['IT', 'Marketing'])
    mailer.send(:working_industries).must_equal 'IT, Marketing'
  end

  it "must merge working areas with comma" do
    mailer.stubs(:talent).returns(talent)
    setting.stubs(:working_locations).returns(['Wien', 'Zurich'])
    mailer.send(:working_locations).must_equal 'Wien, Zurich'
  end

  it "must translate terms of employment" do
    setting.stubs(:prefered_employment_type).returns('self_employed')
    mailer.stubs(:talent).returns(talent)
    I18n.expects(:t).with('talent.data.toe.self_employed')
    mailer.send(:toe)
  end

  it "must translate job interest" do
    setting.stubs(:job_seeker_status).returns('very')
    mailer.stubs(:talent).returns(talent)
    I18n.expects(:t).with('talent.data.job_interest.very')
    mailer.send(:job_seeker_status)
  end

  it "must have assigned variables" do
    mailer.stubs(:talent).returns(talent)
    mailer.stubs(:applicant_name)
    mailer.stubs(:applicant_firstname)
    mailer.stubs(:applicant_lastname)
    mailer.stubs(:applicant_age)
    mailer.stubs(:applicant_city)
    mailer.stubs(:applicant_city_country)
    mailer.stubs(:applicant_country_iso)
    mailer.stubs(:applicant_email)
    mailer.stubs(:applicant_phone)
    mailer.stubs(:job_title)
    mailer.stubs(:job_source)
    mailer.stubs(:availability)
    mailer.stubs(:price_range)
    mailer.stubs(:cover_letter)
    mailer.stubs(:admin_notes)
    mailer.stubs(:attached_documents_br)
    mailer.stubs(:attached_documents_ul)
    mailer.stubs(:recruiter_name)
    mailer.stubs(:recruiter_firstname)
    mailer.stubs(:recruiter_lastname)
    mailer.stubs(:recruiter_email)
    mailer.stubs(:recruiter_phone)
    mailer.stubs(:company_name)
    result = mailer.send(:vars)
    result.has_key?('APPLICANT_NAME').must_equal true
    result.has_key?('APPLICANT_FIRSTNAME').must_equal true
    result.has_key?('APPLICANT_LASTNAME').must_equal true
    result.has_key?('APPLICANT_AGE_YEARS').must_equal true
    result.has_key?('APPLICANT_EMAIL').must_equal true
    result.has_key?('APPLICANT_PHONE').must_equal true
    result.has_key?('JOB_TITLE').must_equal true
    result.has_key?('JOB_SOURCE').must_equal true
    result.has_key?('AVAILABLE_IN').must_equal true
    result.has_key?('PRICE_RANGE').must_equal true
    result.has_key?('COVERLETTER').must_equal true
    result.has_key?('ADMIN_NOTES').must_equal true
    result.has_key?('ADDED_DOCUMENTS_AS_BR').must_equal true
    result.has_key?('ADDED_DOCUMENTS_AS_UL').must_equal true
    result.has_key?('RECRUITER_NAME').must_equal true
    result.has_key?('RECRUITER_FIRSTNAME').must_equal true
    result.has_key?('RECRUITER_LASTNAME').must_equal true
    result.has_key?('RECRUITER_EMAIL').must_equal true
    result.has_key?('RECRUITER_PHONE').must_equal true
    result.has_key?('COMPANY_NAME').must_equal true
  end
end
