require "test_helper"

describe RouterMailHelper do
  RouterMailHelperMaileer = Struct.new(:deliver) do
    include RouterMailHelper
  end

  let(:mailer) { RouterMailHelperMailer.new }

  it "must have routes" do
    mailer.root_path.must_equal '/'
  end
end
