require "test_helper"

describe MandrillMailHelper do
  MandrillMailHelperMailer = Struct.new(:deliver) do
    include MandrillMailHelper
  end

  let(:mailer) { MandrillMailHelperMailer.new }

  it "must generate template name" do
    mailer.template('mandrill-app', :en).must_equal 'mandrill-app'
    mailer.template('mandrill-app', :de).must_equal 'mandrill-app-de'
  end

  it "must invoke sending email mandrill app" do
    mailer.expects(:mandrill_mail).with(template: 'ma-de', to: 'john@doe.com', vars: {'A'=>'B'}, important: true, inline_css: true)
    mailer.send_as_mandrill_mail('ma', 'de', 'john@doe.com', {'A'=>'B'})
  end
end
