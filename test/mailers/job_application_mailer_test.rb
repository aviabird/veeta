require "test_helper"

describe JobApplicationAndUpdateMailer do
  let(:setting)   { build_stubbed(:talent_setting)}
  let(:talent)    { build_stubbed(:talent, setting: setting) }
  let(:resume)    { build_stubbed(:resume, talent: talent)}
  let(:recruiter) { build_stubbed(:recruiter) }
  let(:company)   { build_stubbed(:company, recruiters: [recruiter]) }
  let(:job)       { build_stubbed(:job, company: company) }
  let(:jobapp)    { build_stubbed(:jobapp, job: job, resume: resume) }
  let(:mailer)    { JobApplicationAndUpdateMailer.new }

  # Tests are missing
end
