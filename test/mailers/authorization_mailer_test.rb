require "test_helper"

describe AuthorizationMailer do
  let(:talent) { build_stubbed(:talent) }
  let(:auth) { build_stubbed(:talent_auth, talent: talent) }

  it "must send mail via mandrill" do
    AuthorizationMailer.any_instance.expects(:send_mandrill_mail)
    AuthorizationMailer.reset_password_instructions(auth, '123')
  end

  it "must send mail via mandrill" do
    auth.stubs(:confirmation_token).returns('123')
    AuthorizationMailer.any_instance.expects(:send_mandrill_mail)
    AuthorizationMailer.confirmation_instructions(auth, '123')
  end
end
