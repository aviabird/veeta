require "test_helper"

describe AuthMailer do
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:auth) { FactoryGirl.build_stubbed(:talent_auth, talent: talent, email: 'john@doe.com') }
  before { auth.stubs(:authentication_token).returns('123x') }
  
  # it "must send an email" do
  #   AuthMailer.any_instance.expects(:send_as_mandrill_mail)
  #   AuthMailer.continue_applying(auth)
  # end

  # it "must have right options" do
  #   mailer = AuthMailer.new
  #   mailer.stubs(:send_as_mandrill_mail)
  #   mailer.continue_applying(auth)
  #   vars = mailer.instance_variable_get(:@vars)
  #   vars.must_equal({'TALENT_SITE_URL' => 'http://localhost/redirect?to=%2Fapp%2Ftalent&user_email=john%40doe.com&user_token=123x'})
  # end
end
