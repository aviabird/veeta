require "test_helper"

module DummyTest
  DummyTest::Routes = ActionDispatch::Routing::RouteSet.new
  DummyTest::Routes.draw do
    get '/:controller(/:action(/:id))'
  end

  class Dummy
  end

  class DummyController < Api::ApplicationController
    def index
      @dummies = [ Dummy.new, Dummy.new ]
      respond_with @dummies, @options
    end
  end
end

describe DummyTest::DummyController do
  before do
    @routes = DummyTest::Routes
  end

  it 'should generate empty options hash' do
    get :index, format: :json
    assigns(:options).must_equal({})
  end

  it 'should take page and per params' do
    get :index, format: :json, page: 1, per: 10
    assigns(:options)[:page].must_equal 1
    assigns(:options)[:per].must_equal 10
  end
end