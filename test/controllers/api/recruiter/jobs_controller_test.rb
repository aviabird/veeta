require "test_helper"

describe Api::Recruiter::JobsController do
  let(:job) { build_stubbed(:job) }
  let(:current_user) { build_stubbed(:recruiter) }

  before do 
    Job.stubs(:find).returns(job)
    @controller.stubs(:current_user).returns(current_user)
  end

  describe 'GET /api/recruiter/jobs' do 
    it "should get only jobs from current company" do 
      Job.expects(:maintained_by).with(current_user)
      get :index, format: :json
    end
  end

  # Not necessary to test (here)
  describe 'GET /api/recruiter/jobs/1' do
    it 'should show job' do
      get :show, format: :json, id: job
      response.status.must_equal 200
      assigns(:job).must_equal job
    end
  end

  describe 'GET /api/recruiter/jobs/new' do
    it "should get new structure" do
      get :new, format: :json
      response.status.must_equal 200
      assigns(:job).must_be_instance_of Job
    end
  end

  describe 'POST /api/recruiter/jobs' do
    it "should create new job" do
      Job.expects(:create).returns(job)
      
      post :create, format: :json, job: attributes_for(:job)
      response.status.must_equal 200
    end
  end

  describe 'PUT /api/recruiter/jobs/1' do
    it "should update current job" do 
      job.expects(:update).returns(true)

      put :update, format: :json, id: job, job: attributes_for(:job)
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/recruiter/jobs/1' do
    it "should delete job" do
      job.expects(:destroy)

      delete :destroy, format: :json, id: job
      response.status.must_equal 200
    end
  end

  describe 'POST /api/recruiter/jobs/validate' do
    it "should validate without save" do
      Job.any_instance.expects(:save).never
      Job.any_instance.expects(:valid?)

      post :validate, format: :json, job: attributes_for(:job)
      assigns(:job).must_be_instance_of Job
    end
  end
end
