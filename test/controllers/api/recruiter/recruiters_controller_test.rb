require "test_helper"

describe Api::Recruiter::RecruitersController do
  let(:recruiter) { build_stubbed(:recruiter) }
  let(:current_user) { build_stubbed(:recruiter_auth) }

  before do 
    current_user.stubs(:recruiter).returns(recruiter)
    @controller.stubs(:current_user).returns(current_user)
  end

  describe 'GET /api/recruiter/recruiter' do
    it 'should show recruiter' do
      get :show, format: :json
      response.status.must_equal 200
      assigns(:recruiter).must_equal recruiter
    end
  end

  describe 'PUT /api/recruiter/recruiter' do
    it "should update recruiter" do
      recruiter.expects(:save)
      put :update, format: :json, recruiter: { first_name: 'Steve' }
      response.status.must_equal 200
    end
  end

  describe 'PUT /api/recruiter/recruiter/validate' do
    it "should just validate the input" do      
      put :validate, format: :json, recruiter: { email: nil }
      response.status.must_equal 422
      assigns(:recruiter).email.must_be_nil
    end
  end

  # We don't have proper recruiter in database
  # describe 'PUT /api/recruiter/recruiter/validate' do
  #   it "should return just success" do
  #     put :validate, format: :json, recruiter: attributes_for(:recruiter)
  #     response.status.must_equal 200
  #   end
  # end
end
