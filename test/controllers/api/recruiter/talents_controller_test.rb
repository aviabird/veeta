require "test_helper"

describe Api::Recruiter::TalentsController do
  let(:talent) { build_stubbed(:talent) }
  let(:company) { build_stubbed(:company) }
  let(:recruiter) { build_stubbed(:recruiter, company: company) }
  let(:current_user) { build_stubbed(:recruiter_auth, recruiter: recruiter) }
  let(:company_info) { build_stubbed(:company_talent_info) }

  before do 
    talent.stubs(:company_info).returns(company_info)
    Talent.stubs(:find).returns(talent)
    @controller.stubs(:current_recruiter).returns(recruiter)
    @controller.stubs(:current_user).returns(current_user)
  end

  describe 'GET /api/recruiter/talents' do
    it 'should get list of talents' do
      skip('test with elasticsearch')
      Talent.expects(:in_company).returns([talent])
      get :index, format: :json
      response.status.must_equal 200
    end
  end

  describe 'GET /api/recruiter/talents/:id' do
    it "should show talent" do
      get :show, format: :json, id: talent
      response.status.must_equal 200
    end
  end
end
