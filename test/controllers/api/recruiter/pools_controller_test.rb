require "test_helper"

describe Api::Recruiter::PoolsController do
  let(:pool) { build_stubbed(:pool) }
  let(:current_user) { build_stubbed(:recruiter_auth) }
  let(:current_recruiter) { build_stubbed(:recruiter) }

  before do 
    Pool.stubs(:find).returns(pool)
    current_recruiter.stubs(:company).returns(build_stubbed(:company))
    @controller.stubs(:current_recruiter).returns(current_recruiter)
  end

  describe 'GET /api/recruiter/pools' do
    it 'should get list of pools' do
      Pool.expects(:in_company).returns({pools:[pool]})
      get :index, format: :json
      response.status.must_equal 200
    end
  end

  describe 'GET /api/recruiter/pools/:id' do
    it "should show pool" do
      get :show, format: :json, id: pool
      response.status.must_equal 200
    end
  end
end
