require "test_helper"

class Api::Recruiter::JobappsControllerTest < ActionController::TestCase
  let(:jobapp) { build_stubbed(:jobapp) }
  let(:current_user) { build_stubbed(:recruiter) }

  before do 
    Job.stubs(:find).returns(build_stubbed(:job))
  end

  describe 'GET /api/talent/jobapps' do
    it 'should get list of jobapps' do
      skip('test with elasticsearch')
      Jobapp.expects(:maintained_by).returns([jobapp])
      get :index, format: :json
      response.status.must_equal 200
    end
  end

  describe 'GET /api/talent/jobapps/new' do
    it "shouldn't have new route" do
      -> {
        get :new, format: :json, job_id: 1
      }.must_raise ActionController::UrlGenerationError
    end
  end

  describe 'POST /api/talent/jobapps' do
    it "shouldn't have create route" do
      -> {
        post :create, format: :json, jobapp: attributes_for(:jobapp)
      }.must_raise ActionController::UrlGenerationError
    end
  end
end
