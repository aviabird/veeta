require "test_helper"

describe Api::Recruiter::ShortlistsController do
  let(:talent) { build_stubbed(:talent) }
  let(:current_user) { build_stubbed(:recruiter_auth) }
  let(:current_recruiter) { build_stubbed(:recruiter) }

  before do 
    @controller.stubs(:current_recruiter).returns(current_recruiter)
  end

  describe 'GET /api/recruiter/shortlists' do
    it 'should get list of talents' do
      get :index, format: :json
      response.status.must_equal 200
    end
  end
end
