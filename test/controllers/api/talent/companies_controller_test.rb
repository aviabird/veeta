require 'test_helper'

class Api::Talent::CompaniesControllerTest < ActionController::TestCase
  let(:company) { build_stubbed(:company) }
  let(:talent) { build_stubbed(:talent) }

  before do
    @controller.stubs(:current_talent).returns(talent)
  end

  it "should get index" do
    Company.expects(:in_touch).with(talent)

    get :index, format: :json
    response.status.must_equal 200
  end
end
