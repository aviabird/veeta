require "test_helper"

describe Api::Talent::ResumesController do
  let(:resume) { build_stubbed(:resume) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
    I18n.load_path << File.expand_path('../flash.en.yml', __FILE__)
    I18n.reload!
  end

  describe 'GET /api/talent/resumes' do
    let(:current_talent) { build_stubbed('talent') }
    before { @controller.stubs(:current_talent).returns(current_talent) }

    it 'should return JSON with list of resumes' do
      get :index, format: :json
      response.status.must_equal 200
      result = parse(response.body)
      result.has_key?(:resumes).must_equal true
      result[:resumes].must_equal []
    end
  end

  describe 'GET /api/talent/resumes/new' do
    it 'should return empty JSON structure' do
      Resume.expects(:prepare).returns(resume)

      get :new, format: :json
      response.status.must_equal 200
      parse(response.body).has_key?(:resume).must_equal true
      # assigns(:resume).email.must_equal current_user.talent.email
      # assigns(:resume).phone_number.must_equal current_user.talent.phone_number
    end
  end

  describe 'GET /api/talent/resumes{id}' do
    it 'should return resume, when exist'
    # it "shows resume" do
    #   get :show, id: resume
    #   response.status.must_equal 200
    #   result = parse(response.body)
    #   result[:resume][:id].must_equal resume.id
    # end
  end

  # it "updates resume" do
  #   put :update, id: resume, resume: {  }
  #   assert_redirected_to resume_path(assigns(:resume))
  # end

  # it "destroys resume" do
  #   assert_difference('Api::Talent::Resume.count', -1) do
  #     delete :destroy, id: resume
  #   end

  #   assert_redirected_to resumes_path
  # end

  describe 'POST /api/talent/resumes/validate' do
    it "should validate the input without saving" do
      Resume.any_instance.expects(:save).never
      Resume.any_instance.expects(:save!).never
      post :validate, format: :json, resume: { name: 'Awesome resume', language_id: 1 }
      response.status.must_equal 200
      assigns(:resume).name.must_equal 'Awesome resume'
    end
  end

  describe 'PUT /api/talent/resumes/share' do
    it "shoudl create new sharing" do
      Resume.stubs(:find).returns(resume)
      resume.expects(:share).with(company_ids: [1,2])

      put :share, format: :json, id: 1, resume: { company_ids: [1, 2] }
      response.status.must_equal 200
    end
  end

  describe 'PUT /api/talent/resumes/share_request' do
    it "shoudl create new sharing" do
      resume.sharing_requests.must_be :empty?
      Resume.stubs(:find).returns(resume)
      resume.stubs(:save)

      put :share_request, format: :json, id: 1, resume_sharing: { contact_name: 'Steve', email: 'steve@apple.com' }
      response.status.must_equal 200
      assigns(:resume).sharing_requests.wont_be :empty?
    end
  end

  describe 'PUT /api/talent/resumes/fillin' do
    it "should try to upload pdf" do
      Resume.stubs(:find).returns(resume)
      ResumePdf.any_instance.stubs(:save)
      resume.expects(:fillin)

      put :fillin, format: :json, id: 1, resume_pdf: attributes_for(:resume_pdf)
      assigns(:resume).errors.must_be :empty?
      assigns(:resume).pdfs.wont_be :empty?
    end
  end
end
