require "test_helper"

describe Api::Talent::TalentAuthsController do
  let(:current_user) { build_stubbed(:talent_auth, guest: true) }
  
  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
    @controller.stubs(:current_user).returns(current_user)
  end

  it "shouldn't update password all the time" do
    put :password, format: :json
    response.status.must_equal 422
    assigns(:current_auth).errors.wont_be :empty?
  end

  it "should update password" do
    AuthService::SetPassword.any_instance.expects(:set_password)
    
    put :password, format: :json, talent_auth: { password: 'mypassword' }
    response.status.must_equal 200
    assigns(:current_auth).errors.must_be :empty?
  end

  it "shouldn't update email all the time" do
    put :email, format: :json
    response.status.must_equal 422
    assigns(:current_auth).errors.wont_be :empty?
  end

  it "should update email" do
    current_user.stubs(:confirmed_at).returns(nil)
    AuthService::SetEmail.any_instance.expects(:set_email)
    
    put :email, format: :json, talent_auth: { email: 'services@tts.at' }
    response.status.must_equal 200
    assigns(:current_auth).errors.must_be :empty?
  end

  it "should try to delete account" do
    put :delete, format: :json
    response.status.must_equal 422
    assigns(:current_auth).errors.wont_be :empty?
  end

  it "should delete params" do
    keep_email = current_user.email
    current_user.expects(:delete)

    delete :delete, format: :json, talent_auth: { email: current_user.email, password: current_user.password, delete_reason: "No longer please" }
    response.status.must_equal 200
    assigns(:current_auth).email.wont_equal keep_email
  end

  it "should send new email" do
    current_user.expects(:send_confirmation_instructions)
    put :resend_confirmation, format: :json
    response.status.must_equal 200
  end
end
