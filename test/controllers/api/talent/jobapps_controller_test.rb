require "test_helper"

describe Api::Talent::JobappsController do
  let(:jobapp) { build_stubbed(:jobapp) }

  before do
    Jobapp.stubs(:find).returns(jobapp)
  end

  describe 'GET /api/talent/jobapps' do
  	it 'should get list of jobapps' do
      Jobapp.expects(:my).returns([jobapp])
  	  get :index, format: :json
  	  response.status.must_equal 200
  	  result = parse(response.body)
  	  result.has_key?(:jobapps).must_equal true
  	  result[:jobapps].must_be_kind_of Array
  	end
  end

  describe 'GET /api/talent/jobapps/new' do
    it 'should return empty JSON structure' do
      Jobapp.expects(:prepare).returns(jobapp)
      get :new, format: :json
      response.status.must_equal 200
    end
  end

  describe 'PATCH /api/talent/jobapps/1/finish' do
    it 'should create new jobapp' do
      service = mock('service')
      service.stubs(:info).returns({})
      ApplyService::Apply.expects(:call).returns(service)
      service.expects(:jobapp).returns(jobapp)

      post :finish, format: :json, id: jobapp, jobapp: attributes_for(:jobapp)
      assigns(:jobapp).must_equal jobapp
    end
  end

  describe 'POST /api/talent/jobapps/validate' do
    it "should validate the input without saving" do
      Jobapp.any_instance.expects(:save).never
      Jobapp.any_instance.expects(:save!).never
      post :validate, format: :json, jobapp: { cover_letter: 'Hi there' }
      response.status.must_equal 422
      assigns(:jobapp).cover_letter.must_equal 'Hi there'
    end
  end

  describe 'PUT /api/talent/jobapps/5/withdrawn' do
    it "should withdrawn jobapp" do
      Jobapp.stubs(:find).returns(jobapp)
      jobapp.expects(:withdrawn).with('I have a job')
      put :withdrawn, format: :json, id: jobapp, jobapp: { withdrawn_reason: "I have a job" }
      response.status.must_equal 200
    end
  end
end
