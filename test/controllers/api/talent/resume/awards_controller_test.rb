require "test_helper"

describe Api::Talent::Resume::AwardsController do
  let(:award) { build_stubbed(:rs_award) }
  let(:resume) { build_stubbed(:resume, awards: [award]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/awards' do
    let(:award_foreign) { build_stubbed(:rs_award) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:awards).first.id.must_equal award.id
      assigns(:awards).map { |we| we[:id].wont_equal award_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/award/{:id}' do
    before { Rs::Award.stubs(:find).returns(award) }

    it 'should show just one work experience' do
      get :show, format: :json, id: award
      response.status.must_equal 200
      assigns(:award).must_equal award
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:award).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/awards' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, award: { name: 'CISCO', awarded_by: nil }
      response.status.must_equal 422
      assigns(:award).name.must_equal 'CISCO'
      assigns(:award).awarded_by.must_equal nil
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Award.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, award: attributes_for(:rs_award, name: 'Cisco')
      response.status.must_equal 200
      assigns(:award).name.must_equal 'Cisco'
    end
  end

  describe 'PUT /api/talent/resume/award/{:id}' do
    before { Rs::Award.stubs(:find).returns(award) }

    it 'should update resource' do
      award.expects(:update)

      put :update, format: :json, id: award, award: { name: 'CISCO' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/award/{:id}' do
    before { Rs::Award.stubs(:find).returns(award) }

    it 'should delete resource' do
      award.expects(:destroy)

      delete :destroy, format: :json, id: award
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/award/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Award.any_instance.expects(:save).never
      Rs::Award.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, award: attributes_for(:rs_award)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:award).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Award.any_instance.expects(:save).never
      Rs::Award.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, award: attributes_for(:rs_award, name: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:award).wont_be :persisted?
    end
  end
end
