require "test_helper"

describe Api::Talent::Resume::EducationsController do
  let(:education) { build_stubbed(:rs_education) }
  let(:resume) { build_stubbed(:resume, educations: [education]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
  	current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/educations' do
		let(:education_foreign) { build_stubbed(:rs_education) }
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return JSON with list of work experiences which belongs to given resume' do
  		get :index, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:educations).first.id.must_equal education.id
  		assigns(:educations).map { |we| we[:id].wont_equal education_foreign.id }
  	end
  end

  describe 'GET /api/talent/resume/education/{:id}' do
  	before { Rs::Education.stubs(:find).returns(education) }

  	it 'should show just one work experience' do
  		get :show, format: :json, id: education
  		response.status.must_equal 200
  		assigns(:education).must_equal education
  	end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should get new resume' do
  		get :new, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:education).id.must_equal nil
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/educations' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should save work experience even if its not vali' do
      Rs::Education.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, education: { school_name: 'Universitat at Wien', from: 'now' }
  		response.status.must_equal 200
      assigns(:education).school_name.must_equal 'Universitat at Wien'
      assigns(:education).from.must_equal nil
  	end

  	it 'should create new work experience, when is everything ok' do
  		Rs::Education.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, education: attributes_for(:rs_education)
  		response.status.must_equal 200
      assigns(:education).school_name.must_equal 'High school'
  	end
  end

  describe 'PUT /api/talent/resume/education/{:id}' do
  	before { Rs::Education.stubs(:find).returns(education) }

    it 'should update resource' do
      education.expects(:update)

      put :update, format: :json, id: education, education: { school_name: 'Universitat at Wien' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/education/{:id}' do
  	before { Rs::Education.stubs(:find).returns(education) }

  	it 'should delete resource' do
  		education.expects(:destroy)

  		delete :destroy, format: :json, id: education
  		response.status.must_equal 200
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/education/validate' do
  	before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Education.any_instance.expects(:save).never
      Rs::Education.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, education: attributes_for(:rs_education, country_id: 004)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:education).school_name.must_equal 'High school'
      assigns(:education).wont_be :persisted?
    end

  	it 'should validate the input even with invalid input' do
  		Rs::Education.any_instance.expects(:save).never
  		Rs::Education.any_instance.expects(:save!).never

  		post :validate, format: :json, resume_id: resume.id, education: attributes_for(:rs_education, from: nil)
  		result = parse(response.body)
  		response.status.must_equal 200
  		assigns(:education).school_name.must_equal 'High school'
      assigns(:education).wont_be :persisted?
  	end
  end
end
