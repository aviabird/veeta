require "test_helper"

describe Api::Talent::Resume::MembershipsController do
  let(:membership) { build_stubbed(:rs_membership) }
  let(:resume) { build_stubbed(:resume, memberships: [membership]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/memberships' do
    let(:membership_foreign) { build_stubbed(:rs_membership) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:memberships).first.id.must_equal membership.id
      assigns(:memberships).map { |we| we[:id].wont_equal membership_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/membership/{:id}' do
    before { Rs::Membership.stubs(:find).returns(membership) }

    it 'should show just one work experience' do
      get :show, format: :json, id: membership
      response.status.must_equal 200
      assigns(:membership).must_equal membership
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:membership).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/memberships' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, membership: { organization: 'Apple', role: nil }
      response.status.must_equal 422
      assigns(:membership).organization.must_equal 'Apple'
      assigns(:membership).role.must_equal nil
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Membership.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, membership: attributes_for(:rs_membership, organization: 'Academy')
      response.status.must_equal 200
      assigns(:membership).organization.must_equal 'Academy'
    end
  end

  describe 'PUT /api/talent/resume/membership/{:id}' do
    before { Rs::Membership.stubs(:find).returns(membership) }

    it 'should update resource' do
      membership.expects(:update)

      put :update, format: :json, id: membership, membership: { organization: 'Apple' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/membership/{:id}' do
    before { Rs::Membership.stubs(:find).returns(membership) }

    it 'should delete resource' do
      membership.expects(:destroy)

      delete :destroy, format: :json, id: membership
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/membership/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Membership.any_instance.expects(:save).never
      Rs::Membership.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, membership: attributes_for(:rs_membership)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:membership).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Membership.any_instance.expects(:save).never
      Rs::Membership.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, membership: attributes_for(:rs_membership, role: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:membership).wont_be :persisted?
    end
  end
end
