require "test_helper"

describe Api::Talent::Resume::LinksController do
  let(:link) { build_stubbed(:rs_link) }
  let(:resume) { build_stubbed(:resume, link: link) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    Resume.stubs(:find).returns(resume)
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resume/link/{:id}' do
    before { Rs::Link.stubs(:find).returns(link) }

    it 'should show just one link' do
      get :show, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:link).must_equal link
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:link).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/link' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should create new link, when is everything ok' do
      Rs::Link.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, link: attributes_for(:rs_link)
      response.status.must_equal 200
      assigns(:link).website.must_equal 'http://jirikolarik.com'
    end
  end

  describe 'PUT /api/talent/resume/link/{:id}' do
    before { Rs::Link.stubs(:find).returns(link) }

    it 'should update resource' do
      link.expects(:update)

      put :update, format: :json, resume_id: resume.id, link: { website: 'http://jirikolarik.com' }
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/link/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Link.any_instance.expects(:save).never
      Rs::Link.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, link: attributes_for(:rs_link, website: 'http://jirikolarik.com')
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:link).website.must_equal 'http://jirikolarik.com'
      assigns(:link).wont_be :persisted?
    end

    # it 'should validate the input with invalid resources' do
    #   Rs::Link.any_instance.expects(:save).never
    #   Rs::Link.any_instance.expects(:save!).never

    #   post :validate, format: :json, resume_id: resume.id, link: attributes_for(:rs_link, from: nil)
    #   result = parse(response.body)
    #   response.status.must_equal 422
    #   assigns(:link).website.must_equal 'http://jirikolarik.com'
    #   assigns(:link).wont_be :persisted?
    # end
  end
end
