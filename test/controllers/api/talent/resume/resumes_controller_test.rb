require "test_helper"

describe Api::Recruiter::ResumesController do
  let(:resume) { build_stubbed(:resume) }
  let(:current_user) { build_stubbed(:recruiter) }
  
  before do
    Resume.stubs(:find).returns(resume)
  end

  describe 'GET /api/talent/resumes/{id}' do
    it 'should return resume, when exist' do
      get :show, format: :json, id: resume
      response.status.must_equal 200
      assigns(:resume).must_equal resume
    end
  end
end
