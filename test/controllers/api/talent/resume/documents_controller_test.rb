require "test_helper"

describe Api::Talent::Resume::DocumentsController do
  let(:document) { build_stubbed(:rs_document) }
  let(:resume) { build_stubbed(:resume, documents: [document]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/documents' do
    let(:document_foreign) { build_stubbed(:rs_document) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:documents).first.id.must_equal document.id
      assigns(:documents).map { |we| we[:id].wont_equal document_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/document/{:id}' do
    before { Rs::Document.stubs(:find).returns(document) }

    it 'should show just one work experience' do
      get :show, format: :json, id: document
      response.status.must_equal 200
      assigns(:document).must_equal document
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:document).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/documents' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, document: { file: nil }
      response.status.must_equal 422
      assigns(:document).file.must_be_instance_of DocumentUploader
      assigns(:document).file.file.must_equal nil
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Document.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, document: attributes_for(:rs_document)
      response.status.must_equal 200
      assigns(:document).file.must_be_instance_of DocumentUploader
      assigns(:document).file.file.wont_be :empty?
    end
  end

  describe 'PUT /api/talent/resume/document/{:id}' do
    before { Rs::Document.stubs(:find).returns(document) }
    let(:cv_2) { Rack::Test::UploadedFile.new(File.join(Rails.root, 'test', 'data', 'resume', 'cv.pdf')) }
    
    it 'should update resource' do
      document.expects(:update)

      put :update, format: :json, id: document, document: { file: cv_2 }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/document/{:id}' do
    before { Rs::Document.stubs(:find).returns(document) }

    it 'should delete resource' do
      document.expects(:destroy)

      delete :destroy, format: :json, id: document
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/document/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Document.any_instance.expects(:save).never
      Rs::Document.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, document: attributes_for(:rs_document)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:document).file.must_be_instance_of DocumentUploader
      assigns(:document).file.file.wont_be :empty?
      assigns(:document).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Document.any_instance.expects(:save).never
      Rs::Document.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, document: attributes_for(:rs_document, file: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:document).file.must_be_instance_of DocumentUploader
      assigns(:document).file.file.must_equal nil
      assigns(:document).wont_be :persisted?
    end
  end
end
