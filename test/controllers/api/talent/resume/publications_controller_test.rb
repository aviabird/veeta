require "test_helper"

describe Api::Talent::Resume::PublicationsController do
  let(:publication) { build_stubbed(:rs_publication) }
  let(:resume) { build_stubbed(:resume, publications: [publication]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/publications' do
    let(:publication_foreign) { build_stubbed(:rs_publication) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:publications).first.id.must_equal publication.id
      assigns(:publications).map { |we| we[:id].wont_equal publication_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/publication/{:id}' do
    before { Rs::Publication.stubs(:find).returns(publication) }

    it 'should show just one work experience' do
      get :show, format: :json, id: publication
      response.status.must_equal 200
      assigns(:publication).must_equal publication
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:publication).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/publications' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, publication: { publication_type: 'Book', title: nil }
      response.status.must_equal 422
      assigns(:publication).publication_type.must_equal 'Book'
      assigns(:publication).title.must_equal nil
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Publication.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, publication: attributes_for(:rs_publication, publication_type: 'Book')
      response.status.must_equal 200
      assigns(:publication).publication_type.must_equal 'Book'
    end
  end

  describe 'PUT /api/talent/resume/publication/{:id}' do
    before { Rs::Publication.stubs(:find).returns(publication) }

    it 'should update resource' do
      publication.expects(:update)

      put :update, format: :json, id: publication, publication: { publication_type: 'Book' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/publication/{:id}' do
    before { Rs::Publication.stubs(:find).returns(publication) }

    it 'should delete resource' do
      publication.expects(:destroy)

      delete :destroy, format: :json, id: publication
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/publication/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Publication.any_instance.expects(:save).never
      Rs::Publication.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, publication: attributes_for(:rs_publication)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:publication).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Publication.any_instance.expects(:save).never
      Rs::Publication.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, publication: attributes_for(:rs_publication, title: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:publication).wont_be :persisted?
    end
  end
end
