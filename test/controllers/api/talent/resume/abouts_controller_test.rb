require "test_helper"

describe Api::Talent::Resume::AboutsController do
  let(:about) { build_stubbed(:rs_about) }
  let(:resume) { build_stubbed(:resume, about: about) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    Resume.stubs(:find).returns(resume)
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resume/about/{:id}' do
    before { Rs::About.stubs(:find).returns(about) }

    it 'should show just one about' do
      get :show, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:about).must_equal about
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:about).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/about' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should create new about, when is everything ok' do
      Rs::About.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, about: attributes_for(:rs_about, text: "I'm so cool")
      response.status.must_equal 200
      assigns(:about).text.must_equal "I'm so cool"
    end
  end

  describe 'PUT /api/talent/resume/about/{:id}' do
    before { Rs::About.stubs(:find).returns(about) }

    it 'should update resource' do
      about.expects(:update)

      put :update, format: :json, resume_id: resume.id, about: { text: "I'm so cool" }
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/about/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::About.any_instance.expects(:save).never
      Rs::About.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, about: attributes_for(:rs_about, text: "I'm so cool")
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:about).text.must_equal "I'm so cool"
      assigns(:about).wont_be :persisted?
    end

    # it 'should validate the input with invalid resources' do
    #   Rs::About.any_instance.expects(:save).never
    #   Rs::About.any_instance.expects(:save!).never

    #   post :validate, format: :json, resume_id: resume.id, about: attributes_for(:rs_about, from: nil)
    #   result = parse(response.body)
    #   response.status.must_equal 422
    #   assigns(:about).website.must_equal 'http://jirikolarik.com'
    #   assigns(:about).wont_be :persisted?
    # end
  end
end
