require "test_helper"

describe Api::Talent::Resume::CertificationsController do
  let(:certification) { build_stubbed(:rs_certification) }
  let(:resume) { build_stubbed(:resume, certifications: [certification]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/certifications' do
    let(:certification_foreign) { build_stubbed(:rs_certification) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:certifications).first.id.must_equal certification.id
      assigns(:certifications).map { |we| we[:id].wont_equal certification_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/certification/{:id}' do
    before { Rs::Certification.stubs(:find).returns(certification) }

    it 'should show just one work experience' do
      get :show, format: :json, id: certification
      response.status.must_equal 200
      assigns(:certification).must_equal certification
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:certification).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/certifications' do
    before { Resume.stubs(:find).returns(resume) }
    let(:past_date) { 3.years.ago }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, certification: { organization: 'Universitat at Wien', subject: nil, certification_type: 'wrong type', year: nil }
      response.status.must_equal 422
      assigns(:certification).organization.must_equal 'Universitat at Wien'
      assigns(:certification).subject.must_equal nil
      assigns(:certification).year.must_equal nil
      assigns(:certification).certification_type.must_equal 'wrong type'
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Certification.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, certification: attributes_for(:rs_certification, organization: 'Academy', subject: 'Comp Sci', year: past_date, certification_type: 'continued_education')
      response.status.must_equal 200
      assigns(:certification).organization.must_equal 'Academy'
      assigns(:certification).subject.must_equal 'Comp Sci'
      assigns(:certification).year.must_equal past_date
      assigns(:certification).certification_type.must_equal 'continued_education'
    end
  end

  describe 'PUT /api/talent/resume/certification/{:id}' do
    before { Rs::Certification.stubs(:find).returns(certification) }

    it 'should update resource' do
      certification.expects(:update)

      put :update, format: :json, id: certification, certification: { organization: 'Universitat at Wien' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/certification/{:id}' do
    before { Rs::Certification.stubs(:find).returns(certification) }

    it 'should delete resource' do
      certification.expects(:destroy)

      delete :destroy, format: :json, id: certification
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/certification/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Certification.any_instance.expects(:save).never
      Rs::Certification.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, certification: attributes_for(:rs_certification)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:certification).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Certification.any_instance.expects(:save).never
      Rs::Certification.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, certification: attributes_for(:rs_certification, organization: nil, subject: nil, year: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:certification).wont_be :persisted?
    end
  end
end
