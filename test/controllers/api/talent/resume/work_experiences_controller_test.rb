require "test_helper"

describe Api::Talent::Resume::WorkExperiencesController do
	let(:work_experience) { build_stubbed(:rs_work_experience) }
  let(:resume) { build_stubbed(:resume, work_experiences: [work_experience]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
  	current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/work_experiences' do
		let(:work_experience_foreign) { build_stubbed(:rs_work_experience) }
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return JSON with list of work experiences which belongs to given resume' do
  		get :index, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:work_experiences).first.id.must_equal work_experience.id
  		assigns(:work_experiences).map { |we| we[:id].wont_equal work_experience_foreign.id }
  	end
  end

  describe 'GET /api/talent/resume/work_experience/{:id}' do
  	before { Rs::WorkExperience.stubs(:find).returns(work_experience) }

  	it 'should show just one work experience' do
  		get :show, format: :json, id: work_experience
  		response.status.must_equal 200
  		assigns(:work_experience).must_equal work_experience
  	end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should get new resume' do
  		get :new, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:work_experience).id.must_equal nil
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/work_experiences' do
  	before { Resume.stubs(:find).returns(resume) }

    it 'should create new work experience, when everything is not ok' do
      Rs::WorkExperience.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, work_experience: { company: 'Apple', from: 'now' }
  		response.status.must_equal 200
      assigns(:work_experience).company.must_equal 'Apple'
      assigns(:work_experience).from.must_equal nil
  	end

  	it 'should create new work experience, when is everything ok' do
  		Rs::WorkExperience.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, work_experience: attributes_for(:rs_work_experience)
  		response.status.must_equal 200
      assigns(:work_experience).company.must_equal 'Talent Solutions'
  	end
  end

  describe 'PUT /api/talent/resume/work_experience/{:id}' do
  	before { Rs::WorkExperience.stubs(:find).returns(work_experience) }

    it 'should update resource' do
      work_experience.expects(:update)

      put :update, format: :json, id: work_experience, work_experience: { company: 'Apple' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/work_experience/{:id}' do
  	before { Rs::WorkExperience.stubs(:find).returns(work_experience) }

  	it 'should delete resource' do
  		work_experience.expects(:destroy)

  		delete :destroy, format: :json, id: work_experience
  		response.status.must_equal 200
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/work_experience/validate' do
  	before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::WorkExperience.any_instance.expects(:save).never
      Rs::WorkExperience.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, work_experience: attributes_for(:rs_work_experience, country_id: 004)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:work_experience).company.must_equal 'Talent Solutions'
      assigns(:work_experience).wont_be :persisted?
    end

  	it 'should validate the input with invalid resources' do
  		Rs::WorkExperience.any_instance.expects(:save).never
  		Rs::WorkExperience.any_instance.expects(:save!).never

  		post :validate, format: :json, resume_id: resume.id, work_experience: attributes_for(:rs_work_experience, from: nil)
  		result = parse(response.body)
  		response.status.must_equal 200
  		assigns(:work_experience).company.must_equal 'Talent Solutions'
      assigns(:work_experience).wont_be :persisted?
  	end
  end
end
