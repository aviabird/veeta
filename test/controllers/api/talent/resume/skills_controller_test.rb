require "test_helper"

describe Api::Talent::Resume::SkillsController do
  let(:skill) { build_stubbed(:rs_skill) }
  let(:resume) { build_stubbed(:resume, skills: [skill]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
  	current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/skills' do
		let(:skill_foreign) { build_stubbed(:rs_skill) }
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return JSON with list of work experiences which belongs to given resume' do
  		get :index, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:skills).first.id.must_equal skill.id
  		assigns(:skills).map { |we| we[:id].wont_equal skill_foreign.id }
  	end
  end

  describe 'GET /api/talent/resume/skill/{:id}' do
  	before { Rs::Skill.stubs(:find).returns(skill) }

  	it 'should show just one work experience' do
  		get :show, format: :json, id: skill
  		response.status.must_equal 200
  		assigns(:skill).must_equal skill
  	end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should get new resume' do
  		get :new, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:skill).id.must_equal nil
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/skills' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return work experience which is not created' do
  		resume.stubs(:save).returns(false)
  		post :create, format: :json, resume_id: resume.id, skill: { level: 1 }
  		response.status.must_equal 422
      assigns(:skill).level.must_equal 1
  	end

  	it 'should create new work experience, when is everything ok' do
  		Rs::Skill.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, skill: attributes_for(:rs_skill)
  		response.status.must_equal 200
      assigns(:skill).level.must_equal 5
  	end
  end

  describe 'PUT /api/talent/resume/skill/{:id}' do
  	before { Rs::Skill.stubs(:find).returns(skill) }

    it 'should update resource' do
      skill.expects(:update)

      put :update, format: :json, id: skill, skill: { level: 'Universitat at Wien' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/skill/{:id}' do
  	before { Rs::Skill.stubs(:find).returns(skill) }

  	it 'should delete resource' do
  		skill.expects(:destroy)

  		delete :destroy, format: :json, id: skill
  		response.status.must_equal 200
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/skill/validate' do
  	before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Skill.any_instance.expects(:save).never
      Rs::Skill.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, skill: attributes_for(:rs_skill)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:skill).level.must_equal 5
      assigns(:skill).wont_be :persisted?
    end

  	it 'should validate the input with invalid resources' do
  		Rs::Skill.any_instance.expects(:save).never
  		Rs::Skill.any_instance.expects(:save!).never

  		post :validate, format: :json, resume_id: resume.id, skill: attributes_for(:rs_skill, name: nil)
  		result = parse(response.body)
  		response.status.must_equal 422
  		assigns(:skill).level.must_equal 5
      assigns(:skill).wont_be :persisted?
  	end
  end
end
