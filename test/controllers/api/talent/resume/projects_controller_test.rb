require "test_helper"

describe Api::Talent::Resume::ProjectsController do
  let(:project) { build_stubbed(:rs_project) }
  let(:resume) { build_stubbed(:resume, projects: [project]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
    @controller.stubs(:authorize_resource).returns(true)
    current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/projects' do
    let(:project_foreign) { build_stubbed(:rs_project) }
    before { Resume.stubs(:find).returns(resume) }

    it 'should return JSON with list of work experiences which belongs to given resume' do
      get :index, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:projects).first.id.must_equal project.id
      assigns(:projects).map { |we| we[:id].wont_equal project_foreign.id }
    end
  end

  describe 'GET /api/talent/resume/project/{:id}' do
    before { Rs::Project.stubs(:find).returns(project) }

    it 'should show just one work experience' do
      get :show, format: :json, id: project
      response.status.must_equal 200
      assigns(:project).must_equal project
    end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should get new resume' do
      get :new, format: :json, resume_id: resume.id
      response.status.must_equal 200
      assigns(:project).id.must_equal nil
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/projects' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should return work experience which is not created' do
      resume.stubs(:save).returns(false)
      post :create, format: :json, resume_id: resume.id, project: { name: 'Veeta', role: nil }
      response.status.must_equal 422
      assigns(:project).name.must_equal 'Veeta'
      assigns(:project).role.must_equal nil
    end

    it 'should create new work experience, when is everything ok' do
      Rs::Project.any_instance.expects(:save).returns(true)
      post :create, format: :json, resume_id: resume.id, project: attributes_for(:rs_project, name: 'Veeta')
      response.status.must_equal 200
      assigns(:project).name.must_equal 'Veeta'
    end
  end

  describe 'PUT /api/talent/resume/project/{:id}' do
    before { Rs::Project.stubs(:find).returns(project) }

    it 'should update resource' do
      project.expects(:update)

      put :update, format: :json, id: project, project: { name: 'Veeta' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/project/{:id}' do
    before { Rs::Project.stubs(:find).returns(project) }

    it 'should delete resource' do
      project.expects(:destroy)

      delete :destroy, format: :json, id: project
      response.status.must_equal 200
    end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/project/validate' do
    before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Project.any_instance.expects(:save).never
      Rs::Project.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, project: attributes_for(:rs_project)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:project).wont_be :persisted?
    end

    it 'should validate the input with invalid resources' do
      Rs::Project.any_instance.expects(:save).never
      Rs::Project.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, project: attributes_for(:rs_project, name: nil)
      result = parse(response.body)
      response.status.must_equal 422
      assigns(:project).wont_be :persisted?
    end
  end
end
