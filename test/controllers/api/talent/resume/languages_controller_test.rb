require "test_helper"

describe Api::Talent::Resume::LanguagesController do
  let(:language) { build_stubbed(:rs_language) }
  let(:resume) { build_stubbed(:resume, languages: [language]) }
  let(:current_user) { build_stubbed(:talent) }

  before do
  	current_user.stubs(:talent).returns(build_stubbed(:talent))
  end

  describe 'GET /api/talent/resumes/{:resume_id}/languages' do
		let(:language_foreign) { build_stubbed(:rs_language) }
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return JSON with list of work experiences which belongs to given resume' do
  		get :index, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:languages).first.id.must_equal language.id
  		assigns(:languages).map { |we| we[:id].wont_equal language_foreign.id }
  	end
  end

  describe 'GET /api/talent/resume/language/{:id}' do
  	before { Rs::Language.stubs(:find).returns(language) }

  	it 'should show just one work experience' do
  		get :show, format: :json, id: language
  		response.status.must_equal 200
  		assigns(:language).must_equal language
  	end
  end

  describe 'GET /api/talent/resumes/{:resume_id}/new' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should get new resume' do
  		get :new, format: :json, resume_id: resume.id
  		response.status.must_equal 200
  		assigns(:language).id.must_equal nil
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/languages' do
  	before { Resume.stubs(:find).returns(resume) }

  	it 'should return work experience which is not created' do
  		resume.stubs(:save).returns(false)
  		post :create, format: :json, resume_id: resume.id, language: { level: 1 }
  		response.status.must_equal 422
      assigns(:language).level.must_equal 1
  	end

  	it 'should create new work experience, when is everything ok' do
  		Rs::Language.any_instance.expects(:save).returns(true)
  		post :create, format: :json, resume_id: resume.id, language: attributes_for(:rs_language)
  		response.status.must_equal 200
      assigns(:language).level.must_equal 1
  	end
  end

  describe 'PUT /api/talent/resume/language/{:id}' do
  	before { Rs::Language.stubs(:find).returns(language) }

    it 'should update resource' do
      language.expects(:update)

      put :update, format: :json, id: language, language: { level: 'Universitat at Wien' }
      response.status.must_equal 200
    end
  end

  describe 'DELETE /api/talent/resume/language/{:id}' do
  	before { Rs::Language.stubs(:find).returns(language) }

  	it 'should delete resource' do
  		language.expects(:destroy)

  		delete :destroy, format: :json, id: language
  		response.status.must_equal 200
  	end
  end

  describe 'POST /api/talent/resumes/{:resume_id}/language/validate' do
  	before { Resume.stubs(:find).returns(resume) }

    it 'should validate the input' do
      Rs::Language.any_instance.expects(:save).never
      Rs::Language.any_instance.expects(:save!).never

      post :validate, format: :json, resume_id: resume.id, language: attributes_for(:rs_language, language_id: 99115)
      result = parse(response.body)
      response.status.must_equal 200
      assigns(:language).level.must_equal 1
      assigns(:language).wont_be :persisted?
    end

  	it 'should validate the input with invalid resources' do
  		Rs::Language.any_instance.expects(:save).never
  		Rs::Language.any_instance.expects(:save!).never

  		post :validate, format: :json, resume_id: resume.id, language: attributes_for(:rs_language)
  		result = parse(response.body)
  		response.status.must_equal 422
  		assigns(:language).level.must_equal 1
      assigns(:language).wont_be :persisted?
  	end
  end
end
