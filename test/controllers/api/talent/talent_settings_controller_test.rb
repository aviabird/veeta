require "test_helper"

describe Api::Talent::TalentSettingsController do
  let(:talent_setting) { build_stubbed(:talent_setting) }
  let(:current_user) { build_stubbed(:talent) }
  let(:current_talent) { build_stubbed(:talent) }

  before do
    current_talent.stubs(:setting).returns(talent_setting)
    @controller.stubs(:current_talent).returns(current_talent)
  end

  it "should get show" do
    get :show, format: :json
    response.status.must_equal 200
  end

  it "should get update" do
    talent_setting.expects(:update)
    put :update, format: :json, talent_setting: { professional_experience_level: 'starter' }
    response.status.must_equal 200
  end

  it "should just validate the input" do
    put :validate, format: :json, talent_setting: { professional_experience_level: nil }
    response.status.must_equal 422
    assigns(:setting).professional_experience_level.must_be_nil
  end

  it "should return just success" do
    put :validate, format: :json, talent_setting: attributes_for(:talent_setting)
    response.status.must_equal 200
  end
end
