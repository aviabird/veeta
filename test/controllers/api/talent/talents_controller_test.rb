require "test_helper"

describe Api::Talent::TalentsController do
  let(:talent) { build_stubbed(:talent) }
  let(:current_user) { build_stubbed(:talent) }

  before do 
    @controller.stubs(:current_talent).returns(talent)
  end

  it "should get show" do
    get :show, format: :json
    response.status.must_equal 200
  end

  it "should get update" do
    talent.expects(:save)
    put :update, format: :json, talent: { first_name: 'Cool guy' }
    response.status.must_equal 200
  end

  it "should just validate the input" do
    put :validate, format: :json, talent: { first_name: nil }
    response.status.must_equal 422
    assigns(:talent).first_name.must_be_nil
  end

  it "should return just success" do 
    put :validate, format: :json, talent: attributes_for(:talent)
    response.status.must_equal 200
  end

  it "should try to delete avatar" do
    talent.expects(:remove_avatar!)
    talent.expects(:save)
    
    delete :avatar_remove, format: :json, talent: talent
    response.status.must_equal 200
  end
end
