require "test_helper"

describe Api::Talent::SharingController do
  let(:current_talent) { build_stubbed(:talent) }
  let(:shared_company) { SharedCompany.new(build_stubbed(:company), build_stubbed(:resume), 3.years.ago ) }
  let(:shared_company_2) { SharedCompany.new(build_stubbed(:company), build_stubbed(:resume), 13.days.ago ) }
  let(:resume) { build_stubbed(:resume) }
  let(:resume_sharing) { build_stubbed(:resume_sharing, resume: resume, origin: resume) }

  before do
    resume.stubs(:save).returns(true)
    @controller.stubs(:current_talent).returns(current_talent)
  end

  it "must show list of companies" do
    ResumeSharing.expects(:unique_by_user).with(current_talent).returns([resume_sharing])
    get :index, format: :json
    response.status.must_equal 200
    assigns(:sharings).count.must_equal 1
  end

  it "must revoke access to the company" do
    company = build_stubbed(:company)
    Company.stubs(:find).returns(company)
    ResumeSharing.expects(:revoke).with(current_talent, company, { reason: 'Some', is_company_revocation: true })
    put :revoke, format: :json, company_id: 1, sharing: { reason: 'Some', is_company_revocation: true  }
    response.status.must_equal 200
    result = parse(response.body)
    result[:meta][:notice].must_equal "Access revoked"
  end
end
