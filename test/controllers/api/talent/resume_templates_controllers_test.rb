require "test_helper"

describe Api::Talent::ResumeTemplatesController do
  let(:resume_template) { build_stubbed(:resume_template) }
  let(:current_user) { build_stubbed(:talent) }

  describe 'GET /api/talent/resume_templates' do
    it 'should return JSON with list of resumes' do
      get :index, format: :json
      response.status.must_equal 200
      assigns(:resume_templates).must_equal ResumeTemplate.all
    end
  end
end
