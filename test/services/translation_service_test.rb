require "test_helper"

describe TranslationsService do
  let(:translation_service) { TranslationsService.new('en') }

  before do
    @resource = {
      "user" => {
        "name" => "Name",
        "birthday" => {
          "year" => "Year",
          "month" => nil,
          "day" => "Day"
        },
        "age" => nil
      }
    }

    @result = {
      "user" => {
        "name" => "Name",
        "birthday" => {
          "year" => "Year",
          "day" => "Day"
        }
      }
    }
  end

  it "should replace empty values" do
    translation_service.send(:replace_empty_values, @resource).must_equal @result
  end
end
