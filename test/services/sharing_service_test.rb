require "test_helper"

describe SharingService do
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:shared) { SharingService.new(talent) }

  it "must search in jobapps" do
    shared.jobapps.must_equal talent.jobapps
  end

  it "must search in resumes" do
    shared.resumes.must_equal talent.resumes
  end

  describe 'applied on multiple companies' do
    let(:ts) { FactoryGirl.build_stubbed(:company) }
    let(:apple) { FactoryGirl.build_stubbed(:company, name: 'Apple') }
    let(:werein) { FactoryGirl.build_stubbed(:company, name: "We're in") }

    # it "must get list or resume sharings" do
    #   sharing1 = FactoryGirl.build_stubbed(:resume_sharing, company: ts,)
    #   sharing2 = FactoryGirl.build_stubbed(:resume_sharing, company: apple)
    #   sharing3 = FactoryGirl.build_stubbed(:resume_sharing, company: ts)
    #   resumes = []
    #   resumes << FactoryGirl.build_stubbed(:resume, sharings: [sharing1, sharing2])
    #   resumes << FactoryGirl.build_stubbed(:resume, sharings: [sharing3])
    #   shared.stubs(:resumes).returns(resumes)
    #   shared.resumes_sharings.must_equal([sharing1, sharing2, sharing3])
    # end

    it "must go throw the list of jobapps and resumes" do
      number = shared.jobapps.count + shared.resumes_sharings.count
      shared.expects(:add_company).times(number)
      shared.send :list_companies
    end

    it "must add a company when it's not there" do
      shared.send :add_company, ts, FactoryGirl.build_stubbed(:resume), DateTime.now
      shared.companies.count.must_equal 1
    end

    it "must increate number of resumes and time of applies" do
      rs1 = FactoryGirl.build_stubbed(:resume)
      rs2 = FactoryGirl.build_stubbed(:resume)
      shared.send :add_company, ts, rs1, 2.years.ago
      shared.send :add_company, ts, rs2, 2.days.ago
      shared.companies.count.must_equal 1
      shared.companies[0].resumes.include? rs1
      shared.companies[0].resumes.include? rs2
      shared.companies[0].applied_at.include? 2.years.ago
      shared.companies[0].applied_at.include? 2.days.ago
    end

    describe "revoking access" do
      before do
        @jobapps = []
        3.times do
          @jobapps << FactoryGirl.build_stubbed(:jobapp)
        end
        @jobapps[0].stubs(:applied_to).returns(ts)
        @jobapps[1].stubs(:applied_to).returns(apple)
        @jobapps[2].stubs(:applied_to).returns(ts)
        @resumes_sharings = []
        @resumes_sharings << FactoryGirl.build_stubbed(:resume_sharing, company: ts)
        @resumes_sharings << FactoryGirl.build_stubbed(:resume_sharing, company: werein)

        shared.stubs(:resumes_sharings).returns(@resumes_sharings)
        shared.stubs(:jobapps).returns(@jobapps)
      end

      it "should revoke access on jobapps and resumes" do
        shared.expects(:revoke_on_jobapps)
        shared.expects(:revoke_on_resumes)
        shared.revoke ts
      end

      it "should revoke access to Talent Solutions in jobapps" do
        @jobapps[0].expects(:revoke).once
        @jobapps[1].expects(:revoke).never
        @jobapps[2].expects(:revoke).once
        shared.send :revoke_on_jobapps, ts
      end

      it "should revoke access to Talent Solutions in resumes" do
        @resumes_sharings[0].expects(:revoke).once
        @resumes_sharings[1].expects(:revoke).never
        shared.send :revoke_on_resumes, ts
      end
    end
  end
end
