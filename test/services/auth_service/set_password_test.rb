require "test_helper"

describe AuthService::SetPassword do
  before do
    @talent_auth = TalentAuth.new do |talent|
      talent.password = '123456789'
      talent.password_confirmation = '123456789'
      talent.guest = true
      talent.is_guest = true
      talent.email = 'services@talentsolutions.at'
      talent.resume_type = 'beginner'
      talent.country_id = 1
      talent.skip_confirmation!
    end
    @talent_auth.save!
    @talent_auth.update(guest: true)
  end
  after { DatabaseCleaner.clean_with(:truncation) }
  let(:service) { AuthService::SetPassword.new(@talent_auth, 'mypassword') }

  it "must be valid" do
    @talent_auth.must_be :valid?
  end

  it "should call new instance" do
    AuthService::SetPassword.any_instance.expects(:call)
    AuthService::SetPassword.call @talent_auth, 'new_password'
  end

  it "should define if is possible to update pass" do
    service.send(:possible?).must_equal true
  end

  it "shoun't be possible, when no pass provided" do
    service.instance_variable_set(:@password, nil)
    service.send(:possible?).must_equal false
  end

  it "shoun't be possible, when is not guest" do
    @talent_auth.stubs(:guest?).returns(false)
    service.send(:possible?).must_equal false
  end

  it "should update if is possible" do
    service.expects(:set_password)
    service.call
  end

  it "shouldn't update if is not possible" do
    service.stubs(:possible?).returns(false)
    service.expects(:set_password).never
    service.expects(:set_errors)
    service.call
  end

  it "should set errors when is not pass provided" do
    service.instance_variable_set(:@password, nil)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should set errors when talent is no longer guest" do
    @talent_auth.stubs(:guest?).returns(false)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should update auth" do
    # @talent_auth.expects(:send_confirmation_instructions)
    service.send(:set_password)
    @talent_auth.password.must_equal 'mypassword'
  end

  it "shouldn't set guest when setting password wasn't successfull" do
    service.instance_variable_set(:@password, 'short')
    service.send(:set_password)
    @talent_auth.guest.must_equal true
  end
end
