require "test_helper"

describe AuthService::ReinitializeTalent do
  before do
    @talent_auth = TalentAuth.new do |talent|
      talent.password = '123456789'
      talent.password_confirmation = '123456789'
      talent.guest = true
      talent.is_guest = true
      talent.email = 'services@talentsolutions.at'
      talent.resume_type = 'beginner'
      talent.country_id = 1
      talent.skip_confirmation!
    end
    @talent_auth.save!
    @talent_auth.update(guest: true)
  end
  after { DatabaseCleaner.clean_with(:truncation) }
  let(:service) { AuthService::ReinitializeTalent.new('services@talentsolutions.at') }

  it "must be valid" do
    @talent_auth.must_be :valid?
  end

  it "should try to remove if is possible" do
    service.expects(:remove)
    service.stubs(:possible?).returns(true)
    service.call
  end

  it "should determinate if is possible" do
    service.stubs(:exist?).returns(true)
    service.stubs(:guest?).returns(true)
    service.send(:possible?).must_equal true
    service.stubs(:guest?).returns(false)
    service.send(:possible?).must_equal false
    service.stubs(:guest?).returns(true)
    service.stubs(:exist?).returns(false)
    service.send(:possible?).must_equal false
  end

  it "should try to find" do
    TalentAuth.stubs(:find_by).returns(@talent_auth)
    service.send(:exist?).wont_be_nil
  end

  it "should not exist if not found" do
    TalentAuth.stubs(:find_by).returns(nil)
    service.send(:exist?).must_be_nil
  end

  it "should take guest info from auth" do
    service.instance_variable_set(:@auth, @talent_auth)
    service.send(:guest?).must_equal @talent_auth.guest?
  end

  it "should try to remove" do 
    service.instance_variable_set(:@auth, @talent_auth)
    @talent_auth.expects(:destroy)
    service.send(:remove)
  end
end