require 'test_helper'

describe CloningService::ResumeClone do
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:resume) { FactoryGirl.build_stubbed(:resume, talent: talent) }
  let(:service) { CloningService::ResumeClone.new(resume) }
  # let(:cloned_resume) { CloningService::ResumeClone.new(resume).call }

  it 'should duplicate resume' do
    Resume.any_instance.expects(:save)
    service.stubs(:recreate_avatar)
    service.stubs(:generate_version_number).returns(5)
    service.call
    clone = service.resume
    clone.name.must_equal resume.name
    clone.version.must_equal 5
  end

  it 'should geneate version' do
    result = mock 'resume'
    result.stubs(:count).returns 5
    Resume.expects(:original_and_versions).with(resume).returns(result)
    service.send(:generate_version_number).must_equal 6
  end

  it 'should try to recreate versions' do
    avatar = mock 'avatar'
    talent.stubs(:avatar).returns(avatar)
    talent.avatar.stubs(:recreate_versions!)
    service.stubs(:talent_clone).returns(talent)
    service.send(:recreate_avatar)
  end
end
