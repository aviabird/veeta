require "test_helper"

module ResumeService
  describe PrefillPDF do
    let(:picture) { PictureIO.new }

    it "must be empty without any provided string and not when provided" do
      picture.must_be :empty?
      PictureIO.new('string').wont_be :empty?
    end

    it "must have content type" do
      picture.respond_to?(:content_type).must_equal true
    end

    it "must have original filename" do
      picture.respond_to?(:original_filename).must_equal true
    end

    it "should have jpeg type as default" do
      picture.content_type.must_equal 'image/jpeg'
    end

    it "should set content type when initialize" do
      PictureIO.new(nil, content_type: 'image/png').content_type.must_equal 'image/png'
    end

    it "should allow to set content type afterwards" do
      picture.content_type = 'image/png'
      picture.content_type.must_equal 'image/png'
    end

    it "should have picture as name" do
      picture.original_filename.must_equal 'picture.jpg'
    end

    it "should set content type when initialize" do
      PictureIO.new(nil, original_filename: 'me.png').original_filename.must_equal 'me.png'
    end

    it "should allow to set content type afterwards" do
      picture.original_filename = 'me.png'
      picture.original_filename.must_equal 'me.png'
    end

    it "should know if the image is valid" do
      PictureIO.new('2fjw2').valid?.must_equal false
    end

    it "should use Minimagick to validate image" do
      minimagick = mock('minimagick')
      MiniMagick::Image.expects(:read).returns(minimagick)
      minimagick.expects(:valid?)

      PictureIO.new('2fjw2').valid?
    end
  end

  describe PrefillPDF do
    let(:resume) { FactoryGirl.build_stubbed(:resume) }
    let(:resume_pdf) { FactoryGirl.build_stubbed(:resume_pdf) }
    let(:service) { ResumeService::PrefillPDF.new(resume, resume_pdf) }
    let(:country) { Country.all.first }
    let(:language) { Language.find('en') }

    before do
      resume.stubs(:update_column).returns(true)
      resume.stubs(:reload).returns(resume)
      stub_request(:get, "#{ENV['TEXTKERNEL_URL']}").
        with(:headers => {'Host'=>'home.textkernel.nl:80'}).
        to_return(:status => 200, :body => "", :headers => {})
    end

    it "shoud assign hash to avatar" do
      hash = {
        profile: {
          custom_area: {
            profile_picture: {
              base64_content: '123',
              filename: 'picture.png',
              content_type: 'image/png'
            }
          }
        }
      }

      service.stubs(:hash).returns(hash)
      avatar_hash = service.send(:avatar_hash)
      avatar_hash[:base64_content].must_equal '123'
      avatar_hash[:original_filename].must_equal 'picture.png'
      avatar_hash[:content_type].must_equal 'image/png'
    end

    it "should should generate avatar as a PictureIO" do
      service.stubs(:avatar_hash).returns({})
      service.send(:avatar).must_be_kind_of PictureIO
    end

    it "should invoke prefill when it's called" do
      service.expects(:prefill)
      service.call
    end

    it "should get hash from another service" do
      parse_pdf = mock('service')
      parse_pdf.expects(:hashed)
      ParsePDF.stubs(:call).returns(parse_pdf)
      service.hash
    end

    it "should fill in talent info" do
      hash = {
        profile: {
          personal: {
            title: 'Ing',
            date_of_birth: '1985-04-15',
            nationality_code: 'AT',
            gender_code: 1,
            mobile_phone: '+436765550555'
          }
        }
      }
      service.stubs(:hash).returns(hash)
      service.stubs(:find_country).returns(country)  #TODO find_country now in prefill_util
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))

      service.send(:prefill_talent)
      resume.talent.errors.must_be :empty?
      resume.talent.title.must_equal 'Ing'
      resume.talent.birthdate.must_equal 'Mon, 15 Apr 1985'.to_time
      resume.talent.nationality_country.id.must_equal country.id
      resume.talent.phone_number.must_equal '+436765550555'
    end

    it "should fill in address info" do
      hash = {
        profile: {
          personal: {
            address: {
              street_name: 'Holzweg',
              street_number_base: '12',
              postal_code: '2371',
              city: 'Hinterbrühl',
              country_code: 'AT'
            }
          }
        }
      }
      service.stubs(:hash).returns(hash)
      service.stubs(:find_country).returns(country) #TODO find_country now in prefill_util
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))

      service.send(:prefill_address)
      resume.talent.address.errors.must_be :empty?
      resume.talent.address.country.id.must_equal country.id
      resume.talent.address.zip.must_equal '2371'
      resume.talent.address.city.must_equal 'Hinterbrühl'
      resume.talent.address.address_line.must_equal 'Holzweg 12'
    end

    it "should fill in educations info" do
      hash = {
        profile: {
          education_history: {
            education_item: [{
              institute_name: 'TU Wien (AT)',
              degree_direction: 'Bachelorstudium Wirtschaftsinformatik',
              start_date: '2001-09-01',
              end_date: '2004-06-30'
            }, {
              institute_name: 'Wien; AT',
              degree_direction: 'Controlling Basis Seminar; Österreichisches Controller Institut',
              start_date: nil,
              end_date: '2012-06-30'
            }]
          }
        }
      }
      service.stubs(:hash).returns(hash)
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))

      service.send(:prefill_educations)
      resume.educations.first.errors.must_be :empty?
      resume.educations.first.school_name.must_equal 'TU Wien (AT)'
      resume.educations.first.subject.must_equal 'Bachelorstudium Wirtschaftsinformatik'
      resume.educations.first.from.must_equal 'Sat, 01 Sep 2001'.to_time
      resume.educations.first.to.must_equal 'Wed, 30 Jun 2004'.to_time
      resume.educations.second.wont_equal nil
      resume.educations.first.errors.must_be :empty?
    end

    it "should fill in work experience info" do
      hash = {
        profile: {
          employment_history: {
            employment_item: [{
              job_title: 'Mit-Gründer; Geschäftsführer',
              start_date: '2014-01-01',
              end_date: 'heute',
              employer_name: 'Tralala Solutions GmbH (i.G.), Wien (AT)',
              description: 'Die Firma entwickelt zurzeit eine innovative Software-Lösung für Firmen, welche den Recruiting-Prozess wesentlich einfacher und effizienter machen soll.'
            }, {
              job_title: nil,
              start_date: '2010-01-01',
              end_date: '2014-01-01',
              employer_name: 'MyCorp Corp. Austria GmbH, Wien (AT',
              description: nil
            }]
          }
        }
      }
      service.stubs(:hash).returns(hash)
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))

      service.send(:prefill_work_experiences)
      resume.work_experiences.first.errors.must_be :empty?
      resume.work_experiences.first.position.must_equal 'Mit-Gründer; Geschäftsführer'
      resume.work_experiences.first.company.must_equal 'Tralala Solutions GmbH (i.G.), Wien (AT)'
      resume.work_experiences.first.from.must_equal 'Wed, 01 Jan 2014'.to_time
      resume.work_experiences.first.to.must_equal nil
      resume.work_experiences.first.description.must_equal 'Die Firma entwickelt zurzeit eine innovative Software-Lösung für Firmen, welche den Recruiting-Prozess wesentlich einfacher und effizienter machen soll.'
      resume.work_experiences.second.wont_equal nil
      resume.work_experiences.first.errors.must_be :empty?
    end

    it "should prefill languages" do
      hash = {
        profile: {
          skills: {
            language_skills: {
              language_skill: [{
                language_skill_code: 'AT',
                language_proficiency_code: 3
              }, {
                language_skill_code: 'EN',
                language_proficiency_code: 5
              }, {
                language_skill_code: ' ',
                language_proficiency_code: 3
              }]
            }
          }
        }
      }
      Rs::Language.any_instance.stubs(:save)
      service.stubs(:hash).returns(hash)
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))
      service.stubs(:find_language).returns(language)

      service.send(:prefill_languages)
      resume.languages.length.must_equal 2
      resume.languages.first.errors.must_be :empty?
      resume.languages.first.language_id.to_i.must_equal language.id.to_i
      resume.languages.first.level.must_equal 3
      resume.languages.first.errors.must_be :empty?
      resume.languages.second.wont_equal nil
      resume.languages.third.must_equal nil
    end

    it "should prefill skills" do
      hash = {
        profile: {
          skills: {
            computer_skills: {
              computer_skill: [
                { computer_skill_name: 'Mac OS' },
                { computer_skill_name: 'Apple' }
              ]
            },
            soft_skills: {
              soft_skill: [
                { soft_skill_name: 'Pianno' },
                { soft_skill_name: ' ' }
              ]
            }
          }
        }
      }

      Rs::Skill.any_instance.stubs(:save)
      service.stubs(:hash).returns(hash)
      resume.stubs(:talent).returns(FactoryGirl.build_stubbed(:talent))

      service.send(:prefill_skills)
      resume.skills.first.errors.must_be :empty?
      resume.skills.first.level.must_equal 2
      resume.skills.first.name.must_equal 'Mac OS'
      resume.skills.second.name.must_equal 'Apple'
      resume.skills.third.name.must_equal 'Pianno'
      count = 0
      resume.skills.each{ count += 1 }
      count.must_equal 3
    end

    it "should prefill and save" do
      service.expects(:prefill_talent)
      service.expects(:prefill_address)
      service.expects(:prefill_educations)
      service.expects(:prefill_work_experiences)
      service.expects(:prefill_languages)
      service.expects(:prefill_skills)
      service.expects(:save)
      service.send(:prefill)
    end

    it "should save languages and skills" do
      l = FactoryGirl.build_stubbed(:rs_language)
      s = FactoryGirl.build_stubbed(:rs_skill)
      talent = FactoryGirl.build_stubbed(:talent)
      talent.expects(:save)
      l.expects(:save)
      s.expects(:save)
      service.resume.stubs(:talent).returns(talent)
      service.resume.stubs(:languages).returns([l])
      service.resume.stubs(:skills).returns([s])
      service.send(:save)
    end
  end
end
