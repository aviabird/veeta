require "test_helper"

describe ApplyService::Apply do
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:recruiter) { FactoryGirl.build_stubbed(:recruiter, email: 'services@talentsolutions.at') }
  let(:company) { FactoryGirl.build_stubbed(:company, recruiters: [recruiter]) }
  let(:job) { FactoryGirl.build_stubbed(:job, company: company) }
  let(:jobapp) { FactoryGirl.build_stubbed(:jobapp, job: job, resume: resume) }
  let(:jobapp_params) { FactoryGirl.attributes_for(:jobapp).merge(job_id: jobapp.id) }
  let(:current_talent) { FactoryGirl.build_stubbed(:talent) }
  let(:service) { ApplyService::Apply.new(jobapp, jobapp_params) }

  it "should have empty info" do
    service.info.must_equal({meta: {}})
  end

  it "should perform job application on job" do
    jobapp.stubs(:update).returns(true)
    service.stubs(:jobapp).returns(jobapp)
    service.expects(:perform_job_application)
    service.expects(:share)
    service.expects(:link)
    service.call
  end

  describe "Email application" do
    let(:email_application) { build_stubbed(:email_application) }
    let(:jobapp) { build_stubbed(:jobapp, job: email_application, resume: resume) }

    before do
      jobapp.stubs(:update).returns(true)
      service.stubs(:jobapp).returns(jobapp)
    end

    it "should copy resume and share with email application company" do
      service.expects(:link)
      service.expects(:clone)
      service.expects(:share).never
      service.expects(:share_email_app)
      service.call
    end

    it "should share with email application company" do
      resume = build_stubbed(:resume)
      versioned_resume = build_stubbed(:resume, origin: resume)
      resume.stubs(:latest).returns(versioned_resume)
      jobapp.stubs(:resume).returns(resume)

      Resume::Share.expects(:call).with( resume, [email_application.id], false)
      service.send(:share_email_app)
    end
  end

  it "should send an email on perform application" do
    mailer = mock('mailer')
    JobApplicationAndUpdateMailer.expects(:inform_recruiter).twice.returns(mailer)
    mailer.expects(:deliver).twice

    service.stubs(:recruiters).returns([build_stubbed(:recruiter), build_stubbed(:recruiter)])
    service.send(:perform_job_application)
  end

  it "should share a resume" do
    Resume::Share.expects(:call).with(jobapp.resume, [company.id])
    service.send(:share)
  end

  it "should set correct resume" do
    jobapp.stubs(:save).returns(true)
    service.send(:link)
    jobapp.resume.wont_equal resume
  end
end
