require "test_helper"

describe ApplyService::ViaEmail do
  # after { DatabaseCleaner.clean_with(:truncation) }
  let(:current_talent) { FactoryGirl.build_stubbed(:talent) }
  let(:email_application) { FactoryGirl.build_stubbed(:email_application) }
  let(:jobapp) { FactoryGirl.build_stubbed(:jobapp, job: email_application) }
  let(:service) { ApplyService::ViaEmail.new(jobapp) }

  it "must be valid" do
    service.must_be_instance_of ApplyService::ViaEmail
  end
end