# require "test_helper"
#
# describe JobappSearch do
#
#   def search(attributes = {})
#     JobappSearch.new(attributes).search
#   end
#
#   def import(*args)
#     JobappSearch.import! *args
#   end
#
#   let (:sorting_request) {
#     {
#         sort: ['_score']
#     }
#   }
#
#   let (:aggregations_request) {
#     {
#         aggregations: {
#             skills: {
#                 terms: {
#                     field: 'resume.skills'
#                 }
#             },
#             languages: {
#                 terms: {
#                     field: 'resume.language_ids'
#                 }
#             }
#         }
#     }
#   }
#
#
#   it 'should search for name' do
#     search_params = {
#         # query: 'johnny'
#     }
#
#     request_body = {
#         # query: {
#         #     bool: {
#         #         must: [
#         #             constant_score: {
#         #                 filter: {
#         #                     term: {
#         #                         'talent.name' => 'johnny'
#         #                     }
#         #                 },
#         #                 boost: 0
#         #             }
#         #         ]
#         #     }
#         # }
#     }
#
#     request_body = [request_body, aggregations_request, sorting_request].compact.reduce(:merge).to_json
#
#
#     @search = JobappSearch.new(search_params)
#
#     p @search.search
#
#     @search_scope = @search.search
#
#     stub_request(:get, "http://localhost:9250/test_veeta-test/jobapp/_search")
#         .to_return(status: 200, body: "", headers: {})
#
#     @aggregations = @search_scope.aggregations
#
#     assert_requested :get, "http://localhost:9250/test_veeta-test/jobapp/_search",
#                      body: request_body, times: 1 # ===> Success
#   end
# end
