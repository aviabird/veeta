require "test_helper"
require 'ipaddr'

describe Rack::Attack do
  include Rack::Test::Methods

  def app
    Rails.application
  end
  describe "throttle excessive sign_in requests by IP address" do
    let(:limit) { 51 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/api/talent/auth/sign_in", {email: "example21@t13s.at", password: "12341234"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 422
        last_response.status.must_equal 422
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          post "/api/talent/auth/sign_in", {email: "example21@t13s.at", password: "12341234"},"action_dispatch.request.parameters" => {email: "example21@t13s.at", password: "12341234"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 422
        last_response.status.must_equal 429
      end
    end
  end


  describe "throttle excessive forgot password requests by IP address" do
    let(:limit) { 51 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/api/talent/auth/password", {email: "example#{rand(1..1000)}@t13s.at", redirect_url: "https://app-dev2.t13s.at/#/request-password"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          post "/api/talent/auth/password", {email: "example#{rand(1..1000)}@t13s.at", redirect_url: "https://app-dev2.t13s.at/#/request-password"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive forgot password requests by IP address and email" do
    let(:limit) { 2 }

    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/api/talent/auth/password", {email: "example21@t13s.at", redirect_url: "https://app-dev2.t13s.at/#/request-password"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 4).times do
          post "/api/talent/auth/password", {email: "example21@t13s.at", redirect_url: "https://app-dev2.t13s.at/#/request-password"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive requests by IP address" do
    let(:limit) { 501 }

    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          get "/", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (1001).times do
          get "/", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end
  end



  describe "throttle excessive unlock confirmation requests by IP address" do
    let(:limit) { 51 }

    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/unlock", {email: "example#{rand(1..1000)}@t13s.at"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          post "/unlock", {email: "example#{rand(1..1000)}@t13s.at"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive unlock confirmation requests by IP address and email" do
    let(:limit) { 2 }



    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/unlock", {email: "example21@t13s.at"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 4).times do
          post "/unlock", {email: "example21@t13s.at"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive webhook requests by IP address" do
    let(:limit) { 1001 }
    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit).times do
          post "/webhooks/inbound/reply", {mandrill_events: "{}"} , "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end


    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit).times do
          post "/webhooks/inbound/bounce", {mandrill_events: "{}"} , "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end




  describe "throttle excessive registration via file requests" do
    let(:limit) { 51 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        stub_request(:get, 'https://home.textkernel.nl/sourcebox/soap/processDocument?wsdl').to_return(:status => 200, :body => "{}", :headers => {})

        ResumeService::ParsePDF.any_instance.stubs(:hashed).returns({profile: {personal: {email: nil}}})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        temp = Tempfile.new([SecureRandom.urlsafe_base64(32), '.txt'])
        limit.times do
          post "/api/talent/auth/via_file", {resume_pdf: {file: Rack::Test::UploadedFile.new(File.open(temp.path))}}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        stub_request(:get, 'https://home.textkernel.nl/sourcebox/soap/processDocument?wsdl').to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ResumeService::ParsePDF.any_instance.stubs(:hashed).returns({profile: {personal: {email: nil}}})

        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        temp = Tempfile.new([SecureRandom.urlsafe_base64(32), '.txt'])
        (limit * 2).times do
          post "/api/talent/auth/via_file", {resume_pdf: {file: Rack::Test::UploadedFile.new(File.open(temp.path))}}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive registration via xing" do
    let(:limit) { 51 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        XingApi::Client.any_instance.stubs(:get_request_token).returns({})
        limit.times do
          post "/api/talent/auth/xing_init", {callback: 'http://example.com'}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end
    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        XingApi::Client.any_instance.stubs(:get_request_token).returns({})
        (limit * 2).times do
          post "/api/talent/auth/xing_init", {callback: 'http://example.com'}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end


  describe "throttle excessive registration via linkedin" do
    let(:limit) { 51 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        LinkedinClientService.any_instance.stubs(:trigger_oauth_handshake).returns('http://example.com')
        limit.times do
          post "/api/talent/auth/linkedin_init", {callback: 'http://example.com'}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end
    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        LinkedinClientService.any_instance.stubs(:trigger_oauth_handshake).returns('http://example.com')
        (limit * 2).times do
          post "/api/talent/auth/linkedin_init", {callback: 'http://example.com'}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end


end
