require "test_helper"

describe WorkExperienceCalculator do
  it "should calculate work duration" do
    we = []
    we << { from: Time.parse('1/1/2009'), to: Time.parse('1/1/2010'), part: false }
    we << { from: Time.parse('1/1/2006'), to: Time.parse('1/1/2008'), part: true }
    we << { from: Time.parse('1/1/2005'), to: Time.parse('1/1/2007'), part: false }

    WorkExperienceCalculator.working_duration(we).round.must_equal 110376000
    WorkExperienceCalculator.working_duration(we, in: :hours).round.must_equal 30660
    WorkExperienceCalculator.working_duration(we, in: :days).round.must_equal 1278
    WorkExperienceCalculator.working_duration(we, in: :years).round(2).must_equal 3.50
  end

  it "should calculate internationala duration" do
    we = []
    we << { from: Time.parse('1/1/2009'), to: Time.parse('1/1/2010'), part: false, iso_code: 'at' }
    we << { from: Time.parse('1/1/2006'), to: Time.parse('1/1/2008'), part: true, iso_code: 'at' }
    we << { from: Time.parse('1/1/2005'), to: Time.parse('1/1/2007'), part: false, iso_code: 'de' }

    in_hours = [{ iso_code: 'at', duration: 17520 }, { iso_code: 'de', duration: 17520 }]
    in_days = [{ iso_code: 'at', duration: 730 }, { iso_code: 'de', duration: 730 }]
    in_years = [{ iso_code: 'at', duration: 2.0 }, { iso_code: 'de', duration: 2.0 }]

    WorkExperienceCalculator.international_duration(we, in: :hours).must_equal(in_hours)
    WorkExperienceCalculator.international_duration(we, in: :days).must_equal(in_days)
    WorkExperienceCalculator.international_duration(we, in: :years, round: 2).must_equal(in_years)
  end
end
