require "test_helper"

describe TalentSettingSerializer do
  let(:talent_setting) { FactoryGirl.build_stubbed(:talent_setting) }
  let(:serializer) { TalentSettingSerializer.new(talent_setting) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:talent_setting).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:talent_setting].has_key?(:id).must_equal true
    parsed[:talent_setting].has_key?(:country_id).must_equal true
    parsed[:talent_setting].has_key?(:professional_experience_level).must_equal true
    parsed[:talent_setting].has_key?(:newsletter_locale).must_equal true
    parsed[:talent_setting].has_key?(:newsletter_updates).must_equal true
    parsed[:talent_setting].has_key?(:working_locations).must_equal true
    parsed[:talent_setting].has_key?(:working_industries).must_equal true
    parsed[:talent_setting].has_key?(:availability).must_equal true
    parsed[:talent_setting].has_key?(:job_seeker_status).must_equal true
    parsed[:talent_setting].has_key?(:salary_exp_min).must_equal true
    parsed[:talent_setting].has_key?(:salary_exp_max).must_equal true
    parsed[:talent_setting].has_key?(:prefered_employment_type).must_equal true
  end
end
