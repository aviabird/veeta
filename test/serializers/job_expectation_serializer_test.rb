require "test_helper"

describe JobExpectationSerializer do
  let(:setting) { FactoryGirl.build_stubbed(:talent_setting) }
  let(:talent) { FactoryGirl.build_stubbed(:talent, setting: setting)}
  let(:job_expectation) { JobExpectation.new(talent) }
  let(:serializer) { JobExpectationSerializer.new(job_expectation) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:job_expectation).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:job_expectation].has_key?(:id).must_equal false
    parsed[:job_expectation].has_key?(:interested).must_equal true
    parsed[:job_expectation].has_key?(:availability).must_equal true
    parsed[:job_expectation].has_key?(:price_range).must_equal true
    parsed[:job_expectation].has_key?(:working_industries).must_equal true
    parsed[:job_expectation].has_key?(:working_locations).must_equal true
    parsed[:job_expectation].has_key?(:prefered_employment_type).must_equal true
  end
end
