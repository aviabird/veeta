require "test_helper"

describe EmailApplicationSerializer do
  let(:email_application) { FactoryGirl.build_stubbed(:email_application) }
  let(:serializer) { EmailApplicationSerializer.new(email_application) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:email_application).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:email_application].has_key?(:id).must_equal true
    parsed[:email_application].has_key?(:company_name).must_equal true
    parsed[:email_application].has_key?(:name).must_equal true
    parsed[:email_application].has_key?(:jobname).must_equal true
    parsed[:email_application].has_key?(:recruiter_email).must_equal true
    parsed[:email_application].has_key?(:company).must_equal true
    parsed[:email_application].has_key?(:recruiter).must_equal true
    parsed[:email_application].has_key?(:company_id).must_equal true
    parsed[:email_application].has_key?(:recruiter_id).must_equal true
  end
end