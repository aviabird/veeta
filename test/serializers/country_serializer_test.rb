require "test_helper"

describe CountrySerializer do
  let(:country) { Country.all.first }
  let(:serializer) { CountrySerializer.new(country) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:country).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:country].has_key?(:id).must_equal true
    parsed[:country].has_key?(:name).must_equal true
    parsed[:country].has_key?(:iso_code).must_equal true
  end
end
