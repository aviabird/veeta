require "test_helper"

describe ResumeTemplateSerializer do
  let(:resume_template) { FactoryGirl.build_stubbed(:resume_template) }
  let(:serializer) { ResumeTemplateSerializer.new(resume_template) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:resume_template).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:resume_template].has_key?(:id).must_equal true
    parsed[:resume_template].has_key?(:name).must_equal true
    parsed[:resume_template].has_key?(:preview_url).must_equal true
  end
end