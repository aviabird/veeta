require "test_helper"

describe Recruiters::TalentsSerializer do
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:recruiter) { FactoryGirl.build_stubbed(:recruiter) }
  let(:recruiter_auth) { FactoryGirl.build_stubbed(:recruiter_auth, recruiter: recruiter) }
  let(:serializer) { Recruiters::TalentsSerializer.new(talent) }

  before do
    talent.stubs(:company_rating).returns(3)
    talent.stubs(:followed_by?).returns(true)
    talent.stubs(:latest_resume).returns(resume)
    serializer.stubs(:current_user).returns(recruiter_auth)
  end

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:talents).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:talents].has_key?(:id).must_equal true
    parsed[:talents].has_key?(:first_name).must_equal true
    parsed[:talents].has_key?(:last_name).must_equal true
    parsed[:talents].has_key?(:avatar_url).must_equal true
    parsed[:talents].has_key?(:rating).must_equal true
    parsed[:talents].has_key?(:labels).must_equal true
    parsed[:talents].has_key?(:work_experience_duration).must_equal true
    parsed[:talents].has_key?(:age).must_equal true
    parsed[:talents].has_key?(:work_industries).must_equal true
    parsed[:talents].has_key?(:latest_work_experience).must_equal true
    parsed[:talents].has_key?(:salutation).must_equal true
    parsed[:talents].has_key?(:is_followed).must_equal true
  end
end