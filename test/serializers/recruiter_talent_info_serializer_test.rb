require "test_helper"

describe RecruiterTalentInfoSerializer do
  let(:recruiter_talent_info) { FactoryGirl.build_stubbed(:recruiter_talent_info) }
  let(:serializer) { RecruiterTalentInfoSerializer.new(recruiter_talent_info) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:recruiter_talent_info).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:recruiter_talent_info].has_key?(:id).must_equal true
    parsed[:recruiter_talent_info].has_key?(:talent_id).must_equal true
    parsed[:recruiter_talent_info].has_key?(:recruiter_id).must_equal true
    parsed[:recruiter_talent_info].has_key?(:followed_at).must_equal true
    parsed[:recruiter_talent_info].has_key?(:in_pool_since).must_equal true
    parsed[:recruiter_talent_info].has_key?(:created_at).must_equal true
    parsed[:recruiter_talent_info].has_key?(:updated_at).must_equal true
  end
end