require "test_helper"

describe CompanyTalentInfoSerializer do
  let(:company_talent_info) { FactoryGirl.build_stubbed(:company_talent_info) }
  let(:serializer) { CompanyTalentInfoSerializer.new(company_talent_info) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:company_talent_info).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:company_talent_info].has_key?(:id).must_equal true
    parsed[:company_talent_info].has_key?(:talent_id).must_equal true
    parsed[:company_talent_info].has_key?(:company_id).must_equal true
    parsed[:company_talent_info].has_key?(:rating).must_equal true
    parsed[:company_talent_info].has_key?(:note).must_equal true
    parsed[:company_talent_info].has_key?(:category).must_equal true
  end
end