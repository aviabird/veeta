require "test_helper"

describe LanguageCollectionSerializer do
  let(:language) { Language.find('en') }
  let(:serializer) { LanguageCollectionSerializer.new(language) }

  before do
    serializer.stubs(:current_user).returns(nil)
  end

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:language_collection).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:language_collection].has_key?(:id).must_equal true
    parsed[:language_collection].has_key?(:name).must_equal true
    parsed[:language_collection].has_key?(:iso_code).must_equal true
  end
end
