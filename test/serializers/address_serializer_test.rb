require "test_helper"

describe AddressSerializer do
  let(:address) { FactoryGirl.build_stubbed(:address) }
  let(:serializer) { AddressSerializer.new(address) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:address).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:address].has_key?(:id).must_equal true
    parsed[:address].has_key?(:city).must_equal true
    parsed[:address].has_key?(:zip).must_equal true
    parsed[:address].has_key?(:address_line).must_equal true
    parsed[:address].has_key?(:address_line_2).must_equal true
    parsed[:address].has_key?(:country).must_equal true
    parsed[:address].has_key?(:country_id).must_equal true
  end
end