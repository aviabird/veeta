require "test_helper"

describe Rs::PublicationSerializer do
  let(:publication) { FactoryGirl.build_stubbed(:rs_publication) }
  let(:serializer) { Rs::PublicationSerializer.new(publication) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:publication).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:publication].has_key?(:id).must_equal true
    parsed[:publication].has_key?(:title).must_equal true
    parsed[:publication].has_key?(:publication_type).must_equal true
    parsed[:publication].has_key?(:year).must_equal true
    parsed[:publication].has_key?(:description).must_equal true
  end
end