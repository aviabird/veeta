require "test_helper"

describe Rs::LinkSerializer do
  let(:link) { FactoryGirl.build_stubbed(:rs_link) }
  let(:serializer) { Rs::LinkSerializer.new(link) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:link).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:link].has_key?(:id).must_equal true
    parsed[:link].has_key?(:linkedin).must_equal true
    parsed[:link].has_key?(:xing).must_equal true
    parsed[:link].has_key?(:website).must_equal true
  end
end