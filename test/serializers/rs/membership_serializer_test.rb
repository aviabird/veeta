require "test_helper"

describe Rs::MembershipSerializer do
  let(:membership) { FactoryGirl.build_stubbed(:rs_membership) }
  let(:serializer) { Rs::MembershipSerializer.new(membership) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:membership).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:membership].has_key?(:id).must_equal true
    parsed[:membership].has_key?(:organization).must_equal true
    parsed[:membership].has_key?(:role).must_equal true
    parsed[:membership].has_key?(:area).must_equal true
    parsed[:membership].has_key?(:from).must_equal true
    parsed[:membership].has_key?(:to).must_equal true
  end
end