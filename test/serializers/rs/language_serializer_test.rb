require "test_helper"

describe TalentSerializer do
  let(:language) { FactoryGirl.build_stubbed(:rs_language) }
  let(:serializer) { Rs::LanguageSerializer.new(language) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:language).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:language].has_key?(:id).must_equal true
    parsed[:language].has_key?(:level).must_equal true
    parsed[:language].has_key?(:language).must_equal true
  end
end