require "test_helper"

describe TalentSerializer do
  let(:education) { FactoryGirl.build_stubbed(:rs_education) }
  let(:serializer) { Rs::EducationSerializer.new(education) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:education).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:education].has_key?(:id).must_equal true
    parsed[:education].has_key?(:type_of_education).must_equal true
    parsed[:education].has_key?(:school_name).must_equal true
    parsed[:education].has_key?(:degree).must_equal true
    parsed[:education].has_key?(:subject).must_equal true
    parsed[:education].has_key?(:from).must_equal true
    parsed[:education].has_key?(:to).must_equal true
    parsed[:education].has_key?(:current).must_equal true
    parsed[:education].has_key?(:country).must_equal true
    parsed[:education].has_key?(:description).must_equal true
    parsed[:education][:country].has_key?(:id).must_equal true
    parsed[:education][:country].has_key?(:name).must_equal true
    parsed[:education][:country].has_key?(:iso_code).must_equal true
  end
end