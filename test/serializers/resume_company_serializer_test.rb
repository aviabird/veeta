require "test_helper"

describe TalentSerializer do
  let(:company) { FactoryGirl.build_stubbed(:company) }
  let(:serializer) { ResumeCompanySerializer.new(company) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:resume_company).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:resume_company].has_key?(:id).must_equal true
    parsed[:resume_company].has_key?(:name).must_equal true
    parsed[:resume_company].has_key?(:logo_url).must_equal true
  end
end