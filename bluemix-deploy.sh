#!/bin/bash

# connect to bluemix and deploy
bundle exec rake localeapp:pull
cf api https://api.eu-gb.bluemix.net
cf login -u guenther@talentsolutions.at -o 'Talent Solutions' -s ${1}
#cf zero-downtime-push vta-main-prod2 -f manifest-${1}.yml
./app-deploy-with-rename.sh intapi-${1} intapi-${1}-bak . manifest-${1}.yml
