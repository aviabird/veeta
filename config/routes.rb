require "resque_web"
require 'admin_subdomain'
require "resque"
require 'resque_scheduler'
require 'resque_scheduler/server'

Rails.application.routes.draw do

  #match '/oauth/test_request',  :to => 'oauth#test_request',  :as => :test_request, :via => [:get,:post]
  #match '/oauth/token',         :to => 'oauth#token',         :as => :token, :via => [:get,:post]
  match '/v1/access_token',  :to => 'oauth/oauth#access_token',  :as => :access_token, :via => [:get,:post]
  match '/v1/request_token', :to => 'oauth/oauth#request_token', :as => :request_token, :via => [:post,:get]
  match '/v1/authorize',     :to => 'oauth/oauth#authorize',     :as => :authorize, :via => [:get, :post]
#  match '/v1/oauth',               :to => 'oauth#index',         :as => :oauth, :via => [:get,:post]
  post '/v1/cancel_oauth_handshake', :to => 'oauth/oauth#cancel_oauth_handshake'
  match '/v1/users/me', :to => 'oauth/access#profile', :via => [:get]

  # Oauth client management and vadmin secured behind devise admin
  constraints AdminSubdomain do
    resources :oauth_clients, :controller => "oauth/oauth_clients"
    devise_for :admin_auths, path: '/oauth-mgmt',:skip => [:registerable, :registrations,  :confirmable, :confirmations, :passwords], controllers: {
      sessions: 'oauth/admin_auths/sessions' }
    authenticate :admin_auth do
      require 'resque_scheduler/server'
      mount Vadmin::Engine => '/'
      mount Resque::Server.new, :at => '/veeta-background'
      get "/", to: redirect("/admin", status:302)
    end

  end

  mount MyveetaDomain::Engine => "/easy"


  root 'welcome#index'

  #
  post '/webhooks/inbound/reply', :to => 'webhooks/inbound#reply'
  post '/webhooks/inbound/bounce', :to => 'webhooks/inbound#bounce'
  post '/webhooks/intercom/unsubscribe', :to => 'webhooks/intercom#unsubscribe'


#  namespace :oauth do
#    devise_for :talent_auths, controllers: {
#      sessions: 'oauth/talent_auths/sessions' }
#    devise_for :admin_auths, controllers: {
#      sessions: 'oauth/admin_auths/sessions' }
#  end

  devise_for :recruiter_auths , :skip => [:registerable, :registrations, :passwords], controllers: {
    sessions: 'job_mgmt/recruiter_auths/sessions' }
  devise_for :talent_auths, :path => '', :skip => [:registerable, :registrations], :path_names => {:sign_in => 'login', :sign_out => 'logout'}, controllers: {
    sessions: 'oauth/talent_auths/sessions',
    confirmations: 'api/talent/auth/confirmations',
    passwords: 'api/talent/auth/devise_passwords',
    unlocks: 'api/talent/auth/unlocks' }

  resources :recruiters , controller: 'job_mgmt/recruiters' do
    collection do
      post :recruiter_job
      post :create_job
      post :update_job
    end
  end

  resources :talents, only: [:index], controller: 'job_mgmt/talents' do
    member do
      get :download_cv
    end
  end

  resources :jobs, only: [:disable_job] , controller: 'job_mgmt/jobs' do
    member do
      put :disable_job
    end
  end

  resources :cv_sharing_requests, only: [:edit, :update] , controller: 'job_mgmt/cv_sharing_requests' do
    member do
      get :edit
    end
  end

  #universal functionality introduced to other classes
  concern :validable do
    member do
      put :validate
    end
  end

  concern :validables do
    collection do
      post :validate
    end
  end

  #get '/dl/:file_type/:date_hash/:random_security_hash/:access_security_layer/:basename.:extension', controller: 'api/talent/talents', action: 'avatar'

  namespace :api, defaults: {format: :json} do
    namespace :talent do
      #overwriting controllers of devise with custom controllers
      mount_devise_token_auth_for 'TalentAuth', skip: [:omniauth_callbacks, :confirmable, :confirmations, :unlockable, :unlocks], at: '/auth',
                                  controllers: {
                                      sessions: 'api/talent/auth/sessions',
                                      registrations: 'api/talent/auth/registrations',
                                      passwords: 'api/talent/auth/passwords'
                                  }
      #TODO add rack attack security
      devise_scope :talent_auth do
        post "auth/via_file", to: "auth/registrations#create_via_file"
        post "auth/xing_init", to: "auth/registrations#xing_init"
        post "auth/xing_callback", to: "auth/registrations#xing_callback"
        post "auth/linkedin_init", to: "auth/registrations#linkedin_init"
        post "auth/linkedin_callback", to: "auth/registrations#linkedin_callback"
      end

      #resources :referrers , only: [:get_referrers] do
      #  collection do
      #    get :get_referrers
      #  end
      #end

      resource :jobs, only: [:get_current_openings] do
        collection do
          get :get_current_openings
        end
      end

      get 'sharing', to: 'sharing#index'
      get 'vbsharing', to: 'vb_sharing#index'

      put 'sharing/revoke/:company_id', to: 'sharing#revoke'
      put 'vbsharing/revoke/:client_application_id', to: 'vb_sharing#revoke'
      put 'vbsharing/third_party_data_wiped_out/:client_application_id', to: 'vb_sharing#third_party_data_wiped_out'

      # putting :download to resumes resource did not work...
      get 'resumes/download', to: 'resumes#download'

      resources :companies, only: [:index]
      resources :autosuggestions, only: [:index]

      resources :resumes, except: [:edit, :create], concerns: :validables do
        collection do
          put :share_all
        end

        member do
          put :share
          put :sharing_request
          put :fillin
          get :fillin_from_xing
          get :xingreturn
          get :fillin_from_linkedin
          get :linkedinreturn
        end
      end

      get 'cv_sharing_requests/find_by_ext_id', to: 'cv_sharing_requests#find_by_ext_id'
      resources :cv_sharing_requests, only: [:show]
      resources :jobs, only: [:show] do
        collection do
          get :find_by_ext_id
        end
      end

      namespace :resume do
        resources :educations, except: [:edit], concerns: :validables
        resources :work_experiences, except: [:edit], concerns: :validables
        resources :languages, except: [:edit], concerns: :validables
        resources :skills, except: [:edit], concerns: :validables
        resource  :link, except: [:index, :edit, :destroy], concerns: :validables
        resources :documents, except: [:edit], concerns: :validables
        resource  :about, except: [:index, :edit, :destroy], concerns: :validables
        resources :certifications, except: [:edit], concerns: :validables
        resource :pdf_template_setting, only: [:update], concerns: :validables
        #### MODULES DEACTIVATED FOR NOW - PLEASE CAREFULLY CHECK/IMPLEMENT AUTHORIZATION BEFORE ACTIVATING IT!!!!
        #resources :memberships, except: [:edit], concerns: :validables
        #resources :projects, except: [:edit], concerns: :validables
        #resources :awards, except: [:edit], concerns: :validables
        #resources :publications, except: [:edit], concerns: :validables
      end

      resource :talent, only: [:show, :edit, :update], concerns: :validable do
        #no id of specific objects required, for multiple resources
        collection do
          post :avatar_upload
          delete :avatar_remove
          put :update_locale
        end
      end
      resource :talent_auth, only: [] do
        #specific for certain resource
        member do
          put :email
          put :password
          put :delete
          put :resend_confirmation
        end
      end
      resource :talent_setting, only: [:show, :edit, :update], concerns: :validable
      resources :jobapps, except: [:edit, :destroy], concerns: :validables do
        member do
          put :withdrawn
          put :finish
        end
      end
      resources :cv_sharing_responses, only: [:show, :new, :create, :update, :validate, :finish], concerns: :validables do
        member do
          put :finish
        end
      end
    end

    namespace :freshdesk do
      resources :solutions, only: [:show, :index]
    end
    resources :locales, only: :index
    resources :countries, only: :index
    resources :languages, only: :index

  end

  get '/dl/:file_type/:date_hash/:random_security_hash/:access_security_part/:file_name',:to => 'file_redirect#resource',:file_name => /.*/

end
