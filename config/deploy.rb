# Server is configured using: https://github.com/intercity/chef-repo

# config valid only for Capistrano 3.1
lock '3.3.4'

set :default_stage, 'staging'

set :application, 'veeta'
set :repo_url, 'git@bitbucket.org:talent-solutions/veeta.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}
# set :linked_files, %w{}
# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system vendor/engines private/uploads public config/locales}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :bundle_flags, '--quiet'

namespace :nginx do
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :service, 'nginx restart'
    end
  end
end

namespace :deploy do
  # def within_gem_path(gem_name)
  #   Gem.loaded_specs[gem_name].full_gem_path
  # end

  # task :copy_dotenv do
  #   on roles(:app) do
  #     within release_path do
  #       execute :cp, '.rbenv-vars .env'
  #     end
  #   end
  # end
  # after :updating, :copy_dotenv

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  desc 'Restart unicorn'
  task :unicorn_restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :service, 'unicorn restart'
    end
  end

  # Unicorn is not used everywhere
  # after :publishing, :unicorn_restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  # after :finished, :restart_localeapp do
  #   on roles(:app) do
  #     within release_path do
  #         execute :localeapp, 'daemon -b'
  #     end
  #   end
  # end

  desc 'Rebuild application'
  task :rebuild do
    within current_path do
      with rails_env: fetch(:rails_env) do
        execute :rake, 'db:reset'
      end
    end
  end

  task :copy_dotenv do
    on roles(:app) do
      within release_path do
        execute :cp, '.rbenv-vars .env'
      end
    end
  end
  after :updating, :copy_dotenv
end

after 'deploy:publishing', 'nginx:restart'
