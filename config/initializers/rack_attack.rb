class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::RedisStore.new()

  @@limits = {}
  @@limits[:time_period] = 10 # in minutes
  @@limits[:ban_time] = 60 # in minutes

  @@limits[:general_request] = {}
  @@limits[:general_request][:track] = 999 # tracks 1501st trial
  @@limits[:general_request][:ban] = 2000 # bans 2000th trial
  @@limits[:talent_sign_in] = {}
  @@limits[:talent_sign_in][:track] = 9
  @@limits[:talent_sign_in][:ban] = 100
  @@limits[:talent_vb_sign_in] = {}
  @@limits[:talent_vb_sign_in][:track] = 9
  @@limits[:talent_vb_sign_in][:ban] = 100
  @@limits[:talent_resend_unlock_instructions] = {}
  @@limits[:talent_resend_unlock_instructions][:track] = 4
  @@limits[:talent_resend_unlock_instructions][:ban] = 100
  @@limits[:talent_resend_unlock_instructions_per_email] = {}
  @@limits[:talent_resend_unlock_instructions_per_email][:track] = 3
  @@limits[:talent_resend_unlock_instructions_per_email][:ban] = 5
  @@limits[:talent_forgot_password_request] = {}
  @@limits[:talent_forgot_password_request][:track] = 10
  @@limits[:talent_forgot_password_request][:ban] = 100
  @@limits[:talent_forgot_password_request_per_email] = {}
  @@limits[:talent_forgot_password_request_per_email][:track] = 2
  @@limits[:talent_forgot_password_request_per_email][:ban] = 5
  @@limits[:talent_forgot_password] = {}
  @@limits[:talent_forgot_password][:track] = 3
  @@limits[:talent_forgot_password][:ban] = 5
  @@limits[:talent_email_confirmation] = {}
  @@limits[:talent_email_confirmation][:track] = 2
  @@limits[:talent_email_confirmation][:ban] = 6
  @@limits[:talent_registration] = {}
  @@limits[:talent_registration][:track] = 2
  @@limits[:talent_registration][:ban] = 50
  @@limits[:talent_update] = {}
  @@limits[:talent_update][:track] = 2
  @@limits[:talent_update][:ban] = 6
  @@limits[:talent_email_update] = {}
  @@limits[:talent_email_update][:track] = 2
  @@limits[:talent_email_update][:ban] = 6
  @@limits[:talent_cv_shared_via_mail] = {}
  @@limits[:talent_cv_shared_via_mail][:track] = 2
  @@limits[:talent_cv_shared_via_mail][:ban] = 6
  @@limits[:talent_resend_email_confirmation] = {}
  @@limits[:talent_resend_email_confirmation][:track] = 2
  @@limits[:talent_resend_email_confirmation][:ban] = 6
  @@limits[:talent_set_password] = {}
  @@limits[:talent_set_password][:track] = 2
  @@limits[:talent_set_password][:ban] = 6
  @@limits[:talent_finish_jobapp] = {}
  @@limits[:talent_finish_jobapp][:track] = 10
  @@limits[:talent_finish_jobapp][:ban] = 100
  @@limits[:talent_finish_sharing_response] = {}
  @@limits[:talent_finish_sharing_response][:track] = 10
  @@limits[:talent_finish_sharing_response][:ban] = 100
  @@limits[:admin_sign_in] = {}
  @@limits[:admin_sign_in][:track] = 2
  @@limits[:admin_sign_in][:ban] = 10
  @@limits[:admin_resend_unlock_instructions] = {}
  @@limits[:admin_resend_unlock_instructions][:track] = 2
  @@limits[:admin_resend_unlock_instructions][:ban] = 10
  @@limits[:admin_resend_unlock_instructions_per_email] = {}
  @@limits[:admin_resend_unlock_instructions_per_email][:track] = 2
  @@limits[:admin_resend_unlock_instructions_per_email][:ban] = 5
  @@limits[:recruiter_sign_in] = {}
  @@limits[:recruiter_sign_in][:track] = 2
  @@limits[:recruiter_sign_in][:ban] = 10
  @@limits[:recruiter_resend_unlock_instructions] = {}
  @@limits[:recruiter_resend_unlock_instructions][:track] = 2
  @@limits[:recruiter_resend_unlock_instructions][:ban] = 10
  @@limits[:recruiter_resend_unlock_instructions_per_mail] = {}
  @@limits[:recruiter_resend_unlock_instructions_per_mail][:track] = 2
  @@limits[:recruiter_resend_unlock_instructions_per_mail][:ban] = 5
  @@limits[:webhook_inbound] = {}
  @@limits[:webhook_inbound][:track] = 14
  @@limits[:webhook_inbound][:ban] = 1000
  @@limits[:webhook_bounce] = {}
  @@limits[:webhook_bounce][:track] = 14
  @@limits[:webhook_bounce][:ban] = 1000
  @@limits[:talent_registration_file] = {}
  @@limits[:talent_registration_file][:track] = 9
  @@limits[:talent_registration_file][:ban] = 100
  @@limits[:talent_registration_xing] = {}
  @@limits[:talent_registration_xing][:track] = 9
  @@limits[:talent_registration_xing][:ban] = 100
  @@limits[:talent_registration_linkedin] = {}
  @@limits[:talent_registration_linkedin][:track] = 9
  @@limits[:talent_registration_linkedin][:ban] = 100

  # Cloud flare IPs from https://www.cloudflare.com/ips/

  # Whitelist cloud flare IPs

  begin
    # Always allow requests from localhost
    # (blacklist & throttles are skipped)
    Rack::Attack.whitelist('allow from localhost') do |req|
      # Requests are allowed if the return value is truthy
      '127.0.0.1' == req.ip || '::1' == req.ip
    end
    unless Rails.env.test?
      # Cloud flare IPs from https://www.cloudflare.com/ips/
      @@cloud_flare_ips_v4 = ['103.21.244.0/22','103.22.200.0/22','103.31.4.0/22','104.16.0.0/12','108.162.192.0/18','131.0.72.0/22','141.101.64.0/18','162.158.0.0/15','172.64.0.0/13','173.245.48.0/20','188.114.96.0/20','190.93.240.0/20','197.234.240.0/22','198.41.128.0/17','199.27.128.0/21','2400:cb00::/32','2405:8100::/32','2405:b500::/32','2606:4700::/32','2803:f800::/32','2c0f:f248::/32','2a06:98c0::/29']# HTTP.get("https://www.cloudflare.com/ips-v4").to_s.split("\n")
      @@cloud_flare_ips_v6 = []#HTTP.get("https://www.cloudflare.com/ips-v6").to_s.split("\n")

      # Whitelist cloud flare IPs
      (@@cloud_flare_ips_v4 + @@cloud_flare_ips_v6).each do |cf_ip|
        Rack::Attack.whitelist("allow from cloudflare ip #{cf_ip}") do |request|
          IPAddr.new(cf_ip).include? client_ip(request)
        end
      end
    end
  rescue
    LogService.call "rack_attack.teapi.init", {details: "Could not initialize IP Whitelist fetched from CloudFlare.", priority: :high, notify_im: true}
  end if defined?(HTTP)


  Rack::Attack.blacklisted_response = lambda do |env|
    # Using 503 because it may make attacker think that they have successfully
    # DOSed the site. Rack::Attack returns 403 for blacklists by default
    [ 429, {}, ['Blocked due to too many requsts. This event is tracked for further investigation by myVeeta.']]
  end

  ###
  # Make sure that each blacklist has a different name
  # Also make sure that each Allow2Ban filter overall has a unique identifier (including IP)
  # Once one Allow2Ban filter exceeded the limits, the ip will be banned
  ###


  # Track it using ActiveSupport::Notification
  ActiveSupport::Notifications.subscribe("rack.attack") do |name, start, finish, request_id, req|
    #filter passwords for logging
    params_copy = req.params.dup if req.params
    params_copy.delete("password") if params_copy
    action_dispatch_params_copy = req.env['action_dispatch.request.request_parameters'].dup if req.env['action_dispatch.request.request_parameters'].present?
    action_dispatch_params_copy.delete("password") if action_dispatch_params_copy

    if req.env['rack.attack.match_type'] == :track
      # log with slack notification
      LogService.call "rack_attack.#{req.env['rack.attack.matched']}.track", {source_ip: client_ip(req), details: "Tracking request form suspicious IP #{client_ip(req)}. Most recent request came with following params: {#{action_dispatch_params_copy}}", priority: :high, limit_log: true, notify_im: true}
      # log every track event
      LogService.call "rack_attack.#{req.env['rack.attack.matched']}.track.details", {source_ip: client_ip(req), details: "Request from #{client_ip(req)} : HTTP #{req.env['REQUEST_METHOD']} #{req.env['REQUEST_URI']}; Params: #{action_dispatch_params_copy}".first(255), data: req.env.to_s, priority: :high, limit_log: false, notify_im: false}
    end

    if req.env['rack.attack.match_type'] == :blacklist
      # log with slack notification
      LogService.call "rack_attack.#{req.env['rack.attack.matched']}.ban", {source_ip: client_ip(req), details: "Blocking IP #{client_ip(req)} for an hour due to too many requests. Most recent request came with following params: {#{action_dispatch_params_copy}}", priority: :high, limit_log: true, notify_im: true}
      # log with full details
      LogService.call "rack_attack.#{req.env['rack.attack.matched']}.ban.details", {source_ip: client_ip(req), details: "Request from #{client_ip(req)} : HTTP #{req.env['REQUEST_METHOD']} #{req.env['REQUEST_URI']}; Params: #{action_dispatch_params_copy}".first(255), data: req.env.to_s, priority: :high, limit_log: false, notify_im: false}
    end

  end

  ### Any request hitting the Intapi
  Rack::Attack.track("general_request", :limit => @@limits[:general_request][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path!= "/api/talent/autosuggestions" && client_ip(req)
  end

  #Rack::Attack.blacklist('general_request') do |req|
  #  Rack::Attack::Allow2Ban.filter("general_request-#{client_ip(req)}", :maxretry => @@limits[:general_request][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
  #    client_ip(req)
  #  end
  #end

  ### Login request with specific credentials (talent)
  Rack::Attack.track("talent_sign_in", :limit => @@limits[:talent_sign_in][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/auth/sign_in'  && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('talent_sign_in') do |req|
    Rack::Attack::Allow2Ban.filter("talent_sign_in-#{client_ip(req)}", :maxretry => @@limits[:talent_sign_in][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/auth/sign_in'  && req.post? && client_ip(req)
    end
  end


  # Login Form posted via myVeeta Button
  Rack::Attack.track("talent_vb_sign_in", :limit => @@limits[:talent_vb_sign_in][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/login' && req.post?  && client_ip(req)
  end

  Rack::Attack.blacklist('talent_vb_sign_in') do |req|
    Rack::Attack::Allow2Ban.filter("talent_vb_sign_in-#{client_ip(req)}", :maxretry => @@limits[:talent_vb_sign_in][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/login'  && req.post? && client_ip(req)
    end
  end

  # Requesting an unlock confirmation email for a specific e-mail address (talent)
  Rack::Attack.track("talent_resend_unlock_instructions", :limit => @@limits[:talent_resend_unlock_instructions][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/unlock'  && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('talent_resend_unlock_instructions') do |req|
    Rack::Attack::Allow2Ban.filter("talent_resend_unlock_instructions-#{client_ip(req)}", :maxretry => @@limits[:talent_resend_unlock_instructions][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/unlock' && req.post? && client_ip(req)
    end
  end

  Rack::Attack.track("talent_resend_unlock_instructions_per_email", :limit => @@limits[:talent_resend_unlock_instructions_per_email][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/unlock'  && req.post? && client_ip(req) && self.email(req)
  end

  Rack::Attack.blacklist('talent_resend_unlock_instructions_per_email') do |req|
    Rack::Attack::Allow2Ban.filter("talent_resend_unlock_instructions_per_email-#{client_ip(req)}-#{self.email(req)}", :maxretry => @@limits[:talent_resend_unlock_instructions_per_email][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/unlock' && req.post? && client_ip(req) && self.email(req)
    end
  end

  ### Requesting a password reset email for a specific e-mail address
  #Rack::Attack.track("talent_forgot_password_request", :limit => @@limits[:talent_forgot_password_request][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/api/talent/auth/password' && (req.post? || req.put? || req.patch?)  && client_ip(req)
  #end

  Rack::Attack.blacklist('talent_forgot_password_request') do |req|
    Rack::Attack::Allow2Ban.filter("talent_forgot_password_request-#{client_ip(req)}", :maxretry => @@limits[:talent_forgot_password_request][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/auth/password'  && (req.post? || req.put? || req.patch?) && client_ip(req)
    end
  end

  Rack::Attack.blacklist('talent_forgot_password_request_per_email') do |req|
    Rack::Attack::Allow2Ban.filter("talent_forgot_password_request_per_email-#{client_ip(req)}-#{self.email(req)}", :maxretry => @@limits[:talent_forgot_password_request_per_email][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/auth/password'  && (req.post? || req.put? || req.patch?) && client_ip(req) && self.email(req)
    end
  end


  ### Requesting a password reset email for a specific e-mail address
  #Rack::Attack.track("talent_forgot_password", :limit => @@limits[:talent_forgot_password][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/password' && (req.post? || req.put? || req.patch?)  && client_ip(req)
  #end

  #Rack::Attack.blacklist('talent_forgot_password') do |req|
  #  Rack::Attack::Allow2Ban.filter("talent_forgot_password-#{client_ip(req)}", :maxretry =>  @@limits[:talent_forgot_password][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
  #    req.path == '/password'  && (req.post? || req.put? || req.patch?) && client_ip(req)
  #  end
  #end


  ### Requesting a password reset email for a specific e-mail address
  #Rack::Attack.track("talent_email_confirmation", :limit => @@limits[:talent_email_confirmation][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/confirmation' && req.get?  && client_ip(req)
  #end

  #Rack::Attack.blacklist('talent_email_confirmation') do |req|
  #  Rack::Attack::Allow2Ban.filter("talent_email_confirmation-#{client_ip(req)}", :maxretry => @@limits[:talent_email_confirmation][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
  #    req.path == '/confirmation'  && req.get? && client_ip(req)
  #  end
  #end

  ### Talent registration (which is also called when registering via file, xing, linkedin)
  Rack::Attack.track("talent_registration", :limit => @@limits[:talent_registration][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/auth' && (req.post? || req.put? || req.patch?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_registration') do |req|
    Rack::Attack::Allow2Ban.filter("talent_registration-#{client_ip(req)}", :maxretry => @@limits[:talent_registration][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/auth'  && (req.post? || req.put? || req.patch?) && client_ip(req)
    end
  end

  ### Talent registration via file
  Rack::Attack.track("talent_registration_file", :limit => @@limits[:talent_registration_file][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/auth/via_file' && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('talent_registration_file') do |req|
    Rack::Attack::Allow2Ban.filter("talent_registration_file-#{client_ip(req)}", :maxretry => @@limits[:talent_registration_file][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/auth/via_file' && req.post? && client_ip(req)
    end
  end

  ### Talent registration via xing
  Rack::Attack.track("talent_registration_xing", :limit => @@limits[:talent_registration_xing][:track], :period =>  @@limits[:time_period].minutes) do |req|
    (req.path == '/api/talent/auth/xing_init' || req.path == '/api/talent/auth/xing_callback') && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('talent_registration_xing') do |req|
    Rack::Attack::Allow2Ban.filter("talent_registration_xing-#{client_ip(req)}", :maxretry => @@limits[:talent_registration_xing][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      (req.path == '/api/talent/auth/xing_init' || req.path == '/api/talent/auth/xing_callback') && req.post? && client_ip(req)
    end
  end

  ### Talent registration via linkedin
  Rack::Attack.track("talent_registration_linkedin", :limit => @@limits[:talent_registration_linkedin][:track], :period =>  @@limits[:time_period].minutes) do |req|
    (req.path == '/api/talent/auth/linkedin_init' || req.path == '/api/talent/auth/linkedin_callback') && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('talent_registration_linkedin') do |req|
    Rack::Attack::Allow2Ban.filter("talent_registration_linkedin-#{client_ip(req)}", :maxretry => @@limits[:talent_registration_linkedin][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      (req.path == '/api/talent/auth/linkedin_init' || req.path == '/api/talent/auth/linkedin_callback') && req.post? && client_ip(req)
    end
  end


=begin
  ### Talent profile update (potentially changes email address)
  Rack::Attack.track("talent_update", :limit => @@limits[:talent_update][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/talent' && (req.put? || req.patch?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_update') do |req|
    Rack::Attack::Allow2Ban.filter("talent_update-#{client_ip(req)}", :maxretry =>  @@limits[:talent_update][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/talent'  && (req.put? || req.patch?) && client_ip(req)
    end
  end

  # Changing the e-mail address of a particular talent (e.g. email confirmation box at the end of a jobapp)
  Rack::Attack.track("talent_email_update", :limit => @@limits[:talent_email_update][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/talent_auth/email' && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_email_update') do |req|
    Rack::Attack::Allow2Ban.filter("talent_email_update-#{client_ip(req)}", :maxretry => @@limits[:talent_email_update][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/talent_auth/email'  && (req.put?) && client_ip(req)
    end
  end

  # Sending a CV to a particular e-mail address
  Rack::Attack.track("talent_cv_shared_via_mail", :limit => @@limits[:talent_cv_shared_via_mail][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path.starts_with?('/api/talent/resumes/') && req.path.ends_with?('/sharing_request') && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_cv_shared_via_mail') do |req|
    Rack::Attack::Allow2Ban.filter("talent_cv_shared_via_mail-#{client_ip(req)}", :maxretry => @@limits[:talent_cv_shared_via_mail][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path.starts_with?('/api/talent/resumes/') && req.path.ends_with?('/sharing_request') && (req.put?) && client_ip(req)
    end
  end

  # Sending a CV to a particular e-mail address
  Rack::Attack.track("talent_resend_email_confirmation", :limit => @@limits[:talent_resend_email_confirmation][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/talent_auth/resend_confirmation' && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_resend_email_confirmation') do |req|
    Rack::Attack::Allow2Ban.filter("talent_resend_email_confirmation-#{client_ip(req)}", :maxretry => @@limits[:talent_resend_email_confirmation][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/talent_auth/resend_confirmation'  && (req.put?) && client_ip(req)
    end
  end

  # Setting password for a talent, triggering e-mail confirmation (from first CV in a registration via job-ad)
  Rack::Attack.track("talent_set_password", :limit => @@limits[:talent_set_password][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/talent/talent_auth/password' && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_set_password') do |req|
    Rack::Attack::Allow2Ban.filter("talent_set_password-#{client_ip(req)}", :maxretry => @@limits[:talent_set_password][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/talent/talent_auth/password'  && (req.put?) && client_ip(req)
    end
  end


  # Finishing a jobapp
  Rack::Attack.track("talent_finish_jobapp", :limit => @@limits[:talent_finish_jobapp][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path.starts_with?('/api/talent/jobapps/') && req.path.ends_with?('/finish') && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_finish_jobapp') do |req|
    Rack::Attack::Allow2Ban.filter("talent_finish_jobapp-#{client_ip(req)}", :maxretry => @@limits[:talent_finish_jobapp][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path.starts_with?('/api/talent/jobapps/') && req.path.ends_with?('/finish') && (req.put?) && client_ip(req)
    end
  end

  # Finishing a cv sharing response
  Rack::Attack.track("talent_finish_sharing_response", :limit => @@limits[:talent_finish_sharing_response][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path.starts_with?('/api/talent/cv_sharing_responses/') && req.path.ends_with?('/finish') && (req.put?) && client_ip(req)
  end

  Rack::Attack.blacklist('talent_finish_sharing_response') do |req|
    Rack::Attack::Allow2Ban.filter("talent_finish_sharing_response-#{client_ip(req)}", :maxretry => @@limits[:talent_finish_sharing_response][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path.starts_with?('/api/talent/cv_sharing_responses/') && req.path.ends_with?('/finish') && (req.put?) && client_ip(req)
    end
  end

=end

  ### Login request with specific credentials (admin)
  #Rack::Attack.track("admin_sign_in", :limit => @@limits[:admin_sign_in][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/oauth-mgmt/sign_in' && req.post?  && client_ip(req)
  #end

  Rack::Attack.blacklist('admin_sign_in') do |req|
    Rack::Attack::Allow2Ban.filter("admin_sign_in-#{client_ip(req)}", :maxretry => @@limits[:admin_sign_in][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/oauth-mgmt/sign_in'  && req.post? && client_ip(req)
    end
  end


  ### Requesting an unlock confirmation email for a specific e-mail address (admin)
  #Rack::Attack.track("admin_resend_unlock_instructions", :limit => @@limits[:admin_resend_unlock_instructions][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/oauth-mgmt/unlock' && req.post?  && client_ip(req)
  #end

  Rack::Attack.blacklist('admin_resend_unlock_instructions') do |req|
    Rack::Attack::Allow2Ban.filter("admin_resend_unlock_instructions-#{client_ip(req)}", :maxretry => @@limits[:admin_resend_unlock_instructions][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/oauth-mgmt/unlock'  && req.post? && client_ip(req)
    end
  end

  Rack::Attack.blacklist('admin_resend_unlock_instructions_per_email') do |req|
    Rack::Attack::Allow2Ban.filter("admin_resend_unlock_instructions_per_email-#{client_ip(req)}-#{self.email(req)}", :maxretry => @@limits[:admin_resend_unlock_instructions_per_email][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/oauth-mgmt/unlock'  && req.post? && client_ip(req) && self.email(req)
    end
  end



  ### Login request with specific credentials (recruiter)
  #Rack::Attack.track("recruiter_sign_in", :limit => @@limits[:recruiter_sign_in][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/recruiter_auths/sign_in' && req.post?  && client_ip(req)
  #end

  Rack::Attack.blacklist('recruiter_sign_in') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_sign_in-#{client_ip(req)}", :maxretry => @@limits[:recruiter_sign_in][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/recruiter_auths/sign_in'  && req.post? && client_ip(req)
    end
  end

  ### Requesting an unlock confirmation email for a specific e-mail address (recruiter)
  #Rack::Attack.track("recruiter_resend_unlock_instructions", :limit => @@limits[:recruiter_resend_unlock_instructions][:track], :period =>  @@limits[:time_period].minutes) do |req|
  #  req.path == '/recruiter_auths/unlock' && req.post?  && client_ip(req)
  #end

  Rack::Attack.blacklist('recruiter_resend_unlock_instructions') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_resend_unlock_instructions-#{client_ip(req)}", :maxretry => @@limits[:recruiter_resend_unlock_instructions][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/recruiter_auths/unlock'  && req.post? && client_ip(req)
    end
  end

  Rack::Attack.blacklist('recruiter_resend_unlock_instructions_per_mail') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_resend_unlock_instructions_per_mail-#{client_ip(req)}-#{self.email(req)}", :maxretry => @@limits[:recruiter_resend_unlock_instructions_per_mail][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/recruiter_auths/unlock'  && req.post? && client_ip(req) && self.email(req)
    end
  end



  # Mandrill webhook for inbound mails to *@notify.myveeta.com
  Rack::Attack.track("webhook_inbound", :limit => @@limits[:webhook_inbound][:track], :period => @@limits[:time_period].minutes) do |req|
    req.path == '/webhooks/inbound/reply'
  end

  Rack::Attack.blacklist('webhook_inbound') do |req|
    Rack::Attack::Allow2Ban.filter("webhook_inbound-#{client_ip(req)}", :maxretry => @@limits[:webhook_inbound][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/webhooks/inbound/reply' && client_ip(req)
    end
  end


  # Mandrill webhook for bounced mails to *@notify.myveeta.com
  Rack::Attack.track("webhook_bounce", :limit => @@limits[:webhook_bounce][:track], :period => @@limits[:time_period].minutes) do |req|
    req.path == '/webhooks/inbound/reply'
  end

  Rack::Attack.blacklist('webhook_bounce') do |req|
    Rack::Attack::Allow2Ban.filter("webhook_bounce-#{client_ip(req)}", :maxretry => @@limits[:webhook_bounce][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/webhooks/inbound/bounce' && client_ip(req)
    end
  end

  def self.client_ip request
    ip = ""
    if request.env['HTTP_X_FORWARDED_FOR']
      ips = request.env['HTTP_X_FORWARDED_FOR'].split(",")
      if ips
        ip = ips[0].strip
      end
    end
    if ip.blank?
      ip = request.ip
    end
    ip
  end

  def self.email req
    if req.env['action_dispatch.request.request_parameters']
      req.env['action_dispatch.request.request_parameters']['email']
    elsif req.params['email']
      req.params['email']
    else
      nil
    end
  end

end
