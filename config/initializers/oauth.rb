OAuth::Server.class_eval do |variable|
  @@server_paths = {
    :request_token_path => "/v1/request_token",
    :authorize_path     => "/v1/authorize",
    :access_token_path  => "/v1/access_token"
  }
end
