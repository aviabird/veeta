if defined?(XingApi)
  XingApi::Client.configure do |config|
    config.consumer_key = ENV["XING_CONSUMER_KEY"]
    config.consumer_secret = ENV["XING_CONSUMER_SECRET"]
  end
else
  Rails.logger.warn("XingApi not initialized!")
end
