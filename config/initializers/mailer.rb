# Configure API key for mandrill
MandrillMailer.configure do |config|
  config.api_key = ENV['VEETA_MANDRILL_API_KEY']
end