RailsAdmin.config do |config|

  #secure vadmin subpath with basic http auth
  config.authorize_with do
    authenticate_or_request_with_http_basic('Access to this page is restricted') do |username, password|
      # For handling the session timeout after activity
      last_access = session[:last_access] || Time.now.utc.to_i
      if Time.now.utc.to_i - last_access > 60.minutes.to_i
        # Logout the user with sesion last access time set to nil
        session[:last_access] = nil
        false
      else
        # Setting last request time
        session[:last_access] = Time.now.utc.to_i
        username == ENV['ADMIN_USERNAME'] && password == ENV['ADMIN_PASSWORD']
      end
    end
  end

  config.main_app_name = ['Veeta']

  # config.included_models = ['Talent', 'Resume', 'Recruiter']

  # If you want to track changes on your models:
  # config.audit_with :history, User

  # config.actions do
  #   # root actions
  #   dashboard                     # mandatory
  #   # collection actions
  #   index                         # mandatory
  #   new
  #   export
  #   history_index
  #   bulk_delete
  #   # member actions
  #   show
  #   edit
  #   delete
  #   history_show
  #   show_in_app
  # end

  #  ==> Global show view settings
  # Display empty fields in show views
  # config.compact_show_view = false

  #  ==> Global list view settings
  # Number of default rows per-page:
  # config.default_items_per_page = 20

  #  ==> Included models
  # Add all excluded models here:
  config.excluded_models = ['AcceptedJobsLanguage']

  # Add models here if you want to go 'whitelist mode':
  #config.included_models = [Comment, Role, Story, User, Vote]

  # Application wide tried label methods for models' instances
  # config.label_methods << :description # Default is [:name, :title]

  #  ==> Global models configuration
  # config.models do
  #   # Configuration here will affect all included models in all scopes, handle with care!
  #
  #   list do
  #     # Configuration here will affect all included models in list sections (same for show, export, edit, update, create)
  #
  #     fields_of_type :date do
  #       # Configuration here will affect all date fields, in the list section, for all included models. See README for a comprehensive type list.
  #     end
  #   end
  # end
  #
  #  ==> Model specific configuration
  # Keep in mind that *all* configuration blocks are optional.
  # RailsAdmin will try his best to provide the best defaults for each section, for each field.
  # Try to override as few things as possible, in the most generic way. Try to avoid setting labels for models and attributes, use ActiveRecord I18n API instead.
  # Less code is better code!
  # config.model MyModel do
  #   # Cross-section field configuration
  #   object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #   label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #   label_plural 'My models'      # Same, plural
  #   weight -1                     # Navigation priority. Bigger is higher.
  #   parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #   navigation_label              # Sets dropdown entry's name in navigation. Only for parents!
  #   # Section specific configuration:
  #   list do
  #     filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #     items_per_page 100    # Override default_items_per_page
  #     sort_by :id           # Sort column (default is primary key)
  #     sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     # Here goes the fields configuration for the list view
  #   end
  # end

  [Job, Jobapp, Rs::Education, Rs::WorkExperience, Resume, Talent].each do |model_name|
    config.model model_name do
      exclude_fields :language
      exclude_fields :country
      exclude_fields :nationality_country
    end
  end

  # Your model's configuration, to help you get started:

  # All fields marked as 'hidden' won't be shown anywhere in the rails_admin unless you mark them as visible. (visible(true))

  # config.model Comment do
  #   # Found associations:
  #     configure :commentable, :polymorphic_association
  #     configure :user, :belongs_to_association
  #     configure :parent, :belongs_to_association
  #     configure :children, :has_many_association
  #     configure :votes, :has_many_association         # Hidden   #   # Found columns:
  #     configure :id, :integer
  #     configure :commentable_id, :integer         # Hidden
  #     configure :commentable_type, :string         # Hidden
  #     configure :title, :string
  #     configure :body, :text
  #     configure :subject, :string
  #     configure :user_id, :integer         # Hidden
  #     configure :parent_id, :integer         # Hidden
  #     configure :lft, :integer
  #     configure :rgt, :integer
  #     configure :created_at, :datetime
  #     configure :updated_at, :datetime
  #     configure :story_id, :integer
  #     configure :depth, :integer   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Role do
  #   # Found associations:
  #     configure :users, :has_and_belongs_to_many_association   #   # Found columns:
  #     configure :id, :integer
  #     configure :name, :string
  #     configure :description, :string
  #     configure :created_at, :datetime
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Story do
  #   # Found associations:
  #     configure :user, :belongs_to_association
  #     configure :comment_threads, :has_many_association
  #     configure :votes, :has_many_association         # Hidden
  #     configure :vote, :has_one_association
  #     configure :comments, :has_many_association   #   # Found columns:
  #     configure :id, :integer
  #     configure :link, :text
  #     configure :description, :string
  #     configure :created_at, :datetime
  #     configure :updated_at, :datetime
  #     configure :user_id, :integer         # Hidden
  #     configure :slug, :string   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model User do
  #   include_all_fields
  #   exclude_fields :vote
  # end
  # config.model Vote do
  #   # Found associations:
  #     configure :user, :has_one_association
  #     configure :story, :has_one_association   #   # Found columns:
  #     configure :id, :integer
  #     configure :votable_id, :integer
  #     configure :votable_type, :string
  #     configure :voter_id, :integer
  #     configure :voter_type, :string
  #     configure :vote_flag, :boolean
  #     configure :created_at, :datetime
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end



end if defined?(Vadmin)
