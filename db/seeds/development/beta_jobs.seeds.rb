
company_name = "Stark Industries"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Lisa'
  auth.last_name = 'Lustig'
  auth.email = "stark@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667836633'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'stark_logo.png'))
  company.blocking_notice = false
  company.tnc_link = "https://www.wko.at/Content.Node/Service/Wirtschaftsrecht-und-Gewerberecht/Allgemeines-Zivil--und-Vertragsrecht/AGB-Datenbank/-AGB-Strukutur-/Branchenneutrale-Informationen/Allgemeine-Geschaeftsbedingungen.pdf"
  company.save!

  puts "Importing job for #{company_name}"
  job = Job.new
  job.name = 'Java Web Developer'
  job.company = company
  job.status = 'new'
  job.recruiter = company.owner
  job.language = Language.find('en')
  job.accepted_jobs_languages.build(language: Language.find('en'))
  job.accepted_jobs_languages.build(language: Language.find('de'))
  job.save!
end


company_name = "ACME"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Max'
  auth.last_name = 'Muster'
  auth.email = "acme@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667836633'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'acme_logo.png'))
  company.blocking_notice = false
  company.tnc_text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
  company.save!

  puts "Importing job for #{company_name}"
  job = Job.new
  job.name = 'Software Tester/in'
  job.company = company
  job.status = 'new'
  job.recruiter = company.owner
  job.language = Language.find('de')
  job.accepted_jobs_languages.build(language: Language.find('en'))
  job.accepted_jobs_languages.build(language: Language.find('de'))
  job.save!
end
