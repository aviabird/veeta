after 'development:talents', 'development:recruiters' do


  p 'creating veeta button admin connect@t13s.at...'

  auth = AdminAuth.find_or_create_by(email: "connect@t13s.at") do |auth|
    auth.admin_id = "9c8b97e8-cd95-48e0-bbf4-1fd01219bb45"
    auth.password = 'helloV33ta'
  end

  p 'creating clients applications for veeta button...'

  selected_logo = []
  loops = Rails.env.development? ? rand(5..10) : rand(2..4)
  loops.times do |i|
    name = Forgery::Name.company_name
    unless ClientApplication.find_by(name: "Company ##{i}")
      p 'creating oauth consumer...'
      url = "http://#{Forgery::Internet.domain_name}"
      client_app = ClientApplication.new
      client_app.name = Forgery::Name.company_name
      client_app.url = url
      client_app.key = OAuth::Helper.generate_key(40)[0,40]
      client_app.secret = OAuth::Helper.generate_key(40)[0,40]
      client_app.created_at = DateTime.now
      client_app.updated_at = DateTime.now
      client_app.webhook_url = "#{url}/update_webhook"
      client_app.delete_user_profile_deep_link =  "#{url}/my_profile"
      client_app.cancel_url = "#{url}/canceled"
      client_app.use_cancel_callback = [true, true, false].sample
      client_app.contact = "#{Forgery::Name.last_name}@t13s.at"
      filename = (Dir.entries(File.join(Rails.root, 'test', 'data', 'veetabutton')) - ['.', '..','.DS_Store']).sample

      #prevent duplicate pictures:
#      while selected_logo.include? filename do
#          filename = (Dir.entries(File.join(Rails.root, 'test', 'data', 'veetabutton')) - ['.', '..','.DS_Store']).sample
#      end

      selected_logo << filename
      client_app.logo = File.open(File.join(Rails.root, 'test', 'data', 'veetabutton', filename))
      client_app.save!

      # iterate over talent auths
      selected = []
      TalentAuth.all.sample(TalentAuth.all.count/4).each do |ta|
        p "connecting talent #{ta.email} to #{client_app.name}"
        unless selected.include? ta.id
          p "connecting talent #{ta.email} to #{client_app.name}"
          selected << ta.id
          token = OauthToken.new
          token.user_id=ta.id
          token.type = 'AccessToken'
          token.client_application_id = client_app.id
          token.token = OAuth::Helper.generate_key(40)[0,40]
          token.secret = OAuth::Helper.generate_key(40)[0,40]
          token.authorized_at = DateTime.now
          token.created_at = DateTime.now
          token.updated_at = DateTime.now
          token.save!

          vbsc = VeetaButtonSharingConnection.new
          vbsc.talent_id = ta.talent.id
          vbsc.client_application_id = client_app.id
          vbsc.oauth_token_id=token.id
          vbsc.connected_at = rand(1.years).seconds.ago
          vbsc.created_at = DateTime.now
          vbsc.updated_at = DateTime.now
          vbsc.access_revoked_at = Time.at((DateTime.now.to_f - vbsc.connected_at.to_f)*rand + vbsc.connected_at.to_f) if [true, false, false].sample
          vbsc.third_party_data_wiped_out_at = vbsc.access_revoked_at
          vbsc.save!


          vbs = VeetaButtonSharing.new
          res = ta.talent.resumes.where(origin_id: nil).sample
          if res && res.latest.nil?
            CloningService::ResumeClone.call(res)
          end
          if res
            p "talent #{ta.email} shares resume #{res.name} with #{client_app.name}"
            vbs.resume = res.latest
          end
          vbs.veeta_button_sharing_connection = vbsc
          to_date = vbsc.access_revoked_at.blank? ? DateTime.now.to_f : vbsc.access_revoked_at.to_f
          vbs.last_synced_at = Time.at((to_date - vbsc.connected_at.to_f)*rand + vbsc.connected_at.to_f) if [true,false].sample
          vbs.save!

          if vbsc.access_revoked_at && [true,false].sample
            p "access revoked, new connection for talent #{ta.email} to #{client_app.name}"
            vbsc.third_party_data_wiped_out_at = Time.at((DateTime.now.to_f - vbsc.access_revoked_at.to_f)*rand + vbsc.access_revoked_at.to_f)
            vbsc.save!
            vbsc_follow_up = VeetaButtonSharingConnection.new
            vbsc_follow_up.talent_id = ta.talent.id
            vbsc_follow_up.client_application_id = client_app.id
            vbsc_follow_up.oauth_token_id = token.id
            vbsc_follow_up.connected_at = Time.at((DateTime.now.to_f - vbsc.access_revoked_at.to_f)*rand + vbsc.access_revoked_at.to_f)
            vbsc_follow_up.created_at = DateTime.now
            vbsc_follow_up.updated_at = DateTime.now
            vbsc_follow_up.save!
            vbs_follow_up = VeetaButtonSharing.new
            res = ta.talent.resumes.where(origin_id: nil).sample
            if res && res.latest.nil?
              CloningService::ResumeClone.call(res)
            end
            if res
              vbs_follow_up.resume = res.latest
            end
            vbs_follow_up.veeta_button_sharing_connection = vbsc_follow_up
            vbs_follow_up.last_synced_at = Time.at((DateTime.now.to_f - vbsc_follow_up.connected_at.to_f)*rand + vbsc_follow_up.connected_at.to_f) if [true,false].sample
            vbs_follow_up.save!
          end

        end


      end

    end
  end
end
