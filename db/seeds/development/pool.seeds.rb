
# To create pools use the following command
# $ rake db:seed:development:pool
# after 'development:talents', 'development:recruiters'

# this will create recruiter company and jobs
system "env PROCESS_TYPE=web RAILS_ENV=#{Rails.env} bundle exec rake db:seed:development:recruiters recruiters=#{ENV['recruiters']} jobs=#{ENV['jobs']}"

# this will create talents
system "env PROCESS_TYPE=web RAILS_ENV=#{Rails.env} bundle exec rake db:seed:development:talents talents=#{ENV['talents']}"

filepath = Rails.root+ 'db/seeds/development/seed_data.txt'
file = File.open(filepath, "a+")
hash = file.read
hash = eval(hash)
talent_auth_ids = hash[:talent_auths]
company_ids = hash[:company]
file.truncate(0)
file.close()


EN_LANG = Language.find('en')
p 'Importing job applications'
TalentAuth.where(id: talent_auth_ids).each do |talent_auth|
  talent=talent_auth.talent
  talent.resumes.where(origin_id: nil).each do |res|
    if res && res.latest.nil?
      CloningService::ResumeClone.call(res)
      res.latest.pdf_template_setting.trigger_pdf_generation_sync
    end
    resume = res.latest
    loops = Rails.env.development? ? rand(1..10) : rand(2..3)
    Chewy.strategy(:atomic) do
      loops.times do
        job = Job.where(company_id: company_ids).sample
        loop do
          if talent.jobapps.find_by(job: job)
            job = Job.where(company_id: company_ids).sample
          else
            break
          end
        end
        unless talent.jobapps.find_by(job: job)
          job.accepted_jobs_languages.build(language: EN_LANG)
          job.save!
          #resume.update!(language: EN_LANG)
          source = Jobapp.source.values.sample
          source_details = ["company_website", "personal_recommendation"].include?(source) ? nil : Forgery::LoremIpsum.sentences
          fake_date = rand(2.years - 2.days).seconds.ago
          withdrawn_at = [false, false, false, false,true].sample ? rand(2.years - 2.days).seconds.ago : nil
          withdrawn_reason = withdrawn_at.nil? ? nil : Forgery::LoremIpsum.sentences
          jobapp = Jobapp.create!(
              job: job,
              resume: resume,
              language: EN_LANG,
              draft: false,
              job_type: 'Job',
              cover_letter: Forgery::LoremIpsum.sentences,
              recruiter_rating: rand(1..3),
              recruiter_note: 'He is really good',
              talent_info_status: 'review',
              added_by_recruiter: false,
              read_at: Time.now,
              application_referrer_url: 'http://karriere.at/job',
              allow_recruiter_contact_sharing: true,
              company_terms_accepted_at: Time.now,
              company_terms_accepted_ip: '10.0.0.1',
              withdrawn_at: withdrawn_at,
              withdrawn_reason: withdrawn_reason,
              source: source,
              source_details: source_details,
              submitted_at: fake_date,
              created_at: fake_date,
              updated_at: fake_date
          )
          p "Job application for job #{jobapp.job.name} with resume #{jobapp.resume.name} was created"
          res.share(company_ids: [job.company_id], real_company: "REAL")
        end
      end
    end
    loops = Rails.env.development? ? rand(1..3) : rand(1..2)
    Chewy.strategy(:atomic) do
      loops.times do

        ea = EmailApplication.new
        ea.jobname =  Forgery::Name.job_title
        ea.recruiter_email = "#{Forgery(:internet).user_name}@t13s.at"
        ea.company_name = Forgery::Name.company_name
        ea.save!

        puts "EmailApplication created: applied as #{ea.jobname} at #{ea.company_name} sent to #{ea.recruiter_email} "

        source = Jobapp.source.values.sample
        source_details = ["company_website", "personal_recommendation"].include?(source) ? nil : Forgery::LoremIpsum.sentences
        fake_date = rand(2.years - 2.days).seconds.ago
        withdrawn_at = [false, false, false, false,true].sample ? rand(2.years - 2.days).seconds.ago : nil
        withdrawn_reason = withdrawn_at.nil? ? nil : Forgery::LoremIpsum.sentences
        jobapp = Jobapp.create!(
            job: ea,
            resume: resume,
            language: EN_LANG,
            draft: false,
            job_type: 'EmailApplication',
            cover_letter: Forgery::LoremIpsum.sentences,
            recruiter_rating: rand(1..3),
            recruiter_note: 'He is really good',
            talent_info_status: 'review',
            added_by_recruiter: false,
            read_at: Time.now,
            application_referrer_url: 'http://karriere.at/job',
            allow_recruiter_contact_sharing: true,
            company_terms_accepted_at: Time.now,
            company_terms_accepted_ip: '10.0.0.1',
            withdrawn_at: withdrawn_at,
            withdrawn_reason: withdrawn_reason,
            source: source,
            source_details: source_details,
            submitted_at: fake_date,
            created_at: fake_date,
            updated_at: fake_date
        )
        res.share(company_ids: [ea.id], real_company: "EMAIL")

      end
    end

    loops = Rails.env.development? ? rand(1..3) : rand(1..2)
    Chewy.strategy(:atomic) do
      loops.times do
        cv_sharing_request = CvSharingRequest.where(company_id: company_ids).sample
        ids = Talent.first.resumes.pluck(:id)
        fake_date = rand(2.years - 2.days).seconds.ago
        request_responded = CvSharingResponse.where(resume_id: ids).pluck(:cv_sharing_request_id)
        loop do
          if request_responded.include?(cv_sharing_request)
            cv_sharing_request = CvSharingRequest.where(company_id: company_ids).sample
          else
            break
          end
        end
        cv_sharing_response = CvSharingResponse.new
        cv_sharing_response.resume = resume
        cv_sharing_response.cv_sharing_request = cv_sharing_request
        cv_sharing_response. cv_sharing_request_type= "CvSharingRequest"
        cv_sharing_response.blocked_companies =  [Forgery::Name.company_name]
        cv_sharing_response.read_at = Time.now
        cv_sharing_response.draft = false
        cv_sharing_response.application_referrer_url = 'http://karriere.at/job'
        cv_sharing_response.allow_recruiter_contact_sharing = true
        cv_sharing_response.company_terms_accepted_at = Time.now
        cv_sharing_response.company_terms_accepted_ip = '10.0.0.1'
        cv_sharing_response.submitted_at = fake_date
        cv_sharing_response.save!
        p "cv sharing response for cv sharing request #{cv_sharing_response.cv_sharing_request} with resume #{cv_sharing_response.resume.name} was created"
        res.share(company_ids: [cv_sharing_request.company_id], real_company: "REAL")
      end
    end


    Chewy.strategy(:atomic) do
      rand(1..2).times do |i|
        p "Creating #{i} ResumeSharingRequests for #{ resume.name }"
        contact_name = Forgery::Name.full_name
        fake_date = rand(2.years - 2.months).seconds.ago
        resume.sharing_requests.create!(created_at: fake_date,
                                        updated_at: fake_date,
                                        email: "#{Forgery(:internet).user_name}@t13s.at",
                                        contact_name: contact_name)
      end
    end
  end
end


count=0 # to store number of updated ResumeSharing
ResumeSharing.where("created_at > ?", 3.minutes.ago).each do |rs|
  fake_date = rand(2.years - 2.days).seconds.ago
  count+=1
  rs.update_columns(created_at: fake_date,
                    updated_at: fake_date)
  p "Updated RS##{ rs.id } fake_date to #{ fake_date }"
end

p "Updating created_at and updated_at of #{count} ResumeSharings"

# Pools
Company.where(id: company_ids).each do |company|
  if company.pools.empty?
    loops = Rails.env.development? ? rand(2..9) : rand(1..2)
    loops.times do
      Pool.create!(company: company, name: Forgery(:name).industry)
    end
  end

  company.pools.each do |pool|
    if pool.talents.empty?
      loops = Rails.env.development? ? rand(2..15) : rand(1..3)
      talent_ids =TalentAuth.where(id: talent_auth_ids).pluck(:talent_id)
      loops.times do
        pool.talents << Talent.where(id: talent_ids).order("RANDOM()").first
      end
      pool.save!
    end
  end

  p "#{company.pools.count} pool be created for company #{company.name}"
end

# Tags
# puts 'Importing tags'
# Recruiter.all.each do |recruiter|
#   print '.'
#   Talent.all.each do |talent|
#     print '.'
#     if talent.labels_from(recruiter).empty?
#       print '.'
#       labels = []
#       rand(0..2).times { labels << Forgery(:name).location }
#       recruiter.tag(talent, with: labels.join(','), on: :labels)
#     end
#   end
# end
# puts

# Talent ratings
# puts 'Rating talents'
# Company.all.each do |company|
#   print '.'
#   Talent.all.each do |talent|
#     print '.'
#     info = talent.company_info(company)
#     if info
#       info.rating = rand(1..5)
#       info.note = Forgery(:lorem_ipsum).words(10)
#       info.save!
#     end
#   end
# end
# puts

# Talent ratings
puts 'Rating talents'
Chewy.strategy(:atomic) do
  Company.where(id: company_ids).each do |company|
    print '.'
    puts "company rating talent is #{company.id}"
    TalentAuth.where(id: talent_auth_ids).each do |talent_auth|
      talent= talent_auth.talent
      puts "Talent getting rating is #{talent.first_name}"
      print '.'
      resumes = talent.resumes
      origin = nil
      resumes.each do |resume|
        origin = resume.origin
      end
      talent = Talent.find_by_id origin.talent_id if origin
      if talent.company_infos.where(company_id: company.id).present?
        print '.'
        info = talent.company_infos.where(company_id: company.id).first
        if info
          info.rating = rand(1..5)
          info.save!
          puts "Talent get rating with #{info.rating} by company #{company.id}"
        end
      end
    end
  end
end
puts

# end
