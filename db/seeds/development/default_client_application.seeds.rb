
p 'creating veeta button admin connect@t13s.at...'

auth = AdminAuth.find_or_create_by(email: "connect@t13s.at") do |auth|
  auth.admin_id = 0
  auth.password = 'helloV33ta'
end

p 'creating default client application "Contoso Career Portal" for veeta button...'

unless ClientApplication.find_by(name: "Contoso Career Portal")
  p 'creating oauth consumer...'
  url = "https://www.myveeta.com"
  client_app = ClientApplication.new
  client_app.name = "Contoso Career Portal"
  client_app.url = url
  client_app.created_at = DateTime.now
  client_app.updated_at = DateTime.now
  client_app.webhook_url = "#{url}/update_webhook"
  client_app.delete_user_profile_deep_link =  "#{url}/my_profile"
  client_app.cancel_url = "#{url}/canceled"
  client_app.use_cancel_callback = false
  client_app.contact = "contoso@t13s.at"
  filename = 'contoso.png'
  #prevent duplicate pictures:
#      while selected_logo.include? filename do
#          filename = (Dir.entries(File.join(Rails.root, 'test', 'data', 'veetabutton')) - ['.', '..','.DS_Store']).sample
#      end
  client_app.logo = File.open(File.join(Rails.root, 'test', 'data', 'veetabutton', filename))
  client_app.save!
  client_app.update_columns(key: "Y86J39f335REpYf9Fc80aPeXn9m95OIndgg2gBh5", secret: "eqxGmHTBO1PRpwTkU2t9n5eW6HXtxsoKesE7NSO9")
end
