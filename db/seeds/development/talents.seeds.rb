# IS_DEV_MODE=true
# To create 3000 users use the following command
# $ rake db:seed:development:talents talents=3000

talent_count_requested = nil
p "Talents passed are #{ENV['talents']}"
if ENV['talents']
  talent_count_requested = ENV['talents'].to_i
end
# after :production do

  ActiveRecord::Base.connection.tables.each { |t| ActiveRecord::Base.connection.reset_pk_sequence!(t) }
  EN_LANG = Language.find('en')

  austria = Country.find('AT')
  #Talent.destroy_all

  if Rails.env.development?
    number_of_talents =  talent_count_requested.present? ? talent_count_requested : rand(10..25)
  else
    number_of_talents =  talent_count_requested.present? ? talent_count_requested : 8
  end
  p "talents going to be created ", number_of_talents
  p 'Importing talents with auth'
  i=0
  number_of_talents.times do |c|
  # 1.times do |i|
    i = i+1
    loop do
      if TalentAuth.find_by(email: "t-#{i}@t13s.at").present?
        i = i+1
      else
        break
      end
    end
    unless TalentAuth.find_by(email: "t-#{i}@t13s.at")
      auth = TalentAuth.find_or_create_by(email: "t-#{i}@t13s.at") do |auth|
        auth.resume_type = 1
        auth.country_id = austria.id
        auth.password = 'tel$#@12' # if i < (number_of_talents / 2)
        # auth.password = '5GLsAByrDBMRS959' if i >= (number_of_talents / 2)
        auth.confirmed_at = DateTime.now
        auth.first_name = Forgery::Name.first_name
        auth.last_name = Forgery::Name.last_name
      end
      p "Talent with email #{auth.email} created."
      $new_created_talent_ids << auth.id
      talent = auth.talent

      address = Geo::Address.where(address_line: Forgery::Address.street_address).first_or_create! do |address|
        address.country_id = austria.id
        address.zip = Forgery::Address.zip
        address.city = Forgery::Address.city
      end


      talent.update!(
          nationality_country: austria,
          title: Forgery::Name.academic_title('de'),
          salutation: %w(mr mrs other).sample,
          birthdate: Date.today - rand(18..30).years,
          address: address,
          phone_number: '+436601234123',
          avatar: File.open(File.join(Rails.root, 'test', 'data', 'talents', "avatar-#{i%15+1}.png")),
          military_service: %w(served not_served nil)
      )

      working_locations_array = [Forgery::Location.city('de'),Forgery::Location.city('de'),Forgery::Location.city('de'),Forgery::Location.city('de')]
      talent.setting.availability = %w(immediately 1m 2m 3m 4-6m 6+m).sample
      talent.setting.prefered_employment_type = [:full_time, :part_time, :self_employed].sample
      talent.setting.job_seeker_status = [:very_much, :not_searching, :do_not_contact].sample
      talent.setting.working_industries = [Forgery::Name.job_title, Forgery::Name.job_title, Forgery::Name.job_title, Forgery::Name.job_title]
      talent.setting.working_locations = working_locations_array

      # dummy
      talent.setting.salary_exp_min = [0,4,10,20,30,40,50,70,90,110,130,150].sample * 1000
      if [10000,20000,30000,40000].include?(talent.setting.salary_exp_min)
        talent.setting.salary_exp_max = talent.setting.salary_exp_min + 10000
      elsif [50000,70000,90000,110000,130000].include?(talent.setting.salary_exp_min)
        talent.setting.salary_exp_max = talent.setting.salary_exp_min + 20000
      elsif talent.setting.salary_exp_min == 150000
        talent.setting.salary_exp_max = 0
      elsif talent.setting.salary_exp_min == 0
        talent.setting.salary_exp_max = 4000
      elsif talent.setting.salary_exp_min == 4000
        talent.setting.salary_exp_max = 10000
      end

      talent.setting.save!

      if talent.resumes.empty?
        rand(0..3).times do |j|
          talent.setting.availability = %w(immediately 1m 2m 3m 4-6m 6+m).sample
          talent.setting.prefered_employment_type = [:full_time, :part_time, :self_employed].sample
          talent.setting.job_seeker_status = [:very_much, :not_searching, :do_not_contact].sample
          talent.setting.save!
          fake_date = rand(2.years - 6.months).seconds.ago
          p 'Importing resumes'
          resume = Resume.create!(
              talent: talent,
              name: "My Resume #{j}",
              language: EN_LANG,
              created_at: fake_date,
              updated_at: fake_date + rand(100).days,
              # dummy
              work_experience_duration: rand(1.0..20.0),
          )
          pdf_setting = PdfTemplateSetting.default resume
          pdf_setting.save!


          p "Resume for user #{resume.talent.email} with name #{resume.name} was created."

          p "Setting about for #{resume.name}"
          Rs::About.create!(text: Forgery::LoremIpsum.words, resume:resume)

          p "Setting links for #{resume.name}"
          Rs::Link.create!(linkedin: "https://linkedin.com/in/#{Forgery(:internet).user_name}", xing: "https://www.xing.com/profile/#{Forgery(:internet).user_name}", website:Forgery(:internet).domain_name, resume:resume)

          p 'Importing work experience for resume'

          rand(0..5).times do
            random = rand(2..120)

            from = random.months.ago
            to = (random / 2).months.ago

            Rs::WorkExperience.create!(
                resume: resume,
                company: Forgery::Name.company_name,
                position: Forgery::Name.job_title,
                industry: %w(AgriFish Arts AuditTaxLaw Automotive Construction Consulting Craft EduScience Electronics EnergyWater Engineering Entertainment Finance FoodTourism HR ITSvcSoftware Luxury Manufacturing MarketingDesign Media Medical MiningFuel Public RealEstate Security Social Tech Telco Trade Transport Other).sample,
                from: from,
                to: to,
                job_level: %w(intern starter experienced executive).sample,
                country: austria,
                description: Forgery::LoremIpsum.words,
                terms_of_employment: [:full_time, :part_time, :self_employed].sample
            )
          end

          p 'Importing education for resume'
          rand(0..5).times do
            random = rand(2..120)

            from = random.months.ago
            to = (random / 2).months.ago

            Rs::Education.create!(
                resume: resume,
                school_name: "School of #{Forgery::Address.city}",
                country: austria,
                subject: "#{Forgery::Name.job_title} at #{Forgery::Name.location}",
                type_of_education: %w(highschool university intermediate vocational college other).sample,
                from: from,
                to: to
            )

          end

          p 'Importing Skills for resume'
          rand(1 .. 3).times do
            Rs::Skill.create!(
                resume: resume,
                name: Forgery::Personal.skill('de'),
                level: rand(1..4)
            )
          end

          p 'Importing Certifications for resume'
          rand(2 .. 5).times do
            Rs::Certification.create!(
                resume: resume,
                certification_type: [:certification, :continued_education].sample,
                subject: Forgery::Personal.skill('de'),
                organization: Forgery::Name.company_name,
                description: Forgery::LoremIpsum.words,
                year: rand(1999..2015)
            )
          end

          p 'Importing Languages for resume'
          2.times do
            Rs::Language.create!(
                resume: resume,
                language: Language.all.sample,
                level: rand(1..5)
            )
          end
          p 'attaching documents to resume'
          dir = Dir.entries(File.join(Rails.root, 'test', 'data', 'resume', 'documents')).reject {|f| File.directory?(f) || f[0].include?('.')}
          rand(0..2).times do
            filename = dir.sample
            Rs::Document.create!(
              resume: resume,
              document_type: %w(certificate reference recommendation).sample,
              file: File.open(File.join(Rails.root, 'test', 'data', 'resume', 'documents' "/#{filename}")),
              file_type: File.extname(filename).split('.')[1],
              name: filename
            )
          end
        end
      end
    end
  end
# end

filepath = Rails.root+ 'db/seeds/development/seed_data.txt'
file = File.open(filepath, "a+")
hash = file.read
hash = eval(hash)
hash.merge!({talent_auths: $new_created_talent_ids})
content = hash.inspect
file.truncate(0)
file.write(content)
file.close()
