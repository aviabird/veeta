$stdout.sync = true

# IS_DEV_MODE = true
# USAGE
# $ rake db:seed:development:recruiters recruiters=300 jobs=1000

recruiters_count = nil
p "Recruiters passed are #{ENV['recruiters']}"
if ENV['recruiters']
  recruiters_count = ENV['recruiters'].to_i
end


p "Creating #{recruiters_count} recruiters"

# after :production do
ActiveRecord::Base.connection.tables.each { |t| ActiveRecord::Base.connection.reset_pk_sequence!(t) }

if Rails.env.development?
  number_of_companies = recruiters_count.present? ? recruiters_count : rand(10..25)
else
  number_of_companies = recruiters_count.present? ? recruiters_count : rand(3..5)
end

if ENV['jobs']
  per_company_jobs_count = ENV['jobs'].to_i / number_of_companies
  per_company_jobs_count = 1 if per_company_jobs_count < 1
else
  per_company_jobs_count = rand(1..3)
end
p "Creating #{per_company_jobs_count} jobs per company"

# Remove all existing objects
# For already soft deleted objects

#Recruiter.with_deleted.map { |r| r.really_destroy! }
#Recruiter.all.map { |r| r.really_destroy! }

#RecruiterAuth.all.map{ |r|  r.destroy! }

#Company.all.map {|c| c.jobs }.flatten.map {|j| j.really_destroy! }
#Company.all.map {|c| c.jobs }.flatten.map {|j| j.destroy! }

#Company.with_deleted.map {|c| c.really_destroy! }
#Company.all.map {|c| c.really_destroy! }

def self.find_company_short_name
  p "NEXT COMPANY"
  loop do
    short_name = ("AAA".."ZZZ").to_a.shuffle.sample
    # p "trying again with #{short_name}"
    unless Company.find_by_short_name(short_name)
      # p "Company short name #{short_name} NOT PRESENT breaking"
      break short_name
    else
      # p "Company short name #{short_name} PRESENT"
    end
  end
end

i=0
number_of_companies.times do |c|
  i = i+1
  loop do
    if Recruiter.find_by(email: "c-#{i}@t13s.at").present?
      i = i+1
    else
      break
    end
  end
  company_name = "#{ Forgery::Name.company_name }-#{ i }"
  puts "Company name #{company_name}"
  unless Company.find_by(name: company_name)
    auth = AuthService::CompanySignup.new
    auth.company_name = company_name
    auth.first_name = Forgery::Name.first_name
    auth.last_name = Forgery::Name.last_name
    auth.email = "c-#{i}@t13s.at"
    auth.password = 'rec$#@12'
    auth.password_confirmation = 'rec$#@12'
    auth.plan = %w(basic pro ent).sample
    auth.company_short_name = find_company_short_name
    auth_service = auth.signup!
  end
  company = Company.find_by(name: company_name)
  # company.contact = Forgery::Name.first_name + ' ' + Forgery::Name.last_name + '<br/>' + "c-#{i}@t13s.at" + '<br/>' + '+436601234123'
  filename = ''
  loop do
    filename = Dir.entries(File.join(Rails.root, 'test', 'data', 'company')).reject {|f| File.directory?(f) || f[0].include?('.')}.sample
    puts "Filename: #{ filename }"
    accepted_formats = ['.png', '.gif', '.jpg', '.jpeg']
    break filename if accepted_formats.include?(File.extname(filename))
  end
  # binding.pry
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', "#{filename}"))

  company.save! #rescue binding.pry
  $new_created_company_ids << company.id
  # puts "Importing jobs"
  #if company.jobs.empty?
    auth.ip = '10.0.0.1'
    print 'no jobs present'
    per_company_jobs_count.times do |number|
      print '.'
      job = Job.new
      job.name = Forgery::Name.job_title
      job.company = company
      job.status = 'new'
      job.recruiter = company.owner
      job.language = Language.find('en')
      job.accepted_jobs_languages.build(language: Language.find('en'))
      job.accepted_jobs_languages.build(language: Language.find('de'))
      job.deleted_at = Time.now if number.modulo(3).zero?
      job.save!
    end
  #end
  print 'no Cv sharing request present adding one cv sharing request'
  print '.'
  cv_sharing_request = CvSharingRequest.new
  cv_sharing_request.company = company
  cv_sharing_request.recruiter = company.owner
  cv_sharing_request.language = Language.find('en')
  cv_sharing_request.status = 'new'
  cv_sharing_request.blocking_notice = false
  cv_sharing_request.contact_details = Forgery::Address
  cv_sharing_request.pool_welcome_text = Forgery::LoremIpsum.words
  cv_sharing_request.save!
end
# end

filepath= Rails.root+ 'db/seeds/development/seed_data.txt'
file=File.open(filepath, "a+")
content= {company: $new_created_company_ids}.inspect
file.truncate(0)
file.write(content)
file.close()
