
company_name = "Starbucks AG"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Stefan'
  auth.last_name = 'Konrad'
  auth.email = "starbucks@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43664123412'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'company_logo165.png'))
  company.blocking_notice = false
  company.tnc_link = "https://www.wko.at/Content.Node/Service/Wirtschaftsrecht-und-Gewerberecht/Allgemeines-Zivil--und-Vertragsrecht/AGB-Datenbank/-AGB-Strukutur-/Branchenneutrale-Informationen/Allgemeine-Geschaeftsbedingungen.pdf"
  company.save!
end


company_name = "Adidas AG"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Marlies'
  auth.last_name = 'Morgenstern'
  auth.email = "adidas@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667987698'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'company_logo10.png'))
  company.blocking_notice = false
  company.tnc_text = "Die Verwendung von Allgemeinen Geschäftsbedingungen (AGB) ist nur dann sinnvoll, wenn diese auch zum Vertragsinhalt werden. Fehlt ein deutlicher Hinweis auf AGB oder kann der Vertragspartner nicht auf zumutbare Weise (z.B. weil die AGB unleserlich klein gedruckt wurden) Einsicht nehmen, werden AGB von vornherein nicht Vertragsinhalt.Es ist ratsam, dem Partner die Geltung der AGB zweifelsfrei bekanntzugeben, ihm diese nach Möglichkeit vor seiner Unterschrift lesen und sich dieses Lesen auch bestätigen zu lassen. Zumindest sollte vor der Unterschrift ein deutlicher Hinweis auf die Geltung von AGB vorhanden sein. Die Übermittlung von AGB auf Rechnungen, Lieferscheinen oder dergleichen ist grundsätzlich wirkungs- und damit sinnlos."
  company.save!
end


company_name = "BMW Group"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Bernhard'
  auth.last_name = 'Tauchinger'
  auth.email = "bmw@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+4366023823'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'company_logo20.png'))
  company.blocking_notice = false
  company.tnc_text = "Die Verwendung von Allgemeinen Geschäftsbedingungen (AGB) ist nur dann sinnvoll, wenn diese auch zum Vertragsinhalt werden. Fehlt ein deutlicher Hinweis auf AGB oder kann der Vertragspartner nicht auf zumutbare Weise (z.B. weil die AGB unleserlich klein gedruckt wurden) Einsicht nehmen, werden AGB von vornherein nicht Vertragsinhalt.Es ist ratsam, dem Partner die Geltung der AGB zweifelsfrei bekanntzugeben, ihm diese nach Möglichkeit vor seiner Unterschrift lesen und sich dieses Lesen auch bestätigen zu lassen. Zumindest sollte vor der Unterschrift ein deutlicher Hinweis auf die Geltung von AGB vorhanden sein. Die Übermittlung von AGB auf Rechnungen, Lieferscheinen oder dergleichen ist grundsätzlich wirkungs- und damit sinnlos."
  company.save!
end


company_name = "NewBalance Sport"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Manuel'
  auth.last_name = 'Ehrenschlager'
  auth.email = "balance@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+4366394872943'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'company_logo149.png'))
  company.blocking_notice = false
  company.tnc_link = "https://www.wko.at/Content.Node/Service/Wirtschaftsrecht-und-Gewerberecht/Allgemeines-Zivil--und-Vertragsrecht/AGB-Datenbank/-AGB-Strukutur-/Branchenneutrale-Informationen/Allgemeine-Geschaeftsbedingungen.pdf"
  company.save!
end


company_name = "Crocs Foodwear"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Silvia'
  auth.last_name = 'Heister'
  auth.email = "crocs@t13s.at"
  auth.password = 'dighW!2239aAd'
  auth.password_confirmation = 'dighW!2239aAd'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+436673456343'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'company_logo27.png'))
  company.blocking_notice = false
  company.tnc_text = "Die Verwendung von Allgemeinen Geschäftsbedingungen (AGB) ist nur dann sinnvoll, wenn diese auch zum Vertragsinhalt werden. Fehlt ein deutlicher Hinweis auf AGB oder kann der Vertragspartner nicht auf zumutbare Weise (z.B. weil die AGB unleserlich klein gedruckt wurden) Einsicht nehmen, werden AGB von vornherein nicht Vertragsinhalt.Es ist ratsam, dem Partner die Geltung der AGB zweifelsfrei bekanntzugeben, ihm diese nach Möglichkeit vor seiner Unterschrift lesen und sich dieses Lesen auch bestätigen zu lassen. Zumindest sollte vor der Unterschrift ein deutlicher Hinweis auf die Geltung von AGB vorhanden sein. Die Übermittlung von AGB auf Rechnungen, Lieferscheinen oder dergleichen ist grundsätzlich wirkungs- und damit sinnlos."
  company.save!
end
