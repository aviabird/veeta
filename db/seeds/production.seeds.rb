# RUN ONLY WHEN NOT YET
unless TalentAuth.exists?(email: "t-0@t13s.at") || Company.exists?(name: "Stark Industries")

  # COMPANIES WITH JOBS

  company_name = "Stark Industries"
  auth = nil
  unless Company.find_by(name: company_name)
    puts "Creating company #{company_name}"
    auth = AuthService::CompanySignup.new
    auth.company_name = company_name
    auth.first_name = 'Lisa'
    auth.last_name = 'Lustig'
    auth.email = "stark@t13s.at"
    auth.password = 'ci!2AVt4%y5o'
    auth.password_confirmation = 'ci!2AVt4%y5o'
    auth.plan = 'basic'
    auth.ip = '10.0.0.1'
    p auth.signup!
    company = Company.find_by(name: company_name)
    company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667836633'
    company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'stark_logo.png'))
    company.blocking_notice = false
    company.tnc_link = "https://www.wko.at/Content.Node/Service/Wirtschaftsrecht-und-Gewerberecht/Allgemeines-Zivil--und-Vertragsrecht/AGB-Datenbank/-AGB-Strukutur-/Branchenneutrale-Informationen/Allgemeine-Geschaeftsbedingungen.pdf"
    company.save!

    puts "Importing job for #{company_name}"
    job = Job.new
    job.name = 'Java Web Developer'
    job.company = company
    job.status = 'new'
    job.recruiter = company.owner
    job.language = Language.find('en')
    job.accepted_jobs_languages.build(language: Language.find('en'))
    job.accepted_jobs_languages.build(language: Language.find('de'))
    job.save!
  end


  company_name = "ACME"
  auth = nil
  unless Company.find_by(name: company_name)
    puts "Creating company #{company_name}"
    auth = AuthService::CompanySignup.new
    auth.company_name = company_name
    auth.first_name = 'Max'
    auth.last_name = 'Muster'
    auth.email = "acme@t13s.at"
    auth.password = 'dighW!2239aAd'
    auth.password_confirmation = 'dighW!2239aAd'
    auth.plan = 'basic'
    auth.ip = '10.0.0.1'
    p auth.signup!
    company = Company.find_by(name: company_name)
    company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667836633'
    company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'acme_logo.png'))
    company.blocking_notice = true
    company.tnc_text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
    company.save!

    puts "Importing job for #{company_name}"
    job = Job.new
    job.name = 'Software Tester/in'
    job.company = company
    job.status = 'new'
    job.recruiter = company.owner
    job.language = Language.find('de')
    job.accepted_jobs_languages.build(language: Language.find('en'))
    job.accepted_jobs_languages.build(language: Language.find('de'))
    job.save!
  end


  # MYVEETA BUTTON CLIENT APPLICATION

  p 'creating veeta button admin connect@t13s.at...'

  auth = AdminAuth.find_or_create_by(email: "connect@t13s.at") do |auth|
    auth.admin_id = SecureRandom.uuid
    auth.password = 'yuwYI$13da$DFe_s'
  end

  p 'creating default client application "Contoso Career Portal" for veeta button...'

  unless ClientApplication.find_by(name: "Contoso Career Portal")
    p 'creating oauth consumer...'
    url = "https://www.myveeta.com"
    client_app = ClientApplication.new
    client_app.name = "Contoso Career Portal"
    client_app.url = url
    client_app.created_at = DateTime.now
    client_app.updated_at = DateTime.now
    client_app.webhook_url = "#{url}/update_webhook"
    client_app.delete_user_profile_deep_link =  "#{url}/my_profile"
    client_app.cancel_url = "#{url}/canceled"
    client_app.use_cancel_callback = false
    client_app.contact = "contoso@t13s.at"
    filename = 'contoso.png'
    client_app.logo = File.open(File.join(Rails.root, 'test', 'data', 'veetabutton', filename))
    client_app.save!
    client_app.update_columns(key: "Y86J39f335REpYf9Fc80aPeXn9m95OIndgg2gBh5", secret: "eqxGmHTBO1PRpwTkU2t9n5eW6HXtxsoKesE7NSO9")
  end


  # TALENTS

  ActiveRecord::Base.connection.tables.each { |t| ActiveRecord::Base.connection.reset_pk_sequence!(t) }
  EN_LANG = Language.find('en')

  austria = Country.find('AT')

  number_of_talents = 4

  p 'Importing talents with auth'
  number_of_talents.times do |i|
  # 1.times do |i|
    unless TalentAuth.find_by(email: "t-#{i}@t13s.at")
      auth = TalentAuth.find_or_create_by(email: "t-#{i}@t13s.at") do |auth|
        auth.resume_type = 1
        auth.country_id = austria.id
        auth.password = '#tijaMVta$'
        auth.confirmed_at = DateTime.now
        auth.first_name = Forgery::Name.first_name
        auth.last_name = Forgery::Name.last_name
      end
      p "Talent with email #{auth.email} created."

      talent = auth.talent

      address = Geo::Address.where(address_line: Forgery::Address.street_address).create! do |address|
        address.country_id = austria.id
        address.zip = '1080'
        address.city = 'Wien'
      end


      talent.update!(
          nationality_country: austria,
          title: Forgery::Name.academic_title('de'),
          salutation: %w(mr mrs).sample,
          birthdate: Date.today - rand(18..30).years,
          address: address,
          phone_number: '+436601234123',
          avatar: File.open(File.join(Rails.root, 'test', 'data', 'talents', "avatar-#{i%15+1}.png"))
      )

      working_locations_array = %w(Vienna Baden Salzburg Linz)
      talent.setting.availability = %w(immediately 1m 2m 3m 4-6m 6+m).sample
      talent.setting.prefered_employment_type = [:full_time, :part_time, :self_employed].sample
      talent.setting.job_seeker_status = [:very_much, :not_searching, :do_not_contact].sample
      talent.setting.working_industries = ['IT', 'Recruiting', 'Sales']
      talent.setting.working_locations = [working_locations_array[rand(0..3)]]

      # dummy
      talent.setting.salary_exp_min = [0,4,10,20,30,40,50,70,90,110,130,150].sample * 1000
      if [10000,20000,30000,40000].include?(talent.setting.salary_exp_min)
        talent.setting.salary_exp_max = talent.setting.salary_exp_min + 10000
      elsif [50000,70000,90000,110000,130000].include?(talent.setting.salary_exp_min)
        talent.setting.salary_exp_max = talent.setting.salary_exp_min + 20000
      elsif talent.setting.salary_exp_min == 150000
        talent.setting.salary_exp_max = 0
      elsif talent.setting.salary_exp_min == 0
        talent.setting.salary_exp_max = 4000
      elsif talent.setting.salary_exp_min == 4000
        talent.setting.salary_exp_max = 10000
      end

      talent.setting.save!

      if talent.resumes.empty?
        rand(0..3).times do |j|
          fake_date = rand(2.years - 6.months).seconds.ago
          p 'Importing resumes'
          resume = Resume.create!(
              talent: talent,
              name: "My Resume #{j}",
              language: EN_LANG,
              created_at: fake_date,
              updated_at: fake_date + rand(100).days,
              # dummy
              work_experience_duration: rand(1.0..20.0),
          )


          p "Resume for user #{resume.talent.email} with name #{resume.name} was created."

          p "Setting about for #{resume.name}"
          Rs::About.create!(text: Forgery::LoremIpsum.words, resume:resume)

          p "Setting links for #{resume.name}"
          Rs::Link.create!(linkedin: "https://linkedin.com/in/#{Forgery(:internet).user_name}", xing: "https://www.xing.com/profile/#{Forgery(:internet).user_name}", website:Forgery(:internet).domain_name, resume:resume)

          p 'Importing work experience for resume'

          rand(0..5).times do
            random = rand(2..120)

            from = random.months.ago
            to = (random / 2).months.ago

            Rs::WorkExperience.create!(
                resume: resume,
                company: Forgery::Name.company_name,
                position: Forgery::Name.job_title,
                industry: %w(AgriFish Arts AuditTaxLaw Automotive Construction Consulting Craft EduScience Electronics EnergyWater Engineering Entertainment Finance FoodTourism HR ITSvcSoftware Luxury Manufacturing MarketingDesign Media Medical MiningFuel Public RealEstate Security Social Tech Telco Trade Transport Other).sample,
                from: from,
                to: to,
                job_level: %w(intern starter experienced executive).sample,
                country: austria,
                description: Forgery::LoremIpsum.words,
                terms_of_employment: [:full_time, :part_time, :self_employed].sample
            )
          end

          p 'Importing education for resume'
          rand(0..5).times do
            random = rand(2..120)

            from = random.months.ago
            to = (random / 2).months.ago

            Rs::Education.create!(
                resume: resume,
                school_name: "School of #{Forgery::Address.city}",
                country: austria,
                subject: "#{Forgery::Name.job_title} at #{Forgery::Name.location}",
                type_of_education: 'Uni',
                from: from,
                to: to
            )

          end

          p 'Importing Skills for resume'
          rand(1 .. 3).times do
            Rs::Skill.create!(
                resume: resume,
                name: Forgery::Personal.skill('de'),
                level: rand(1..4)
            )
          end

          p 'Importing Certifications for resume'
          rand(2 .. 5).times do
            Rs::Certification.create!(
                resume: resume,
                certification_type: [:certification, :continued_education].sample,
                subject: Forgery::Personal.skill('de'),
                organization: Forgery::Name.company_name,
                description: Forgery::LoremIpsum.words,
                year: rand(1999..2015)
            )
          end

          p 'Importing Languages for resume'
          2.times do
            Rs::Language.create!(
                resume: resume,
                language: Language.all.sample,
                level: rand(1..5)
            )
          end
        end
      end
    end
  end


  #JOBAPPS, SHARINGS
  p 'Importing job applications'
  Talent.all.each do |talent|
    res = talent.resumes.where(origin_id: nil).sample
    if res
      if res && res.latest.nil?
        CloningService::ResumeClone.call(res)
      elsif res && res.latest && !CloningService::ResumeComparison.call(res, res.latest)
        CloningService::ResumeClone.call(res)
      end
      resume = res.latest
      loops = 2
      loops.times do |i|
        job = Job.all[i]
        unless talent.jobapps.find_by(job: job)
          job.accepted_jobs_languages.build(language: EN_LANG)
          job.save!
          #resume.update!(language: EN_LANG)
          source = Jobapp.source.values.sample
          source_details = ["company_website", "personal_recommendation"].include?(source) ? nil : Forgery::LoremIpsum.sentences
          fake_date = rand(2.years - 2.days).seconds.ago
          withdrawn_at = [false, false, false, false,true].sample ? rand(2.years - 2.days).seconds.ago : nil
          withdrawn_reason = withdrawn_at.nil? ? nil : Forgery::LoremIpsum.sentences
          jobapp = Jobapp.create!(
              job: job,
              resume: resume,
              language: EN_LANG,
              draft: false,
              job_type: 'Job',
              cover_letter: Forgery::LoremIpsum.sentences,
              recruiter_rating: rand(1..3),
              recruiter_note: 'He is really good',
              talent_info_status: 'review',
              added_by_recruiter: false,
              read_at: Time.now,
              application_referrer_url: 'http://karriere.at/job',
              allow_recruiter_contact_sharing: true,
              company_terms_accepted_at: Time.now,
              company_terms_accepted_ip: '10.0.0.1',
              withdrawn_at: withdrawn_at,
              withdrawn_reason: withdrawn_reason,
              source: source,
              source_details: source_details,
              created_at: fake_date,
              updated_at: fake_date
          )
          p "Job application for job #{jobapp.job.name} with resume #{jobapp.resume.name} was created"
          res.share(company_ids: [job.company_id], real_company: "REAL")
        end
      end
      loops = rand(1..2)
      loops.times do

        ea = EmailApplication.new
        ea.jobname =  Forgery::Name.job_title
        ea.recruiter_email = "#{Forgery(:internet).user_name}@t13s.at"
        ea.company_name = Forgery::Name.company_name
        ea.save!

        puts "EmailApplication created: applied as #{ea.jobname} at #{ea.company_name} sent to #{ea.recruiter_email} "

        source = Jobapp.source.values.sample
        source_details = ["company_website", "personal_recommendation"].include?(source) ? nil : Forgery::LoremIpsum.sentences
        fake_date = rand(2.years - 2.days).seconds.ago
        withdrawn_at = [false, false, false, false,true].sample ? rand(2.years - 2.days).seconds.ago : nil
        withdrawn_reason = withdrawn_at.nil? ? nil : Forgery::LoremIpsum.sentences
        jobapp = Jobapp.create!(
            job: ea,
            resume: resume,
            language: EN_LANG,
            draft: false,
            job_type: 'EmailApplication',
            cover_letter: Forgery::LoremIpsum.sentences,
            recruiter_rating: rand(1..3),
            recruiter_note: 'He is really good',
            talent_info_status: 'review',
            added_by_recruiter: false,
            read_at: Time.now,
            application_referrer_url: 'http://karriere.at/job',
            allow_recruiter_contact_sharing: true,
            company_terms_accepted_at: Time.now,
            company_terms_accepted_ip: '10.0.0.1',
            withdrawn_at: withdrawn_at,
            withdrawn_reason: withdrawn_reason,
            source: source,
            source_details: source_details,
            created_at: fake_date,
            updated_at: fake_date
        )
        res.share(company_ids: [ea.id], real_company: "EMAIL")

      end

      rand(1..2).times do |i|
        p "Creating #{i} ResumeSharingRequests for #{ resume.name }"
        contact_name = Forgery::Name.full_name
        fake_date = rand(2.years - 2.months).seconds.ago
        resume.sharing_requests.create!(created_at: fake_date,
                                        updated_at: fake_date,
                                        email: "#{contact_name}@t13s.at",
                                        contact_name: contact_name)
      end
    end
  end

  p "Updating created_at and updated_at of #{ ResumeSharing.count } ResumeSharings"
  ResumeSharing.where("created_at > ?", 3.minutes.ago).each do |rs|
    fake_date = rand(2.years - 2.days).seconds.ago
    rs.update_columns(created_at: fake_date,
                      updated_at: fake_date)
    p "Updated RS##{ rs.id } fake_date to #{ fake_date }"
  end
end

company_name = "myVeeta"
auth = nil
unless Company.find_by(name: company_name)
  puts "Creating company #{company_name}"
  auth = AuthService::CompanySignup.new
  auth.company_name = company_name
  auth.first_name = 'Lisa'
  auth.last_name = 'Lustig'
  auth.email = "myveeta@t13s.at"
  auth.password = 'ci!2AVt4%y5o'
  auth.password_confirmation = 'ci!2AVt4%y5o'
  auth.plan = 'basic'
  auth.ip = '10.0.0.1'
  p auth.signup!
  company = Company.find_by(name: company_name)
  company.contact = auth.first_name + ' ' + auth.last_name + '<br/>' + auth.email + '<br/>' + '+43667836633'
  company.logo = File.open(File.join(Rails.root, 'test', 'data', 'company', 'myveeta_logo.png'))
  company.blocking_notice = false
  company.tnc_link = "https://www.wko.at/Content.Node/Service/Wirtschaftsrecht-und-Gewerberecht/Allgemeines-Zivil--und-Vertragsrecht/AGB-Datenbank/-AGB-Strukutur-/Branchenneutrale-Informationen/Allgemeine-Geschaeftsbedingungen.pdf"
  company.save!
end
