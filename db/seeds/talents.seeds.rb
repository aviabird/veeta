# # JAN'S COMMENTS ON TEST DATA
#
# # A profile (person) can belong to one of 8 profile types. For every type the profile is composed differently. See below:
#
# austria = Geo::Country.where(iso_code: 'at').first
#
# # Marketing Profile
#
# rand(8..32).times do |i|
#   first_name = Forgery::Name.first_name
#   last_name = Forgery::Name.last_name
#   provider = %w(gmail me).sample
#   email = "#{first_name}.#{last_name}@#{provider}.tst"
#
#   # unless TalentAuth.find_by(email: email)
#   auth = TalentAuth.find_or_create_by(email: email) do |auth|
#     auth.resume_type = 'skilled'
#     auth.country_id = austria.id
#     auth.password = '12345678'
#     auth.confirmed_at = DateTime.now
#     auth.first_name = first_name
#     auth.last_name = last_name
#   end
#
#   talent = auth.talent
#
#   address = Geo::Address.find_or_create_by(address_line: Forgery::Address.street_address) do |address|
#     address.country_id = austria.id
#     address.zip = %w(1010 1020 1030 1040 1050 1060 1070 1080 1090).sample
#     address.city = 'Wien'
#   end
#
#   talent.address = address
#   talent.nationality_country = austria
#   talent.salutation = Forgery::Personal.gender
#   talent.birthdate = rand(25..55).years.ago
#   talent.phone_number = '+436601234123'
#   talent.avatar = File.open(File.join(Rails.root, 'test', 'data', 'talents', "avatar-#{rand(1..16)}.png"))
#   talent.save!
#
#
# # Name, Phone, email: random (all emails have tld .tst)
#
# # Age random between 25 and 55
#
# # Pick one entry from education_subjects_marketing_de and education_schools_marketing_de
#
# # TYPE OF EDUCATION (TOE):
# # => If school contains "uni" then university
# # => If school contains "FH" then Fachhochschule (FH)
# # => If school contains "HTL" then HTL
# # => Otherwise "Andere"
# # Academic Degree:
# # => If TOE is university or FH then random: [none] / Mag. / Dipl. Ing.
# # => Otherwise [none]
#
# # Country always Austria
# # Completed
# # => if TOE=HTL: yes
# # => if TOE=Uni or FH: random yes/no (chance 80% yes)
#
# # From:
# # => HTL: Age of 16
# # => Uni, FH: Between 18 and 21 years of age
#
# # To (calc from Duration):
# # => HTL: 5 years
# # => If TOE is FH: If completed 4 years, otherwise random 1-3y
# # => If TOE is Uni: If completed random 4 to 9 years, otherwise random 1-9y
#
# # If TOE is HTL: Randomly decide on whether to add another education
# # => If yes, type of new eductation must be Uni or FH (not again HTL)
# # => Repeat steps above, From starts after previous education finished
#
# # Work Experience
# # Pick random position (from workexperience_jobtitles_marketing_de) and company (from workexperience_companies_general_de)
# # Industry is in company file, split by ;
#
# end
#
# # Automotive Profile
# # - Take one of the educations in education_schools_automotive_de
# #
#
# # IT Profile
# # Take
#
#
# # Marketing Profile
