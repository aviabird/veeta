class MailSharingResponseJob

  @@ATTEMPTS = ENV['PDF_MAILING_ATTEMPTS'].to_i
  @@RETRY_DELAYS = ENV['PDF_MAILING_RETRY_DELAYS'].split(",")

  def self.queue
    :mailer
  end

  def self.perform type, cv_sharing_response_id, recruiter_id = 0, call_count = 0
    cv_sharing_response = CvSharingResponse.find(cv_sharing_response_id)
    res = cv_sharing_response.resume
    raise_critical = false
    begin
      if res.pdf && res.pdf.url && res.file_exists?(res.pdf.url.to_s)
        LogService.call "#{MailSharingResponseJob.name}.attempt", {resource: cv_sharing_response, details: "Attempt: Send cv_sharing_response, type is #{type}",priority: :low}
        if type == 'company'
          recruiter = Recruiter.find(recruiter_id)
          JobApplicationAndUpdateMailer.inform_recruiter_about_pool_join(res, recruiter, cv_sharing_response.id).deliver
          LogService.call "#{MailSharingResponseJob.name}.success", {resource: cv_sharing_response, recruiter: recruiter, details:  "Done: Send cv_sharing_response, type is #{type}",priority: :low}
        #elsif type == 'emailcompany'
        #  JobApplicationAndUpdateMailer.inform_email_company_recruiter(locale, jobapp).deliver
        end
      else
        raise_critical = true
      end
    rescue
      raise_critical = true
    end
    if raise_critical && call_count == @@ATTEMPTS
      unless Rails.env.development?
        notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
        notifier.ping "*Action required:* Could not attach CV PDF to CvSharingResponse mail!\ncv_sharing_response: #{cv_sharing_response.to_yaml}\nresume: #{res.to_yaml}\ntalent: #{res.talent.to_yaml}", username: "myVeeta MailSharingResponseJob #{Rails.env} Mail Bot"
      end
      LogService.call "#{MailSharingResponseJob.name}.failure", {resource: cv_sharing_response, details: "Aborted after all retries: Sending CvSharingResponse",priority: :high}
      raise "Could not attach CV PDF to CvSharingResponse mail for cv_sharing_response: #{cv_sharing_response.inspect}, resume: #{res.inspect}, talent: #{res.talent.inspect}"
    elsif raise_critical && call_count < @@ATTEMPTS
      delay = call_count < @@RETRY_DELAYS.size ? @@RETRY_DELAYS[call_count].to_i : @@RETRY_DELAYS.last.to_i
      call_count = call_count + 1
      LogService.call "#{MailSharingResponseJob.name}.failure", {resource: cv_sharing_response, details: "Retry: Rescheduled CvSharingResponse Mailing Resque-Job",priority: :high}
      Resque.enqueue_in(delay.minutes, MailSharingResponseJob, type, cv_sharing_response_id, recruiter_id, call_count)
    end
  end
end
