class EmailQueueJob

  def self.queue
    :relaxed_mailer
  end

  def self.perform
    Rails.logger.info("Starting CV update mailing")
    last_synced_at = ApplicationSetting.timestamp_setting 'EmailQueueJob.last_updates_sent_at'
    last_synced_at = last_synced_at.nil?  ? Rails.configuration.update_intervall.ago : last_synced_at
    next_synced_at = DateTime.now.utc
    sharing_updates = ResumeSharing.ready_to_share last_synced_at
    ApplicationSetting.set_timestamp_setting 'EmailQueueJob.last_updates_sent_at',next_synced_at
    Rails.logger.info("Going to update #{sharing_updates.length} updates.")
    # sharings = ResumeSharing.ready_to_share
    # sharings.each { |s| send_update(s.resume, s.company_id) }

    sharing_updates.each do |s|
      #either company or email application company
      begin
        if !s.company_id.nil? && s.company.setting.update_mail_enabled?
          self.send_update(s.resume, s.company_id, s.id)
        elsif s.company_id.nil?
          self.send_update_to_email_application_company(s.resume, s.email_application_id, s.id)
        end
      rescue
        LogService.call EmailQueueJob.name, {talent:s.resume.talent, resume: s.resume,resource: s, details: "Failed email update, sharing id: #{s.id}", priority: :high}
      end
    end
    Rails.logger.info("Finished CV update mailing")
  end

  # Duplicate of private method used in Resume::Share class
  def self.send_update(resume, company_id, resume_sharing_id)
    company = Company.find(company_id)
    if company.recruiters.exists?
      company.recruiters.each do |r|
        if r.email_notifications_enabled
          JobApplicationAndUpdateMailer.send_cv_update(resume, r, resume_sharing_id).deliver
          LogService.call EmailQueueJob.name, {talent:resume.talent,company: company, recruiter: r, resume: resume, details: 'Email update sent to company',priority: :low}
        end
      end
    elsif company.owner.email_notifications_enabled
      JobApplicationAndUpdateMailer.send_cv_update(resume, company.owner, resume_sharing_id).deliver
      LogService.call EmailQueueJob.name, {talent:resume.talent,company: company, recruiter: company.owner, resume: resume, details: 'Email update sent to company',priority: :low}
    end
  end

  # Duplicate of private method used in Resume::Share class
  def self.send_update_to_email_application_company(resume, email_application_id, resume_sharing_id)
    company = EmailApplication.find(email_application_id)
    JobApplicationAndUpdateMailer.send_cv_update_to_email_company(resume, company, resume_sharing_id).deliver
    LogService.call EmailQueueJob.name, {talent:resume.talent,company: company, resume: resume, details: 'Email update sent to email company',priority: :low}
  end
end
