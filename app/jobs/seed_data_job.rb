class SeedDataJob

  def self.queue
    :seed  # use this queue as seed data
  end

  def self.perform talents=1000, recruiters = 1, jobs=50
    system "env PROCESS_TYPE=web RAILS_ENV=#{Rails.env} bundle exec rake db:seed:development:pool talents=#{talents} recruiters=#{recruiters} jobs=#{jobs}"
    system "env PROCESS_TYPE=web RAILS_ENV=#{Rails.env} bundle exec rake chewy:reset"
  end
end
