class MyveetaPDFGenerationJob

  def self.queue
    :kickresume  # use this queue as pdf generation queue
  end

  def self.perform resume_id, call_count = 0
    res = Resume.find(resume_id)
    I18n.locale = res.talent.locale
    LogService.call "#{MyveetaPDFGenerationJob.name}.perform", {resource: res, resume: res, details: "Attempt: Generate myVeeta PDF from resume",priority: :low}
    res.pdf_template_setting.trigger_pdf_generation_sync
    LogService.call "#{MyveetaPDFGenerationJob.name}.perform_done", {resource: res, resume: res, details: "Done: Generate myVeeta PDF from resume",priority: :low}
  end
end
