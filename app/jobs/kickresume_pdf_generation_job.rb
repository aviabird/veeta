class KickresumePDFGenerationJob

  @@ATTEMPTS =  ENV['KICKRESUME_ATTEMPTS'].to_i
  @@RETRY_DELAYS = ENV['KICKRESUME_RETRY_DELAYS'].split(",")


  @@KR_API_INIT_WAITING = 7
  @@KR_API_CHECK_WAITING = 3
  @@KR_API_CHECK_WAITING_LOOPS = 10

  def self.queue
    :kickresume
  end

  def self.perform resume_id, call_count = 0
    res = Resume.find(resume_id)
    I18n.locale = res.talent.locale
    LogService.call "#{KickresumePDFGenerationJob.name}.perform", {resource: res, resume: res, details: "Attempt: Generate Kickresume PDF from resume",priority: :low}
    begin
      self.generate_pdf res
      #remove external link to avatar picture
      ExternalLinkService.new.remove_kickresume_mapping res.talent
      LogService.call "#{KickresumePDFGenerationJob.name}.perform", {resource: res, resume: res, details: "Done: Generate Kickresume PDF from resume",priority: :low}
    rescue Exception => e
      if call_count == @@ATTEMPTS
        unless Rails.env.development?
          notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
          notifier.ping "*Action required:* Could not generate Kickresume PDF via API!\nresume: #{res.to_yaml}\ntalent: #{res.talent.to_yaml}", username: "myVeeta #{Rails.env} Kickresume Bot"
        end
        #remove external link to avatar picture
        ExternalLinkService.new.remove_kickresume_mapping res.talent
        LogService.call "#{KickresumePDFGenerationJob.name}.perform", {resource: res, resume: res, talent: res.talent, details: "Aborted after all retries: Generate Kickresume PDF from resume",priority: :high}
        raise e
      end
      delay = call_count < @@RETRY_DELAYS.size ? @@RETRY_DELAYS[call_count].to_i : @@RETRY_DELAYS.last.to_i
      #reschedule here if not successfully created
      call_count = call_count + 1
      LogService.call "#{KickresumePDFGenerationJob.name}.perform", {resource: res, resume: res, talent: res.talent, details: "Retry: Rescheduled  Kickresume PDF Generation Resque-Job",priority: :high}
      Resque.enqueue_in(delay.minutes,KickresumePDFGenerationJob, resume_id, call_count)
    end
  end


  def self.generate_pdf res
    service = PDFService::Kickresume.new
    #add job
    if service.add res
      # wait for pdf to be generated
      sleep(@@KR_API_INIT_WAITING.seconds)
      counter = 0
      while !service.check(res) && counter < @@KR_API_CHECK_WAITING_LOOPS do
        sleep(@@KR_API_CHECK_WAITING.seconds)
        counter = counter + 1
      end
      if !service.check(res) && counter >= 10
        LogService.call "#{KickresumePDFGenerationJob.name}.generate_pdf", {resource: res, resume: res, details: "Aborted: Generate Kickresume PDF from resume aborted after 37 seconds",priority: :high}
        raise "Aborted: Generate Kickresume PDF from resume aborted after 37 seconds. Resume was #{res.inspect}"
      else
        # attach to resume (if ext_id is still the same)
        reloaded_res = Resume.find(res.id)
        if(res.pdf_template_setting.ext_id == reloaded_res.pdf_template_setting.ext_id)
          tempfile = service.get(res)
          reloaded_res.pdf = File.open(tempfile.path)
          reloaded_res.save
          reloaded_res.pdf_template_setting.update_column(:last_generated_at, DateTime.now)
          tempfile.unlink
          LogService.call "#{KickresumePDFGenerationJob.name}.generate_pdf", {resource: res, resume: res, details: "Done: Generate Kickresume PDF via API successfully finished",priority: :low}
        end
      end
    else
      LogService.call "#{KickresumePDFGenerationJob.name}.generate_pdf", {resource: res, resume: res, details: "Failed: Generate Kickresume PDF from resume could not be triggered via API",priority: :high}
      raise "Aborted: Generate Kickresume PDF from resume could not be triggered via API. Resume was #{res.inspect}"
    end
  end

end
