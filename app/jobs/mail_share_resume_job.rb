class MailShareResumeJob

  @@ATTEMPTS = ENV['PDF_MAILING_ATTEMPTS'].to_i
  @@RETRY_DELAYS = ENV['PDF_MAILING_RETRY_DELAYS'].split(",")

  def self.queue
    :mailer
  end

  def self.perform locale, resume_id, contact_name, contact_email,resume_sharing_request_id, call_count = 0
    res = Resume.find(resume_id)
    raise_critical = false
    begin
      if res.pdf && res.pdf.url && res.file_exists?(res.pdf.url.to_s)
        LogService.call MailShareResumeJob.name, {resource: res, resume: res, details: "Attempt: Share resume, language is #{locale}, contact_name is #{contact_name}, contact_email is #{contact_email}",priority: :low}
        JobApplicationAndUpdateMailer.send_shared_resume(locale, resume_id, contact_name, contact_email, resume_sharing_request_id).deliver
        LogService.call MailShareResumeJob.name, {resource: res, resume: res, details: "Done: Share resume, language is #{locale}, contact_name is #{contact_name}, contact_email is #{contact_email}",priority: :low}
      else
        # if myveeta cv template, try  to recreate 
        res.pdf_template_setting.trigger_pdf_generation if res.pdf_template_setting.is_myveeta_template?
        raise_critical = true
      end
    rescue
      raise_critical = true
    end
    if raise_critical && call_count == @@ATTEMPTS
      unless Rails.env.development?
        notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
        notifier.ping "*Action required:* Could not attach CV PDF to mail!\nresume: #{res.to_yaml}\ntalent: #{res.talent.to_yaml}", username: "myVeeta #{Rails.env} Mail Bot"
      end
      LogService.call "#{MailShareResumeJob.name}", {resource: res, resume: res, details: "Aborted after all retries: Sending Share CV mail",priority: :high}
      raise "Could not attach CV PDF to mail for resume : #{res.inspect}, talent: #{res.talent.inspect}"
    elsif raise_critical && call_count < @@ATTEMPTS
      delay = call_count < @@RETRY_DELAYS.size ? @@RETRY_DELAYS[call_count].to_i : @@RETRY_DELAYS.last.to_i
      call_count = call_count + 1
      LogService.call "#{MailShareResumeJob.name}", {resource: res, resume: res,  details: "Retry: Rescheduled Share CV Mailing Resque-Job",priority: :high}
      Resque.enqueue_in(delay.minutes, MailShareResumeJob, locale, resume_id, contact_name, contact_email, resume_sharing_request_id, call_count)
    end
  end

end
