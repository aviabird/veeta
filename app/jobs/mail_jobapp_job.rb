class MailJobappJob

  @@ATTEMPTS = ENV['PDF_MAILING_ATTEMPTS'].to_i
  @@RETRY_DELAYS = ENV['PDF_MAILING_RETRY_DELAYS'].split(",")

  def self.queue
    :mailer
  end

  def self.perform locale, type, jobapp_id, recruiter_id = 0, call_count = 0
    jobapp = Jobapp.find(jobapp_id)
    res = jobapp.resume
    raise_critical = false
    begin
      if res.pdf && res.pdf.url && res.file_exists?(res.pdf.url.to_s)
        LogService.call MailJobappJob.name, {resource: jobapp, jobapp: jobapp, details: "Attempt: Send jobapp, language is #{locale}, type is #{type}",priority: :low}
        if type == 'company'
          recruiter = Recruiter.find(recruiter_id)
          JobApplicationAndUpdateMailer.inform_recruiter(locale, jobapp, recruiter).deliver
          LogService.call MailJobappJob.name, {resource: jobapp, jobapp: jobapp, recruiter: recruiter, details: "Done: Send jobapp, language is #{locale}, type is #{type}",priority: :low}
        elsif type == 'emailcompany'
          JobApplicationAndUpdateMailer.inform_email_company_recruiter(locale, jobapp).deliver
          LogService.call MailJobappJob.name, {resource: jobapp, jobapp: jobapp, details: "Done: Send jobapp, language is #{locale}, type is #{type}",priority: :low}
        end
      else
        raise_critical = true
      end
    rescue
      raise_critical = true
    end
    if raise_critical && call_count == @@ATTEMPTS
      unless Rails.env.development?
        notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
        notifier.ping "*Action required:* Could not attach CV PDF to Jobapp mail!\njobapp: #{jobapp.to_yaml}\nresume: #{res.to_yaml}\ntalent: #{res.talent.to_yaml}", username: "myVeeta MailJobappJob #{Rails.env} Mail Bot"
      end
      LogService.call "#{MailJobappJob.name}", {resource: jobapp, jobapp: jobapp, details: "Aborted after all retries: Sending Jobapp",priority: :high}
      raise "Could not attach CV PDF to Jobapp mail for jobapp: #{jobapp.inspect}, resume: #{res.inspect}, talent: #{res.talent.inspect}"
    elsif raise_critical && call_count < @@ATTEMPTS
      delay = call_count < @@RETRY_DELAYS.size ? @@RETRY_DELAYS[call_count].to_i : @@RETRY_DELAYS.last.to_i
      call_count = call_count + 1
      LogService.call "#{MailJobappJob.name}", {resource: jobapp, jobapp: jobapp, details: "Retry: Rescheduled Jobapp Mailing Resque-Job",priority: :high}
      Resque.enqueue_in(delay.minutes, MailJobappJob, locale, type, jobapp_id, recruiter_id, call_count)
    end
  end
end
