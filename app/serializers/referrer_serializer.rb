class ReferrerSerializer < ActiveModel::Serializer
  attributes :id, :slug
end