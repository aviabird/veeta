# == Schema Information
#
# Table name: jobapps
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  job_id                          :uuid             not null
#  job_type                        :string           not null
#  language_id                     :integer
#  cover_letter                    :text
#  contact_preferences             :text
#  blocked_companies               :text
#  source                          :string
#  recruiter_rating                :integer
#  recruiter_note                  :text
#  talent_info_status              :string
#  added_by_recruiter              :boolean          default(FALSE), not null
#  read_at                         :datetime
#  resume_snapshot_id              :uuid
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  withdrawn_at                    :datetime
#  withdrawn_reason                :text
#  access_revoked_at               :datetime
#  hidden_at                       :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  draft                           :boolean          default(TRUE)
#  source_details                  :text
#  revoke_reason                   :string
#  submitted_at                    :datetime
#
# Indexes
#
#  index_jobapps_on_access_revoked_at   (access_revoked_at)
#  index_jobapps_on_added_by_recruiter  (added_by_recruiter)
#  index_jobapps_on_created_at          (created_at)
#  index_jobapps_on_job_id              (job_id)
#  index_jobapps_on_job_type            (job_type)
#  index_jobapps_on_language_id         (language_id)
#  index_jobapps_on_read_at             (read_at)
#  index_jobapps_on_recruiter_rating    (recruiter_rating)
#  index_jobapps_on_resume_id           (resume_id)
#  index_jobapps_on_talent_info_status  (talent_info_status)
#

class JobappSerializer < ActiveModel::Serializer

  #attributes :attributes
  attributes :id, :cover_letter, :contact_preferences, :blocked_companies,
    			 :allow_recruiter_contact_sharing, :withdrawn_at, :withdrawn_reason,
           :created_at, :updated_at, :available, :resume_name, :job_id,
           :job_type, :resume_id, :source, :source_details, :origin_resume_id,
           :access_revoked_at, :unsolicited, :origin_resume_updated_at,
           :resume_changed, :started_as,
           :submitted_at,:rejected_at, :rejected_by, :reject_reason

#  alias_method :attributes, :my_atts

  has_one :job, :company
  has_one :recruiter, serializer: RecruiterSerializer

  # Get information if is object available
  def available
    object.available?
  end

  def origin_resume_id
    object.origin_resume.try(:id)
  end

  def origin_resume
    object.origin_resume
  end

  def resume_name
    object.resume.name if object.resume
  end

  def unsolicited
    object.try(:job).try(:unsolicited) || object.try(:job).try(:unsolicited_job)
  end

  def origin_resume_updated_at
    object.resume.origin_updated_at if object.resume
  end

  def resume_changed
    intial_resume_version = object.try(:intial_resume_version)
    if intial_resume_version && object.resume.present?
      new_resume_version = object.resume.version
      !intial_resume_version.eql?(new_resume_version)
    else
      false
    end
  end

  def started_as
    object.try(:started_as)
  end
end
