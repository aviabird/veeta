class XingProfileSerializer < ActiveModel::Serializer

  attributes :id,:first_name,:last_name,:academic_title, :display_name,:page_name,:employment_status,:gender,:haves, :active_email,:languages, :private_address, :business_address, :web_profiles, :educational_background,
    :permalink,:time_zone,:birth_date, :premium_services, :badges,  :wants, :interests, :organisation_member, :instant_messaging_accounts,:photo_urls,:educational_background,
    :professional_experience, :top_haves, :legal_information, :photo_attributes

  def id
    object.talent.id
  end

  def academic_title
    # xing returns a value of a list, which can't be mapped to any field in myVeeta
    nil
  end

  def first_name
      object.talent.first_name
  end

  def last_name
      object.talent.last_name
  end

  def display_name
      "#{object.talent.first_name} #{object.talent.last_name}"
  end

  def page_name
    object.talent.id
  end

  #take type of position of primary job
  def employment_status
    primary_job = nil
    primary_edu = nil
    primary_job_xing = primary_element(work_experiences)
    primary_job = Rs::WorkExperience.find(primary_job_xing[:id]) unless primary_job_xing.nil?
    primary_edu_xing = primary_element(educations)
    primary_edu = Rs::Education.find(primary_edu_xing[:id]) unless primary_edu_xing.nil?
    emp_status = 'ENTREPRENEUR'
    #intern starter experienced executive
    #full_time part_time self_employed
    if primary_job.nil?
      #student if ongoing education
      if !primary_edu.nil? && primary_edu.to.nil?
        emp_status = 'STUDENT'
      end
    else
      if ['intern', 'starter', 'experienced'].include?(primary_job.job_level) && ['full_time', 'part_time'].include?(primary_job.terms_of_employment)
        emp_status = 'EMPLOYEE'
      elsif ['executive'].include?(primary_job.job_level) && ['full_time', 'part_time'].include?(primary_job.terms_of_employment)
        emp_status = 'EXECUTIVE'
      elsif ['executive'].include?(primary_job.job_level) && ['self_employed'].include?(primary_job.terms_of_employment)
        emp_status = 'ENTREPRENEUR'
      elsif ['intern', 'starter', 'experienced'].include?(primary_job.job_level) && ['self_employed'].include?(primary_job.terms_of_employment)
        emp_status = 'FREELANCER'
      end
    end
    emp_status
  end

  def permalink
    "https://www.myveeta.com/users/#{object.talent.id}"
  end

  def time_zone
    {}
  end

  def birth_date
    bd ={}
    unless(object.talent.birthdate.nil?)
      bd[:day] = object.talent.birthdate.day
      bd[:month] = object.talent.birthdate.month
      bd[:year] = object.talent.birthdate.year
    else
      bd[:day] = nil
      bd[:month] = nil
      bd[:year] = nil
    end
    bd
  end

  def gender
    if object.talent.salutation == 'mr'
      "m"
    else
      "f"
    end
  end

  def active_email
    object.talent.email
  end

  def premium_services
    []
  end

  def badges
    []
  end

  def wants
    nil
  end

  def haves
    skills
  end

  def top_haves
    nil
  end

  def photo_attributes
    atts = {}
    atts[:uploaded] = !object.talent.avatar_url.blank?
    atts
  end

  def legal_information
    content = {}
    content[:preview_content] = nil
    content
  end

  def organisation_member
    nil
  end

  def instant_messaging_accounts
    {}
  end

  def interests
    object.about.text unless object.about.nil?
  end



  def languages
    mapping_service = MappingService.new
    langs = {}
    object.languages.each do |lang|
      langs[lang.language.iso_code] = mapping_service.xing_language_level 'fromVeeta', lang.level
    end unless  object.languages.nil?
    langs
  end

  def private_address
    address = {}
    address[:city] = object.talent.address.city
    address[:country] = object.talent.address.country.iso_code
    address[:zip_code] = object.talent.address.zip
    address[:street] = "#{object.talent.address.address_line}, #{object.talent.address.address_line_2}"
    address[:phone] = fix_phone_fax_number object.talent.phone_number
    address[:email] = object.talent.email
    address[:fax] = '||'
    address[:province] = nil
    address[:mobile_phone] = '||'
    address
  end

  def business_address
    address = {}
    # city and country mandatory by xing
    address[:city] = object.talent.address.city
    address[:country] = object.talent.address.country.iso_code
    address[:zip_code] = nil
    address[:street] = nil
    address[:phone] = '||'
    address[:email] = nil
    address[:fax] = '||'
    address[:province] = nil
    address[:mobile_phone] = '||'
    address
  end

  def web_profiles
    profiles = {}
    unless object.link.nil?
      homepage = []
      website_link = object.link.website
      website_link = "http://#{website_link}" unless website_link.nil? || website_link.starts_with?('http://') || website_link.starts_with?('https://')
      homepage << website_link unless website_link.nil?
      other = []
      other << object.link.xing unless object.link.xing.nil?
      other << object.link.linkedin unless object.link.linkedin.nil?
      profiles[:hompage] = homepage unless homepage.empty?
      profiles[:other] = other unless other.empty?
    end
    profiles
  end

  def educational_background
    edu = {}
    edu[:degree] = object.talent.title
    edu[:schools] = educations
    primary_edu =  primary_element(edu[:schools])
    edu[:primary_school] = primary_edu.nil? ? empty_primary_edu : primary_edu
    edu[:qualifications] = qualifications
    edu
  end

  def professional_experience
    work = {}    # sort by end date desc (nil on top), then by begin date desc
    work[:awards] = []
    work[:companies] = work_experiences
    primary_work =  primary_element(work[:companies])
    work[:primary_company] = primary_work.nil? ? empty_primary_work : primary_work
    work
  end

  def photo_urls
    urls = {}
    if object.talent.avatar_url
      avatar_urls = object.talent.external_avatar_url(@options[:client_application])
    else
      profile_picture_url = ENV["DEFAULT_AVATAR_URL_#{gender.upcase}"]
    end
    urls[:size_original] = object.talent.avatar_url ? avatar_urls[:orginal] : profile_picture_url
    urls[:large] = object.talent.avatar_url ? avatar_urls[:large] : profile_picture_url
    urls[:maxi_thumb] = object.talent.avatar_url ? avatar_urls[:thumb] : profile_picture_url
    urls[:medium_thumb] = object.talent.avatar_url ? avatar_urls[:thumb] : profile_picture_url
    urls[:mini_thumb] = object.talent.avatar_url ? avatar_urls[:thumb] : profile_picture_url
    urls[:thumb] = object.talent.avatar_url ? avatar_urls[:thumb] : profile_picture_url
    urls[:size_32x32] = object.talent.avatar_url ? avatar_urls[:small] : profile_picture_url
    urls[:size_48x48] = object.talent.avatar_url ? avatar_urls[:small] : profile_picture_url
    urls[:size_64x64] = object.talent.avatar_url ? avatar_urls[:small] : profile_picture_url
    urls[:size_96x96] = object.talent.avatar_url ? avatar_urls[:medium] : profile_picture_url
    urls[:size_128x128] = object.talent.avatar_url ? avatar_urls[:medium] : profile_picture_url
    urls[:size_192x192] = object.talent.avatar_url ? avatar_urls[:medium] : profile_picture_url
    urls[:size_256x256] = object.talent.avatar_url ? avatar_urls[:large] : profile_picture_url
    urls[:size_1024x1024] = object.talent.avatar_url ? avatar_urls[:large] : profile_picture_url
    urls
  end




  private
    def qualifications
      qualis = []
      object.certifications.each do |cert|
          qualis << cert.subject
      end unless object.skills.nil?
      qualis
    end

    def skills
      sklz = []
      object.skills.each do |skill|
        sklz << skill.name
      end unless object.skills.nil?
      sklz.join(', ') unless sklz.empty?
    end

    def educations
      edus = []
      object.educations.each  do |_edu|
        edu = {}
        edu[:id] = _edu.id #mandatory
        edu[:name] = _edu.school_name #mandatory
        edu[:degree] = _edu.degree
        edu[:notes] = _edu.description
        edu[:subject] = _edu.subject.blank? ? '-' : _edu.subject#mandatory
        edu[:begin_date] = month_fixer _edu.from #mandatory
        edu[:end_date] = month_fixer _edu.to
        edus << edu
      end unless object.educations.nil?
      edus
    end

    def work_experiences
      mapping_service = MappingService.new
      experiences = []
      object.work_experiences.each  do |_experience|
        experience = {}
        experience[:id] = _experience.id #mandatory
        experience[:name] = _experience.company #mandatory
        experience[:title] = _experience.position #mandatory
        experience[:company_size] = nil
        experience[:tag] = nil
        experience[:url] = nil
        experience[:discipline] = nil
        experience[:description] = _experience.description
        experience[:begin_date] = month_fixer _experience.from #mandatory
        experience[:end_date] = month_fixer _experience.to
        experience[:until_now] = experience[:end_date].nil?
        experience[:career_level] = mapping_service.xing_career_level('fromVeeta',_experience.job_level)
        if _experience.terms_of_employment.blank?
          experience[:form_of_employment] = 'FULL_TIME_EMPLOYEE' #DEFAULT VALUE
        else
          experience[:form_of_employment] = mapping_service.xing_employment_type('fromVeeta',_experience.terms_of_employment) #mandatory
        end
        if _experience.industry.blank?
          # default values
          experience[:industry]= "OTHERS"
          experience[:industries]= []
        else
          experience[:industry]= mapping_service.xing_legacy_industry('fromVeeta',_experience.industry ) #mandatory
          experience[:industries]= industries  _experience.industry #mandatory
        end
        experiences << experience
      end unless object.work_experiences.nil?
      experiences
    end

    def industries ve_industry
      ng_industries = []
      ng_industry = {}
      mapping_service = MappingService.new
      ng_industry[:ng_industry_id] = mapping_service.xing_industry('fromVeeta',ve_industry)
      ng_industry[:ng_industry_key] = mapping_service.xing_industry_name('fromXing',ng_industry[:ng_industry_id], object.talent.locale )
      ng_industries << ng_industry
      ng_industries
    end

    #get primary school or education
    def primary_element coll
      unless coll.blank? || coll.empty?
          #get entries which are currently ongoing
          current = coll.select {|c| c[:end_date].nil?}
          #no current entry
          if current.empty?
            #sort initial list by end date and then by start date
            sorted = coll.sort_by {|k| [k[:end_date],k[:begin_date]]}
            #select latest
            sorted.last
          else
            #sort by start date - so get the least recent entry
            sorted = current.sort_by {|k| k[:begin_date]}
            #select least recent
            sorted.first
          end
      else
        nil
      end
    end

    def month_fixer date
      unless date.nil?
        m = date.month.to_s
        m = m.length == 1 ? "0#{m}" : m
        "#{date.year}\-#{m}"
      else
        nil
      end
    end

    def fix_phone_fax_number number
      number = prepare_phone_fax_prefix number
      unless number.blank? || number.length < 4
        if !number.index("|") || number.index("|") > 2
          number = number.insert(2,'|')
        end
        number = number.gsub "||","|"
        offset = number.index("|")
        if !number.index("|",offset+1)
          if number.length > (offset + 4)
            number = number.insert(offset +4,'|')
          else
            return '||'
          end
        end
        number = number.gsub "||","|"
        if number.count('|') > 2
          offset = number.index('|', number.index('|')+1)
          p number.index('|', number.index('|'))
          number = "#{number[0,offset+1]}#{number[offset,number.length].gsub('|', '')}"
        end
        if number.ends_with?('|')
          return '||'
        end
      else
        number = '||'
      end
      number
    end

    def prepare_phone_fax_prefix number
      result = '||'
      unless number.blank?
        number.gsub('(0)','')
        if number.starts_with?("+")
          result = number.slice(1, number.length-1)
        elsif number.starts_with?("00")
          result = number.slice(2, number.length-1)
        elsif number.starts_with?("0")
          #GET COUNTRY PHONE CODE
          if object.talent.address &&  object.talent.address.country && country = IsoCountryCodes.find(object.talent.address.country.iso_code.downcase)
            result = "#{country.calling.slice(1,country.calling.length)}|#{number.slice(1, number.length-1)}"
          end
        end
        result = result.squish.gsub(/[^0-9]/i, ' ').squish.tr(' ','|')
      end
      result
    end

    def empty_primary_edu
      epe = {}
      epe[:id] = nil
      epe[:subject] = nil
      epe[:degree] = nil
      epe[:begin_date] = nil
      epe[:end_date] = nil
      epe[:notes] = nil
      epe
    end

    def empty_primary_work
      epw = {}
      epw[:id]  = nil
      epw[:name]  = nil
      epw[:url]  = nil
      epw[:tag]  = nil
      epw[:title]  = nil
      epw[:begin_date]  = nil
      epw[:end_date]  = nil
      epw[:description]  = nil
      epw[:discipline]  = nil
      epw[:until_now]  = false
      epw[:industry]  = "OTHERS"
      epw[:industries]  = []
      epw[:company_size]  = nil
      epw[:career_level]  = nil
      epw[:form_of_employment]  = nil
      epw
    end

end
