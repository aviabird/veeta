# == Schema Information
#
# Table name: veeta_button_sharings
#
#  id                                 :uuid             not null, primary key
#  resume_id                          :uuid
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  last_synced_at                     :datetime
#  deleted_at                         :datetime
#  veeta_button_sharing_connection_id :uuid
#
# Indexes
#
#  index_veeta_button_sharings_on_created_at  (created_at)
#  index_veeta_button_sharings_on_resume_id   (resume_id)
#

class VeetaButtonSharingSerializer < ActiveModel::Serializer
  root false
  attributes :id, :logo_url, :job_type, :created_at, :updated_at,:is_latest, :third_party_name, :company_name,:company, :third_party_link, :resume_name, :resume_id, :access_revoked_at, :changed_since_last_sync?,
  :never_synced?, :deleted_since_last_sync?, :last_synced_at, :third_party_data_wiped_out_at, :connected_at, :third_party_delete_profile_link, :resume_shared?, :latest_shared_resume_id, :latest_shared_resume_name,
  :submitted_at


  def resume_shared?
    !object.veeta_button_sharing_connection.latest_synced_sharing_of_connection.blank? && !object.veeta_button_sharing_connection.latest_synced_sharing_of_connection.resume.blank?
  end

  def latest_shared_resume_id
    object.veeta_button_sharing_connection.latest_synced_sharing_of_connection.resume.id if resume_shared?
  end

  def latest_shared_resume_name
    object.veeta_button_sharing_connection.latest_synced_sharing_of_connection.resume.name if resume_shared?
  end

  def resume_id
    object.resume.id if object.resume
  end

  def resume_name
    object.resume.name if object.resume
  end

  def third_party_name
    object.veeta_button_sharing_connection.client_application.name
  end

  def third_party_link
    object.veeta_button_sharing_connection.client_application.url
  end

  def third_party_delete_profile_link
    object.veeta_button_sharing_connection.client_application.delete_user_profile_deep_link
  end

  def logo_url
    object.veeta_button_sharing_connection.client_application.logo_url
  end

  def company_name
    object.veeta_button_sharing_connection.client_application.name
  end

  def client_application
    object.veeta_button_sharing_connection.client_application
  end

  def third_party_data_wiped_out_at
    object.veeta_button_sharing_connection.third_party_data_wiped_out_at
  end

  def connected_at
    object.veeta_button_sharing_connection.connected_at
  end

  def submitted_at
    object.veeta_button_sharing_connection.connected_at
  end


  alias_method :company, :client_application

  has_one :company, serializer: ClientApplicationSerializer


end
