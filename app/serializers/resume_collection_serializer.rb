class ResumeCollectionSerializer < ActiveModel::Serializer
  attributes :id, :name, :image_snapshot_url, :created_at, :updated_at

  has_many :shared_with, serializer: ResumeCompanySerializer, root: :shared_with
  has_many :latest_sharings
  has_one :pdf_template_setting, serializer: PdfTemplateSettingSerializer

end
