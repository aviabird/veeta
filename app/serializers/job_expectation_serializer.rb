class JobExpectationSerializer < ActiveModel::Serializer
  attributes :job_seeker_status, :availability, :working_industries, :working_locations, :prefered_employment_type
end
