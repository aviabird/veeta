class TempFileSerializer < ActiveModel::Serializer
  attributes :filename
end
