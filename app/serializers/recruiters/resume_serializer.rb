module Recruiters
  class ResumeSerializer < ActiveModel::Serializer
    attributes :id, :language, :name, :deleted_at, :created_at, :updated_at,
               :work_experience_duration, :international_work_experience_durations

    has_many :work_experiences, :educations, :languages, :skills, :documents,
      :memberships, :projects, :awards, :publications, :certifications
    has_one :about, :link
  end
end
