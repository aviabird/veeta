class Recruiters::PoolsSerializer < ActiveModel::Serializer
  attributes :id, :name, :talents_count
end
