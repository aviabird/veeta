class Recruiters::PoolSerializer < ActiveModel::Serializer
  attributes :id, :name, :talents_count
end
