class Recruiters::JobSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :recruiter_id, :assigned_recruiter_id, :language_id, :status,
    :name, :code, :reference, :contact_details, :deleted_at, :created_at, :updated_at,
    :jobapps_count

  has_many :jobapps, serializer: Recruiters::Job::JobappSerializer
end