class Recruiters::JobappsSerializer < ActiveModel::Serializer
  attributes :id, :resume_id, :job_id, :job_type, :language_id, :cover_letter, :contact_preferences, :blocked_companies, :recruiter_rating,
    :recruiter_note, :talent_info_status, :added_by_recruiter, :application_referrer_url,
    :allow_recruiter_contact_sharing, :company_terms_accepted_at, :company_terms_accepted_ip,
    :withdrawn_at, :withdrawn_reason, :access_revoked_at, :hidden_at, :created_at, :updated_at,
    :is_read

  has_one :resume, serializer: Recruiters::Jobapps::ResumeSerializer

  def is_read
    object.read_at?
  end
end
