module Recruiters
  class TalentsOnShortlistSerializer < ActiveModel::Serializer    
    attributes :id, :first_name, :last_name, :avatar_url
  end
end
