class Recruiters::Jobapps::Resume::TalentSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper

  attributes :id, :first_name, :last_name, :birthdate, :salutation, :title, :email, :phone_number,
             :veeta_terms_accepted_at, :veeta_terms_accepted_ip, :created_at, :updated_at, :avatar_url,
             :nationality_country_id, :address_id, :rating, :company_info_id, :labels, :age,
             :work_experience_duration_in_words

  def rating
    object.company_rating(current_user.recruiter.company)
  end

  def company_info_id
    object.company_info(current_user.recruiter.company).try(:id)
  end

  def labels
    object.labels_from(current_user.recruiter)
  end

  def work_experience_duration_in_words
    wed = object.work_experience_duration
    time_ago_in_words(Time.now - (wed * 3600 * 24 * 365)) if wed
  end
end