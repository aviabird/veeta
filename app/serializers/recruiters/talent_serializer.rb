module Recruiters
  class TalentSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name, :birthdate, :salutation, :title, :email, :phone_number,
               :veeta_terms_accepted_at, :veeta_terms_accepted_ip, :created_at, :updated_at, :avatar_url,
               :nationality_country_id, :address_id, :resumes, :rating, :note,
               :company_info_id, :labels, :in_pools, :applied_for, :first_applied_at

    has_one :nationality_country, serializer: CountrySerializer
    has_one :address, serializer: AddressSerializer
    has_one :job_expectation

    def resumes
      object.resumes.map { |resume| {id: resume.id, name: resume.name, updated_at: resume.updated_at} }
    end

    def rating
      object.company_rating(current_user.recruiter.company)
    end

    def note
      object.note(current_user.recruiter.company)
    end

    def company_info_id
      object.company_info(current_user.recruiter.company).try(:id)
    end

    def labels
      object.labels_from(current_user.recruiter)
    end

    def in_pools
      object.in_pools(current_user.recruiter.company).map{ |p| p.name }
    end

    def applied_for
      object.applied_for_jobs_in(current_user.recruiter.company).map(&:name)
    end

    def first_applied_at
      object.first_applied_for_jobs_in(current_user.recruiter.company).try(:created_at)
    end
  end
end
