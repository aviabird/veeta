module Recruiters
  class TalentsSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name, :age,:work_industries, :latest_work_experience,
               :salutation, :work_experience_duration, :avatar_url,
               :rating, :labels, :company_info_id,  :applied_for_jobs_positions, :is_followed

    def rating
      object.company_rating(current_user.recruiter.company)
    end

    def labels
      object.labels_from(current_user.recruiter)
    end

    def company_info_id
      object.company_info(current_user.recruiter.company).try(:id)
    end

    def applied_for_jobs_positions
      object.applied_for_jobs_positions_in(current_user.recruiter.company)
    end

    def is_followed
      object.followed_by?(current_user.recruiter)
    end
  end
end
