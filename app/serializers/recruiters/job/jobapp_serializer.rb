class Recruiters::Job::JobappSerializer < ActiveModel::Serializer
  attributes :id, :resume_id, :job_id, :job_type, :language_id, :cover_letter, :contact_preferences, :blocked_companies, :recruiter_rating,
    :recruiter_note, :talent_info_status, :added_by_recruiter, :read_at, :application_referrer_url,
    :allow_recruiter_contact_sharing, :company_terms_accepted_at, :company_terms_accepted_ip,
    :withdrawn_at, :withdrawn_reason, :access_revoked_at, :hidden_at, :created_at, :updated_at

  has_one :resume, serializer: Recruiters::Job::Jobapp::ResumeSerializer
end
