class Recruiters::Job::Jobapp::Resume::TalentSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
    
  attributes :id, :first_name, :last_name, :name, :avatar_url, :rating, 
    :labels, :company_info_id, :age, :work_experience_duration_in_words

  def rating
    object.company_rating(current_user.recruiter.company)
  end

  def work_experience_duration_in_words
    wed = object.work_experience_duration
    time_ago_in_words(Time.now - (wed * 3600 * 24 * 365)) if wed
  end

  def labels
    object.labels_from(current_user.recruiter)
  end

  def company_info_id
    object.company_info(current_user.recruiter.company).try(:id)
  end
end