class Recruiters::Job::Jobapp::ResumeSerializer < ActiveModel::Serializer
  attributes :id, :language, :name, :deleted_at, :created_at, :updated_at

  has_one :talent, serializer: Recruiters::Job::Jobapp::Resume::TalentSerializer
end
