class Recruiters::Jobapp::Resume::TalentSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  
  attributes :id, :first_name, :last_name, :birthdate, :salutation, :title, :email, :phone_number,
             :veeta_terms_accepted_at, :veeta_terms_accepted_ip, :created_at, :updated_at, :avatar_url,
             :nationality_country_id, :address_id, :resumes, :rating, :company_info_id, :labels, :applied_for,
             :first_applied_for_time_ago

  has_one :nationality_country, serializer: CountrySerializer
  has_one :address, serializer: AddressSerializer
  has_one :job_expectation

  def resumes
    object.resumes.map { |resume| {id: resume.id, name: resume.name, updated_at: resume.updated_at} }
  end

  def rating
    object.company_rating(current_user.recruiter.company)
  end

  def company_info_id
    object.company_info(current_user.recruiter.company).try(:id)
  end

  def labels
    object.labels_from(current_user.recruiter)
  end

  def applied_for
    object.applied_for_jobs_in(current_user.recruiter.company).map(&:name)
  end

  def first_applied_for_time_ago
    time_ago_in_words(object.first_applied_for_jobs_in(current_user.recruiter.company).created_at)
  end
end