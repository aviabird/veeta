class Recruiters::Jobapp::ResumeSerializer < ActiveModel::Serializer
  attributes :id, :language, :name, :deleted_at, :created_at, :updated_at

  has_many :work_experiences, :educations, :languages, :skills, :documents,
    :memberships, :projects, :awards, :publications, :certifications
  has_one :about, :link

  has_one :talent, serializer: Recruiters::Jobapp::Resume::TalentSerializer
end
