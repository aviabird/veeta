# == Schema Information
#
# Table name: recruiter_talent_infos
#
#  id            :integer          not null, primary key
#  recruiter_id  :uuid
#  talent_id     :uuid
#  followed_at   :datetime
#  hidden_at     :datetime
#  in_pool_since :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_recruiter_talent_infos_on_followed_at    (followed_at)
#  index_recruiter_talent_infos_on_in_pool_since  (in_pool_since)
#  index_recruiter_talent_infos_on_recruiter_id   (recruiter_id)
#  index_recruiter_talent_infos_on_talent_id      (talent_id)
#

class RecruiterTalentInfoSerializer < ActiveModel::Serializer
  attributes :id, :followed_at, :in_pool_since, :created_at, :updated_at

  has_one :recruiter, embed: :ids
  has_one :talent, embed: :ids
end
