# == Schema Information
#
# Table name: cv_sharing_responses
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  cv_sharing_request_id           :uuid             not null
#  cv_sharing_request_type         :string           not null
#  blocked_companies               :text
#  read_at                         :datetime
#  draft                           :boolean          default(TRUE)
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  access_revoked_at               :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  update_only                     :boolean          default(FALSE)
#  revoke_reason                   :string
#  submitted_at                    :datetime
#
# Indexes
#
#  index_cv_sharing_responses_on_access_revoked_at      (access_revoked_at)
#  index_cv_sharing_responses_on_cv_sharing_request_id  (cv_sharing_request_id)
#  index_cv_sharing_responses_on_read_at                (read_at)
#  index_cv_sharing_responses_on_resume_id              (resume_id)
#

class CvSharingResponseSerializer < ActiveModel::Serializer

  #attributes :attributes
  attributes :id, :blocked_companies,
    			 :allow_recruiter_contact_sharing, :job_type,
    			 :created_at, :resume_name, :currently_shared_cv,
           :cv_sharing_request_id, :cv_sharing_request_type, :resume_id,
           :access_revoked_at, :update_only, :recently_blocked_companies,
           :submitted_at

#  alias_method :attributes, :my_atts

  has_one :cv_sharing_request, :company

  has_one :talent_setting

  has_one :currently_shared_cv, serializer: CurrentCvSerializer

  def recently_blocked_companies
    current_user.talent.recently_blocked_companies company.id
  end

  def resume_name
    object.resume.name if object.resume
  end

  def talent_setting
    object.resume.talent.setting if object.resume
  end

  def currently_shared_cv
    object.currently_shared_cv current_user.talent
  end

end
