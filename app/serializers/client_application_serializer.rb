# == Schema Information
#
# Table name: client_applications
#
#  id                            :uuid             not null, primary key
#  name                          :string
#  url                           :string
#  support_url                   :string
#  callback_url                  :string
#  key                           :string(40)
#  secret                        :string(40)
#  user_id                       :uuid
#  created_at                    :datetime
#  updated_at                    :datetime
#  webhook_url                   :string
#  delete_user_profile_deep_link :string
#  logo                          :string
#  cancel_url                    :string
#  use_cancel_callback           :boolean          default(TRUE)
#  contact                       :string
#  deleted_at                    :datetime
#  token                         :string
#  logo_meta                     :text
#
# Indexes
#
#  index_client_applications_on_created_at  (created_at)
#  index_client_applications_on_key         (key) UNIQUE
#  index_client_applications_on_token       (token)
#

class ClientApplicationSerializer < ActiveModel::Serializer
  attributes :id, :name, :logo_url, :contact

end
