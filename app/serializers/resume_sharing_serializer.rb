# == Schema Information
#
# Table name: resume_sharings
#
#  id                   :integer          not null, primary key
#  resume_id            :uuid             not null
#  company_id           :uuid
#  recruiter_id         :uuid
#  access_revoked_at    :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  revoke_reason        :string
#  email_application_id :uuid
#
# Indexes
#
#  index_resume_sharings_on_access_revoked_at  (access_revoked_at)
#  index_resume_sharings_on_company_id         (company_id)
#  index_resume_sharings_on_recruiter_id       (recruiter_id)
#  index_resume_sharings_on_resume_id          (resume_id)
#

class ResumeSharingSerializer < ActiveModel::Serializer
 attributes :id, :resume_id, :origin_id, :company_id, :company,
 :access_revoked_at, :updated_at, :is_latest, :name, :company_name, :company_or_email_application_company_id, :results_from, :origin ,:email_application

  def company
    comp = object.company
    if comp.present?
      { id: comp.id, logo_url: {medium: "#{comp.logo_url[:medium] if comp.logo_url}"}, custom_company_type: comp.custom_company_type }
    end
  end
   
 def company_name
   if object.email_application.nil?
     object.company.name
   else
     object.email_application.company_name
   end
 end

 def email_application
   { id: object.email_application.id } if object.email_application.present?
 end
 
 def company_or_email_application_company_id
   if object.company.nil?
     object.email_application.id
   else
     object.company.id
   end
 end

 def name
   object.resume.name
 end

 def origin_id
   object.resume.origin.id unless object.resume.deleted?
 end

 def origin
   object.resume.origin.updated_at  unless object.resume.deleted?
 end
end