class CvSharingRequestLandingPageSerializer < ActiveModel::Serializer
  attributes :id, :ext_id, :company, :pool_welcome_text, :language

  def company
    comp = object.company
    { blocking_notice: comp.blocking_notice, custom_company_type: comp.custom_company_type, logo_url: {medium: "#{comp.logo_url[:medium] if comp.logo_url}"}, name: comp.name} if comp.present?
  end

  def language
    {origin: {iso6391: object.language.iso_639_1}} if object.language.present?
  end
end