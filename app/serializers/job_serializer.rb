# == Schema Information
#
# Table name: jobs
#
#  id                    :uuid             not null, primary key
#  company_id            :uuid             not null
#  recruiter_id          :uuid
#  assigned_recruiter_id :uuid
#  language_id           :integer          not null
#  status                :string
#  name                  :string           not null
#  code                  :string
#  reference             :string
#  contact_details       :text
#  deleted_at            :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  ext_id                :string
#
# Indexes
#
#  index_jobs_on_company_id    (company_id)
#  index_jobs_on_created_at    (created_at)
#  index_jobs_on_ext_id        (ext_id)
#  index_jobs_on_name          (name)
#  index_jobs_on_recruiter_id  (recruiter_id)
#  index_jobs_on_status        (status)
#

class JobSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :recruiter_id, :status, :name,
    :jobname, :company_name, :phone_number, :recruiter_email,
    :recruiter_name, :language, :available, :unsolicited_job, :has_custom_referrers,
    :unsolicited, :redirect_link, :contact_details, :ext_id

  has_one :company, :recruiter
  has_many :accepted_jobs_languages, :tags

  def jobname
    name
  end

  def company_name
    object.company.name if object.company
  end

  def phone_number
    object.recruiter.phone_number if object.recruiter
  end

  def recruiter_email
    object.recruiter.email if object.recruiter
  end

  def recruiter_name
    object.recruiter.name if object.recruiter
  end

  def available
    object.available?
  end

  def has_custom_referrers
    object.try(:company).try(:referrers).present?
  end

  def unsolicited
    unsolicited_job
  end
end
