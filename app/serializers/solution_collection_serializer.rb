class SolutionCollectionSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
