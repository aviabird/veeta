# == Schema Information
#
# Table name: rs_documents
#
#  id            :uuid             not null, primary key
#  resume_id     :uuid
#  file          :string
#  document_type :string
#  created_at    :datetime
#  updated_at    :datetime
#  name          :string
#  file_type     :string
#  token         :string
#
# Indexes
#
#  index_rs_documents_on_created_at  (created_at)
#  index_rs_documents_on_resume_id   (resume_id)
#  index_rs_documents_on_token       (token)
#

class Rs::DocumentSerializer < ActiveModel::Serializer
  attributes :id, :name, :file_type, :document_type, :file
end
