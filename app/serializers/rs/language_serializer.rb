# == Schema Information
#
# Table name: rs_languages
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  language_id :integer
#  level       :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_languages_on_created_at   (created_at)
#  index_rs_languages_on_language_id  (language_id)
#  index_rs_languages_on_level        (level)
#  index_rs_languages_on_resume_id    (resume_id)
#

class Rs::LanguageSerializer < ActiveModel::Serializer
  attributes :id, :level, :language_id, :hello_by_iso

  has_one :language

  def hello_by_iso
    Language::Hello.by_iso(object.iso_code)
  end
end
