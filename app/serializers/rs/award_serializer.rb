# == Schema Information
#
# Table name: rs_awards
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  name        :string
#  occupation  :string
#  awarded_by  :string
#  year        :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_awards_on_created_at  (created_at)
#  index_rs_awards_on_resume_id   (resume_id)
#

class Rs::AwardSerializer < ActiveModel::Serializer
  attributes :id, :name, :occupation, :awarded_by, :year, :description
end
