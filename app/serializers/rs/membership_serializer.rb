# == Schema Information
#
# Table name: rs_memberships
#
#  id           :uuid             not null, primary key
#  resume_id    :uuid
#  organization :string
#  role         :string
#  area         :string
#  from         :datetime
#  to           :datetime
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_rs_memberships_on_created_at  (created_at)
#  index_rs_memberships_on_resume_id   (resume_id)
#

class Rs::MembershipSerializer < ActiveModel::Serializer
  attributes :id, :organization, :role, :area, :from, :to
end
