# == Schema Information
#
# Table name: rs_educations
#
#  id                :uuid             not null, primary key
#  resume_id         :uuid             not null
#  type_of_education :string
#  school_name       :string
#  degree            :string
#  subject           :string
#  country_id        :integer
#  from              :date
#  to                :date
#  description       :text
#  created_at        :datetime
#  updated_at        :datetime
#  completed         :boolean
#
# Indexes
#
#  index_rs_educations_on_country_id         (country_id)
#  index_rs_educations_on_created_at         (created_at)
#  index_rs_educations_on_from               (from)
#  index_rs_educations_on_resume_id          (resume_id)
#  index_rs_educations_on_school_name        (school_name)
#  index_rs_educations_on_subject            (subject)
#  index_rs_educations_on_to                 (to)
#  index_rs_educations_on_type_of_education  (type_of_education)
#

class Rs::EducationSerializer < ActiveModel::Serializer
  attributes :id, :type_of_education, :school_name, :degree, :subject, :from, :to, :description, :current, :country_id, :is_valid, :completed

  has_one :country

  def is_valid
    object.valid?
  end
end
