# == Schema Information
#
# Table name: rs_links
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid             not null
#  linkedin   :string
#  xing       :string
#  website    :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_rs_links_on_created_at  (created_at)
#

class Rs::LinkSerializer < ActiveModel::Serializer
  attributes :id, :linkedin, :xing, :website
end
