# == Schema Information
#
# Table name: rs_skills
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid
#  name       :string
#  level      :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_rs_skills_on_created_at  (created_at)
#  index_rs_skills_on_level       (level)
#  index_rs_skills_on_name        (name)
#  index_rs_skills_on_resume_id   (resume_id)
#

class Rs::SkillSerializer < ActiveModel::Serializer
  attributes :id, :name, :level
end
