# == Schema Information
#
# Table name: rs_certifications
#
#  id                 :uuid             not null, primary key
#  resume_id          :uuid
#  subject            :string
#  organization       :string
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#  certification_type :string
#  year               :integer
#
# Indexes
#
#  index_rs_certifications_on_created_at  (created_at)
#  index_rs_certifications_on_resume_id   (resume_id)
#

class Rs::CertificationSerializer < ActiveModel::Serializer
  attributes :id, :certification_type, :subject, :organization, :year, :description, :is_valid
  def is_valid
    object.valid?
  end
end
