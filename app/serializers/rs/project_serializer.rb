# == Schema Information
#
# Table name: rs_projects
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  name        :string
#  role        :string
#  link        :string
#  from        :datetime
#  to          :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_projects_on_created_at  (created_at)
#  index_rs_projects_on_resume_id   (resume_id)
#

class Rs::ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :role, :link, :from, :to, :description
end
