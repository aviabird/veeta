# == Schema Information
#
# Table name: rs_work_experiences
#
#  id                  :uuid             not null, primary key
#  resume_id           :uuid             not null
#  position            :string
#  company             :string
#  industry            :string
#  job_level           :string
#  terms_of_employment :string
#  from                :datetime
#  to                  :datetime
#  country_id          :integer
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#
# Indexes
#
#  index_rs_work_experiences_on_company     (company)
#  index_rs_work_experiences_on_country_id  (country_id)
#  index_rs_work_experiences_on_created_at  (created_at)
#  index_rs_work_experiences_on_from        (from)
#  index_rs_work_experiences_on_industry    (industry)
#  index_rs_work_experiences_on_position    (position)
#  index_rs_work_experiences_on_resume_id   (resume_id)
#  index_rs_work_experiences_on_to          (to)
#

class Rs::WorkExperienceSerializer < ActiveModel::Serializer
  attributes :id, :position, :company, :industry, :job_level, :terms_of_employment, :from, :to, :current, :description, :country_id, :is_valid

  has_one :resume, embed: :id
  has_one :country

  def is_valid
    object.valid?
  end
end
