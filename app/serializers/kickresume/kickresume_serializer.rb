class KickresumeSerializer < ActiveModel::Serializer
  include ApplicationHelper
  attributes :cv, :avatarUrl

  def cv
    cv = {}
    cv[:sections] = sections
    cv[:settings] = settings
    cv[:general] = general
    cv
  end

  def avatarUrl
    talent = object.talent
    # KR cannot access local file storage web dav service
    if !Rails.env.development? && talent.avatar && talent.avatar_url && talent.avatar_url[:medium] && object.file_exists?(talent.avatar_url[:medium])
      mapping = AccessSecurityTokenMapping.find_kickresume_mapping talent
      if mapping
        "#{ENV['HOST']}#{talent.parpare_external_path(talent.avatar.medium.path,mapping.individual_key)}"
      else
        talent.avatar_url[:medium]
      end
    else
      ''
    end
  end

  def general
    talent = object.talent
    address = talent.address
    custom_settings = JSON.parse(object.pdf_template_setting.custom_render_settings)

    general = {}
    general[:name] = I18n.t('talent.kickresume.general.personal_data').html_safe
    general[:icon] = "icon-person"
    general[:avatarShown] = !Rails.env.development? && talent.avatar && talent.avatar_url && talent.avatar_url[:medium] && object.file_exists?(talent.avatar_url[:medium])

    general[:frontTitle] = talent.title
    general[:firstNameKey] = I18n.t('talent.kickresume.general.first_name_key').html_safe
    general[:firstNameValue] = talent.first_name
    general[:lastNameKey] = I18n.t('talent.kickresume.general.last_name_key').html_safe
    general[:lastNameValue] = talent.last_name
    general[:backTitle] = ''
    general[:emailKey] = I18n.t('talent.kickresume.general.email_key').html_safe
    general[:emailValue] = talent.email
    general[:phoneKey] = I18n.t('talent.kickresume.general.phone_key').html_safe
    general[:phoneValue] = talent.phone_number
    general[:birthKey] = I18n.t('talent.kickresume.general.birth_key').html_safe
    general[:birthDate] = talent.birthdate ? birthdate_helper(talent.birthdate, talent.locale) : ''

    # address line workaround
    general[:street] = address_line custom_settings
    unless custom_settings['custom_address_line']
      general[:postal] = address.zip
      general[:city] = address.city
      general[:country] = address.country ? "#{iso_code_translator(address.country.name)}" : ''
    end
    general[:nationalityKey] = I18n.t('talent.kickresume.general.nationality_key').html_safe
    general[:nationality] = talent.nationality_country ? iso_code_translator(talent.nationality_country.name) : ''
    general[:addressKey] = I18n.t('talent.kickresume.general.address_key').html_safe

    #possibly filled with content of Summary module - set to empty values cause otherwise the PDF includes an empty section
    general[:descriptionKey] = ''
    general[:description] = ''

    general
  end

  def settings
    settings = {}
    pdf_template_settings = object.safe_pdf_template_setting
    settings[:color] = pdf_template_settings.color
    settings[:fontFace] = pdf_template_settings.font_face
    settings[:name] =  pdf_template_settings.name
    settings[:pageFormat] = pdf_template_settings.page_format.downcase
    settings[:format] = pdf_template_settings.format
    settings[:lineHeight] = pdf_template_settings.line_height
    settings[:template] = pdf_template_settings.template_identifier
    settings
  end

  def sections
    sections = []
    #LogService.call "KR-Serializer", {resource: object, details: "sections-10",priority: :low}
    sections << work_experiences unless object.work_experiences.nil?  || object.work_experiences.empty?
    #LogService.call "KR-Serializer", {resource: object, details: "sections-20",priority: :low}
    sections << educations unless object.educations.nil?  || object.educations.empty?
    #LogService.call "KR-Serializer", {resource: object, details: "sections-30",priority: :low}
    sections << certifications unless object.certifications.nil? || object.certifications.empty?
    #LogService.call "KR-Serializer", {resource: object, details: "sections-40",priority: :low}
    sections << skills unless object.skills.empty?
    sections << languages unless object.languages.empty?
    #LogService.call "KR-Serializer", {resource: object, details: "sections-50",priority: :low}
    sections << about unless object.about.nil? || object.about.text.blank?
    #LogService.call "KR-Serializer", {resource: object, details: "sections-60",priority: :low}
    sections << online_resources  unless object.link.nil? || (object.link.linkedin.blank? && object.link.xing.blank? && object.link.website.blank?)
    #LogService.call "KR-Serializer", {resource: object, details: "sections-70",priority: :low}
    sections
  end

  private
    def work_experiences
      ac = ApplicationController.new()
      custom_settings = JSON.parse(object.pdf_template_setting.custom_render_settings)
      work_experiences = {}
      work_experiences[:name] = I18n.t('talent.kickresume.sections.work_experiences').html_safe
      work_experiences[:type] = "experience"
      work_experiences[:icon] = "ion-briefcase"
      work_experiences[:data] = {}
      work_experiences[:data][:type] = "work"
      #iterate work experiences
      work_experiences[:data][:entries] = []
      object.work_experiences.from_start.each  do |_experience|
        experience = {}
        experience[:current] = _experience[:to].nil?
        experience[:endDate] = []
        experience[:endDate] = [_experience[:to].month.to_s, _experience[:to].year.to_s] unless _experience[:to].nil?
        experience[:company] = _experience.company
        experience[:country] = iso_code_translator(_experience.country.name) if _experience.country
        experience[:jobTitle] = _experience.position
        experience[:startDate] = []
        experience[:startDate] = [_experience[:from].month.to_s, _experience[:from].year.to_s] unless _experience[:from].nil?
        experience[:description] = _experience.description

        if object.pdf_template_setting && object.pdf_template_setting.template_identifier && File.exist?(Rails.root.join('app', 'views',"pdfs/kickresume/partials/work_experiences_custom/#{object.pdf_template_setting.template_identifier}.html.erb"))
          we_custom_template = "pdfs/kickresume/partials/work_experiences_custom/#{object.pdf_template_setting.template_identifier}.html.erb"
        else
          we_custom_template = "pdfs/kickresume/partials/work_experiences_custom/default.html.erb"
        end
        partial = ac.render_to_string(we_custom_template, layout: false, :locals => {:_experience => _experience})

        experience[:description] = partial
        # jobTitle <-> company workaround
        if custom_settings['switch_company_job_title']
          jobTitle = experience[:jobTitle]
          experience[:jobTitle] = experience[:company]
          experience[:company] = jobTitle
        end
        work_experiences[:data][:entries] << experience
      end unless object.work_experiences.nil?
      work_experiences
    end

    def educations
      ac = ApplicationController.new()
      custom_settings = JSON.parse(object.pdf_template_setting.custom_render_settings)
      educations = {}
      educations[:name] = I18n.t('talent.kickresume.sections.educations').html_safe
      educations[:data] = {}
      educations[:type] = "experience"
      educations[:icon] = "ion-university"
      educations[:data] = {}
      educations[:data][:type] = "work"
      #iterate work experiences
      educations[:data][:entries] = []
      object.educations.from_start.each  do |_education|
        education = {}
        education[:current] = _education[:to].nil?
        education[:endDate] = []
        education[:endDate] = [_education[:to].month, _education[:to].year] unless _education[:to].nil?
        education[:company] = _education.school_name
        education[:country] = iso_code_translator(_education.country.name) if _education.country
        education[:jobTitle] = _education.subject
        education[:startDate] = []
        education[:startDate] = [_education[:from].month, _education[:from].year] unless _education[:from].nil?
        additional_info = ''
        unless _education.type_of_education.blank?
          typ = I18n.t("talent.data.education.#{_education.type_of_education}")
          additional_info = "#{additional_info}#{typ}"
        end
        unless _education.degree.blank?
          additional_info = "#{additional_info}, " unless additional_info.blank?
          deg = I18n.t('talent.pdf.education.degree', degree: _education.degree)
          additional_info = "#{additional_info}#{deg}"
        end
        unless _education.completed.blank?
          additional_info = "#{additional_info}, " unless additional_info.blank?
          additional_info = "#{additional_info}#{I18n.t('talent.pdf.education.completed')}"
        end
        if object.pdf_template_setting && object.pdf_template_setting.template_identifier && File.exist?(Rails.root.join('app', 'views',"pdfs/kickresume/partials/educations_custom/#{object.pdf_template_setting.template_identifier}.html.erb"))
          edu_custom_template = "pdfs/kickresume/partials/educations_custom/#{object.pdf_template_setting.template_identifier}.html.erb"
        else
          edu_custom_template = "pdfs/kickresume/partials/educations_custom/default.html.erb"
        end
        partial = ac.render_to_string(edu_custom_template, layout: false, :locals => {:additional_info => additional_info.html_safe, :_education => _education})
        education[:description] =  partial

        # jobTitle <-> company workaround
        if custom_settings['switch_company_job_title']
          jobTitle = education[:jobTitle]
          education[:jobTitle] = education[:company]
          education[:company] = jobTitle
          if education[:company].blank?
            education[:company] = education[:jobTitle]
            education[:jobTitle] = ''
          end
        else # subject is empty
          if education[:jobTitle].blank?
            education[:jobTitle] = education[:company]
            education[:company] = ''
          end
        end

        educations[:data][:entries] << education
      end unless object.educations.nil?
      educations
    end

    def languages
      custom_settings = JSON.parse(object.pdf_template_setting.custom_render_settings)
      languages = {}
      languages[:name] =  I18n.t('talent.kickresume.sections.languages').html_safe
      languages[:type] = "hobby"
      languages[:icon] = "ion-chatbubbles"
      languages[:data] = {}
      languages[:data][:entries] = []

      #languages
      unless  object.languages.nil?
        object.languages.sort_by(&:sort_helper).each do |_l|
          l = {}
          l[:name] = _l.language.name
          l[:icon] = 'ion-ios-chatbubble-outline'
          if _l.level_key != 'undefined'
            if custom_settings['split_language_levels']
              level_label = I18n.t("talent.kickresume.sections.language_level.#{_l.level_key}")
            else
              level_label = I18n.t("talent.data.language_level.#{_l.level_key}")
            end
            l[:name] = "#{l[:name]} (#{level_label})"
          end
          languages[:data][:entries] << l
        end
      end
      languages
    end

    def skills
      custom_settings = JSON.parse(object.pdf_template_setting.custom_render_settings)
      skills = {}
      skills[:name] =  I18n.t('talent.kickresume.sections.skills').html_safe
      skills[:type] = "skill"
      skills[:icon] = "ion-star"
      skills[:data] = []


      #skills
      skill_set = []

      unless  object.skills.nil?
        split_at = (object.skills.size - 1) / custom_settings['no_of_skill_categories']
        category_no = 0

        skill_set = {}
        skill_set[:name] = ''
        skill_set[:skills] = []
        elements = 0
        object.skills.pdf_sorting_order.each do |_s|
          if elements > split_at
            elements = 0
            skills[:data] << skill_set
            skill_set = {}
            skill_set[:name] = ''
            skill_set[:skills] = []
          end

          s = {}
          s[:skillLevel] = (_s.level > 1 ? _s.level + 1 : _s.level) if  _s.level
          s[:skillLevel] = 0 if _s.level.blank?
          s[:name] = _s.name
          s[:shown] = 'level'
          if _s.level_key != 'undefined'
            s[:description] = I18n.t("talent.data.skill_level.#{_s.level_key}")
          end
          skill_set[:skills] << s
          elements = elements + 1
        end
        skills[:data] << skill_set
      end
      skills
    end

    def certifications
      certifications = {}
      certifications[:name] =  I18n.t('talent.kickresume.sections.certifications').html_safe
      certifications[:type] = 'publication'
      certifications[:icon] = 'ion-ribbon-b'
      certifications[:data] = {}
      #iterate certifications
      certifications[:data][:entries] = []
      object.certifications.sort{|a,b| a.year <=> b.year}.reverse.each  do |_certification|
        certification = {}

        certification[:date] = ['', _certification.year]
        certification[:publisher] = _certification.organization
        certification[:name] = _certification.subject
        certification[:description] = html_encode(_certification.description).html_safe

        certifications[:data][:entries] << certification
      end unless object.certifications.nil?
      certifications
    end

    def about
      about = {}
      about[:name] =  I18n.t('talent.kickresume.sections.about').html_safe
      about[:type] = 'text'
      about[:icon] = 'ion-bookmark'
      about[:data] = "<div style='width:100% !important'>#{html_encode object.about.text}</div>"
      about
    end

    def online_resources
      online_resources = {}
      online_resources[:name] =  I18n.t('talent.kickresume.sections.online_resources').html_safe
      online_resources[:type] = 'text'
      online_resources[:icon] = 'ion-link'
      online_resources[:data] = ''
      unless object.link.linkedin.blank?
        online_resources[:data] = "#{I18n.t('talent.kickresume.sections.online_resources_linkedin').html_safe} <a href='#{html_encode(link_fixer object.link.linkedin)}' target='_blank' style='font-size:12px;font-family:inherit;text-decoration:none'>#{html_encode(link_fixer object.link.linkedin)}</a>"
      end
      unless object.link.xing.blank?
        online_resources[:data] = "#{online_resources[:data]}<br/>" unless online_resources[:data].blank?
        online_resources[:data] = "#{online_resources[:data]}#{I18n.t('talent.kickresume.sections.online_resources_xing').html_safe} <a href='#{html_encode(link_fixer object.link.xing)}' target='_blank' style='font-size:12px;font-family:inherit;text-decoration:none'>#{html_encode(link_fixer object.link.xing)}</a>"
      end
      unless object.link.website.blank?
        online_resources[:data] = "#{online_resources[:data]}<br/>" unless online_resources[:data].blank?
        online_resources[:data] = "#{online_resources[:data]}#{I18n.t('talent.kickresume.sections.online_resources_website').html_safe} <a href='#{html_encode(link_fixer object.link.website)}' target='_blank' style='font-size:12px;font-family:inherit;text-decoration:none'>#{html_encode(link_fixer object.link.website)}</a>"
      end
      online_resources
    end

    private
      def link_fixer link
        unless link && link.starts_with?('http')
          link = "http://#{link}"
        end
        link
      end

      def address_line custom_settings
        address = object.talent.address
        if address && custom_settings['custom_address_line']
          address_line = []
          address_line << address.address_line if address.address_line
          address_line << "#{address.zip} #{address.city}".strip if address.zip || address.city
          address_line << (address.country ? "#{iso_code_translator(address.country.name)}" : '') if address.country
          return address_line.join(', ')
        elsif address
          return address.address_line
        end
      end

end
