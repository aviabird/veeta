class TalentAvatarSerializer < ActiveModel::Serializer
  attributes :avatar_url
end
