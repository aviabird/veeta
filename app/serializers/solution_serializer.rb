class SolutionSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
