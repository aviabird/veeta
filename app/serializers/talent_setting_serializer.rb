# == Schema Information
#
# Table name: talent_settings
#
#  id                            :uuid             not null, primary key
#  talent_id                     :uuid             not null
#  additional_settings           :text
#  country_id                    :integer
#  professional_experience_level :integer
#  newsletter_locale             :string
#  newsletter_updates            :string
#  working_locations             :text
#  working_industries            :text
#  availability                  :string
#  job_seeker_status             :string           default("very_much")
#  prefered_employment_type      :string
#  salary_exp_min                :integer
#  salary_exp_max                :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
# Indexes
#
#  index_talent_settings_on_created_at  (created_at)
#  index_talent_settings_on_talent_id   (talent_id)
#

class TalentSettingSerializer < ActiveModel::Serializer
  attributes :id,
             :country_id,
             :professional_experience_level,
             :resume_simplified_type,
             :newsletter_locale,
             :newsletter_updates,
             :working_locations,
             :working_industries,
             :availability,
             :job_seeker_status,
             :salary_exp_min,
             :salary_exp_max,
             :prefered_employment_type
end
