# == Schema Information
#
# Table name: cv_sharing_requests
#
#  id                :uuid             not null, primary key
#  company_id        :uuid             not null
#  recruiter_id      :uuid
#  pool_id           :uuid
#  language_id       :integer          not null
#  status            :string
#  blocking_notice   :boolean
#  contact_details   :text
#  ext_id            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  pool_welcome_text :string
#  redirect_link     :string
# Indexes
#
#  index_cv_sharing_requests_on_company_id   (company_id)
#  index_cv_sharing_requests_on_ext_id       (ext_id)
#  index_cv_sharing_requests_on_language_id  (language_id)
#  index_cv_sharing_requests_on_pool_id      (pool_id)
#

class CvSharingRequestSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :recruiter_id, :status, :pool_welcome_text,
     :company_name, :phone_number, :recruiter_email, :recruiter_name, :language, :available, :redirect_link,
     :ext_id

  has_one :company, :recruiter

  def company_name
    object.company.name if object.company
  end

  def phone_number
    object.recruiter.phone_number if object.recruiter
  end

  def recruiter_email
    object.recruiter.email if object.recruiter
  end

  def recruiter_name
    object.recruiter.name if object.recruiter
  end

  def available
    true
  end
end
