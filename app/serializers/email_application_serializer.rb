# == Schema Information
#
# Table name: email_applications
#
#  id              :uuid             not null, primary key
#  company_id      :uuid
#  recruiter_id    :uuid
#  company_name    :string
#  jobname         :string
#  recruiter_email :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  draft           :boolean          default(TRUE)
#  unsolicited     :boolean
#
# Indexes
#
#  index_email_applications_on_company_id       (company_id)
#  index_email_applications_on_created_at       (created_at)
#  index_email_applications_on_recruiter_email  (recruiter_email)
#  index_email_applications_on_recruiter_id     (recruiter_id)
#

class EmailApplicationSerializer < ActiveModel::Serializer
  attributes :id, :company_name, :jobname, :recruiter_email, :company_id, :recruiter_id,
    :name, :phone_number, :recruiter_name, :unsolicited

  has_one :company, :recruiter

  def name
    jobname
  end

  def company_name
    object.company ? object.company.name : object.company_name
  end

  def recruiter_email
    object.recruiter ? object.recruiter.email : object.recruiter_email
  end

  def phone_number
    object.recruiter.phone_number if object.recruiter
  end

  def recruiter_name
    object.recruiter.name if object.recruiter
  end
end
