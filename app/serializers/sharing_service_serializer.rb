class SharingServiceSerializer < ActiveModel::Serializer
  attributes :applied_at

  has_one :company
  has_many :resumes
end