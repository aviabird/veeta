class AddressSerializer < ActiveModel::Serializer
  attributes :id, :city, :zip, :address_line, :address_line_2, :country_id

  has_one :country, serializer: CountrySerializer
end
