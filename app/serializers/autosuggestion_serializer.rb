class AutosuggestionSerializer < ActiveModel::Serializer
  attributes :text
end