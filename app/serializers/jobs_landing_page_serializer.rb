class JobsLandingPageSerializer < ActiveModel::Serializer
  attributes :id, :name, :ext_id, :unsolicited_job, :language, :company, :jobname

  def jobname
    name
  end

  def company
    comp = object.company
    {logo_url: {medium: "#{comp.logo_url[:medium] if comp.logo_url}"}, name: comp.name} if comp.present?
  end

  def language
    {origin: {iso6391: object.language.iso_639_1}} if object.language.present?
  end
end