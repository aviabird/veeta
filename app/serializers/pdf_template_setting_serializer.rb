# == Schema Information
#
# Table name: pdf_template_settings
#
#  id                     :uuid             not null, primary key
#  ext_id                 :integer
#  pdf_template_type      :string
#  color                  :string
#  font_face              :string
#  name                   :string
#  page_format            :string
#  format                 :integer
#  line_height            :string
#  template               :string
#  resume_id              :uuid
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  last_generated_at      :datetime
#  template_identifier    :string
#  custom_render_settings :text
#

class PdfTemplateSettingSerializer < ActiveModel::Serializer
  attributes :id, :pdf_template_type, :color, :template
end
