# == Schema Information
#
# Table name: resume_sharing_requests
#
#  id           :uuid             not null, primary key
#  email        :string
#  contact_name :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  resume_id    :uuid             not null
#
# Indexes
#
#  index_resume_sharing_requests_on_created_at  (created_at)
#  index_resume_sharing_requests_on_email       (email)
#

class ResumeSharingRequestSerializer < ActiveModel::Serializer
  root false
  attributes :email, :contact_name, :job_type, :created_at, :origin_resume_id, :origin_resume, :resume_id, :resume_name, :submitted_at

  def resume_id
    object.resume.id
  end

  def resume_name
    object.resume.name
  end

  def origin_resume_id
    object.origin_resume.try(:id)
  end

  def origin_resume
    object.origin_resume
  end

  def submitted_at
    object.created_at
  end

end
