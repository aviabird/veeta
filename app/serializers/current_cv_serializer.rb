# == Schema Information
#
# Table name: resumes
#
#  id                       :uuid             not null, primary key
#  talent_id                :uuid             not null
#  language_id              :integer          not null
#  name                     :string           not null
#  deleted_at               :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  work_experience_duration :float
#  origin_id                :uuid
#  version                  :integer          default(1)
#  origin_updated_at        :datetime
#  pdf                      :string
#  token                    :string
#
# Indexes
#
#  index_resumes_on_created_at   (created_at)
#  index_resumes_on_deleted_at   (deleted_at)
#  index_resumes_on_language_id  (language_id)
#  index_resumes_on_name         (name)
#  index_resumes_on_talent_id    (talent_id)
#

class CurrentCvSerializer < ActiveModel::Serializer
  attributes :id, :name, :created_at, :updated_at

  has_one :talent_setting

  def talent_setting
    object.talent.setting if object
  end

end
