# == Schema Information
#
# Table name: talents
#
#  id                      :uuid             not null, primary key
#  first_name              :string
#  last_name               :string
#  birthdate               :datetime
#  avatar                  :string
#  salutation              :string
#  avatar_meta             :text
#  nationality_country_id  :integer
#  title                   :string
#  email                   :string           not null
#  phone_number            :string
#  address_id              :uuid
#  veeta_terms_accepted_at :datetime
#  veeta_terms_accepted_ip :string
#  locale                  :string
#  deleted_at              :datetime
#  delete_reason           :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  ext_id                  :string
#  new                     :boolean          default(TRUE)
#  token                   :string
#  military_service        :string
#
# Indexes
#
#  index_talents_on_avatar                  (avatar)
#  index_talents_on_birthdate               (birthdate)
#  index_talents_on_created_at              (created_at)
#  index_talents_on_deleted_at              (deleted_at)
#  index_talents_on_email                   (email)
#  index_talents_on_first_name              (first_name)
#  index_talents_on_last_name               (last_name)
#  index_talents_on_nationality_country_id  (nationality_country_id)
#  index_talents_on_salutation              (salutation)
#  index_talents_on_token                   (token)
#

class TalentSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :birthdate, :salutation, :title, :email,
             :phone_number, :avatar_url, :guest, :confirmed, :nationality_country_id,
             :address_id, :uid, :new, :is_confirmed, :locale,
             :military_service, :intercom_user_id, :intercom_user_hash


  has_one :nationality_country, serializer: CountrySerializer
  has_one :address, serializer: AddressSerializer
  has_one :job_expectation

  def guest
    object.guest?
  end

  def email
    if object.email.ends_with?('@tmp.myveeta.com')
      ''
    else
      object.email
    end
  end

  def confirmed
    object.confirmed?
  end

end
