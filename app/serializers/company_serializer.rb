# == Schema Information
#
# Table name: companies
#
#  id                      :uuid             not null, primary key
#  owner_id                :uuid
#  name                    :string           not null
#  tnc_text                :text
#  tnc_link                :string
#  veeta_terms_accepted_at :datetime
#  veeta_terms_accepted_ip :string
#  veeta_terms_accepted_by :string
#  plan                    :string
#  deleted_at              :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  contact                 :text
#  logo                    :string
#  blocking_notice         :boolean          default(TRUE)
#  token                   :string
#  logo_meta               :text
#  short_name              :string
#
# Indexes
#
#  index_companies_on_created_at  (created_at)
#  index_companies_on_deleted_at  (deleted_at)
#  index_companies_on_name        (name)
#  index_companies_on_owner_id    (owner_id)
#  index_companies_on_short_name  (short_name) UNIQUE
#  index_companies_on_token       (token)
#

class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :tnc_text, :tnc_link, :logo_url, :contact, :blocking_notice, :referrers, :custom_company_type, :contact

  has_one :recruiter
  has_many :referrers , Serializer:ReferrerSerializer
  
  def referrers
    object.referrers
  end

end
