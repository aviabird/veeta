# == Schema Information
#
# Table name: resume_templates
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  preview    :string
#  html       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_resume_templates_on_created_at  (created_at)
#  index_resume_templates_on_name        (name)
#

class ResumeTemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :preview_url
end
