class ResumeCompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :logo_url
end
