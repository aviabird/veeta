class SimpleJobSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :name,
    :jobname, :company_name, :language, :available, :unsolicited_job, :jobname_addition

  def jobname
    name
  end

  def company_name
    object.company.name if object.company
  end

  def phone_number
    object.recruiter.phone_number if object.recruiter
  end

  def recruiter_email
    object.recruiter.email if object.recruiter
  end

  def recruiter_name
    object.recruiter.name if object.recruiter
  end

  def available
    object.available?
  end
end
