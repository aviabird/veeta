# == Schema Information
#
# Table name: talent_auths
#
#  id                              :uuid             not null, primary key
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  reset_password_token            :string
#  reset_password_sent_at          :datetime
#  remember_created_at             :datetime
#  sign_in_count                   :integer          default(0), not null
#  current_sign_in_at              :datetime
#  last_sign_in_at                 :datetime
#  current_sign_in_ip              :string
#  last_sign_in_ip                 :string
#  confirmation_token              :string
#  confirmed_at                    :datetime
#  confirmation_sent_at            :datetime
#  unconfirmed_email               :string
#  tokens                          :text
#  provider                        :string
#  uid                             :string           default(""), not null
#  guest                           :boolean          default(FALSE)
#  talent_id                       :uuid             not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  authentication_token            :string
#  authentication_token_created_at :datetime
#  xing_request_token              :string
#  xing_request_token_secret       :string
#  xing_access_token               :string
#  xing_access_token_secret        :string
#  linkedin_request_token          :string
#  linkedin_request_token_secret   :string
#  linkedin_access_token           :string
#  linkedin_access_token_secret    :string
#  failed_attempts                 :integer          default(0)
#  unlock_token                    :string
#  locked_at                       :datetime
#
# Indexes
#
#  index_talent_auths_on_authentication_token  (authentication_token)
#  index_talent_auths_on_confirmation_token    (confirmation_token) UNIQUE
#  index_talent_auths_on_created_at            (created_at)
#  index_talent_auths_on_email                 (email) UNIQUE
#  index_talent_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_talent_auths_on_talent_id             (talent_id) UNIQUE
#  index_talent_auths_on_unlock_token          (unlock_token) UNIQUE
#

class TalentAuthSerializer < ActiveModel::Serializer
  attributes :id, :email, :guest

  #has_one :talent
end
