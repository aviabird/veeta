class JobMgmt::JobsController < JobMgmt::ApplicationController

  #load and secure access to job for disable action
  load_and_authorize_resource class: 'Job'

  def disable_job
    @job.update(status: 'closed')
    @job.save
    redirect_to recruiters_path
  end

end
