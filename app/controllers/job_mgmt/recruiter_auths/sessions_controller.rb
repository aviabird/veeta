module JobMgmt

  class RecruiterAuths::SessionsController < Devise::SessionsController

    ###
    # No authorization required, override devise controller
    ###
    before_filter :find_recruiter_auth, only: :new

    def new
      super
    end

    include Oauth::BrowserLanguageDetector

    layout 'recruiter_auths/recruiter_auths_layout'

    # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      new_recruiter_auth_session_path
    end

    def after_sign_in_path_for(resource)
      recruiters_path
    end

    private

      def find_recruiter_auth
        @auth = RecruiterAuth.find_by_email params["recruiter_auth"]["email"] if params["recruiter_auth"].present?
      end

  end
end
