class JobMgmt::CvSharingRequestsController < JobMgmt::ApplicationController

  #load and secure access to job for update action
  #before_action :set_job, except: [:index, :create_job]
  load_and_authorize_resource class: 'CvSharingRequest'

  def edit
  end

  def update
    @cv_sharing_request.update(cv_sharing_request_params)
    redirect_to recruiters_path
  end

  private

    def cv_sharing_request_params
      params[:cv_sharing_request].permit(:pool_welcome_text)
    end

end
