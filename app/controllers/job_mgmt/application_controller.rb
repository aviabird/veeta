module JobMgmt
  class ApplicationController < ActionController::Base

    ###
    # No authorization required, base application controller
    ###

    include Oauth::BrowserLanguageDetector
    include BaseLogger
    include CanCan::ControllerAdditions

    before_action :authenticate_recruiter_auth!

    layout 'layouts/recruiter_auths/recruiter_auths_layout'

    alias :current_user :current_recruiter_auth

    def current_ability
      @current_ability ||= Ability::RecruiterAbility.new(current_user.recruiter)
    end

    rescue_from CanCan::AccessDenied do |exception|
      render json: { error: :unauthorized}, status: 403
    end

  end
end
