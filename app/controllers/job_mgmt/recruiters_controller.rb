class JobMgmt::RecruitersController < JobMgmt::ApplicationController

  #load and secure access to job for update action
  before_action :set_job, except: [:index, :create_job]
  authorize_resource :job, class: 'Job', except: [:index, :create_job]

  def index
    @jobs = Job.where(company: current_user.user.company, unsolicited_job: false)
    # All jobs which are unsolicited
    # @unsolicited_jobs = Job.where(recruiter: current_user.user, unsolicited_job: true).order(:created_at)
    @unsolicited_jobs = Job.where(company: current_user.user.company, unsolicited_job: true).order(:created_at)
  end

  def create_job
    job = Job.new
    if update_params[:name].present?
      job.name = update_params[:name]
      job.company = current_user.user.company
      job.status = 'new'
      job.recruiter = current_user.user.company.owner
      job.language = Language.find('de')
      job.accepted_jobs_languages.build(language: Language.find('en'))
      job.accepted_jobs_languages.build(language: Language.find('de'))
      job.save
      redirect_to recruiters_path
    end
  end

  def update_job
    @job.update(name: update_params[:name])
    @job.save
    redirect_to recruiters_path
  end

  private
    def set_job
      @job = Job.find(params[:job][:id])
    end

    def update_params
      params.require(:job).permit(:name)
    end
end
