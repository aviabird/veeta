class JobMgmt::TalentsController < JobMgmt::ApplicationController

  #load and secure access to resume for update action
  before_action :set_resume_sharing, only: [:download_cv]
  authorize_resource :resume_sharing, class: 'ResumeSharing', except: [:index]

  def index
    @sharing_request = CvSharingRequest.find_all_active_or_initialize(current_user.user).first
    @sharings =  filter(current_user.user.company.sharings.includes(resume: :talent)).select {|v| v["access_revoked_at"] == nil }.sort_by {|obj| obj.resume.talent.last_name.downcase}
  end

  def download_cv
    I18n.locale = @resume_sharing.resume.talent.locale
    # check if setting exists, otherwise create default
    if @resume_sharing.resume.new_pdf_required?
      if @resume_sharing.resume.pdf_template_setting.nil?
        @resume_sharing.resume.pdf_template_setting = PdfTemplateSetting.default @resume_sharing.resume
        @resume_sharing.resume.pdf_template_setting.save
      end
      begin
        @resume_sharing.resume.pdf_template_setting.trigger_pdf_generation_sync
      rescue
        return render json: {
            status: 'error',
            #data: @resource,
            errors: ["PDF file not available at the moment."]
        }, status: 404
      end
      @resume_sharing.resume.reload
    else  # if a pdf is available return it here, otherwise trigger pdf generation
      @resume_sharing.resume.pdf_template_setting.trigger_pdf_generation_sync
    end
    if @resume_sharing.resume.pdf.file
      send_data(@resume_sharing.resume.pdf.file.read, :filename => "#{@resume_sharing.resume.talent.last_name}_#{@resume_sharing.resume.talent.first_name}_#{(@resume_sharing.updated_at.day)}-#{(@resume_sharing.updated_at.month)}-#{(@resume_sharing.updated_at.year)}.pdf", :type => "application/pdf")
    end
  end



  private
    def set_resume_sharing
      @resume_sharing = ResumeSharing.find(params[:id])
    end

    def filter sharings
      sharing_per_talent = {}
      sharings.each do |sharing|
        origin_id = sharing.resume.origin_id
        talent_id = Resume.with_deleted.find(origin_id) && Resume.with_deleted.find(origin_id).talent ? Resume.with_deleted.find(origin_id).talent.id : nil
        if talent_id && sharing_per_talent[talent_id] && sharing_per_talent[talent_id].updated_at < sharing.updated_at
          sharing_per_talent[talent_id] = sharing
        elsif talent_id && sharing_per_talent[talent_id].blank?
          sharing_per_talent[talent_id] = sharing
        end
      end
      sharing_per_talent.values
    end

end
