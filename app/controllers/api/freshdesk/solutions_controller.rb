class Api::Freshdesk::SolutionsController < Api::ApplicationController
   #before_filter :set_freshdesk_locale

  def index
    # puts I18n.locale
    if params[:search] && !params[:search].blank?
      client = RestClient::Resource.new freshdesk_locale + 'search/solutions.json?term=' + safe_search_query, ENV['FRESHDESK_API_KEY'], 'X'
      @solutions = client.get(accept: 'application/json')
    else
      client = RestClient::Resource.new freshdesk_locale + 'solutions.json', ENV['FRESHDESK_API_KEY'], 'X'
      @solutions = I18n.t('talent.ui.freshdesk.start_screen_articles')
    end
    #solutions = client.get(accept: 'application/json')
    respond_with @solutions, @options.merge(log: true)
  end

  def show
    client = RestClient::Resource.new freshdesk_locale + 'solutions/articles/' + params[:id] +  '.json', ENV['FRESHDESK_API_KEY'], 'X'
    @solution = client.get(accept: 'application/json')
    respond_with @solution, @options.merge(log: true, root: false)
  end

  private
    def freshdesk_locale
      I18n.locale.to_s == 'de' ? 'http://support.myveeta.com/support/' :
                                 'http://support-eu.myveeta.com/support/'
    end

    def safe_search_query
      CGI::escape params[:search].to_s
    end
end
