class Api::CountriesController < Api::ApplicationController


  ###
  # No authorization required, access to countries is public
  ###

  def index
    @countries = Country.all
    # order(:name).where('name ilike ?', "%#{params[:q]}%")
    respond_with @countries, @options.merge(each_serializer: CountryCollectionSerializer)
  end
end
