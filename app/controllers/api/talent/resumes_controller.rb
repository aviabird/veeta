class Api::Talent::ResumesController < Api::ApplicationController
  include TalentApi
  respond_to :pdf, :json

  #authorize access to resume
  load_and_authorize_resource
  #sharing update (new version, different cv) checked via check_existing_sharings


  skip_after_action :update_auth_header, only: [:fillin]

  before_filter :default_format_pdf, only: [:download]

  def index
    @resumes = Resume.includes(:talent, :pdf_template_setting, {sharings: [:companies, :email_application]}).my(current_talent)
    respond_with @resumes, @options.merge(each_serializer: ResumeCollectionSerializer, root: :resumes, log: true)
  end

  def show
    #create default template settings if not set yet
    if @resume.pdf_template_setting.nil?
      @resume.pdf_template_setting = PdfTemplateSetting.default @resume
    end
    respond_with @resume, @options.merge(log: true)
  end

  def new
    # consolidate inconsistant resumes (link to cloned talent was not made as
    # talent object was not created successfully back then)
    # by makeing a copy of current talent
    current_talent.resumes.each do |r|
      if !r.origin_id.nil? && current_talent == r.talent
        t = r.talent.amoeba_dup
        t.save!
        r.talent = t
        r.save!
      end
    end if current_talent.resumes.count >= 3
    if current_talent.resumes.count < 3
      @resume = Resume.prepare(current_talent)
      respond_with @resume, @options.merge(log: true)
    else
      # errors.add(:base, "Error message")
      render json: { error: 'You cannot have more than 3 resumes' }, status: 404
    end
  end

  def update
    @resume.update(resume_params)
    respond_with @resume, @options.merge(log: { priority: :medium })
  end

  def destroy
    @resume.destroy
    respond_with @resume, @options.merge(log: { priority: :high })
  end

  def validate
    @resume = Resume.new(complete_params)
    @resume.valid?
    respond_with @resume, @options.merge(log: { priority: :medium })
  end

  def share_all
    params = all_resume_sharing_params[:resume_ids]
    params.each do |param|
      resume_id = param["resume_id"]
      resume_param = param.slice("real_company", "company_ids").symbolize_keys
      resume = Resume.find(resume_id)
      resume.share(resume_param)
    end
    respond_with @options.merge(log: { priority: :medium })
  end

  def share
    # check if company/email_company is allowed to share
    @resume.share(resume_sharing_params)
    respond_with @resume, @options.merge(log: { priority: :medium })
  end

  def sharing_request
    resume_to_share = @resume.sharing_request
    resume_sharing_request = resume_to_share.sharing_requests.build(resume_sharing_request_params)
    resume_to_share.save
    Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i, MailShareResumeJob, I18n.locale, resume_to_share.id, resume_sharing_request_params[:contact_name],resume_sharing_request_params[:email], resume_sharing_request.id)
    respond_with @resume, @options.merge(log: { priority: :high })
  end

  def fillin
    begin
      pdf = @resume.pdfs.build(resume_pdf_params)
      @resume.fillin(pdf)
      @resume.errors.clear
      respond_with @resume, @options.merge(log: true)
    rescue ResumeService::ParsePDFError => e
      render json: { error: 'talent.errors.cv_import.pdf_import_error' }, status: 400
      log_event self, @resume, request, @options.merge(log: { priority: :high })
    rescue ResumeService::ServiceOutageError => error
      render json: { error: 'talent.errors.cv_import.service_outage_error' }, status: 400
      log_event self, @resume, request, @options.merge(log: { priority: :high })
    ensure
      #delete uploaded file
      File.delete(pdf.file.current_path)
    end
  end

  #callback action after xing authorization
  def xingreturn
    begin
      xing_client_service = XingClientService.new
      xing_client_service.handle_xing_callback(@resume,@options, current_talent,resume_xing_return_params)
      respond_with @resume, @options.merge(log: { priority: :medium })
      log_event self, @resume, request, @options.merge(log: { priority: :medium })
    rescue XingApi::RateLimitExceededError, XingApi::ServerError,  XingApi::Error, StandardError => e
      logger.error("XingApi failed: #{e}")
      raise e
      render json: { error: 'talent.errors.xing_import.xing_import_error' }, status: 500
      log_event self, @resume, request, @options.merge(log: { priority: :high })
    end
  end


  # called on import, if first import or access token not valid --> redirect to xing
  def fillin_from_xing
    begin
      callback_url = resume_xing_params[:callback]
      if callback_url.ends_with? "/new"
        callback_url = resume_xing_params[:callback].sub! '/new', ""
      end
      xing_client_service = XingClientService.new

      # VEETA-1790: always ask for credentials!
      # could retrieve data withouth oauth handshake?
      #if xing_client_service.retrieve_from_api(@resume,@options, current_talent)
      #  respond_with @resume, @options.merge(log: { priority: :medium })
      #else # oauth handshake required
      xing_auth_url = xing_client_service.trigger_oauth_handshake(callback_url, current_talent)
      log_event self, xing_auth_url, request, @options.merge(log: { priority: :medium })
      render json: { xing_auth_url: xing_auth_url }
      #end
    rescue XingApi::RateLimitExceededError, XingApi::ServerError,  XingApi::Error => e
      logger.error("XingApi failed: #{e}")
      raise e
      render json: { error: 'talent.errors.xing_import.xing_import_error' }, status: 500
      log_event self, @resume, request, @options.merge(log: { priority: :high })
    end
  end


  #callback action after linkedin authorization
  def linkedinreturn
    begin
      linkedin_client_service = LinkedinClientService.new
      linkedin_client_service.handle_linkedin_callback(@resume,@options, current_talent,resume_linkedin_return_params)
      respond_with @resume, @options.merge(log: { priority: :medium })
    rescue StandardError => e
      logger.error("LinkedInApi failed: #{e}")
      raise e
      render json: { error: 'talent.errors.linkedin_import.linkedin_import_error' }, status: 500
      log_event self, @resume, request, @options.merge(log: { priority: :high })
    end
  end

  # called on import, if first import or access token not valid --> redirect to linkedin
  def fillin_from_linkedin
    begin
      callback_url = resume_linkedin_params[:callback]
      if callback_url.ends_with? "/new"
        callback_url = resume_linkedin_params[:callback].sub! '/new', ""
      end
      linkedin_client_service = LinkedinClientService.new

      # VEETA-1790: always ask for credentials!
      # could retrieve data withouth oauth handshake?
      #if linkedin_client_service.retrieve_from_api(@resume,@options, current_talent)
      #  respond_with @resume, @options.merge(log: { priority: :medium })
      #else # oauth handshake required
      linkedin_auth_url = linkedin_client_service.trigger_oauth_handshake(callback_url, current_talent)
      log_event self, linkedin_auth_url, request, @options.merge(log: { priority: :medium })
      render json: { linkedin_auth_url: linkedin_auth_url }
      #end
    rescue StandardError => e
      logger.error("LinkedInApi failed: #{e}")
      raise e
      log_event self, @resume, request, @options.merge(log: { priority: :high })
      render json: { error: 'talent.errors.linkedin_import.linkedin_import_error' }, status: 500
    end
  end


    def download
      I18n.locale = @resume.talent.locale
      # check if setting exists, otherwise create default
      if @resume.new_pdf_required?
        if @resume.pdf_template_setting.nil?
          @resume.pdf_template_setting = PdfTemplateSetting.default @resume
          @resume.pdf_template_setting.save
        end
        begin
          @resume.pdf_template_setting.trigger_pdf_generation_sync
        rescue
          log_event self, @resume, request, @options.merge(log: { priority: :high, details: "PDF could not be generated; #{@resume.pdf_template_setting.to_yaml}" })
          return render json: {
              status: 'error',
              #data: @resource,
              errors: ["PDF file not available at the moment."]
          }, status: 404
        end
        @resume.reload
      end
      if @resume.pdf.file
        send_data(@resume.pdf.file.read, :filename => "#{@resume.talent.name}.pdf", :type => "application/pdf")
        log_event self, @resume, request, @options.merge(log: { priority: :medium, details: "PDF found; #{@resume.pdf_template_setting.to_yaml}" })

      else
        # when pdf is not yet available, return error
        log_event self, @resume, request, @options.merge(log: { priority: :high, details: "PDF not found; #{@resume.pdf_template_setting.to_yaml}" })

        return render json: {
            status: 'error',
            #data: @resource,
            errors: ["PDF file not available at the moment."]
        }, status: 404
      end
    end


    def default_format_pdf
      request.format = "pdf" # unless params[:format]
      #use params[:format] for downloading in different formats in future
    end

  private
    def resume_params
      params[:resume].permit(:name, :language_id,# :company_ids,
        talent_attributes: [ :first_name, :last_name, :birthdate, :avatar, :salutation, :nationality_country_id, :title, :email, :phone_number,
        address_attributes: [ :country_id, :state_id, :zip, :city, :address_line, :address_line_2] ])
    end

    def resume_pdf_params
      params[:resume_pdf].permit(:file)
    end

    def complete_params
      resume_params.merge!(talent: current_talent)
    end

    def all_resume_sharing_params
      params[:resume].permit(resume_ids: [:resume_id, :real_company, company_ids: []])
    end

    def resume_sharing_params
      check_existing_sharings params[:resume].permit(:real_company,company_ids: []).symbolize_keys
    end

    def resume_sharing_request_params
      params[:resume_sharing].permit(:email, :contact_name)
    end

    def resume_xing_params
      params.permit(:callback)
    end

    def resume_xing_return_params
      params.permit(:oauth_verifier,:oauth_token)
    end

    def resume_linkedin_params
      params.permit(:callback)
    end

    def resume_linkedin_return_params
      params.permit(:oauth_verifier, :oauth_token)
    end

    def check_existing_sharings sharing_params
      company_ids = sharing_params[:company_ids]
      company_type =  sharing_params.has_key?(:real_company) ? sharing_params[:real_company]  : "REAL"
      permitted_company_ids = []
      unless company_ids.nil?
        company_ids.each do |company_id|
          if company_type == "REAL"
            company =  Company.find(company_id) if Company.exists?(company_id)
          elsif company_type == "APP"
            company = ClientApplication.find(company_id) if ClientApplication.exists?(company_id)
          else
            company =  EmailApplication.find(company_id) if EmailApplication.exists?(company_id)
          end
          # check if company can be accessed by current user:
          authorize! :read, company
        end
      end
      sharing_params
    end
end
