class Api::Talent::Resume::AboutsController < Api::ApplicationController
  include TalentApi

  # secure resume access
  load_and_authorize_resource :resume

  #about associated with resume, so load it manually
  before_action :set_about, except: [:new, :create]

  #secure direct access to about
  authorize_resource class: 'Rs::About', except: [:new, :create]

  def show
    respond_with @about, @options
  end

  def new
    @about = Rs::About.new()
    @about.resume = @resume
    respond_with @about, @options
  end

  def create
    @about = Rs::About.new(about_params)
    @about.resume = @resume
    @about.save
    respond_with @about, @options.merge(log: { priority: :high })
  end

  def update
    @about.update(about_params)
    respond_with @about, @options.merge(log: { priority: :medium })
  end

  def validate
    @about = Rs::About.new(about_params)
    @about.resume = @resume
    @about.valid?
    respond_with @about, @options.merge(log: true)
  end

  private
    def set_resume
      @resume = Resume.find(params[:resume_id])
    end

    def set_about
      set_resume
      @about = @resume.about || @resume.build_about
    end

    def about_params
      params[:about].permit(:text)
    end
end
