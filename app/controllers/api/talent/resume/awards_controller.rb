class Api::Talent::Resume::AwardsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure award access
  load_and_authorize_resource class: 'Rs::Award', except: [:index, :new, :create, :validate]

  def index
    @awards = @resume.awards
    respond_with @awards, @options
  end

  def show
    respond_with @award, @options
  end

  def new
    @award = @resume.awards.build
    respond_with @award, @options
  end

  def create
    @award = @resume.awards.build(award_params)
    @award.save
    respond_with @award, @options.merge(log: { priority: :high })
  end

  def update
    @award.update(award_params)
    respond_with @award, @options.merge(log: { priority: :medium })
  end

  def destroy
    @award.destroy
    respond_with @award, @options.merge(log: { priority: :high })
  end

  def validate
    @award = @resume.awards.build(award_params)
    @award.valid?
    respond_with @award, @options.merge(log: true)
  end

  private
    def award_params
      params[:award].permit(:name, :occupation, :awarded_by, :year, :description)
    end
end
