class Api::Talent::Resume::MembershipsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure membership access
  load_and_authorize_resource class: 'Rs::Membership', except: [:index, :new, :create, :validate]

  def index
    @memberships = @resume.memberships
    respond_with @memberships, @options
  end

  def show
    respond_with @membership, @options
  end

  def new
    @membership = @resume.memberships.build
    respond_with @membership, @options
  end

  def create
    @membership = @resume.memberships.build(membership_params)
    @membership.save
    respond_with @membership, @options.merge(log: { priority: :high })
  end

  def update
    @membership.update(membership_params)
    respond_with @membership, @options.merge(log: { priority: :medium })
  end

  def destroy
    @membership.destroy
    respond_with @membership, @options.merge(log: { priority: :high })
  end

  def validate
    @membership = @resume.memberships.build(membership_params)
    @membership.valid?
    respond_with @membership, @options.merge(log: true)
  end

  private

    def membership_params
      params[:membership].permit(:organization, :role, :area, :from, :to)
    end
end
