class Api::Talent::Resume::WorkExperiencesController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure work_experience access
  load_and_authorize_resource class: 'Rs::WorkExperience', except: [:index, :new, :create, :validate]

  def index
    @work_experiences = @resume.work_experiences
    respond_with @work_experiences, @options
  end

  def show
    respond_with @work_experience, @options
  end

  def new
    @work_experience = @resume.work_experiences.build
    respond_with @work_experience, @options
  end

  def create
    @work_experience = @resume.work_experiences.build(work_experience_params)
    @work_experience.save
    respond_with @work_experience, @options.merge(log: { priority: :high })
  end

  def update
    @work_experience.update(work_experience_params)
    respond_with @work_experience, @options.merge(log: { priority: :medium })
  end

  def destroy
    @work_experience.destroy
    respond_with @work_experience, @options.merge(log: { priority: :high })
  end

  def validate
    @work_experience = @resume.work_experiences.build(work_experience_params)
    @work_experience.valid?
    respond_with @work_experience, @options.merge(log: true)
  end

  private
    def work_experience_params
      params[:work_experience].permit(:position, :company, :industry, :job_level, :from, :to,
        :description, :country_id, :terms_of_employment)
    end
end
