class Api::Talent::Resume::ProjectsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure project access
  load_and_authorize_resource class: 'Rs::Project', except: [:index, :new, :create, :validate]

  def index
    @projects = @resume.projects
    respond_with @projects, @options
  end

  def show
    respond_with @project, @options
  end

  def new
    @project = @resume.projects.build
    respond_with @project, @options
  end

  def create
    @project = @resume.projects.build(project_params)
    @project.save
    respond_with @project, @options.merge(log: { priority: :high })
  end

  def update
    @project.update(project_params)
    respond_with @project, @options.merge(log: { priority: :medium })
  end

  def destroy
    @project.destroy
    respond_with @project, @options.merge(log: { priority: :high })
  end

  def validate
    @project = @resume.projects.build(project_params)
    @project.valid?
    respond_with @project, @options.merge(log: true)
  end

  private
    def project_params
      params[:project].permit(:name, :role, :link, :from, :to, :description)
    end
end
