class Api::Talent::Resume::DocumentsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure document access
  load_and_authorize_resource class: 'Rs::Document', except: [:index, :new, :create, :validate]

  # todo Hack for assets validation, maybe use X-accel-redirect of nginx to improve sercuirty
  skip_after_action :update_auth_header, only: [:create]

  def index
    @documents = @resume.documents
    respond_with @documents, @options
  end

  def show
    respond_with @document, @options
  end

  def new
    @document = @resume.documents.build
    respond_with @document, @options
  end

  def create
    @document = @resume.documents.build(document_params)
    @document.save
    respond_with @document, @options.merge(log: { priority: :high })
  end

  def update
    @document.update(document_params)
    respond_with @document, @options.merge(log: { priority: :medium })
  end

  def destroy
    @document.destroy
    respond_with @document, @options.merge(log: { priority: :high })
  end

  def validate
    @document = @resume.documents.build(document_params)
    @document.valid?
    respond_with @document, @options.merge(log: true)
  end

  private
    def document_params
      params[:document].permit(:file, :document_type)
    end
end
