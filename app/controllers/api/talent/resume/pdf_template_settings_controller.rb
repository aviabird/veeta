class Api::Talent::Resume::PdfTemplateSettingsController < Api::ApplicationController
  include TalentApi

  # secure resume access
  before_action :set_resume
  authorize_resource :resume, class: 'Resume'


  #pdf_template_setting associated with resume, so load it manually
  before_action :set_pdf_template_setting

  #secure direct access to pdf_template_setting
  authorize_resource :pdf_template_setting, class: 'PdfTemplateSetting'

  def update
    if @resume.pdf_template_setting.nil?
      @resume.pdf_template_setting = PdfTemplateSetting.default @resume
      @resume.pdf_template_setting.save
    end
    @pdf_template_setting.update(pdf_template_setting_params)
    respond_with @pdf_template_setting, @options.merge(log: { priority: :medium })
  end

  def validate
    @pdf_template_setting = PdfTemplateSetting.new(about_params)
    @pdf_template_setting.resume = @resume
    @pdf_template_setting.valid?
    respond_with @pdf_template_setting, @options.merge(log: true)
  end

  private
    def set_resume
      resume_id = params[:resume_id] || params[:template_setting][:resume_id]
      @resume = Resume.find(resume_id)
    end

    def set_pdf_template_setting
      set_resume
      @pdf_template_setting = @resume.pdf_template_setting
    end

    def pdf_template_setting_params
      params[:template_setting].permit(:resume_id, :pdf_template_type, :color, :template)
    end
end
