class Api::Talent::Resume::SkillsController < Api::ApplicationController
  include TalentApi

  # secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure direct access to skill
  load_and_authorize_resource class: 'Rs::Skill', except: [:index, :new, :create, :validate]

  def index
    @skills = @resume.skills.app_sorting_order
    respond_with @skills, @options
  end

  def show
    respond_with @skill, @options
  end

  def new
    @skill = @resume.skills.build
    respond_with @skill, @options
  end

  def create
    @skill = @resume.skills.build(skill_params)
    @skill.save
    respond_with @skill, @options.merge(log: { priority: :high })
  end

  def update
    @skill.update(skill_params)
    respond_with @skill, @options.merge(log: { priority: :medium })
  end

  def destroy
    @skill.destroy
    respond_with @skill, @options.merge(log: { priority: :high })
  end

  def validate
    @skill = @resume.skills.build(skill_params)
    @skill.valid?
    respond_with @skill, @options.merge(log: true)
  end

  private
    def skill_params
      params[:skill].permit(:name, :level)
    end
end
