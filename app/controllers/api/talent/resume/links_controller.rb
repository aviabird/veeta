class Api::Talent::Resume::LinksController < Api::ApplicationController
  include TalentApi

  # secure resume access
  load_and_authorize_resource :resume

  #link associated with resume, so load it manually
  before_action :set_link, except: [:new, :create]

  #secure direct access to links
  authorize_resource class: 'Rs::Link', except: [:new, :create]

  def show
    respond_with @link, @options
  end

  def new
    @link = Rs::Link.new()
    @link.resume = @resume
    respond_with @link, @options
  end

  def create
    @link = Rs::Link.new(link_params)
    @link.resume = @resume
    @link.save
    respond_with @link, @options.merge(log: { priority: :high })
  end

  def update
    @link.update(link_params)
    respond_with @link, @options.merge(log: { priority: :medium })
  end

  def validate
    @link = Rs::Link.new(link_params)
    @link.resume = @resume
    @link.valid?
    respond_with @link, @options.merge(log: true)
  end

  private
    def set_resume
      @resume = Resume.find(params[:resume_id])
    end

    def set_link
      set_resume
      @link = @resume.link || @resume.build_link
    end

    def link_params
      params[:link].permit(:linkedin, :xing, :website)
    end
end
