class Api::Talent::Resume::PublicationsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure publication access
  load_and_authorize_resource class: 'Rs::Publication', except: [:index, :new, :create, :validate]

  def index
    @publications = @resume.publications
    respond_with @publications, @options
  end

  def show
    respond_with @publication, @options
  end

  def new
    @publication = @resume.publications.build
    respond_with @publication, @options
  end

  def create
    @publication = @resume.publications.build(publication_params)
    @publication.save
    respond_with @publication, @options.merge(log: { priority: :high })
  end

  def update
    @publication.update(publication_params)
    respond_with @publication, @options.merge(log: { priority: :medium })
  end

  def destroy
    @publication.destroy
    respond_with @publication, @options.merge(log: { priority: :high })
  end

  def validate
    @publication = @resume.publications.build(publication_params)
    @publication.valid?
    respond_with @publication, @options.merge(log: true)
  end

  private
    def set_resume
      @resume = Resume.find(params[:resume_id])
    end

    def set_publication
      @publication = Rs::Publication.find(params[:id])
      authorize_by_resume @publication
    end

    def publication_params
      params[:publication].permit(:title, :publication_type, :year)
    end
end
