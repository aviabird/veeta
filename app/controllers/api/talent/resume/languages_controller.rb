class Api::Talent::Resume::LanguagesController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure language access
  load_and_authorize_resource class: 'Rs::Language', except: [:index, :new, :create, :validate]

  def index
    @languages = @resume.languages
    respond_with @languages, @options
  end

  def show
    respond_with @language, @options
  end

  def new
    @language = @resume.languages.build
    respond_with @language, @options
  end

  def create
    @language = @resume.languages.build(language_params)
    @language.save
    respond_with @language, @options.merge(log: { priority: :high })
  end

  def update
    @language.update(language_params)
    respond_with @language, @options.merge(log: { priority: :medium })
  end

  def destroy
    @language.destroy
    respond_with @language, @options.merge(log: { priority: :high })
  end

  def validate
    @language = @resume.languages.build(language_params)
    @language.valid?
    respond_with @language, @options.merge(log: true)
  end

  private
    def language_params
      params[:language].permit(:language_id, :level)
    end
end
