class Api::Talent::Resume::CertificationsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure certification access
  load_and_authorize_resource class: 'Rs::Certification', except: [:index, :new, :create, :validate]

  def index
    @certifications = @resume.certifications
    respond_with @certifications, @options
  end

  def show
    respond_with @certification, @options
  end

  def new
    @certification = @resume.certifications.build
    respond_with @certification, @options
  end

  def create
    @certification = @resume.certifications.build(certification_params)
    @certification.save
    respond_with @certification, @options.merge(log: { priority: :high })
  end

  def update
    @certification.update(certification_params)
    respond_with @certification, @options.merge(log: { priority: :medium })
  end

  def destroy
    @certification.destroy
    respond_with @certification, @options.merge(log: { priority: :high })
  end

  def validate
    @certification = @resume.certifications.build(certification_params)
    @certification.valid?
    respond_with @certification, @options.merge(log: true)
  end

  private
    def certification_params
      params[:certification].permit(:certification_type,
                                    :subject,
                                    :organization,
                                    :year,
                                    :description)
    end
end
