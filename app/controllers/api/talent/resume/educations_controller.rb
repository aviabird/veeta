class Api::Talent::Resume::EducationsController < Api::ApplicationController
  include TalentApi

  #secure resume access
  load_and_authorize_resource :resume, except: [:show, :update, :destroy]

  #secure work_experience access
  load_and_authorize_resource class: 'Rs::Education', except: [:index, :new, :create, :validate]

  def index
    @educations = @resume.educations
    respond_with @educations, @options
  end

  def show
    respond_with @education, @options
  end

  def new
    @education = @resume.educations.build
    respond_with @education, @options
  end

  def create
    @education = @resume.educations.build(education_params)
    @education.save
    respond_with @education, @options.merge(log: { priority: :high })
  end

  def update
    @education.update(education_params)
    respond_with @education, @options.merge(log: { priority: :medium })
  end

  def destroy
    @education.destroy
    respond_with @education, @options.merge(log: { priority: :high })
  end

  def validate
    @education = @resume.educations.build(education_params)
    @education.valid?
    respond_with @education, @options.merge(log: true)
  end

  private
    def education_params
      params[:education].permit(:type_of_education, :school_name, :degree, :country_id, :subject, :from, :to, :description, :completed)
    end
end
