class Api::Talent::CvSharingRequestsController < Api::ApplicationController
  include TalentApi

  # secure access to cv_sharing_request
  load_and_authorize_resource

  # also if accessed via ext id
  before_action :set_cv_sharing_request, only: [:find_by_ext_id]
  authorize_resource :cv_sharing_request

  skip_before_filter :authenticate_talent_auth!, only: [:show, :find_by_ext_id]
  skip_before_filter :set_user_by_token, only: [:show, :find_by_ext_id]
  skip_after_filter :update_auth_header, only: [:show, :find_by_ext_id]



  def show
    return_cv_sharing_request
  end

  def find_by_ext_id
    return_cv_sharing_request
  end

  private

    def return_cv_sharing_request
      if @cv_sharing_request && @cv_sharing_request.status == 'new'
        respond_with @cv_sharing_request, @options.merge(serializer: CvSharingRequestLandingPageSerializer, log: true, root: "cv_sharing_request")
      else
        render json: {
          success: false,
          errors: "not found"
        }, status: :not_found
      end
    end

    def set_cv_sharing_request
      if !params[:ext_id].blank? && CvSharingRequest.exists?(ext_id: params[:ext_id])
        @cv_sharing_request = CvSharingRequest.find_by(ext_id: params[:ext_id])
      end
    end
end
