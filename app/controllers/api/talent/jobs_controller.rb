class Api::Talent::JobsController < Api::ApplicationController
  include TalentApi

  # secure access to job
  load_and_authorize_resource

  # also if accessed via ext id
  before_action :set_job_from_ext_id, only: :find_by_ext_id
  authorize_resource :job, only: :find_by_ext_id

  skip_before_filter :authenticate_talent_auth!, only: [:show, :find_by_ext_id]
  skip_before_filter :set_user_by_token, only: [:show, :find_by_ext_id]
  skip_after_filter :update_auth_header, only: [:show, :find_by_ext_id]


  def show
    return_job
  end

  def find_by_ext_id
    return_job
  end

  # this is returning all current jobs for the company of a recruiter
  def get_current_openings
    # method to return currently open position of company searched based on recruiter
    recruiter = find_recrutier(params[:email])
    company = recruiter.try(:company)
    jobs = []
    JobService::GenericJobReadingService.call_list company
    @jobs = company.try(:jobs).try(:active) || []
    if @jobs.present?
      @jobs = @jobs.order(updated_at: :desc,name: :asc)
    end
    respond_with  @jobs, @options.merge(each_serializer: SimpleJobSerializer,log: true)
  end

  private

    def return_job
      if @job && @job.status == 'new'
        respond_with @job, @options.merge(serializer: JobsLandingPageSerializer, log: true, root: "job")
      else
        render json: {
          success: false,
          errors: "not found"
        }, status: :not_found
      end
    end

    def set_job_from_ext_id
      @job = JobService::GenericJobReadingService.call params[:ext_id]
    end

    # consider the entered recruiter email and try to get the corresponding company
    def find_recrutier(email)
      # method to find recruiter by email or based on domain of company
      recruiter = ::Recruiter.where('lower(email) = ?', email.try(:downcase)).first
      if recruiter.blank? && email
        email_array = email.split("@")
        domain = email_array[1] if email_array.length > 1
        recruiter = ::Recruiter.where("email LIKE ?", "%@#{domain}").first if domain.present?
      end
      recruiter
    end

end
