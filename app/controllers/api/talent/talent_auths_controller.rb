class Api::Talent::TalentAuthsController < Api::ApplicationController
  include TalentApi


  ###
  # No authorization required, actions are all related to current user
  ###

  def email
    service = AuthService::SetEmail.call(current_user, email_param)
    @current_auth = service.current_auth
    if @current_auth.talent.valid?
      respond_with @current_auth, @options.merge(serializer: TalentAuthSerializer,log: { priority: :highest })
    else
      render json: {
        success: false,
        errors:  @current_auth.talent.errors.messages
      }, status: 422
    end
  end

  # Called from new job application for guests when they set their password
  def password
    service = AuthService::SetPassword.call(current_user, password_param)
    @current_auth = service.current_auth
    respond_with @current_auth, @options.merge(serializer: TalentAuthSerializer,log: { priority: :highest })
  end

  def delete
    service = AuthService::DeleteAccount.call(current_user, confirmation_params)
    if service.success?
      # set newsletter settings to off
      # set unsubscribe from email
      unsubscribe_after_deletion
    end
    @current_auth = service.current_auth
    #hide archived email
    @current_auth.email = ''
    respond_with @current_auth, @options.merge( serializer: TalentAuthSerializer, log: { priority: :highest })
  end

  # https://github.com/plataformatec/devise/blob/0a27a0da3b48daceff3721bb728d041f3e445db4/lib/devise/models/confirmable.rb
  def resend_confirmation
    @current_auth = current_user
    @current_auth.send_confirmation_instructions
    respond_with @current_auth, @options.merge(serializer: TalentAuthSerializer, log: { priority: :high })
  end


  private
    def confirmation_params
      params[:talent_auth] ? params[:talent_auth].permit(:email, :password, :delete_reason) : {}
    end

    def password_param
      params[:talent_auth][:password] if params[:talent_auth]
    end

    def email_param
      params[:talent_auth][:email] if params[:talent_auth]
    end

    def unsubscribe_after_deletion
      current_user.talent.setting.update_columns(newsletter_updates: 'disabled')
      intercom = Intercom::Client.new(app_id: ENV['INTERCOM_APP_ID'], api_key: ENV['INTERCOM_API_KEY'])
      begin
        user = intercom.users.find(:user_id => current_user.talent.intercom_user_id)
        if user
          user.unsubscribed_from_emails = true
          intercom.users.save(user)
        end
      end
    end

end
