class Api::Talent::CvSharingResponsesController < Api::ApplicationController
  include TalentApi

  # secure cv_sharing_response
  load_and_authorize_resource except: [:index]

  #set resume as part of the cv_sharing_response object
  before_action :set_resume, except: [:index]

  #secure resume attached to cv_sharing_response
  authorize_resource :resume, class: 'Resume', except: [:index]

  #set CvSharingRequest as part of the CvSharingResponse object
  before_action :set_cv_sharing_request, except: [:index]
#  before_action :set_email_application, except: [:index]

  # check CvSharingRequest access
  authorize_resource :cv_sharing_request, class: 'CvSharingRequest',  only: [:new]
#  authorize_resource :email_application, class: 'EmailApplication',  only: [:new]

  def show
    # a cv_sharing_response should always be viewable, even if job was deleted or invalidated
    respond_with @cv_sharing_response, options(log: true)
  end

  def new
    @cv_sharing_response = CvSharingResponse.prepare(cv_sharing_response_new_params)
    respond_with @cv_sharing_response, options(log: { priority: :medium })
  end

  def create
    @cv_sharing_response = CvSharingResponse.create(cv_sharing_response_params)
    respond_with @cv_sharing_response, options(log: { priority: :medium })
  end

  def update
    @cv_sharing_response.update(cv_sharing_response_params)
    respond_with @cv_sharing_response, options(log: { priority: :medium })
  end

  #def destroy
  #  @cv_sharing_response.destroy
  #  respond_with @cv_sharing_response, options(log: { priority: :high })
  #end

  def validate
    @cv_sharing_response = CvSharingResponse.new(complete_params)
    @cv_sharing_response.valid?
    respond_with @cv_sharing_response, options(log: { priority: :medium })
  end

  def finish
    service = SharingService::CvSharingResponseService.call(@cv_sharing_response, cv_sharing_response_params)
    @cv_sharing_response = service.cv_sharing_response
    respond_with @cv_sharing_response, options(service.info.merge(log: { priority: :medium }))
  end

  private

    def set_resume
      unless params[:resume_id].blank?
        @resume = Resume.find(params[:resume_id])
        # if resume is already found, ensure that only this resume ID is used
        params[:cv_sharing_response][:resume_id] = @resume.id if params[:cv_sharing_response].present? && params[:cv_sharing_response][:resume_id].present?
      end
      if @resume.blank? && !params[:cv_sharing_response].blank? && !params[:cv_sharing_response][:resume_id].blank?
        @resume = Resume.find(params[:cv_sharing_response][:resume_id])
      end
    end

    def cv_sharing_response_params
      params[:cv_sharing_response].permit(:resume_id, :cv_sharing_request_type, :allow_recruiter_contact_sharing, :terms,
      :blocked_companies=>[]) #        job_attributes: [:id, :company_name, :jobname, :recruiter_email ],
    end

    def complete_params
      cv_sharing_response_params.merge!(talent: current_talent)
    end

    def cv_sharing_response_new_params
      params.permit(:resume_id, :cv_sharing_request_id, :application_referrer_url)
    end

    def set_cv_sharing_request
      if !params[:cv_sharing_response].blank? && params[:cv_sharing_response][:cv_sharing_request_type] == 'CvSharingRequest' && @cv_sharing_request.blank? && !params[:cv_sharing_response][:cv_sharing_request_id].blank?
        @cv_sharing_request = CvSharingRequest.find(params[:cv_sharing_response][:cv_sharing_request_id])
      end
    end

#    def set_email_application
#      if !params[:jobapp].blank? && params[:jobapp][:job_type] == 'EmailApplication' && @email_application.blank? &&  !params[:jobapp][:job_id].blank?
#        @email_application = EmailApplication.find(params[:jobapp][:job_id])
#      end
#    end

end
