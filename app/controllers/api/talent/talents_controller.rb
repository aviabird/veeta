class Api::Talent::TalentsController < Api::ApplicationController
  include TalentApi

  #load current talent for all actions
  before_action :set_talent

  #authorize talent
  authorize_resource

  skip_after_action :update_auth_header, only: [:avatar, :avatar_upload]

  def show
    respond_with @talent, @options.merge(log: true)
  end

  def update
    if @talent.update(talent_params)
      respond_with @talent, @options.merge(log: { priority: :medium })
    else
      render json: {
        success: false,
        errors: @talent.errors.messages
      }, status: 422
    end
  end

  def update_locale
    if @talent
      @talent.update_attribute(:locale, talent_params[:locale])
    end
    respond_with @talent, @options.merge(log: true)
  end

  def avatar_upload

    if @talent.validate_image params[:talent][:avatar]
      @talent.remove_avatar!
      @talent.avatar_meta = nil
      @talent.token = nil
      @talent.save(validate: false)
      @talent.refresh_full_token
    end
    @talent = Talent.find(@talent.id)
    @talent.update_avatar params[:talent][:avatar]
    respond_with @talent, @options.merge(serializer: TalentAvatarSerializer, log: { priority: :medium })
  end

  def avatar_remove
    @talent.remove_avatar!
    @talent.avatar_meta = nil
    @talent.token = nil
    @talent.save(validate: false)
    respond_with @talent, @options.merge(serializer: TalentAvatarSerializer, log: { priority: :medium })
  end

  def validate
    @talent.assign_attributes(talent_params)
    @talent.valid?
    respond_with @talent, @options.merge(log: { priority: :medium })
  end

  private
    def set_talent
      @talent = current_talent
    end

    def talent_params
      params[:talent].permit(:first_name, :last_name, :salutation, :title, :birthdate, :nationality_country_id,
        :phone_number, :email, :locale, :military_service ,address_attributes: [:address_line, :address_line_2, :city, :zip, :country_id ])
    end
end
