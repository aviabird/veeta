class Api::Talent::ReferrersController < Api::ApplicationController
  include TalentApi
=begin
  authorize_resource :talent, class: 'Talent'
  before_filter :exist_job_id?

  def get_referrers
    company = (Job.find_by_id params[:job_id]).company
    referrers= company ? company.referrers : []
    respond_with referrers, @options.merge(each_serializer: ReferrerSerializer)
  end

  private

  def exist_job_id?
    render :json => { :errors => "Can't find job_id." } ,status: 404 and return if  params[:job_id].blank?
  end
=end
end
