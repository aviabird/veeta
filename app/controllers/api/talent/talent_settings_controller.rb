class Api::Talent::TalentSettingsController < Api::ApplicationController
  include TalentApi

  # load setting from current talent setting
  before_action :set_setting

  # authorize talent_setting access
  authorize_resource


  def show
    respond_with @setting, @options.merge(log: true)
  end

  def update
    @setting.update(setting_params)
    respond_with @setting, @options.merge(log: {priority: :high})
  end

  def validate
    @setting.assign_attributes(setting_params)
    @setting.valid?
    respond_with @setting, @options.merge(log: {priority: :medium})
  end

  private
  def set_setting
    @setting = current_talent.setting
  end

  def setting_params
    params[:talent_setting][:working_locations] = [] if params[:talent_setting][:working_locations] == nil
    params[:talent_setting][:working_industries] = [] if params[:talent_setting][:working_industries] == nil
    params[:talent_setting].permit(:country_id,
                                   :professional_experience_level,
                                   :newsletter_locale,
                                   :newsletter_updates,
                                   :availability,
                                   :job_seeker_status,
                                   :salary_exp_min,
                                   :salary_exp_max,
                                   :prefered_employment_type,
                                   :working_locations => [],
                                   :working_industries => [])
  end
end
