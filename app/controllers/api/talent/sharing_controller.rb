class Api::Talent::SharingController < Api::ApplicationController
  include TalentApi

  #load (email)company which should be revoked
  before_action :set_company, except: [:index]

  #authorize company access for revoking access
  authorize_resource :company, class: 'Company',  except: [:index]

  #authorize email company access for revoking access
  authorize_resource :email_company, class: 'EmailApplication',  except: [:index]


  def index
    @sharings = ResumeSharing.includes(:resume, :origin).unique_by_user(current_talent)
    respond_with @sharings, @options.merge(each_serializer: ResumeSharingSerializer, root: :sharings, log: true)
  end

  def revoke
    if sharing_params[:is_company_revocation]
      sharing = ResumeSharing.revoke current_talent, @company, sharing_params
    elsif @email_company
      sharing = ResumeSharing.revoke_email_application current_talent, @email_company, sharing_params
    end
    log_event self, sharing, request, @options.merge(log: { priority: :medium })
    render json: { meta: { notice: "Access revoked", message: :notice } }, status: 200
  end

  private

    def set_company
      if sharing_params[:is_company_revocation]
        @company = Company.find(params[:company_id])
      else
        @email_company = EmailApplication.find(params[:company_id])
      end
    end

    def sharing_params
      params[:sharing].symbolize_keys
    end
end
