class Api::Talent::VbSharingController < Api::ApplicationController
  include TalentApi

  # load client app which should be revoked
  before_action :set_client_application, except: [:index]

  # authorize company access for revoking access
  authorize_resource :client_application, class: 'ClientApplication',  except: [:index]

  def index
    @sharings = VeetaButtonSharingConnection.current_connections(current_talent)
    respond_with @sharings, @options.merge(each_serializer: VeetaButtonSharingSerializer, root: :sharings, log: true)
  end

  def revoke
    vbsc = VeetaButtonSharingConnection.find_by(talent_id: current_talent.id,client_application_id: @client_application.id, access_revoked_at: nil)
    if vbsc
      vbsc.revoke sharing_params[:revoke_reason]
      vbsc.third_party_data_wiped_out
      # invalidate token
      if !vbsc.oauth_token.blank?
        access_tokens = OauthToken.where(user_id: vbsc.oauth_token.user_id,client_application_id: vbsc.oauth_token.client_application_id,invalidated_at: nil)
        access_tokens.each do |t|
          t.invalidate!
        end
      end
    end
    log_event self, vbsc, request, @options.merge(log: { priority: :medium })
    render json: { meta: { notice: "Access revoked", message: :notice } }, status: 200
  end

  def third_party_data_wiped_out
    # get vbsc of the connection which of the data is not yet wiped out
    # only one entry for a talent+client_application can be without third_party_data_wiped_out_at date at a time
    vbsc = VeetaButtonSharingConnection.find_by(talent_id: current_talent.id,client_application_id: @client_application.id, third_party_data_wiped_out_at: nil)
    if vbsc
      vbsc.third_party_data_wiped_out
    end
    log_event self, vbsc, request, @options.merge(log: { priority: :medium })
    render json: { meta: { notice: "Access revoked", message: :notice } }, status: 200
  end

  private

    def set_client_application
      @client_application = ClientApplication.find(params[:client_application_id])
    end

    def sharing_params
      params[:vbsharing].permit(:revoke_reason)
    end
end
