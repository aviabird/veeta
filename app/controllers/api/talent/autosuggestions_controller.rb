class Api::Talent::AutosuggestionsController < Api::ApplicationController
  include TalentApi

  ###
  # No authorization required - just returning strings
  ###

  def index
    autosuggestions = Autosuggestion.retrieve_suggestion params
    suggestions = set_prorities(autosuggestions, params[:text]) || []
    respond_with suggestions, options(serialize_each: AutosuggestionSerializer, log: true)
  end

  private
    def set_prorities autosuggestions, search
      if autosuggestions.present?
        word = search.downcase
        list1, list2, list3 = [], [], []
        autosuggestions.each do |suggestion|
          if suggestion.text.downcase.start_with?(word)   # beginning of an autosuggest term
            list1 << suggestion
          elsif suggestion.text.downcase =~ / #{word}/  # beginning of a word in the autosuggest term
            list2 << suggestion
          else
            list3 << suggestion   # everyting else
          end
        end
        (list1 + list2 + list3).flatten.first(100)  # limit results to 100
      end
    end
end
