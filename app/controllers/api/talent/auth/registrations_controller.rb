module Api
  module Talent
    module Auth
      class RegistrationsController < DeviseTokenAuth::RegistrationsController
        include Api::Talent::Auth::LocaleDetector
        ###
        # No authorization required, override devise controller
        ###


        before_filter :set_user_by_token, :only => [:destroy, :update]
        skip_after_filter :update_auth_header, :only => [:create, :destroy,:linkedin_callback, :xing_callback,:create_via_file]
        before_action :configure_permitted_parameters
        before_action :create_via_token, only: :create
        before_action :set_devise_params
        before_action :set_locale
        before_action :ensure_email, only: :create
        before_action :set_reg_type, only: :create

        respond_to :json

        def linkedin_init
          tmp_import = TempCvImport.new
          tmp_import.source_type = :linkedin
          #save infos to tmp_import
          tmp_import.details[:ga_tracking_details] = tracking_params
          tmp_import.save
          begin
            callback_url = resume_linkedin_params[:callback]
            linkedin_client_service = LinkedinClientService.new
            linkedin_auth_url = linkedin_client_service.trigger_oauth_handshake(callback_url, nil, tmp_import)
            log_event self, linkedin_auth_url, request, {log: { priority: :low }}
            render json: { linkedin_auth_url: linkedin_auth_url }
          rescue StandardError => e
            logger.error("LinkedInApi failed: #{e}")
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, details: "Could not initialize LinkedIn import.", data: e, priority: :high}
            render json: { error: 'talent.errors.linkedin_import.linkedin_import_error' }, status: 400
          end
        end

        def linkedin_callback
          begin
            linkedin_client_service = LinkedinClientService.new
            temp_cv = linkedin_client_service.handle_linkedin_callback_without_talent(resume_linkedin_return_params)
            log_event self, temp_cv.token, request, {log: { priority: :low }}
            render_custom_reg_response temp_cv
          rescue XingApi::RateLimitExceededError, XingApi::ServerError,  XingApi::Error, StandardError => e
            logger.error("XingApi failed: #{e}")
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, details: "Could not process LinkedIn callback.", data: e, priority: :high}
            render json: { error: 'talent.errors.xing_import.xing_import_error' }, status: 400
          end
        end

        def xing_init
          tmp_import = TempCvImport.new
          tmp_import.source_type = :xing
          # save infos to tmp_import
          tmp_import.details[:ga_tracking_details] = tracking_params
          tmp_import.save
          begin
            callback_url = resume_xing_params[:callback]
            xing_client_service = XingClientService.new
            xing_auth_url = xing_client_service.trigger_oauth_handshake(callback_url, nil, tmp_import)
            log_event self, xing_auth_url, request, {log: { priority: :low }}
            render json: { xing_auth_url: xing_auth_url}
          rescue XingApi::RateLimitExceededError, XingApi::ServerError,  XingApi::Error => e
            logger.error("XingApi failed: #{e}")
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, details: "Could not initialize Xing import.", data: e, priority: :high}
            render json: { error: 'talent.errors.xing_import.xing_import_error' }, status: 400
          end
        end

        #callback action after xing authorization
        def xing_callback
          begin
            xing_client_service = XingClientService.new
            temp_cv = xing_client_service.handle_xing_callback_without_talent(resume_xing_return_params)
            log_event self, temp_cv.token, request, {log: { priority: :low }}
            render_custom_reg_response temp_cv
          rescue XingApi::RateLimitExceededError, XingApi::ServerError,  XingApi::Error, StandardError => e
            logger.error("XingApi failed: #{e}")
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, details: "Could not process Xing callback.", data: e, priority: :high}
            render json: { error: 'talent.errors.xing_import.xing_import_error' }, status: 400
          end
        end

        # create a user by uploading a file
        def create_via_file
          begin
            parsed_pdf = ResumeService::ParsePDF.call(resume_pdf_params[:file].path).hashed
          rescue ResumeService::ParsePDFError => e
            render json: { error: 'talent.errors.cv_import.pdf_import_error' }, status: 400
            return
          rescue ResumeService::ServiceOutageError => error
            render json: { error: 'talent.errors.cv_import.service_outage_error' }, status: 503
            return
          end
          # create empty talent + talent_auth with random email

          sign_up_params = {}
          sign_up_params[:email] = parsed_pdf[:profile][:personal][:email]

          if sign_up_params[:email].blank?
            #set custom signup email as no email was extracted
            sign_up_params[:email] = "#{SecureRandom.hex(4)}@tmp.myveeta.com"
            while TalentAuth.exists?(email: sign_up_params[:email].downcase) do
              sign_up_params[:email] = "#{SecureRandom.hex(4)}@tmp.myveeta.com"
            end
          end

          tmp_import = TempCvImport.new
          tmp_import.email = sign_up_params[:email]
          tmp_import.source_type = :file
          #save infos to tmp_import
          tmp_import.details[:ga_tracking_details] = tracking_params
          tmp_import.cv_content = parsed_pdf
          tmp_import.save
          log_event self, tmp_import, request, {log: { priority: :low }}
          render_custom_reg_response tmp_import

        end

        def create
          guest_auth = AuthService::ReinitializeTalent.call(sign_up_params[:email], sign_up_params)
          #AuthService::ReinitializeTalent.call(sign_up_params[:email])
          if guest_auth.present?
            @resource = guest_auth
            @resource.update(sign_up_params)
          else
            @resource = resource_class.new(sign_up_params)
          end
          @resource.uid = sign_up_params[:email]
          @resource.provider = "email"
          @resource.guest = sign_up_params[:password].blank? || params['is_guest'] == true   # if password set, no guest anymore
          # success redirect url is required
          unless params[:confirm_success_url]
            return render json: {
                status: 'error',
                #data: @resource,
                errors: ["Missing `confirm_success_url` param."]
            }, status: 422
          end

          begin
            # override email confirmation, must be sent manually from ctrl
            resource_class.skip_callback("create", :after, :send_on_create_confirmation_instructions)
            if @resource.save
              # todo skip confirmation because of dev server env
              # @resource.confirm!
              unless @resource.confirmed? || @resource.guest?
                # user will require email authentication
                @resource.send_confirmation_instructions
              end
              # email auth has been bypassed, authenticate user

              @user = @resource
              @client_id = SecureRandom.urlsafe_base64(nil, false)
              @token = SecureRandom.urlsafe_base64(nil, false)

              @user.tokens[@client_id] = {
                  token: BCrypt::Password.create(@token),
                  expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
              }

              @user.save!
              #if token, assign new resume
              unless @temp_cv.blank?
                @resume_id = @temp_cv.connect_to_talent @user.talent
                LogService.call "#{self.class.to_s}.#{self.action_name}.#{@reg_type}.#{@temp_cv.source_type}.success", {request: request, email: @user.email , resource: @resource, details: "Successfully created user.", priority: :low, tracking_params: @temp_cv.details[:ga_tracking_details] }
              else
                LogService.call "#{self.class.to_s}.#{self.action_name}.#{@reg_type}.success", {request: request, email: @user.email, details: "Successfully created user.", resource: @resource, priority: :low, tracking_params: tracking_params}
              end

              update_auth_header
              render json: {
                  status: 'success',
                  resumeId: @resume_id,
                  data: @resource.token_validation_response
              }
            else
              clean_up_passwords @resource
              LogService.call "#{self.class.to_s}.#{self.action_name}.#{@reg_type}.failed", {request: request, details: "Could not create user.", resource: @resource, priority: :high}
              render json: {
                  status: 'error',
                  #data: @resource,
                  errors: @resource.errors.to_hash.merge(full_messages: @resource.errors.full_messages)
              }, status: 422
            end
          rescue ActiveRecord::RecordNotUnique
            clean_up_passwords @resource
            #LogWrapper.new self, @resource, request, {log: { priority: :high, user: @resource }}
            LogService.call "#{self.class.to_s}.#{self.action_name}.#{@reg_type}.failed", {request: request, details: "Could not create user, email already exists.", resource: @resource, priority: :high}
            render json: {
                status: 'error',
                #data: @resource,
                errors: ["An account already exists for #{@resource.email}"]
            }, status: 422
          end
        end

        def update
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 422
        end

        def destroy
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 422
        end

        def edit
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 406
        end

        def new
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 406
        end

        def cancel
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 406
        end

        protected

        def resume_pdf_params
          params[:resume_pdf].permit(:file)
        end

        def resume_xing_params
          params.permit(:callback, :oid, :cn, :cs, :cm)
        end

        def resume_xing_return_params
          params.permit(:oauth_verifier,:oauth_token, :token)
        end

        def resume_linkedin_params
          params.permit(:callback, :oid, :cn, :cs, :cm)
        end

        def resume_linkedin_return_params
          params.permit(:oauth_verifier, :oauth_token, :token)
        end

        def configure_permitted_parameters
          devise_parameter_sanitizer.for(:sign_up).delete(:password_confirmation)
          devise_parameter_sanitizer.for(:sign_up) << [:reg_type, :resume_type, :country_id, :signed_ip, :is_guest, :first_name, :last_name, :password, :gender]
        end

        def set_devise_params
          params['signed_ip'] = request.env['REMOTE_ADDR']
          params['password'] = Devise.friendly_token.first(8) if params['is_guest'] == true
        end

        # if registration w/o email address - make a fake one
        def ensure_email
          if sign_up_params[:email].blank?
            #set custom signup email as no email was extracted
            params[:email] = "#{SecureRandom.hex(4)}@tmp.myveeta.com"
            while TalentAuth.exists?(email: sign_up_params[:email].downcase) do
              params[:email] = "#{SecureRandom.hex(4)}@tmp.myveeta.com"
            end
          end
        end

        # set reg type for logging
        def set_reg_type
          #byebug
          @reg_type = 'user_reg'
          if params['reg_type'] && params['reg_type'] == 'job_link'
            @reg_type = 'job_link'
          elsif params['reg_type'] && params['reg_type'] == 'pool_link'
            @reg_type = 'pool_link'
          end
        end

        # render response for creating an account via xing/linkedin/file
        def render_custom_reg_response temp_cv
          AuthService::ReinitializeTalent.call(temp_cv.email)
          if !TalentAuth.exists?(email: temp_cv.email.try(:downcase), guest: false)
            render json: {
                status: 'email_not_existing',
                data: {email:  temp_cv.email, token: temp_cv.token},
                errors: ["Account with email #{temp_cv.email} can be created"]
            }, status: 200
          else
            render json: {
                status: 'email_existing',
                data: {email:  temp_cv.email, token: temp_cv.token},
                errors: ["An account already exists for #{temp_cv.email}"]
            }, status: 422
          end
        end

        # if token for temp cv is present, load it and set default values
        def create_via_token
          if params && params[:token]
            @temp_cv = TempCvImport.find_not_expired_by_token(params[:token])
            params['resume_type'] = 50
            params['is_guest'] = true
          end
        end

        # get GA campaign infos for tracking
        def tracking_params
          if request.try(:params)
            t_params = [URI::encode(params["oid"]||''), URI::encode(params["cn"]||''), URI::encode(params["cs"]||''), URI::encode(params["cm"]||'')]
            t_params.flatten.join('|') unless t_params.reject{|e| e.blank?}.blank?
          end
        end

      end
    end
  end
end
