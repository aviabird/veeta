module Api
  module Talent
    module Auth
      class TokenValidationsController < DeviseTokenAuth::TokenValidationsController
        ###
        # No authorization required, override devise controller
        ###
      end
    end
  end
end
