
module Api::Talent::Auth::LocaleDetector
  def set_locale

    if request.headers['x-locale'] == 'xx'
      request.headers['x-locale'] = nil
      if !request.env['HTTP_ACCEPT_LANGUAGE'].nil?
        request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      else
        :de  #default = de
      end
    end
    I18n.locale = request.headers['x-locale'] || (current_user.locale if current_user && current_user.respond_to?(:locale)) || I18n.default_locale
  end
end
