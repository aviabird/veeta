class Api::Talent::Auth::DevisePasswordsController < Devise::PasswordsController

  ###
  # No authorization required, override devise controller
  ###

  respond_to :json
  before_filter :set_default_response_format

  #set after forgot password
  def update
    if !resource_params[:password] || !resource_params[:password_confirmation] || resource_params[:password] != resource_params[:password_confirmation]
      resource = resource_class.new
      resource.errors.add(:password, 'needs to be filled out') unless resource_params[:password]
      resource.errors.add(:password_confirmation, 'needs to be filled out') unless resource_params[:password_confirmation]
      resource.errors.add(:password, 'doesnt match') if resource_params[:password] != resource_params[:password_confirmation]
      respond_with resource
    else
      super
      LogService.call "#{self.class.to_s}.#{self.action_name}", {resource: self.resource, details: "Password reset", priority: :medium}
    end
  end

  def create
    render json: {
        status: 'error',
        errors: ["Not allowed to perform this operation."]
    }, status: 406
  end

  def new
    render json: {
        status: 'error',
        errors: ["Not allowed to perform this operation."]
    }, status: 406
  end

  def edit
    render json: {
        status: 'error',
        errors: ["Not allowed to perform this operation."]
    }, status: 406
  end

  private
    def set_default_response_format
      request.format = :json
    end
end
