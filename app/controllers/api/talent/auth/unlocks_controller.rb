module Api
  module Talent
    module Auth
      class UnlocksController < Devise::UnlocksController
        include BaseLogger
        ###
        # No authorization required, override devise controller
        ###

        respond_to :json
        before_filter :set_default_response_format

        # POST /resource/unlock
        def create
          #todo avoid resend within x minutes
          @options = {}
          self.resource = resource_class.send_unlock_instructions(unlock_params)
          if !self.resource.persisted?
            #log_event self, unlock_params, request, @options.merge(log: { priority: :medium, details: "Unlock instructions could not be sent for #{unlock_params}" })
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, email: params['email'], resource: self.resource, details: "Unlock instructions could not be sent for #{unlock_params}", priority: :medium}
          elsif self.resource.persisted? && self.resource.access_locked?
            LogService.call "#{self.class.to_s}.#{self.action_name}.success", {request: request, email: params['email'], resource: self.resource, details: "Unlock instructions requested for #{unlock_params}", priority: :medium}
          else
            LogService.call "#{self.class.to_s}.#{self.action_name}.unable", {request: request, email: params['email'], resource: self.resource, details: "Unlock instructions requested though account is not locked for #{unlock_params}", priority: :high}
          end
          yield resource if block_given?

          render json: {
            message: ["Instructions sent."]
          }, status: 200

        end

        # GET /resource/unlock?unlock_token=abcdef
        def show
          self.resource = resource_class.unlock_access_by_token(params[:unlock_token])
          if !self.resource.persisted?
            #log_event self, unlock_params, request, @options.merge(log: { priority: :medium, details: "Unlock instructions could not be sent for #{unlock_params}" })
            LogService.call "#{self.class.to_s}.#{self.action_name}.failed", {request: request, resource: self.resource, details: "Could not unlock with token #{params}", priority: :medium}
          else
            LogService.call "#{self.class.to_s}.#{self.action_name}.success", {request: request, resource: self.resource, details: "Unlocked successfully with token #{params}", priority: :low}
          end
          yield resource if block_given?
          #todo: log
          #LogWrapper.new self, @resource, request, {log: { priority: :medium }}
          render json: {
            message: ["Unlocked."]
          }, status: 200
        end

        def new
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 406
        end

        private
          def set_default_response_format
            request.format = :json
          end

          def unlock_params
            params.permit(:email)
          end

      end
    end
  end
end
