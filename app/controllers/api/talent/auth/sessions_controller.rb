module Api
  module Talent
    module Auth
      class SessionsController < DeviseTokenAuth::SessionsController
        respond_to :json
        before_filter :set_default_response_format
        before_filter :set_devise_token_auth_lockable_fix, only: :create
        ###
        # No authorization required, override devise controller
        ###

        # Last update 3.5.2015
        # https://github.com/lynndylanhurley/devise_token_auth/blob/master/app/controllers/devise_token_auth/sessions_controller.rb
        def create
          # Check
          field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

          @resource = nil
          if field
            q_value = resource_params[field]

            if resource_class.case_insensitive_keys.include?(field)
              q_value.downcase!
            end

            q = "#{field.to_s} = ? AND provider='email'"

            if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
              q = "BINARY " + q
            end

            @resource = resource_class.where(q, q_value).first
          end
          @resource.extend(DeviseTokenAuthLockableFix)
          # This line was updated for VEETA-1836 to allow users to sign-in even if they haven't confirmed their account
          # They still need to confirm to complete a job application, however.
          # if @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password]) and @resource.confirmed?
#          if @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password])
          if @resource && !@resource.access_locked? && @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password])
            # create client id
            @client_id = SecureRandom.urlsafe_base64(nil, false)
            @token     = SecureRandom.urlsafe_base64(nil, false)

            @resource.tokens[@client_id] = {
              token: BCrypt::Password.create(@token),
              expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
            }
            @resource.save
            @resource.user.update(new: false)

            sign_in(:user, @resource, store: false, bypass: false)
            LogWrapper.new self, @resource, request, {log: { priority: :medium, user: @resource }}

            render json: {
              data: @resource.token_validation_response
            }
          elsif @resource && @resource.access_locked?
            #account is already locked^
            render json: {
              errors: ["talent.ui.sign_in.locked_account","enable-action"],
            }, status: 422
            LogService.call "#{self.class.to_s}.#{self.action_name}.login_attempt_with_locked_account", {request: request, email: resource_params['email'], resource: @resource, details: "Tried login with locked account: #{params.except(:password,:session,:action,:controller)}", priority: :medium}
            @resource = nil
          else
            @resource.valid_for_authentication? if @resource
            if @resource &&  @resource.access_locked?
              #account was just locked in the current login attempt
              render json: {
                errors: ["talent.ui.sign_in.locked_account","enable-action"],
              }, status: 422
              LogService.call "#{self.class.to_s}.#{self.action_name}.login_failed_account_locked", {request: request, email: resource_params['email'], resource: @resource, details: "Login failed and account was locked due to too many attempts with parameters: #{params.except(:password,:session,:action,:controller)}", priority: :medium}
            else
              # login failed
              render json: {
                errors: ["talent.ui.sign_in.invalid_credentials"]
              }, status: 422
              LogService.call "#{self.class.to_s}.#{self.action_name}.login_failed", {request: request, email: resource_params['email'], resource: @resource, details: "Login failed with parameters: #{params.except(:password,:session,:action,:controller)}", priority: :medium}
            end
          #  LogWrapper.new self, response.body, request, {log: { priority: :medium }}
          end
        end

        def destroy
          LogWrapper.new self, @resource, request, {log: { priority: :medium, user: @resource }}
          super
        end

        def new
          render json: {
              status: 'error',
              errors: ["Not allowed to perform this operation."]
          }, status: 406
        end

        private
          def set_default_response_format
            request.format = :json
          end

          def set_devise_token_auth_lockable_fix

          end
      end
    end
  end
end
