class Api::Talent::Auth::ConfirmationsController < Devise::ConfirmationsController

  ###
  # No authorization required, override devise controller
  ###

  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    if resource.errors.empty?
      render json: { message: :success }, status: 200
    else
      render json: { message: :fail }, status: 422
    end
    LogService.call "#{self.class.to_s}.#{self.action_name}", {resource: self.resource, details: "E-mail confirmation", priority: :medium}
  end

  def create
    render json: {
        status: 'error',
        errors: ["Not allowed to perform this operation."]
    }, status: 406
  end

  def new
    render json: {
        status: 'error',
        errors: ["Not allowed to perform this operation."]
    }, status: 406
  end
end
