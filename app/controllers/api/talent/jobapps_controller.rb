class Api::Talent::JobappsController < Api::ApplicationController
  include TalentApi

  # secure jobapp
  load_and_authorize_resource except: [:index]

  #set resume as part of the jobapp object
  before_action :set_resume, except: [:index]

  #secure resume attached to jobapp
  authorize_resource :resume, class: 'Resume', except: [:index]

  # set job_type
  before_action :set_job_type, only: [:create, :update]

  #set resume as part of the jobapp object
  before_action :set_job, except: [:index]
  before_action :set_email_application, except: [:index]

  # check job access
  authorize_resource :job, class: 'Job',  only: [:new]
  authorize_resource :email_application, class: 'EmailApplication',  only: [:new]

  def index
    combined = ActivityService.new.sorted_activities current_talent
    respond_with combined, options(serialize_each: JobappSerializer, log: true)
  end

  def show
    # a jobapp should always be viewable, even if job was deleted or invalidated
    # @jobapp.check
    respond_with @jobapp, options(log: true)
  end

  def new
    @jobapp = Jobapp.prepare(jobapp_new_params)
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def create
    if params[:jobapp][:job_attributes]     #stared application as email application
      if params[:jobapp][:job_attributes][:unsolicited]   #is unsolicited checkbox ticked?
        #try to fetch unsolicited job of this recruiter's company
        job = fetch_unsolicited_job_from_company
        if job.present?
          @jobapp = Jobapp.prepare_unsolicited_job_application(job.id, 'email_application', jobapp_params[@resume.id])
        end
      end
      #create new, if not yet created with  unsolicited job of this recruiter's company
      @jobapp = Jobapp.create(jobapp_params_with_job_id.merge!(started_as: 'EmailApplication')) unless @jobapp.persisted?
    elsif params[:jobapp][:started_as] == 'EmailApplication'  # create jobapplication to company from email application page
      @jobapp = Jobapp.create(jobapp_params_with_job_id.merge!(started_as: 'EmailApplication'))
    else   # create regular jobapp from landing page
      @jobapp = Jobapp.create(jobapp_params_with_job_id.merge!(started_as: 'JobApplication'))
    end
    #resume_id = params[:jobapp][:resume_id]    # rather get resumes id from loaded resume
    @jobapp.assign_current_resume_version(@resume.id) if @resume.present?
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def update
    @jobapp.assign_current_resume_version(@resume.id) if @resume.present?
    @jobapp.update(jobapp_params)
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def destroy
    @jobapp.destroy
    respond_with @jobapp, options(log: { priority: :high })
  end

  def validate
    @jobapp = Jobapp.new(complet_params)
    @jobapp.valid?
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def withdrawn
    @jobapp.withdrawn params[:jobapp][:withdrawn_reason]
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def finish
    jobapp_params.merge!(source: 'other_source')  if params[:jobapp][:referrer_id].present? 
    service = ApplyService::Apply.call(@jobapp,jobapp_params)
    @jobapp = service.jobapp
    respond_with @jobapp, options(service.info.merge(log: { priority: :high }))
  end

  private

    # fetch unsolicited job from a company
    def fetch_unsolicited_job_from_company
      recruiter = find_recrutier params[:jobapp][:job_attributes][:recruiter_email] if  params[:jobapp][:job_attributes]
      if recruiter.present?
        company = recruiter.try(:company)
        JobService::GenericJobReadingService.call_list company
        unsolicited_jobs = company.jobs.where(unsolicited_job: true, status: "new")
        if unsolicited_jobs.present?      # an unsolicited job was found - convert Jobapp into Jobapp connected to a real job
          return unsolicited_jobs.first
        end
      end
    end

    def find_recrutier(email)
      # method to find recruiter by email or based on domain of company
      recruiter = ::Recruiter.where('lower(email) = ?', email.try(:downcase)).first
      if recruiter.blank? && email
        email_array = email.split("@")
        domain = email_array[1] if email_array.length > 1
        recruiter = ::Recruiter.where("email LIKE ?", "%@#{domain}").first if domain.present?
      end
      recruiter
    end

    def set_resume
      unless params[:resume_id].blank?
        @resume = Resume.find(params[:resume_id])
        # if resume is already found, ensure that only this resume ID is used
        params[:jobapp][:resume_id] = @resume.id if params[:jobapp].present? && params[:jobapp][:resume_id].present?
      end
      if @resume.blank? && !params[:jobapp].blank? && !params[:jobapp][:resume_id].blank?
        @resume = Resume.find(params[:jobapp][:resume_id])
      end
    end

    def jobapp_params
      params[:jobapp].permit(:resume_id, :job_type, :cover_letter, :contact_preferences,:started_as,
        :allow_recruiter_contact_sharing, :withdrawn_reason, :terms,  :source, :source_details, :referrer_id,
        job_attributes: [:id, :company_name, :jobname, :recruiter_email, :unsolicited ], :blocked_companies=>[])
    end

    def complet_params
      jobapp_params.merge!(talent: current_talent)
    end

    def jobapp_new_params
      params.permit(:resume_id, :job_id, :application_referrer_url)
    end

    def jobapp_params_with_job_id
      jobapp_params.merge!(job_id: params[:jobapp][:job_id])
    end

    def set_job
      if !params[:jobapp].blank? && params[:jobapp][:job_type] == 'Job' && @job.blank? && !params[:jobapp][:job_id].blank?
        @job = Job.find(params[:jobapp][:job_id])
      end
    end

    def set_email_application
      if !params[:jobapp].blank? && params[:jobapp][:job_type] == 'EmailApplication' && @email_application.blank? &&  !params[:jobapp][:job_id].blank?
        @email_application = EmailApplication.find(params[:jobapp][:job_attributes][:id])   #email application id can always be found in job attributes
      end
    end

    def set_job_type
      if Job.exists?(params[:jobapp][:job_id])
        params[:jobapp][:job_type] = "Job"
      elsif  EmailApplication.exists?(params[:jobapp][:job_id])
        params[:jobapp][:job_type] = "EmailApplication"
      end
    end
    #
    # def resume
    #   Resume.find(params[:resume_id]) if params[:resume_id]
    # end
end
