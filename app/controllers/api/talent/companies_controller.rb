class Api::Talent::CompaniesController < Api::ApplicationController
  include TalentApi

  #authorize company access for revoking access
  load_and_authorize_resource :company, class: 'Company',  except: [:index]

  before_action :set_talent

  def index
    @companies = Company.in_touch(@talent)
    respond_with @companies, @options.merge(each_serializer: CompanySerializer, log: true)
  end

  private
    def set_talent
      @talent = current_talent
    end
end
