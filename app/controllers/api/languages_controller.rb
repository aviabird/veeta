class Api::LanguagesController < Api::ApplicationController


  ###
  # No authorization required, access to languages is public
  ###

  def index
    @languages = Language.all
    respond_with @languages, @options.merge(each_serializer: CountryCollectionSerializer)
  end
end
