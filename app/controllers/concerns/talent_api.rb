module TalentApi
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_talent_auth!

    include DeviseTokenAuth::Concerns::SetUserByToken

    alias_method :current_user, :current_talent_auth
    alias_method :user_signed_in?, :talent_auth_signed_in?

    acts_as_token_authentication_handler_for TalentAuth, fallback_to_devise: false

  end

  def current_talent
    current_user.talent if current_user
  end

  def current_ability
    @current_ability ||= Ability::TalentAbility.new(current_talent)
  end
end
