class FileRedirectController < ActionController::Base
  include BaseLogger
  include Oauth::BrowserLanguageDetector

  ###
  # No authorization required, access to static files
  ###

  # this controller redirects requests to "vitual" paths to physical paths



  # localhost:3000/file/t/jw4lmhtm/u50nz5e0l7dyokahleaxhql0kohxazmd/please_replace_me/small_335249cc-788d-4292-ab6c-a76fae94106c.png
  # /file/:file_type/:date_hash/:random_security_hash/:access_security_part/:file_name
  def resource
    val = AccessSecurityTokenMapping.find_by(individual_key: params[:access_security_part]) if  AccessSecurityTokenMapping.exists?(individual_key: params[:access_security_part])
    if !val.blank?
      url_path = File.join(params[:file_type], params[:date_hash], params[:random_security_hash], val.access_security_part, params[:file_name])
      redirect_to "#{ENV['WEBDAV_READ_URL']}/dl/#{url_path}", :status => 301
    else
      respond_to do |format|
        format.json { render json: { error: "resource not found" }, status: 404 }
        format.all { render template: "error_pages/404.html.erb", content_type: 'text/html', layout: 'layouts/common', status: 404 }
      end
      if AccessSecurityTokenMapping.with_deleted.exists?(individual_key: params[:access_security_part])
        res = AccessSecurityTokenMapping.with_deleted.find_by(individual_key: params[:access_security_part])
        log_event self, res, request, {log: { priority: :medium }}
      else
        log_event self, params[:access_security_part], request, {log: { priority: :high }}
      end
    end
  end

  #temp user for logging
  def current_user
    RecruiterAuth.new( :email => "guest_#{Time.now.to_i}#{rand(99)}@example.com")
  end

  #random for logging
  def current_email
    "guest_#{Time.now.to_i}#{rand(99)}@t13s.at"
  end

end
