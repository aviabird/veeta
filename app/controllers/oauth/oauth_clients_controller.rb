module Oauth
  class OauthClientsController < Oauth::ApplicationController

    ###
    # Authorized admins can manage all client applications, no further authorization required
    ###

    before_filter :authenticate_admin_auth!
    before_filter :get_client_application, :only => [:show, :edit, :update, :destroy]
    before_filter :set_default_response_format
    layout 'layouts/admin_auths/admin_auths_layout'


    def index
      @client_applications = current_user.client_applications
      @tokens = []# current_user.tokens.find :all, :conditions => 'oauth_tokens.invalidated_at is null and oauth_tokens.authorized_at is not null'
    end

    def new
      @client_application = ClientApplication.new
    end

    def create
      @client_application = ClientApplication.new(client_application_params)

      if @client_application.save
        flash[:notice] = "Registered the information successfully"
        redirect_to :action => "show", :id => @client_application.id
      else
        render :action => "new"
      end
    end

    def show
    end

    def edit
    end

    def update
      if @client_application.update_attributes(client_application_params)
        flash[:notice] = "Updated the client information successfully"
        redirect_to :action => "show", :id => @client_application.id
      else
        render :action => "edit"
      end
    end

    def destroy
      @client_application.destroy
      flash[:notice] = "Destroyed the client application registration"
      redirect_to :action => "index"
    end


    def current_user
      return current_admin_auth if current_admin_auth
    end


    private
    def set_default_response_format
      request.format = :html
    end


    def client_application_params
        params[:client_application].permit(:name,:url,:callback_url,:support_url,:delete_user_profile_deep_link,:logo,:webhook_url, :cancel_url,:use_cancel_callback, :contact)
    end

    def get_client_application
      unless @client_application = current_user.client_applications.find(params[:id])
        flash.now[:error] = "Wrong application id"
        raise ActiveRecord::RecordNotFound
      end
    end
  end
end
