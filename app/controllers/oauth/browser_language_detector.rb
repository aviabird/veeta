module Oauth
  module BrowserLanguageDetector

    def self.included(base)
        base.before_filter :set_locale
    end

    def set_locale
      I18n.locale = extract_locale_from_accept_language_header

    end

    def not_found
      raise ActionController::RoutingError.new('Not Found')
    end

    private

      def extract_locale_from_accept_language_header
        if !request.env['HTTP_ACCEPT_LANGUAGE'].nil?
          request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
        else
          'en'
        end
      end
  end
end
