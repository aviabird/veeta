module Oauth
  class ApplicationController < ActionController::Base
    include Oauth::BrowserLanguageDetector
    include BaseLogger

    ###
    # No authorization required, base controller
    ###

    alias :current_user :current_talent_auth

    def current_ability
      @current_ability ||= Ability::TalentAbility.new(current_user.talent)
    end

    rescue_from CanCan::AccessDenied do |exception|
      render json: { error: :unauthorized}, status: 403
    end

  end
end
