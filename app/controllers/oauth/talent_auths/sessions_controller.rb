module Oauth

  class TalentAuths::SessionsController < Devise::SessionsController
    include Oauth::BrowserLanguageDetector

    ###
    # No authorization required, override devise controller
    ###

    layout 'talent_auths/talent_auths_layout'

    # check if referer matches with client_application
    before_filter :check_referer, except: [:create, :destroy]


  # before_filter :configure_sign_in_params, only: [:create]

    # GET /resource/sign_in
#     def new

#     end

    def check_referer
      if session[:client_application_name].blank? || session[:client_application_url].blank? || !request.referer #|| !request.referer.starts_with?(session[:client_application_url])
        render json: '{"Message":"Not found"}', status: 404
        false
      end
    end

    # POST /resource/sign_in
     def create
       #error message
       flash[:custom] = t('devise.failure.invalid')
       super
     end

    # DELETE /resource/sign_out
    # def destroy
    #   super
    # end

    # protected

    # You can put the params you want to permit in the empty array.
    # def configure_sign_in_params
    #   devise_parameter_sanitizer.for(:sign_in) << :attribute
    # end
  end
end
