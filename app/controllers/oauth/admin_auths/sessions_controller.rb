  module Oauth
  class AdminAuths::SessionsController < Devise::SessionsController
    include Oauth::BrowserLanguageDetector
    http_basic_authenticate_with name: ENV['ADMIN_USERNAME'], password: ENV['ADMIN_PASSWORD']

    ###
    # No authorization required, override devise controller
    ###

    layout 'admin_auths/admin_auths_layout'

    def after_sign_in_path_for(resource)
      '/'
    end

  # before_filter :configure_sign_in_params, only: [:create]

    # GET /resource/sign_in
    # def new
    #   super
    # end

    # POST /resource/sign_in
    # def create
    #   super
    # end

    # DELETE /resource/sign_out
    # def destroy
    #   super
    # end

    # protected

    # You can put the params you want to permit in the empty array.
    # def configure_sign_in_params
    #   devise_parameter_sanitizer.for(:sign_in) << :attribute
    # end
  end
end
