require 'oauth/controllers/provider_controller'
module Oauth

  class OauthController < Oauth::ApplicationController
    include OAuth::Controllers::ProviderController

    # authorize access to resume upon resume selection
    before_action :set_resume, only: [:authorize]
    authorize_resource :resume, class: 'Resume', only: [:authorize]

    prepend_before_action :prepare_login_mask, only: :authorize
    before_filter :authenticate_talent_auth!, :except => [:request_token, :cancel_oauth_handshake, :access_token]
    layout 'layouts/talent_auths/talent_auths_layout'

    alias :logged_in? :talent_auth_signed_in?
    alias :login_required :authenticate_talent_auth!
    alias :current_user :current_talent_auth

    def current_email
      current_talent_auth ? current_talent_auth.email : nil
    end

    def authorize
      vbsc_already_exists = false
      show_cv_selection_mask = false
      super
      if current_user && current_user.talent && @token && @token.client_application && !@token.expired? && !@token.invalidated?
        vbsc = VeetaButtonSharingConnection.find_by(talent_id: current_user.talent.id, client_application_id:  @token.client_application.id, access_revoked_at: nil)
        vbsc_already_exists =  !vbsc.nil?
        # no connection yet or resume shared with the current connection was deleted
        if vbsc_already_exists && !vbsc.latest_sharing_of_connection.resume.nil?
          vbsc_already_exists = !vbsc.latest_sharing_of_connection.resume.deleted?
        end
        # no resume shared but resumes available
        if vbsc_already_exists && vbsc.latest_sharing_of_connection.resume.nil?
          current_resumes = Resume.my(current_user.talent)
          show_cv_selection_mask = !current_resumes.nil? && !current_resumes.empty?
        end
        if !vbsc_already_exists || show_cv_selection_mask
          @resumes = Resume.my current_user.talent
        elsif !performed? # valid sharing exists, try to make callback directly after login
          if !@token || @token.invalidated?
            render :action=>"authorize_failure"
            return
          else
            @token.authorize!(current_user)
            callback_url  = @token.oob? ? @token.client_application.callback_url : @token.callback_url
            @redirect_url = URI.parse(callback_url) unless callback_url.blank?
            unless @redirect_url.to_s.blank?
              @redirect_url.query = @redirect_url.query.blank? ?
                                    "oauth_token=#{@token.token}&oauth_verifier=#{@token.verifier}" :
                                    @redirect_url.query + "&oauth_token=#{@token.token}&oauth_verifier=#{@token.verifier}"
              redirect_to @redirect_url.to_s
              sign_out
            end
          end
        else
          sign_out
        end
      else
        render :action=>"authorize_failure"
      end
      log_event self, @token, request, {log: { priority: :medium }}
    end

    def request_token
      super
      # set response to 201 when successfully processed - as xing does
      if @token && response.status = 200
        response.status = 201
      end
      log_event self, @token, request, {log: { priority: :medium }}
    end

    def access_token
      super
      if @token && @token.client_application && !@token.invalidated?
        vbsc = VeetaButtonSharingConnection.find_by(talent_id: @token.user.talent.id, client_application_id:  @token.client_application.id, access_revoked_at: nil)
        vbsc.oauth_token = @token
        vbsc.save!
        # set response to 201 when successfully processed - as xing does
        if response.status = 200
          response.status = 201
        end
      end
      log_event self, @token, request, {log: { priority: :medium }}
    end

    def cancel_oauth_handshake
      referer = session[:cancel_url]
      session[:cancel_url] = nil
      session[:client_application_name] = nil
      session[:client_application_url] = nil
      sign_out
      redirect_to referer
    end

    def prepare_login_mask
      token = RequestToken.find_by_token! params[:oauth_token]
      if request.referer.blank?  || !request.referer.starts_with?(ENV['HOST'])
        sign_out
      end
      unless token.nil? || token.invalidated?
        # handle cancel button url
        if token.client_application.use_cancel_callback
          if token.client_application.cancel_url.blank?
            session[:cancel_url] = token.client_application.cancel_url = request.referer
          else
            session[:cancel_url] = token.client_application.cancel_url
          end
        else # else -> no cancel button
          session[:cancel_url] = nil
        end

        #set app name for login mask
        unless talent_auth_signed_in?
          session[:client_application_name] = token.client_application.name
          session[:client_application_url] = token.client_application.url
        end
      end
    end

    protected
      # Override this to match your authorization page form
      # It currently expects a checkbox called authorize
      def user_authorizes_token?
        #create a new VeetaButtonSharing
        set_resume
        vbs = VeetaButtonSharing.find_or_create(current_user.talent, @token.client_application, @resume ? @resume.id : nil)
        vbs.save
      end

    private
      def set_resume
        if !params[:authorize].blank?
          @resume = Resume.find(params[:authorize])
        end
      end

  end

end
