require 'xing_profile_serializer'

class Oauth::AccessController < Oauth::ApplicationController

  ###
  # No common authorization required, oauth authorization via oauth_required before filter
  ###

  #no devise authentication required for profile action
  before_filter :authenticate_talent_auth!, :except => [:profile]

  #respond_to :json
  before_filter :set_default_response_format

  #ensure that oauth authorization is enforced
  before_filter :oauth_required , :only => [:profile]


  def profile
    # get current veeta button sharing
    vbsc = VeetaButtonSharingConnection.find_by(talent_id: current_token.user.talent.id,client_application_id: current_client_application.id,access_revoked_at: nil)
    if vbsc.nil? #not found
      render json: '{"Message":"No access."}', status: 401
      log_event self, response.body, request, {log: { priority: :high }}
    elsif vbsc.latest_sharing_of_connection.resume && vbsc.latest_sharing_of_connection.resume.deleted? # resume was deleted but not yet synced, create sharing entry with no resume, sync pofile only
      vbs = VeetaButtonSharing.new
      vbs.resume = nil
      vbs.veeta_button_sharing_connection = vbsc
      vbs.save!
      AccessSecurityService.new.establish_sharing vbs, vbsc.talent
      serialize_profile_only vbs
      log_event self, vbs, request, {log: { priority: :medium }}
    elsif vbsc.latest_sharing_of_connection.resume.nil? # resume was deleted, sync pofile only
      AccessSecurityService.new.establish_sharing vbsc.latest_sharing_of_connection, vbsc.talent
      serialize_profile_only vbsc.latest_sharing_of_connection
      log_event self, vbsc, request, {log: { priority: :medium }}
    elsif !vbsc.access_revoked_at.nil? #access was revoked, should never happen, as tokens are invalidated upon revoking access
      render json: '{"Message":"No access."}', status: 401
      log_event self, response.body, request, {log: { priority: :high }}
    else
      # clone the currently shared resume if new version needs to be created
      vbs = vbsc.latest_sharing_of_connection
      clone = Resume::Share.clone(vbs.resume.origin)
      latest = vbs.resume.origin.latest

      # is latest shared version the most recent one?
      if latest.id != vbs.resume.id
        #share the new version
        vbs = VeetaButtonSharing.new
        vbs.resume = latest
        vbs.veeta_button_sharing_connection = vbsc
        AccessSecurityService.new.establish_sharing vbs
        vbs.save!
      end
      vbs.update_column(:last_synced_at, DateTime.now)
      users = []
      users <<  vbs.resume
      log_event self, users, request, {log: { priority: :medium }}
      render json: users, each_serializer: XingProfileSerializer, root: "users", client_application: vbsc.client_application
    end
  end


  def current_user=(user)
      current_talent_auth = user
  end

  def current_user
    current_token.user.talent if current_token && current_token.user
  end

  def current_email
    current_token.user.talent.email if current_token && current_token.user  && current_token.user.talent
  end

  private
    def set_default_response_format
      request.format = :json
    end

    def serialize_profile_only vbs
      fake_resume = Resume.new
      fake_resume.talent =  current_token.user.talent
      vbs.update_column(:last_synced_at, DateTime.now)
      users = []
      users <<  fake_resume
      render json: users, each_serializer: XingProfileSerializer, root: "users", client_application: vbs.veeta_button_sharing_connection.client_application

    end
end
