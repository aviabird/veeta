class Webhooks::InboundController < ApplicationController
  include BaseLogger

  #No Authorization possible, Authentication via signature check
  before_action :check_signature_reply, only: :reply if !(Rails.env.development? || Rails.env.test?)
  before_action :check_signature_bounce, only: :bounce if !(Rails.env.development? || Rails.env.test?)


  #https://mandrill.zendesk.com/hc/en-us/articles/205583197-Inbound-Email-Processing-Overview#test-inbound-webhooks
  def reply
    @options = {}
    parsed = JSON.parse(params[:mandrill_events], symbolize_names: true)
    parsed.each_with_index do |event, index|
      if event[:event] == 'inbound'
        handle_inbound_mail event
      else
        LogService.call "#{Webhooks::InboundController}.reply", {details: "Event not processable.", data: event, priority: :high}
      end
    end
    render json: '{"Status":"OK"}', status: 200
  end

  #https://mandrill.zendesk.com/hc/en-us/articles/205583197-Inbound-Email-Processing-Overview#test-inbound-webhooks
  def bounce
    @options = {}
    parsed = JSON.parse(params[:mandrill_events], symbolize_names: true)
    parsed.each_with_index do |event, index|
      if event[:event] == 'hard_bounce' || event[:event] == 'soft_bounce' || event[:event] == 'reject'
        handle_bounced_mail event
      else
        LogService.call "#{Webhooks::InboundController}.bounce", {details: "Event not processable", data: event, priority: :high}
      end
    end
    render json: '{"Status":"OK"}', status: 200
  end

  private

    def handle_inbound_mail event
      new_recipient = extract_new_recipient event[:msg][:email]
      if !new_recipient.empty?
        WebhookForwardMailer.forward_reply_email(new_recipient[:locale], new_recipient[:from_name], new_recipient[:from_email], event[:msg][:from_email], event[:msg][:from_name], event[:msg][:subject], event[:msg][:html], event[:msg][:text], event[:msg][:images], event[:msg][:attachments]).deliver
        log_event self, event, request, @options.merge(log: { priority: :medium ,details: "Forwarded inbound mail from #{event[:msg][:email]} to #{new_recipient}.", data: event })
      elsif is_noreplyable?(event[:msg][:email])
        locale =  is_noreply?(event[:msg][:email]) ? extract_locale(event[:msg][:email]) : ''
        WebhookForwardMailerWithTemplate.bounce_noreply_email(locale, event[:msg][:from_name], event[:msg][:from_email], event[:msg][:subject], '').deliver
        log_event self, event, request, @options.merge(log: { priority: :medium , details: "Answered with noreply bounce mail to #{event[:msg][:from_email]}.", data: event})
      else
        log_event self, event, request, @options.merge(log: { priority: :high , details: "Inbound mail was not processable: no recipient could be identified.", data: event})
      end
    end

    def handle_bounced_mail event
      new_recipient = extract_new_recipient event[:msg][:sender] # e.g. e174ddd4-808b-494f-89bc-d9741d9df2dc.share.mailer@notify.t13s.at
      # as Mandrill does not allow domain specific webhook events, check return path if this event should be processed by this instance
      if event[:msg][:sender].ends_with?(ENV['RETURN_PATH_DOMAIN']) && !new_recipient.empty?
        WebhookForwardMailerWithTemplate.bounce_noreply_email(new_recipient[:locale], new_recipient[:from_name], new_recipient[:from_email], "#{event[:msg][:subject]}", "#{event[:msg][:bounce_description]}, #{event[:msg][:email]}: #{event[:msg][:diag]}",  event[:msg][:event]).deliver
        log_event self, event, request, @options.merge(log: { priority: :medium ,details: "Forwarded bounced mail from #{event[:msg][:sender]} to #{new_recipient}.", data: event })
      else
        log_event self, event, request, @options.merge(log: { priority: :high , details: "Bounced mail was not processable: no recipient could be identified", data: event})
      end
    end

    def extract_new_recipient old_recipient
      new_recipient = {}
      return new_recipient if old_recipient.blank?
      if old_recipient.split(".apply.mailer@").size > 1
        id = old_recipient.split(".apply.mailer@").first
        begin
          jobapp = Jobapp.find(id)
          if jobapp.resume && jobapp.resume.talent
            new_recipient[:from_name] = jobapp.talent.name
            new_recipient[:from_email] = jobapp.talent.email
            new_recipient[:locale] = jobapp.talent.locale
          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      elsif old_recipient.split(".update.mailer@").size > 1
        id = old_recipient.split(".update.mailer@").first
        begin
          resume_sharing = ResumeSharing.find(id)
          if resume_sharing.resume && resume_sharing.resume.talent
            new_recipient[:from_name] = resume_sharing.resume.talent.name
            new_recipient[:from_email] = resume_sharing.resume.talent.email
            new_recipient[:locale] = resume_sharing.resume.talent.locale
          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      elsif old_recipient.split(".share.mailer@").size > 1
        id = old_recipient.split(".share.mailer@").first
        begin
          resume_sharing = ResumeSharingRequest.find(id)
          if resume_sharing.resume && resume_sharing.resume.talent
            new_recipient[:from_name] = resume_sharing.resume.talent.name
            new_recipient[:from_email] = resume_sharing.resume.talent.email
            new_recipient[:locale] = resume_sharing.resume.talent.locale
          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      elsif old_recipient.split(".withdraw.mailer@").size > 1
        id = old_recipient.split(".withdraw.mailer@").first
        begin
          jobapp = Jobapp.find(id)
          if jobapp.resume && jobapp.resume.talent
            new_recipient[:from_name] = jobapp.talent.name
            new_recipient[:from_email] = jobapp.talent.email
            new_recipient[:locale] = jobapp.talent.locale

          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      elsif old_recipient.split(".revoke.mailer@").size > 1
        id = old_recipient.split(".revoke.mailer@").first
        begin
          resume_sharing = ResumeSharing.find(id)
          if resume_sharing.resume && resume_sharing.resume.talent
            new_recipient[:from_name] = resume_sharing.resume.talent.name
            new_recipient[:from_email] = resume_sharing.resume.talent.email
            new_recipient[:locale] = resume_sharing.resume.talent.locale
          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      elsif old_recipient.split(".join.mailer@").size > 1
        id = old_recipient.split(".join.mailer@").first
        begin
          sharing_response = CvSharingResponse.find(id)
          if sharing_response.resume && sharing_response.resume.talent
            new_recipient[:from_name] = sharing_response.resume.talent.name
            new_recipient[:from_email] = sharing_response.resume.talent.email
            new_recipient[:locale] = sharing_response.resume.talent.locale
          end
        rescue
          LogService.call "#{Webhooks::InboundController}.extract_new_recipient", {details: "Object could not be obtained from email address: #{old_recipient}", priority: :high}
        end
      end
      new_recipient
    end


    def check_signature_reply
      check_signature ENV["MANDRILL_INBOUND_WEBHOOK_KEY"], ENV["MANDRILL_INBOUND_WEBHOOK_URL"]
    end

    def check_signature_bounce
      check_signature ENV["MANDRILL_BOUNCE_WEBHOOK_KEY"], ENV["MANDRILL_BOUNCE_WEBHOOK_URL"]
    end


    # Signature based on https://mandrill.zendesk.com/hc/en-us/articles/205583257-How-to-Authenticate-Webhook-Requests
    def check_signature key, url
      # use key 'test-webhook' if no entries in mandrill_events - this is a test request
      webhook_key = params[:mandrill_events]=='[]' ? 'test-webhook' : key
      url = url
      sorted_params = params.except(:action, :controller).sort.to_h
      signed_data = url
      sorted_params.each do |key, value|
        signed_data = "#{signed_data}#{key}#{value}"
      end
      signature = Base64.strict_encode64(OpenSSL::HMAC.digest('sha1', webhook_key, signed_data))
      if signature != request.headers['X-Mandrill-Signature']
        LogService.call "#{Webhooks::InboundController}.check_signature", {details: "Invalid signature of Mandrill request, X-Mandrill-Signature: #{request.headers['X-Mandrill-Signature']}, calculated: #{signature}", data: params[:mandrill_events], priority: :high}
        render json: '{"Status":"Unauthorized"}', status: 401
      end
    end

    def is_noreply? email
        email.starts_with?('noreply')
    end

    def is_noreplyable? email
      is_noreply?(email) && email.split(".mailer@").size > 1
    end

    def extract_locale email
      if !email.blank? && !email.scan(/-(..)\.mailer@/).empty?
        email.scan(/-(..)\.mailer@/).first.first
      else
        ''
      end
    end
end
