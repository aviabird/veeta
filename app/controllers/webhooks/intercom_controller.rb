class Webhooks::IntercomController < ApplicationController
  include BaseLogger

  respond_to :json

  #No Authorization possible, Authentication via signature check
  before_action :check_signature_unsubscribe, only: :unsubscribe if !(Rails.env.development? || Rails.env.test?)

  def unsubscribe
    @options = {}
    json_request = JSON.parse(request.raw_post)
    hash_request = json_request.deep_symbolize_keys!
    intercom_user = hash_request[:data][:item] if hash_request[:data].present? && hash_request[:data][:item].present?
    if intercom_user.present?
      ta = TalentAuth.where(email: intercom_user[:email]).first
      if ta.present? && ta.talent.intercom_user_id == intercom_user[:user_id]
        if intercom_user[:unsubscribed_from_emails] == false
          current_user.setting.update_columns(newsletter_updates: 'enabled')
          LogService.call "#{Webhooks::IntercomController}.disabled_newsletter", {resource: ta, details: "Disabled newsletter for #{ta.email}.", priority: :low}
        elsif intercom_user[:unsubscribed_from_emails] == true
          current_user.setting.update_columns(newsletter_updates: 'disabled')
          LogService.call "#{Webhooks::IntercomController}.enabled_newsletter", {resource: ta, details: "Enabled newsletter for #{ta.email}.", priority: :low}
        else
          LogService.call "#{Webhooks::IntercomController}.webhook_processing_failed", {resource: ta, details: "unsubscribed_from_emails flag not found in intercom user object.", priority: :high}
        end
      else
        LogService.call "#{Webhooks::IntercomController}.webhook_processing_failed", {resource: ta, details: "User with email #{intercom_user[:email]} not found in myVeeta.", priority: :high}
      end
    else
      LogService.call "#{Webhooks::IntercomController}.webhook_processing_failed", {data: request.raw_post, details: "Could not process post data.", priority: :high}
    end
    #identify user
    #update notification setting if applicable
    #return 200 OK
    render json: '{"Status":"OK"}', status: 200
  end


  private

    def check_signature_unsubscribe
      check_signature ENV["INTERCOM_HUB_SECRET"]
    end

    # Signature based on https://developers.intercom.com/reference#signed-notifications
    def check_signature key, url
      signature = Base64.strict_encode64(OpenSSL::HMAC.digest('sha1', key, request.raw_post))
      if signature != request.headers['X-Hub-Signature']
        LogService.call "#{Webhooks::IntercomController}.check_signature", {details: "Invalid signature of Intercom request, X-Hub-Signature: #{request.headers['X-Hub-Signature']}, calculated: #{signature}", data: request.raw_post, priority: :high}
        render json: '{"Status":"Unauthorized"}', status: 401
      end
    end

end
