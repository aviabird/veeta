class WelcomeController < ApplicationController
  ###
  # No authorization required, base controller
  ###

  respond_to :json
  before_filter :set_default_response_format

  def index
    respond_to do |format|
      format.json { render json: { message: "Up and running" } }
    end
  end

  private
    def set_default_response_format
      request.format = :json
    end
end
