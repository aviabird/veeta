class AuthMailer <  MandrillMailer::TemplateMailer
  include MailHelper
  default from: ENV['NOREPLY_MAIL_SENDER_ADDRESS']

  # Send mandrill email for continue applying
  # def continue_applying auth
  #   @template = 'continue-applying'
  #   @locale = auth.user.locale
  #   @to = { email: auth.email, name: auth.user.name }
  #   redirect_to = URI::encode('/app/talent')
  #   @vars = {
  #     'TALENT_SITE_URL' => redirect_url(user_email: auth.email, user_token: auth.authentication_token, to: redirect_to)
  #   }
  #   send_as_mandrill_mail(@template, @locale, @to, @vars)
  # end
end
