class WebhookForwardMailer < MandrillMailer::MessageMailer
  include WebhookForwardMailHelper

  def forward_reply_email locale, recipient_name, recipient_email, from_email, from_name, subject, html, text, images, attachments
    from = ENV["NOREPLY_MAIL_SENDER_ADDRESS"]
    from = from.gsub(".mailer@", "-#{locale}.mailer@")
    from_name = from_name.blank? ?  ENV['MAIL_SENDER_FROM_NAME'] : from_name
    @fwd_images = images
    @fwd_attachments = attachments
    mandrill_mail(
      to: { email: recipient_email, name: recipient_name },
      from_name: from_name,
      from: from,
      headers: {"Reply-To": from_email},
      text: text,
      html: html,
      subject: subject,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: fwd_attachment_prepared,
      images: fwd_images_prepared,
      tags: ['forward-reply-email']
    )
  end

end
