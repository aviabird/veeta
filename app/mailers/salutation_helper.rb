module SalutationHelper
    include ActionView::Helpers
    def full_salutation
      if  talent && talent.kind_of?(Talent) && talent.last_name
        unless talent.salutation.blank?
          gender = talent.salutation == 'other' ? 'unknown' : talent.salutation == 'mr' ? 'm' : 'f'
          I18n.t("common.phrases.full_salutation.#{gender}",  applicant_name: talent.name,
                                                                        applicant_first_name: talent.first_name,
                                                                        applicant_last_name: talent.last_name,
                                                                        last_name: talent.last_name,
                                                                        applicant_email: talent.email,
                                                                        full_name: talent.name)
        else
          I18n.t("common.phrases.full_salutation.unknown", full_name: talent.name)
        end
      else
        ''
      end
    end

    def generic_salutation name = nil
      unless name.blank?
        I18n.t("common.phrases.full_salutation.unknown", full_name: html_encode(name))
      else
        I18n.t("common.phrases.full_salutation.empty")
      end
    end
end
