class JobApplicationAndUpdateMailer < MandrillMailer::TemplateMailer
  include MailHelper
  include NotificationMailHelper

  default from: ENV['NOREPLY_MAIL_SENDER_ADDRESS']

  # Send email with jobapp application to recruiter
  def inform_recruiter locale, jobapp, recruiter
    @jobapp = jobapp
    @recruiter = recruiter
    @job = jobapp.job
    @company = job.company
    @external_entity = job.company
    @resume = jobapp.resume
    @talent = resume.talent
    I18n.locale = locale
    mandrill_mail(
      template: 'recruiter-new-jobapp-notification-full-content',
      to: { email: recruiter.email, name: recruiter.name },
      from: "#{jobapp.id}.#{ENV['APPLY_MAIL_SENDER_ADDRESS']}",
      from_name: talent.name,
      headers: {"Reply-To": talent.email},
      vars: vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images + images,
      tags: ['notify-newjobapp','company-connected']
    )
  end

  # Send email with jobapp application to recruiter
  def inform_email_company_recruiter locale, jobapp
    email_company = jobapp.job
    @jobapp = jobapp
    @job = Job.new(name: email_company.jobname)
    #company and recuirter not available in email application
    @company = Company.new(name: email_company.company_name)
    @external_entity = email_company
    @recruiter = Recruiter.new(first_name: "",phone_number: "", last_name: email_company.company_name, email: email_company.recruiter_email)
    @resume = jobapp.resume
    @talent = resume.talent
    I18n.locale = locale
    mandrill_mail(
      template: 'recruiter-new-jobapp-notification-full-content',
      to: { email: email_company.recruiter_email, name: email_company.company_name },
      from: "#{jobapp.id}.#{ENV['APPLY_MAIL_SENDER_ADDRESS']}",
      from_name: talent.name,
      headers: {"Reply-To": talent.email},
      vars: vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images + images,
      tags: ['notify-newjobapp', 'company-email']
    )
  end

  # Send email after CV update to recruiter(s)
  def send_cv_update resume, recipient, resume_sharing_id
    @recruiter = recipient
    @company = recipient.company
    @external_entity = recipient.company
    @resume = resume
    @talent = resume.talent
    I18n.locale = @talent.locale || I18n.default_locale
    mandrill_mail(
      template: 'send-updates-to-companies',
      to: { email: filter_recipient(recruiter.email), name: recruiter.name },
      from_name: talent.name,
      from: "#{resume_sharing_id}.#{ENV['UPDATE_MAIL_SENDER_ADDRESS']}",
      headers: {"Reply-To": talent.email},
      vars: vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images + images,
      tags: ['notify-update', 'company-connected']
    )
  end

  # Send email after CV update to recruiter(s)
  def inform_recruiter_about_pool_join resume, recipient, cv_sharing_response_id
    @recruiter = recipient
    @company = recipient.company
    @external_entity = recipient.company
    @resume = resume
    @talent = resume.talent
    @sharing_response = CvSharingResponse.find(cv_sharing_response_id)
    I18n.locale = @talent.locale || I18n.default_locale
    mandrill_mail(
      template: 'send-join-pool-to-companies',
      to: { email: filter_recipient(recruiter.email), name: recruiter.name },
      from_name: talent.name,
      from: "#{cv_sharing_response_id}.#{ENV['SHARING_RESPONSE_MAIL_SENDER_ADDRESS']}",
      headers: {"Reply-To": talent.email},
      vars: vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images + images,
      tags: ['notify-cv-sharing-response', 'company-connected']
    )
  end

  # Send email after CV update to recruiter(s)
  def send_cv_update_to_email_company resume, email_company,resume_sharing_id
    #company and recuirter not available in email application
    @company = Company.new(name: email_company.company_name)
    @external_entity = email_company
    @recruiter = Recruiter.new(first_name: "",phone_number: "", last_name: email_company.company_name, email: email_company.recruiter_email)
    @resume = resume
    @talent = resume.talent
    I18n.locale = @talent.locale || I18n.default_locale
    mandrill_mail(
      template: 'send-updates-to-email-companies',
      to: { email: filter_recipient(email_company.recruiter_email), name: email_company.company_name },
      from: "#{resume_sharing_id}.#{ENV['UPDATE_MAIL_SENDER_ADDRESS']}",
      from_name: talent.name,
      headers: {"Reply-To": talent.email},
      vars: vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images + images,
      tags: ['notify-update', 'company-connected']
    )
  end

  # Send email after CV update to recruiter(s)
  def send_shared_resume locale, resume_id, contact_name, email, resume_sharing_request_id
    #company and recuirter not available in email application
    @company = Company.new
    @recruiter = Recruiter.new(first_name: "",phone_number: "", last_name: contact_name, email: email)
    @resume = Resume.find(resume_id)
    @talent = resume.talent
    @type = 'share_cv'
    I18n.locale = locale
    mail_vars = general_talent_mail_vars
    mail_vars[:SALUTATION_LINE] = generic_salutation(contact_name)
    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: recruiter.email, name: recruiter.name },
      from: "#{resume_sharing_request_id}.#{ENV['SHARE_MAIL_SENDER_ADDRESS']}",
      from_name: talent.name,
      headers: {"Reply-To": talent.email},
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      attachments: cv_as_attachment,
      images: static_images,
      tags: ["notify-sharecv"]
    )
  end

  # send notification to recruiter after withdrawing jobapp
  def send_withdrawn_jobapp locale, talent, recruiter, jobapp
    @recruiter = recruiter
    @talent = talent
    @type = 'withdrawn_jobapp'
    @reason = jobapp.withdrawn_reason
    @job_name = jobapp.job.name
    I18n.locale = locale
    mail_vars = general_talent_mail_vars
    mail_vars[:SALUTATION_LINE] = generic_salutation(recruiter.name)
    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: recruiter.email, name: recruiter.name },
      from_name: talent.name,
      from: "#{jobapp.id}.#{ENV['WITHDRAW_MAIL_SENDER_ADDRESS']}",
      headers: {"Reply-To": talent.email},
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      images: static_images,
      tags: ['notify-withdrawn-jobapp']
    )
  end

  # send
  def send_revoked_access locale, talent, recruiter, reason, resume_sharing_id
    @recruiter = recruiter
    @talent = talent
    @type = 'revoked_access'
    @reason = reason
    I18n.locale = locale
    mail_vars = general_talent_mail_vars
    mail_vars[:SALUTATION_LINE] = generic_salutation(recruiter.name)
    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: recruiter.email, name: recruiter.name },
      from_name: talent.name,
      from: "#{resume_sharing_id}.#{ENV['REVOKE_MAIL_SENDER_ADDRESS']}",
      headers: {"Reply-To": talent.email},
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      images: static_images,
      tags: ['notify-revoked-access']
    )
  end

  def send_first_jobapp_notification locale, jobapp
    #company and recuirter not available in email application
    @talent = jobapp.resume.talent
    @job_name = jobapp.job.name
    @company_name = jobapp.job.company_name
    @type = 'first_jobapp'
    I18n.locale = locale
    mail_vars = general_talent_mail_vars
    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: talent.email, name: talent.name },
      from_name: ENV['MAIL_SENDER_FROM_NAME'],
      from: ENV['NOREPLY_MAIL_SENDER_ADDRESS'].gsub(".mailer@", "-#{locale}.mailer@"),
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      images: static_images,
      tags: ["notify-first-jobapp"]
    )
  end

  def send_first_cv_sharing_response_notification locale, cv_sharing_response
    #company and recuirter not available in email application
    @talent = cv_sharing_response.resume.talent
    @company_name = cv_sharing_response.company.name
    @type = 'first_cv_sharing_response'
    I18n.locale = locale
    mail_vars = general_talent_mail_vars

    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: talent.email, name: talent.name },
      from_name: ENV['MAIL_SENDER_FROM_NAME'],
      from: ENV['NOREPLY_MAIL_SENDER_ADDRESS'].gsub(".mailer@", "-#{locale}.mailer@"),
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      images: static_images,
      tags: ["notify-first-cv-sharing-response"]
    )
  end

  def cv_as_attachment
    updated_date = resume.updated_at.strftime("%d-%m-%Y")
    resume_caption = I18n.t('activerecord.models.resume')
    [{
      content: resume_pdf,
      name: "#{talent.first_name}_#{talent.last_name}_#{resume_caption}_#{updated_date}.pdf",
      type: 'application/pdf'
    }]
  end

  # List of images
  def images
    if resume.safe_profile_picture && resume.profile_picture_available?
      [{
        content: resume.safe_profile_picture,
        name: 'IMAGECID',
        type: 'image/png'
      }]
    else
      []
    end
  end

  def filter_recipient to
    if Rails.configuration.mail_to_t13s_only
      unless to.ends_with?('@t13s.at')
        "#{to.split('@').first}@t13s.at"
      else
        to
      end
    else
      to
    end
  end
end
