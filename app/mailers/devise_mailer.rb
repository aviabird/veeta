# Setting up custom mailer
# https://github.com/plataformatec/devise/wiki/How-To:-Use-custom-mailer
class DeviseMailer < Devise::Mailer

  # Override default reset password instructions mailer
  def reset_password_instructions(record, token, opts={})
    AuthorizationMailer.reset_password_instructions(record, token, opts).deliver
    LogService.call __method__, {resource: record, details: "Reset password mail sent to #{record.try(:email)}",priority: :low}
  end

  # Override default reset password instructions mailer
  def confirmation_instructions(record, token, opts={})
    if record.recruiter?
      AuthorizationMailer.recruiter_confirmation_instructions(record, record.confirmation_token, opts).deliver
    else
      AuthorizationMailer.confirmation_instructions(record, token, opts).deliver
    end
    LogService.call __method__, {resource: record, details: "Confirmation mail sent to #{record.try(:email)}",priority: :low}
  end

  def unlock_instructions(record, token, opts={})
    if record.talent?
      AuthorizationMailer.unlock_instructions(record, token, opts).deliver
      LogService.call __method__, {resource: record, details: "Unlock mail sent to #{record.try(:email)}",priority: :low}
    elsif record.recruiter?
      AuthorizationMailer.unlock_instructions(record, token, opts).deliver
      LogService.call __method__, {resource: record, details: "Unlock mail sent to #{record.try(:email)}",priority: :low}
    elsif record.admin?
      #AuthorizationMailer.unlock_instructions(record.admin, token, opts).deliver
      LogService.call __method__, {resource: record, details: "Admin locked #{record.admin.name}",priority: :high}
    end
  end
end
