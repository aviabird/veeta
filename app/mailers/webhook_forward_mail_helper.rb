module WebhookForwardMailHelper
  extend ActiveSupport::Concern

  included do
    attr_accessor :fwd_images, :fwd_attachments
  end

  # List of images
  def fwd_images_prepared
    fwd_images_prepared = []
    fwd_images.each do |fwd_image_key, fwd_image_data|
      fwd_images_prepared << ensure_b64_decoding(fwd_image_data)
    end unless fwd_images.nil?
    fwd_images_prepared
  end

  # List of attachments
  def fwd_attachment_prepared
    fwd_attachment_prepared = []
    fwd_attachments.each do |fwd_attachment_key, fwd_attachment_data|
      fwd_attachment_prepared << ensure_b64_decoding(fwd_attachment_data)
    end unless fwd_attachments.nil?
    fwd_attachment_prepared
  end

  private

    def ensure_b64_encoding fwd_data
      if !fwd_data[:base64].nil? && !fwd_data[:base64]
        fwd_data[:content] = Base64.encode64(fwd_data[:content])
      end
      fwd_data
    end

    def ensure_b64_decoding fwd_data
      if fwd_data[:base64].nil? || fwd_data[:base64]
        fwd_data[:content] = Base64.decode64(fwd_data[:content]).force_encoding('UTF-8')
      end
      fwd_data
    end

end
