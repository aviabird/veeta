class AuthorizationMailer < MandrillMailer::TemplateMailer
  include RouterMailHelper
  include MandrillMailHelper
  include NotificationMailHelper
  include Devise::Controllers::UrlHelpers

  default from: ENV['NOREPLY_MAIL_SENDER_ADDRESS']

  def reset_password_instructions auth, token, opts = {}
    extra_slash = ENV['TALENT_HOST'].ends_with?('/') ? '' : '/'
    @talent = auth.talent
    @type = 'forgot_password'
    I18n.locale = @talent.locale || I18n.default_locale
    url = "#{ENV['TALENT_HOST']}#{extra_slash}request-password-form?reset_password_token=#{token}&locale=#{auth.talent.locale}"
    vars = general_talent_mail_vars
    vars[:BUTTON_LINK] = url
    send_mandrill_mail('talent-general-notification-with-action', 'notify-forgotpassword', auth, auth.talent, token, vars, opts = {})
  end

  def confirmation_instructions auth, token, opts = {}
    extra_slash = ENV['TALENT_HOST'].ends_with?('/') ? '' : '/'
    @talent = auth.talent
    @type = 'confirm_email_address'
    I18n.locale = @talent.locale || I18n.default_locale
    url = "#{ENV['TALENT_HOST']}#{extra_slash}email-confirmation?confirmation_token=#{token}"
    vars = general_talent_mail_vars
    vars[:BUTTON_LINK] = url
    send_mandrill_mail('talent-general-notification-with-action','notify-validateemail', auth, auth.talent, token, vars, opts = {})
  end

  def recruiter_confirmation_instructions auth, token, opts = {}
    @recruiter = auth.recruiter
    @type = 'confirm_email_address'
    I18n.locale = @recruiter.try(:recruiter).try(:locale) || I18n.default_locale
    url = "#{ENV['HOST']}/recruiter_auths/confirmation?confirmation_token=#{token}"
    vars = general_talent_mail_vars
    vars[:BUTTON_LINK] = url
    send_mandrill_mail('talent-general-notification-with-action','notify-validateemail', auth, @recruiter, token, vars, opts = {})
  end

  def unlock_instructions auth, token, opts = {}
    extra_slash = ENV['TALENT_HOST'].ends_with?('/') ? '' : '/'
    @type = 'unlock'
    if auth.kind_of? TalentAuth
      @talent = auth.talent
      I18n.locale = @talent.locale || I18n.default_locale
      mail_vars = general_talent_mail_vars
      url = "#{ENV['TALENT_HOST']}#{extra_slash}unlock-confirmation?unlock_token=#{token}"
    elsif auth.kind_of? RecruiterAuth
      @talent = auth.recruiter  #fake talent user object
      mail_vars = general_talent_mail_vars
      mail_vars[:SALUTATION_LINE] = generic_salutation(auth.recruiter.name)
      url = "#{ENV['HOST']}#{extra_slash}recruiter_auths/unlock?unlock_token=#{token}"
    elsif auth.kind_of? AdminAuth
      return
    end
    vars = mail_vars
    vars[:BUTTON_LINK] = url
    send_mandrill_mail('talent-general-notification-with-action','notify-unlock', auth, @talent, token, vars, opts = {})
  end

  def send_mandrill_mail template_name,tag, auth, user_obj, token, vars, opts = {}
    from = auth.talent? ? ENV['NOREPLY_MAIL_SENDER_ADDRESS'].gsub(".mailer@", "-#{auth.talent.locale}.mailer@") : ENV['NOREPLY_MAIL_SENDER_ADDRESS']
    mandrill_mail(
      template: template_name,
      to: { email: auth.email, name: "#{user_obj.first_name} #{user_obj.last_name}" },
      from: from,
      from_name: ENV['MAIL_SENDER_FROM_NAME'],
      vars: vars,
      important: true,
      inline_css: true,
      images: static_images,
      tags: [tag]
    )
  end
end
