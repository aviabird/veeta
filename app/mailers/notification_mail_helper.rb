module NotificationMailHelper
  extend ActiveSupport::Concern
  include ActionView::Helpers
  include SalutationHelper
  include ApplicationHelper

  included do
    attr_accessor :talent, :type, :custom_subject
  end

  def notification_subject
    I18n.t("talent.emails.notification.#{type}.subject",  applicant_name: talent ? (talent.name) : '',
                                                          applicant_first_name: talent ? (talent.first_name) : '',
                                                          applicant_last_name: talent ? (talent.last_name) : '',
                                                          applicant_email: talent ? talent.email : '',
                                                          subject: custom_subject ? (custom_subject) : '')
  end

  def text_part1
    I18n.t("talent.emails.notification.#{type}.text_part1", applicant_name: talent ? html_encode(talent.name) : '',
                                                          applicant_first_name: talent ? html_encode(talent.first_name) : '',
                                                          applicant_last_name: talent ? html_encode(talent.last_name) : '',
                                                          applicant_email: talent ? talent.email : '')
  end

  def text_part
    if ['withdrawn_jobapp', 'revoked_access'].include?(type)
      translation_key = "talent.emails.notification.#{type}_#{@reason.blank? ? 'without_reason' : 'with_reason'}.text_part"
    else
      translation_key = "talent.emails.notification.#{type}.text_part"
    end

    I18n.t(translation_key, applicant_name: talent ? html_encode(talent.name) : '',
                                                          applicant_first_name: talent ? html_encode(talent.first_name) : '',
                                                          applicant_last_name: talent ? html_encode(talent.last_name) : '',
                                                          applicant_email: talent ? talent.email : '',
                                                          reason: html_encode(@reason),
                                                          job_name: html_encode(@job_name),
                                                          company_name: html_encode(@company_name))


  end

  def text_part2
    I18n.t("talent.emails.notification.#{type}.text_part2", applicant_name: talent ? html_encode(talent.name) : '',
                                                          applicant_first_name: talent ? html_encode(talent.first_name) : '',
                                                          applicant_last_name: talent ? html_encode(talent.last_name) : '',
                                                          applicant_email: talent ? talent.email : '')
  end

  def button_text
    I18n.t("talent.emails.notification.#{type}.button_text")
  end

  # List of images
  def static_images
    logo = static_logo
    unless logo.nil?
      [static_logo]
    else
      []
    end
  end

  def general_talent_mail_vars
    {
      'SUBJECT' => notification_subject,
      'SALUTATION_LINE' => full_salutation,
      'TEXT_PART1' => text_part1,
      'TEXT_PART' => text_part,
      'TEXT_PART2' => text_part2,
      'BUTTON_TEXT' => button_text,
    }
  end

  private

    def static_logo
      if File.exist?(Rails.root.join('app/assets/images/veeta_logo.png'))
        {
          content: File.read(Rails.root.join('app/assets/images/veeta_logo.png')),
          name: 'IMAGEVEETALOGO',
          type: 'image/png'
        }
      else
        nil
      end
    end
end
