module MailHelper
  extend ActiveSupport::Concern

  included do
    include RouterMailHelper
    include MandrillMailHelper
    include GeneralMailHelper
  end
end
