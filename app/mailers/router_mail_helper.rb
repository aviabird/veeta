# HOTFIX
# http://stackoverflow.com/questions/16720514/how-to-use-url-helpers-in-lib-modules-and-set-host-for-multiple-environments
module RouterMailHelper
  extend ActiveSupport::Concern

  # URL Helpers in emails part 1/3
  class Base
    include Rails.application.routes.url_helpers

    def default_url_options
      ActionMailer::Base.default_url_options
    end
  end

  private
    # URL Helpers in emails part 2/3
    def main_app
      @main_app ||= RouterMailHelper::Base.new
    end

  class << self
    # URL Helpers in emails part 3/3
    def method_missing method, *args, &block
      main_app ||= RouterMailHelper::Base.new

      if main_app.respond_to?(method)
        main_app.send(method, *args, &block)
      else
        super method, *args, &block
      end
    end
  end
end
