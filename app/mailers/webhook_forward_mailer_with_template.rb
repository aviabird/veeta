class WebhookForwardMailerWithTemplate < MandrillMailer::TemplateMailer
  include NotificationMailHelper

  def bounce_noreply_email locale, recipient_name, recipient_email, subject, text, event = 'noreply'
    @type = event == 'noreply' ? 'noreply' : 'bounce'
    @custom_subject = CGI::escapeHTML(subject) if subject
    @reason =  text
    unless locale.blank?
      I18n.locale = locale
      from = ENV["NOREPLY_MAIL_SENDER_ADDRESS"]
      from = from.gsub(".mailer@", "-#{locale}.mailer@")
    else
      I18n.locale = 'en'
      from = ENV["NOREPLY_MAIL_SENDER_ADDRESS"]
      @type = "#{@type}_all_locales"
    end
    mail_vars = general_talent_mail_vars
    mail_vars[:SALUTATION_LINE] = generic_salutation
    mandrill_mail(
      template: 'talent-general-notification',
      to: { email: recipient_email, name: recipient_name },
      from_name: ENV['MAIL_SENDER_FROM_NAME'],
      from: from,
      vars: mail_vars,
      important: true,
      inline_css: true,
      return_path_domain: ENV['RETURN_PATH_DOMAIN'],
      images: static_images,
      tags: ["bounce-noreply-email", "#{event}-handling-email"]
    )
  end

end
