module MandrillMailHelper
  extend ActiveSupport::Concern

  # Use template based on locale
  #def template name, locale
  #  if locale == :en or locale == 'en'
  #    name
  #  else
  #    "#{name}-#{locale}"
  #  end
  #end

  # Fast mandrill mail sender
  def send_as_mandrill_mail template_name, locale, to, with_variables
    mandrill_mail(template: template_name, to: to, vars: with_variables, important: true, inline_css: true, return_path_domain: ENV['RETURN_PATH_DOMAIN'],)
  end

end
