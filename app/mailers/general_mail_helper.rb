require 'slack-notifier'

module GeneralMailHelper
  extend ActiveSupport::Concern
  include ActionView::Helpers
  include SalutationHelper
  include ApplicationHelper

  included do
    attr_accessor :job, :jobapp, :resume, :talent, :recruiter, :company,
    :external_entity, :sharing_response, :unsolicited
  end

  def my_vars
    vars
  end

  private

    def email_type
      if jobapp
        # For unsolicited EmailApplications
        @unsolicited = jobapp.job.try(:unsolicited?) ? '_unsolicited' : ''
        # For real jobs without email application.
        @unsolicited = jobapp.job.try(:unsolicited_job) ? '_unsolicited' : '' unless @unsolicited.present?
        'jobapp'
      elsif sharing_response
        'sharing_response'
      else
        'update'
      end
    end

    def currency(amount)
      if !amount.blank?
        number_to_currency(amount, precision: 0, delimiter: '.', locale: 'de')
      else
        ""
      end
    end

    def address
      if talent && !talent.address.blank?
        talent.address
      else
        ""
      end
    end

    def country
      if !address.blank? && !address.country.blank?
        address.country
      else
        ""
      end
    end

    # Generate pdf from resume
    def resume_pdf
      #PDFService::Resume.pdf(resume)

      raise_critical = false
      begin
        if resume.file_exists? resume.pdf.url.to_s
          return resume.pdf.file.read
        else
          raise_critical = true
        end
      rescue
        raise_critical = true
      end
      if raise_critical
        unless Rails.env.development?
          notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
          notifier.ping "*Action required:* Could not attach CV PDF to mail!\nresume: #{resume.to_yaml}\ntalent: #{talent.to_yaml}", username: "myVeeta #{Rails.env} Mail Bot"
        end
        LogService.call "#{GeneralMailHelper.name}.resume_pdf", {resource: resume, resume: resume, details: "Could not attach CV PDF to mail, file not found",priority: :high}
        raise "Could not attach CV PDF to mail for resume : #{resume.inspect}, talent: #{talent.inspect}"
      end
    end

    # Get applicant name from talent
    def applicant_name
      if talent && !talent.name.blank?
        html_encode talent.name
      else
        ""
      end
    end

    # Get applicant name from talent
    def applicant_name_unencoded
      if talent && !talent.name.blank?
        talent.name
      else
        ""
      end
    end

    # Get applicant first name from talent
    def applicant_firstname
      if talent && !talent.first_name.blank?
        html_encode talent.first_name
      else
        ""
      end
    end

    # Get applicant last name from talent
    def applicant_lastname
      if talent && !talent.last_name.blank?
        html_encode talent.last_name
      else
        ""
      end
    end

    # Get applicant age from talent
    def applicant_age
      if talent && !talent.age.blank?
        talent.age
      else
        ""
      end
    end

    # Get applicant email from talent
    def applicant_email
      if talent && !talent.email.blank?
        talent.email
      else
        ""
      end
    end

    # Get applicant phone from talent
    def applicant_phone
      if talent && !talent.phone_number.blank?
        html_encode talent.phone_number
      else
        ""
      end
    end

    def applicant_city
      if !address.blank? && !address.city.blank?
        html_encode address.city
      else
        ""
      end
    end

    def applicant_country_iso
      if !country.blank? && !country.iso_code.blank?
        country.iso_code
      else
        ""
      end
    end

    # Get translation with city
    def applicant_city_country
      _zip = talent && talent.address && !talent.address.zip.blank? ? talent.address.zip : ""
      _address =  talent && talent.address && !talent.address.address_line.blank? ? talent.address.address_line : ""
      _city = talent && talent.address && !talent.address.city.blank? ? talent.address.city : ""
      _country = talent && talent.address && talent.address.country && !talent.address.country.name.blank? ? I18n.t(talent.address.country.name) : ""
      html_encode [_address, "#{_zip} #{_city}".strip, _country].reject(&:empty?).join(", ")
    end

    # Get job title from job
    def job_title
    # first condition is true when email_application is unsolicited and second condition is true when job application is unsolicited
      if (job && job.unsolicited_job.present?) || (jobapp && jobapp.job.try(:unsolicited?))
        I18n.t "recruiter.emails.notification.#{email_type}.full.job_title_unsolicited"
      elsif job && !job.name.blank?
        html_encode job.name
      else
        ''
      end
    end

    # Get job title from job
    def job_title_unencoded
      if job && !job.name.blank?
        job.name
      else
        ''
      end
    end

    # Get translation with referrer
    def job_source
      if jobapp
        if jobapp.referrer
          I18n.t 'talent.data.referrer.company.' + jobapp.referrer.slug, locale: talent.locale
        else
         I18n.t 'talent.data.referrer.' +jobapp.source, locale: talent.locale
        end
      else
        I18n.t 'talent.data.referrer.undefined'
      end
    end
    
    def blocking_list
      if(email_type!='update')
        list = jobapp.present? ? jobapp : sharing_response 
        list = list.blocked_companies.join(',') if list && !list.blocked_companies.blank?
        if list.present?
          (I18n.t 'recruiter.emails.general.additional_information.blocking_notice', locale: talent.locale, blocked_companies: list)
        else
          ''
        end
      else
        ''
      end
    end

    # Get cover letter from job application
    def cover_letter
      if jobapp && !jobapp.cover_letter.blank?
        html_encode jobapp.cover_letter
      else
        ''
      end
    end

    def cover_letter_translation
      I18n.t "recruiter.emails.notification.#{email_type}.full.coverletter", coverletter: cover_letter
    end

    # Get admin note from job application
    def admin_notes
      if jobapp && !jobapp.contact_preferences.blank?
        html_encode jobapp.contact_preferences
      else
        '-'
      end
    end

    # Get list of attached documents from breaked
    def attached_documents_br
      if resume && resume.documents
        documents = []
        resume.documents.each do |document|
          if document.document_type.blank?
            document_type = I18n.t 'talent.data.document.others'
          else
            document_type = I18n.t 'talent.data.document.' + document.document_type
          end
          documents << I18n.t('recruiter.emails.resume.documents_BR', doc_name: html_encode(document.name),
                                                                      doc_type: document_type,
                                                                       doc_url: document.external_url(external_entity))
        end
        documents.join('<br/>')
      else
        I18n.t 'recruiter.emails.resume.documents_BR_none'
      end
    end

    def email_to_applicant_btn_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.email_to_applicant_btn_text"
    end

    def job_title_uenc
      if job_title
        URI.encode(job_title)
      else
        URI.encode("n/a")
      end
    end

    def email_to_applicant_btn_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.email_to_applicant_btn_link", applicant_email: applicant_email,
                                                                                             job_title_uenc: job_title_uenc,
                                                                                             full_salutation_uenc: URI.encode(full_salutation),
                                                                                             company_name_uenc: URI.encode(company_name)

    end

    # Get list of attached documents from resume in list
    def attached_documents_ul
      if resume && resume.documents
        html = ''
        resume.documents.each do |document|
          if document.document_type.blank?
            document_type = I18n.t 'talent.data.document.others'
          else
            document_type = I18n.t 'talent.data.document.' + document.document_type
          end
          html += I18n.t 'recruiter.emails.notification.jobapp.full.documents_UL', doc_name: html_encode(document.name),
                                                                 doc_type: document_type,
                                                                  doc_url: document.external_url(external_entity),
                                                             li_css_class: ''
        end
        html += ''
      else
        I18n.t 'recruiter.emails.notification.jobapp.full.documents_UL_none'
      end
    end

    # Get list of attached documents from resume in list
    def attached_documents_ul_cv_row
      if resume
        html = I18n.t 'recruiter.emails.notification.jobapp.full.documents_UL_cv_row', applicant_name: html_encode(talent.name)
      else
        I18n.t 'recruiter.emails.notification.jobapp.full.documents_UL_none'
      end
    end


    # Get recruiter name from recruiter
    def recruiter_name
      if recruiter && !recruiter.name.blank?
        html_encode recruiter.name
      else
        ''
      end
    end

    # Get recruiter first name from recruiter
    def recruiter_firstname
      if recruiter && !recruiter.first_name.blank?
        html_encode recruiter.first_name
      else
        ''
      end
    end

    # Get recruiter last name from recruiter
    def recruiter_lastname
      if recruiter && !recruiter.last_name.blank?
        html_encode recruiter.last_name
      else
        ''
      end
    end

    # Get recruiter email from recruiter
    def recruiter_email
      if recruiter && !recruiter.email.blank?
        recruiter.email
      else
        ''
      end
    end

    # Get recruiter phone from recruiter
    def recruiter_phone
      if recruiter && !recruiter.phone_number.blank?
        html_encode recruiter.phone_number
      else
        ''
      end
    end

    # Get company name from recruiter
    def company_name
      (company && company.name) ? html_encode(company.name) : 'n/a'
    end

    # Get list of working industries
    def working_industries
      if talent && talent.setting && talent.setting.working_industries && !talent.setting.working_industries.empty?
        html_encode talent.setting.working_industries.join(', ')
      else
        '-'
      end
    end

    # Get link of working areas
    def working_locations
      if talent && talent.setting && talent.setting.working_locations && !talent.setting.working_locations.empty?
        html_encode talent.setting.working_locations.join(', ')
      else
        '-'
      end
    end

    # Get translation of terms of employment
    def toe
      if talent && talent.setting
        I18n.t 'talent.data.toe.' + (talent.setting.prefered_employment_type ? talent.setting.prefered_employment_type : 'undefined')
      else
        I18n.t 'talent.data.toe.undefined'
      end
    end


    # Get translation with talent availability
    def availability
      if talent && talent.setting && talent.setting.availability_key
        I18n.t 'talent.data.available.' + (talent.setting.availability ? talent.setting.availability_key : 'undefined')
      else
        I18n.t 'talent.data.available.undefined'
      end
    end

    # Get translation with expected salary
    def price_range
      if talent && talent.setting
        I18n.t "recruiter.emails.notification.#{email_type}.full.price_range_email_key", from: currency(talent.setting.salary_exp_min),
                                                                                         to: currency(talent.setting.salary_exp_max)
       else
         ''
       end
    end


    def subject
      I18n.t "recruiter.emails.notification.#{email_type}.full.subject#{unsolicited}", job_title: job_title_unencoded,
                                                             applicant_name: applicant_name_unencoded
    end

    # Get translation of job interest
    def job_seeker_status
      if talent && talent.setting
        I18n.t 'talent.data.job_interest.' + (talent.setting.job_seeker_status ? talent.setting.job_seeker_status : 'undefined')
      else
        I18n.t 'talent.data.job_interest.undefined'
      end
    end

    def header_link1_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link1_text"
    end

    def header_link1_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link1_link"
    end

    def header_link2_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link2_text"
    end

    def header_link2_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link2_link"
    end

    def header_link3_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link3_text"
    end

    def header_link3_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_link3_link"
    end

    def header_sentence
      I18n.t "recruiter.emails.notification.#{email_type}.full.header_sentence"
    end

    def big_heading
      I18n.t "recruiter.emails.notification.#{email_type}.full.big_heading", job_title: job_title,
                                                                 applicant_name: applicant_name
    end

    def summary_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.summary_text", recruiter_name: recruiter_name,
                                                                            job_title: job_title,
                                                                       applicant_name: applicant_name,
                                                                         company_name: company_name
    end


    def applicant_info_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.applicant_info", applicant_name: applicant_name,
                                                                    applicant_age_years: applicant_age,
                                                                         applicant_city: applicant_city,
                                                                  applicant_country_iso: applicant_country_iso,
                                                                        applicant_email: applicant_email,
                                                                        applicant_phone: applicant_phone
    end

    def section_heading_other_info
      I18n.t "recruiter.emails.notification.#{email_type}.full.section_heading_other_info"
    end


    def ed_documents
      # attached_documents_br
    end

    def section_heading_included_documents
      I18n.t "recruiter.emails.notification.#{email_type}.full.section_heading_included_documents"
    end

    def section_heading_send_email_to_applicant
      I18n.t "recruiter.emails.notification.#{email_type}.full.section_heading_send_email_to_applicant"
    end

    def section_heading_certifications
      I18n.t "recruiter.emails.notification.#{email_type}.full.section_heading_certifications"
    end

    def included_documents_as_ul
      attached_documents_ul
    end

    def bottom_disclaimer_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.bottom_disclaimer_text"
    end

    def footer_link1_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link1_text"
    end

    def footer_link1_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link1_link"
    end

    def footer_link2_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link2_text"
    end

    def footer_link2_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link2_link"
    end

    def footer_link3_text
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link3_text"
    end

    def footer_link3_link
      I18n.t "recruiter.emails.notification.#{email_type}.full.footer_link3_link"
    end

    def heading_available_in
      I18n.t "recruiter.emails.notification.jobapp.full.heading_available_in"
    end

    def heading_admin_notes
      I18n.t "recruiter.emails.notification.jobapp.full.heading_admin_notes"
    end

    def heading_job_title
      I18n.t "recruiter.emails.notification.#{email_type}.full.heading_job_title"
    end

    def heading_applicant_address
      I18n.t "recruiter.emails.notification.jobapp.full.heading_applicant_address"
    end

    def job_seeker_info
      jsi = ''
      #admin notice
      #availability
      #sal expectaions
      #toe
      #industries
      #locations
      if jobapp && !jobapp.contact_preferences.blank?
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.admin_notes', admin_notes: admin_notes)}"
      end
      if talent && talent.setting && talent.setting.availability
        availability = I18n.t('talent.data.available.'+talent.setting.availability_key)
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.availability', availability: availability)}"
      end
      if talent && talent.setting && talent.setting.salary_exp_min && talent.setting.salary_exp_max
        salary = I18n.t(talent.setting.price_range_email_key, from: currency(talent.setting.salary_exp_min),to: currency(talent.setting.salary_exp_max))
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.salary_expectation', salary_expectation: salary)}"
      end
      if talent && talent.setting && talent.setting.prefered_employment_type
        toe = I18n.t('talent.data.toe.' + talent.setting.prefered_employment_type)
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.toe', toe: toe)}"
      end
      if talent && talent.setting && talent.setting.working_industries && !talent.setting.working_industries.empty?
        industries = html_encode talent.setting.working_industries.join(', ')
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.working_industries', working_industries: industries)}"
      end
      if talent && talent.setting && talent.setting.working_locations && !talent.setting.working_locations.empty?
        locations = html_encode talent.setting.working_locations.join(', ')
        jsi = "#{jsi}#{I18n.t('recruiter.emails.general.job_seeker_information.working_locations', working_locations: locations)}"
      end
      jsi
    end

    def profile_picture_display
      if resume.profile_picture_available?
        '<tr>
          <td valign="top" align="center" style="border-collapse:collapse;">
          <img src="cid:IMAGECID" alt="Applicant Image" width="140" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;">
          </td>
        </tr>'
      else
        ''
      end
    end

    # Asssign requested informations into hash variable
    def vars
      {
        'APPLICANT_NAME' => applicant_name,
        'APPLICANT_FIRSTNAME' => applicant_firstname,
        'APPLICANT_LASTNAME' => applicant_lastname,
        'APPLICANT_AGE_YEARS' => applicant_age,
        'APPLICANT_CITY_CTRY' => applicant_city_country,
        'APPLICANT_EMAIL' => applicant_email,
        'APPLICANT_INFO_TEXT' => applicant_info_text,
        'APPLICANT_PHONE' => applicant_phone,
        'BIG_HEADING' => big_heading,
        'BOTTOM_DISCLAIMER_TEXT' => bottom_disclaimer_text,
        'FOOTER_LINK1_TEXT' => footer_link1_text,
        'FOOTER_LINK1_LINK' => footer_link1_link,
        'FOOTER_LINK2_TEXT' => footer_link2_text,
        'FOOTER_LINK2_LINK' => footer_link2_link,
        'FOOTER_LINK3_TEXT' => footer_link3_text,
        'FOOTER_LINK3_LINK' => footer_link3_link,
        'HEADER_LINK1_TEXT' => header_link1_text,
        'HEADER_LINK1_LINK' => header_link1_link,
        'HEADER_LINK2_TEXT' => header_link2_text,
        'HEADER_LINK2_LINK' => header_link2_link,
        'HEADER_SENTENCE' => header_sentence,
        'INCLUDED_DOCUMENTS' => included_documents_as_ul,
        'INCLUDED_DOCUMENTS_AS_UL' => included_documents_as_ul,
        'EMAIL_TO_APPLICANT_BTN_TEXT' => email_to_applicant_btn_text,
        'EMAIL_TO_APPLICANT_BTN_LINK' => email_to_applicant_btn_link,
        'JOB_TITLE' => job_title,
        'JOB_SOURCE' => job_source,
        'BLOCKING_LIST' => blocking_list,
        'AVAILABLE_IN' => availability,
        'PRICE_RANGE' => price_range,
        'COVERLETTER' => cover_letter,
        'ADMIN_NOTES' => admin_notes,
        'ADDED_DOCUMENTS_AS_BR' => attached_documents_br,
        'ADDED_DOCUMENTS_AS_UL' => attached_documents_ul,
        'ADDED_DOCUMENTS_AS_UL_CV_ROW' => attached_documents_ul_cv_row,
        'RECRUITER_NAME' => recruiter_name,
        'RECRUITER_FIRSTNAME' => recruiter_firstname,
        'RECRUITER_LASTNAME' => recruiter_lastname,
        'RECRUITER_EMAIL' => recruiter_email,
        'RECRUITER_PHONE' => recruiter_phone,
        'SECTION_HEADING_OTHER_INFO' => section_heading_other_info,
        'SECTION_HEADING_INCLUDED_DOCUMENTS' => section_heading_included_documents,
        'SECTION_HEADING_SEND_EMAIL_TO_APPLICANT' => section_heading_send_email_to_applicant,
        'SECTION_HEADING_CERTIFICATIONS' => section_heading_certifications,
        'SUBJECT' => subject,
        'SUMMARY_TEXT' => summary_text,
        'COMPANY_NAME' => company_name,
        'TALENT_NAME' => applicant_name,
        'HEADING_AVAILABLE_IN' => heading_available_in,
        'HEADING_ADMIN_NOTES' => heading_admin_notes,
        'HEADING_JOB_TITLE' => heading_job_title,
        'HEADING_APPLICANT_ADDRESS' => heading_applicant_address,
        'JOB_SEEKER_INFO_BLOCK' => job_seeker_info,
        'PROFILE_PICTURE_DISPLAY' => profile_picture_display
      }
    end

end
