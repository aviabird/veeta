// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var ready;
  ready = function(){
        // button is disabled by default
        $("#create_job_button").prop( "disabled", true );

        $(".edit-button").click(function () {
          var id = $(this).attr('id');
          fetchJobData(id);
          $("#job-edit-box").show();
        });
        $("#close-edit").click(function() {
          $("#job-edit-box").hide();
        });

        function fetchJobData(id) {
          var jobname = $("#"+id).parent().parent().parent().siblings()[0].id
          $("#edit-job-name").val(jobname);
          $("#edit_job_id").val(id);
        }

        // validation for create button
        $( "#job_name" ).keyup(function() {
          if ($("#job_name").val().length == 0) {
            $("#create_job_button").prop( "disabled", true );
          } else {
            $("#create_job_button").prop( "disabled", false );
          }
        });

        // Validation for update button
        $( "#edit-job-name" ).keyup(function() {
          if ($("#edit-job-name").val().length == 0) {
            $("#update_job_button").prop( "disabled", true );
          } else {
            $("#update_job_button").prop( "disabled", false );
          }
        });

        $( "#copy-to-clipboard" ).click(function() {
          link = $("#talent-pool-link").attr('href')
          window.prompt("Link kopieren: Ctrl+C, Enter", link);
        });

        $( "#copy-embed-code-to-clipboard" ).click(function() {
          content = $("#embed-code").text()
          window.prompt("Link kopieren: Ctrl+C, Enter", content);
        });
    };

$(document).ready(ready);
$(document).on('page:load', ready);
