require 'rubygems'
require 'linkedin'


class LinkedinClientService


#https://api.linkedin.com/v1/people/~:(id,first-name,skills,educations,languages,twitter-accounts,num-connections,picture-url,three-current-positions,interests,date-of-birth,phone-numbers,positions,main-address)?format=json

  # try to get data from xing
  def retrieve_from_api resume, options, current_talent
    linkedin_data_retrieved = false
    if !current_talent.auth.linkedin_access_token.blank? && !current_talent.auth.linkedin_access_token_secret.blank?
      begin
        client = linkedin_client
        client.authorize_from_access(current_talent.auth.linkedin_access_token, current_talent.auth.linkedin_access_token_secret)
        ResumeService::PrefillLinkedin.new(resume, client).call
        linkedin_data_retrieved = true
      rescue
         # Retrieving data from LinkedIn needs authorization
      end
    end
    linkedin_data_retrieved
  end

  # first step in oauth handshake with linkedin
  def trigger_oauth_handshake  callback_url, current_talent = nil, temp_cv = nil
    client = linkedin_client
    #import for existing user
    unless current_talent.blank?
      request_token = client.request_token(:oauth_callback => callback_url + '/linkedinreturn')
      current_talent.auth.linkedin_request_token = request_token.token
      current_talent.auth.linkedin_request_token_secret = request_token.secret
      current_talent.auth.save
    end
    #import for job landing page (not existing user)
    unless temp_cv.blank?
      request_token = client.request_token(:oauth_callback => callback_url + '/linkedinreturn/' + temp_cv.token)
      oauth_details = {}
      oauth_details[:request_token] = request_token.token
      oauth_details[:request_token_secret] = request_token.secret
      temp_cv.cv_content = oauth_details
      temp_cv.save
    end
    client.request_token.authorize_url #callback url coming from linkedin
  end

  #coming back from xing, save access token and get data from xing
  def handle_linkedin_callback  resume, options, current_talent, resume_linkedin_return_params
    #only this user can recieve callback
    if resume_linkedin_return_params[:oauth_verifier]
      client = linkedin_client
      pin = resume_linkedin_return_params[:oauth_verifier]
      atoken, asecret = client.authorize_from_request(current_talent.auth.linkedin_request_token, current_talent.auth.linkedin_request_token_secret, pin)
      current_talent.auth.linkedin_access_token = atoken
      current_talent.auth.linkedin_access_token_secret = asecret
      current_talent.auth.save
      client.authorize_from_access(current_talent.auth.linkedin_access_token, current_talent.auth.linkedin_access_token_secret)
      ResumeService::PrefillLinkedin.new(resume, client).call
    else
      raise StandardError.new ("oauth token does not match with the one of current_talent")
    end
  end

  def handle_linkedin_callback_without_talent  resume_linkedin_return_params
    #only this user can recieve callback
    temp_cv = TempCvImport.find_not_expired_by_token(resume_linkedin_return_params[:token])
    if temp_cv && resume_linkedin_return_params[:oauth_token] == temp_cv.cv_content[:request_token]
      client = linkedin_client
      pin = resume_linkedin_return_params[:oauth_verifier]
      atoken, asecret = client.authorize_from_request(temp_cv.cv_content[:request_token], temp_cv.cv_content[:request_token_secret], pin)

      temp_cv.cv_content[:access_token] = atoken
      temp_cv.cv_content[:access_token_secret] = asecret
      client.authorize_from_access(atoken, asecret)

      temp_cv.email = client.profile(:fields => %w(email-address)).email_address
      temp_cv.save
    else
      raise StandardError.new ("oauth token not found")
    end
    temp_cv
  end


  private

    def linkedin_client
      LinkedIn::Client.new(ENV["LINKEDIN_CONSUMER_KEY"], ENV["LINKEDIN_CONSUMER_SECRET"])
    end


end
