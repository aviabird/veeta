class XingClientService



  # try to get data from xing
  def retrieve_from_api resume, options, current_talent
    xing_data_retrieved = false
    if !current_talent.auth.xing_access_token.blank? && !current_talent.auth.xing_access_token_secret.blank?
      begin
        client = XingApi::Client.new(oauth_token: current_talent.auth.xing_access_token, oauth_token_secret: current_talent.auth.xing_access_token_secret)
        ResumeService::PrefillXing.new(resume, XingApi::User.me(client: client)).call
        xing_data_retrieved = true
      rescue XingApi::InvalidOauthTokenError => iote
         # Retrieving data from Xing needs authorization
      end
    end
    xing_data_retrieved
  end

  # first step in oauth handshake with xing
  def trigger_oauth_handshake callback_url, current_talent = nil, temp_cv = nil
    # import for existing user
    unless current_talent.blank?
      xing_request_info = XingApi::Client.new.get_request_token(callback_url + '/xingreturn')
      current_talent.auth.xing_request_token = xing_request_info[:request_token]
      current_talent.auth.xing_request_token_secret = xing_request_info[:request_token_secret]
      current_talent.auth.save
    end
    #import via job landing page
    unless temp_cv.blank?
      xing_request_info = XingApi::Client.new.get_request_token(callback_url + '/xingreturn/' + temp_cv.token)
      oauth_details = {}
      oauth_details[:request_token] = xing_request_info[:request_token]
      oauth_details[:request_token_secret] = xing_request_info[:request_token_secret]
      temp_cv.cv_content = oauth_details
      temp_cv.save
    end
    xing_request_info[:authorize_url] #callback url coming from xing
  end

  #coming back from xing, save access token and get data from xing
  def handle_xing_callback  resume, options, current_talent, resume_xing_return_params
    #only this user can recieve callback
    if resume_xing_return_params[:oauth_token] == current_talent.auth.xing_request_token
      xing_access_info = XingApi::Client.new.get_access_token(resume_xing_return_params[:oauth_verifier], request_token: current_talent.auth.xing_request_token, request_token_secret: current_talent.auth.xing_request_token_secret)
      current_talent.auth.xing_access_token = xing_access_info[:access_token]
      current_talent.auth.xing_access_token_secret = xing_access_info[:access_token_secret]
      current_talent.auth.save
      client = XingApi::Client.new(oauth_token: current_talent.auth.xing_access_token, oauth_token_secret: current_talent.auth.xing_access_token_secret)
      ResumeService::PrefillXing.new(resume, XingApi::User.me(client: client)).call
    else
      raise StandardError.new ("oauth token does not match with the one of current_talent")
    end
  end

  def handle_xing_callback_without_talent  resume_xing_return_params
    #only this user can recieve callback
    temp_cv = TempCvImport.find_not_expired_by_token(resume_xing_return_params[:token])
    if temp_cv && resume_xing_return_params[:oauth_token] == temp_cv.cv_content[:request_token]
      xing_access_info = XingApi::Client.new.get_access_token(resume_xing_return_params[:oauth_verifier], request_token:  temp_cv.cv_content[:request_token], request_token_secret:  temp_cv.cv_content[:request_token_secret])
      temp_cv.cv_content[:access_token] = xing_access_info[:access_token]
      temp_cv.cv_content[:access_token_secret] = xing_access_info[:access_token_secret]
      client = XingApi::Client.new(oauth_token: xing_access_info[:access_token], oauth_token_secret: xing_access_info[:access_token_secret])
      temp_cv.email = XingApi::User.me(client: client)[:users].first[:active_email]
      temp_cv.save
    else
      raise StandardError.new ("oauth token not found")
    end
    temp_cv
  end

end
