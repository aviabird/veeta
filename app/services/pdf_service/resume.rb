# Wrap service into Sidekiq or any other background service provider
# https://github.com/mileszs/wicked_pdf/wiki/Background-PDF-creation-via-delayed_job-gem

module PDFService
  class Resume < Service
    private_class_method :new
    attr_reader :resume

    def initialize resume
      @resume = resume

      raise 'Resume is empty' if resume == nil
      raise 'Provided object is not resume' unless resume.instance_of?(::Resume)
    end

    def pdf
      @pdf ||= WickedPdf.new.pdf_from_string(template, page_size: 'A4')
    end

    def file
      File.open(store_location, 'wb') do |f|
        f << pdf
      end
    end
    alias_method :store, :file

    def path
      store
      store_location
    end

    private
      def av
        av = ActionView::Base.new()
        av.view_paths = ActionController::Base.view_paths

        # need these in case your view constructs any links or references any helper methods.
        av.class_eval do
          include Rails.application.routes.url_helpers
          include ApplicationHelper
        end
        av
      end

      def template
        av.render template: 'pdfs/resume.html.erb', layout: nil, locals: { resume: resume }
      end

      def store_location
        Rails.root.join('tmp', 'pdfs', "#{resume.id}.pdf")
      end

    class << self
      # Get PDF of resume
      def pdf resume
        new(resume).pdf
      end

      # Get path to resume with saving the latest version
      def path resume
        new(resume).path
      end

      # Get file with current resume as a pdf
      def file resume
        new(resume).file
      end
      alias_method :store, :file
    end
  end
end
