require "http"

module PDFService
  class Kickresume

    # /job/add/ - Method: POST
    def add resume
      # check ping
      # create job (pass resume id)
      LogService.call "#{PDFService::Kickresume.name}.add", {resource: resume, resume: resume, details: "Serialize resume for Kickresume call...",priority: :low}
      serialized = JSON.generate(KickresumeSerializer.new(resume).serializable_hash)
      LogService.call "#{PDFService::Kickresume.name}.add", {resource: resume, resume: resume, details: "Adding job to Kickresume", data:  serialized,  priority: :low}
      result = HTTP.basic_auth(:user => ENV['KICKRESUME_USER'], :pass => ENV['KICKRESUME_PASSWORD']).post("#{ENV['KICKRESUME_BASE_API_URL']}/job/add/",:body => serialized).body
      kr_result = JSON.parse(result.first)

      # set kick resume id
      if kr_result['id']
        resume.pdf_template_setting.update_column(:ext_id, kr_result['id'])
        LogService.call "#{PDFService::Kickresume.name}.add.success", {resource: resume, resume: resume, details: "PDF generation job added via Kickresume API:  #{kr_result}",priority: :low}
        true
      else
        LogService.call "#{PDFService::Kickresume.name}.add.error", {resource: resume, resume: resume, details: "Failed to add PDF generation job added via Kickresume API: #{kr_result}",priority: :high}
        false
      end
    end

    # /job/check/<number>/ - Method: GET
    def check resume
      # get id and check
      if resume.pdf_template_setting && resume.pdf_template_setting.ext_id
        result = HTTP.basic_auth(:user => ENV['KICKRESUME_USER'], :pass => ENV['KICKRESUME_PASSWORD']).get("#{ENV['KICKRESUME_BASE_API_URL']}/job/check/#{resume.pdf_template_setting.ext_id}/").body
        kr_result = JSON.parse(result.first)
        if kr_result['ready']
          if kr_result['errors'] && !kr_result['errors'].empty? # errors occured
            LogService.call "#{PDFService::Kickresume.name}.check.ready-error", {resource: resume, resume: resume, details: "Errors occured when checking status of job: #{kr_result}",priority: :high}
            true
          else
            LogService.call "#{PDFService::Kickresume.name}.check.ready-success", {resource: resume, resume: resume, details: "Job successfully finished: #{kr_result}",priority: :low}
            true
          end
        else
          LogService.call "#{PDFService::Kickresume.name}.check.inprogress", {resource: resume, resume: resume, details: "Job not yet processed by Kickresume: #{kr_result}",priority: :low}
          false
        end
      else
        LogService.call "#{PDFService::Kickresume.name}.check.no-ext-id", {resource: resume, resume: resume, details: "No Kickresume ID available for resume: #{resume.inspect}",priority: :medium}
      end
    end

    # /job/get/<number>/ - Method GET
    def get resume
      # get id and check
      if resume.pdf_template_setting && resume.pdf_template_setting.ext_id
        result = HTTP.basic_auth(:user => ENV['KICKRESUME_USER'], :pass => ENV['KICKRESUME_PASSWORD']).get("#{ENV['KICKRESUME_BASE_API_URL']}/job/get/#{resume.pdf_template_setting.ext_id}/")

        if result.status == 200
          tempfile = Tempfile.new([SecureRandom.urlsafe_base64(32), '.pdf'], Rails.root.join('tmp'))
          tempfile.binmode
          pdf_chunk = result.body.readpartial
          begin
            tempfile.write pdf_chunk
            pdf_chunk = result.body.readpartial
          end while !pdf_chunk.nil?
          tempfile.close
          LogService.call "#{PDFService::Kickresume.name}.get.success", {resource: resume, resume: resume, details: "PDF successfully downloaded from Kickresume",priority: :medium}
          tempfile
        else
          LogService.call "#{PDFService::Kickresume.name}.get.not-found", {resource: resume, resume: resume, details: "PDF couldn't be downloaded from Kickresume for resume: #{resume.inspect}",priority: :high}
          nil
        end
      else
        LogService.call "#{PDFService::Kickresume.name}.get.no-ext-id", {resource: resume, resume: resume, details: "No Kickresume ID available for resume: #{resume.inspect}",priority: :medium}
      end
    end

  end
end
