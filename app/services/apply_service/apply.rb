module ApplyService
  # Resume latest may cause inconsitency because it can be created somewhere
  # else at the same time
  class Apply < Service
    attr_accessor :jobapp

    delegate :job, to: :jobapp
    delegate :resume, to: :jobapp
    delegate :company, to: :job
    delegate :recruiters, to: :company

    def initialize jobapp, jobapp_params
      @jobapp = jobapp
      @jobapp_params = jobapp_params
      @options = {}
    end

    def call
      if jobapp.update(jobapp_params)
        if job.kind_of?(Job)
          share
          link
          perform_job_application
        elsif job.kind_of?(EmailApplication)
          #clone
          # It is necessary to share before we update resume on jobapp to
          # the versioned copy
          # perform job application!
          share_email_app
          link
          perform_email_job_application
        end
      end
      super
    end

    def info
      { meta: @options }
    end

    private
      def perform_job_application
        if !company.nil? && company.setting.jobapp_mail_enabled?
          recruiters.each { |r| Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i,MailJobappJob, I18n.locale, 'company',jobapp.id, r.id)  if r.email_notifications_enabled  }
        end
        ### Disabled, as this will be handled via Intercom
        #if ActivityService.new.activities_count(resume.origin.talent) == 1 # first entry in activity stream
        #  Resque.enqueue(MailFirstJobappJob, resume.talent.locale, jobapp.id)
        #end
        jobapp.update_column(:submitted_at, DateTime.now)
      end

      def perform_email_job_application
         Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i,MailJobappJob, I18n.locale, 'emailcompany', jobapp.id)
         jobapp.update_column(:submitted_at, DateTime.now)
      end

      def share
        Resume::Share.call(resume, [company.id])
      end

      def share_email_app
        Resume::Share.call(resume, [jobapp.job.id],false)
      end

      def link
        jobapp.update(resume: resume.latest)
      end

      def jobapp_params
        @jobapp_params.merge(draft: false)
      end
  end
end
