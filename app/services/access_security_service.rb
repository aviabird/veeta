class AccessSecurityService

  ###
  # handles access securty paths of sharings
  ###

  def initialize
    @webdav_service = WebdavService.new
    @els = ExternalLinkService.new
  end

  #when a new sharing is created, the old ones are being kept
  def establish_sharing sharing, talent = nil
    #revoke_access sharing, false if talent.nil?
    Rails.logger.info("#{AccessSecurityService.name}: Start to create new mapping for sharing #{sharing.id}")
    if sharing.kind_of?(VeetaButtonSharing) && !sharing.company.nil?
      # if no resume was selectable or resume was deleted share only talent
      if !sharing.resume.nil?
        @els.resume_mapping sharing.resume, sharing.company
      else
        @els.talent_mapping talent, sharing.company
      end
    elsif sharing.kind_of?(ResumeSharing) && !sharing.company.nil?
      @els.resume_mapping sharing.resume, sharing.company
    elsif sharing.kind_of?(ResumeSharing) && !sharing.email_application.nil?
      @els.resume_mapping sharing.resume, sharing.email_application
    end
  end

  def revoke_access sharings_to_protect#, except_current_sharing = false
    #Rails.logger.info("#{AccessSecurityService.name}:Revoke sharings for #{sharing.id}, exclude old one = #{except_current_sharing}")

    #origin_ids = []
    # get origin ids of talent
    #if sharing.resume #veetabutton sharings can exist without resume
    #  origin_ids = Resume.select(:id).where(talent_id: Resume.with_deleted.find(sharing.resume.origin_id).talent_id).to_a
    #end

    # include all previous sharings for each sharing of talent-company (by company_id/emailaddress and origin's talent_id)
    #if sharing.kind_of?(VeetaButtonSharing) && !sharing.company.nil?
    #  talent = sharing.veeta_button_sharing_connection.talent
    #  sharings_to_protect = VeetaButtonSharing.joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {client_application_id: sharing.company.id, access_revoked_at: nil, talent_id: talent.id})
    #elsif sharing.kind_of?(ResumeSharing) && !sharing.company.nil?
    #  sharings_to_protect = ResumeSharing.where(company: sharing.company).joins(:resume).where("origin_id in (?)",origin_ids)
    #elsif sharing.kind_of?(ResumeSharing) && !sharing.email_application.nil?
    #  sharings_to_protect = ResumeSharing.joins(:email_application).where(email_applications: {recruiter_email: sharing.email_application.recruiter_email}).joins(:resume).where("origin_id in (?)",origin_ids)
    #else
    #  sharings_to_protect = []
    #end

    Rails.logger.info("#{AccessSecurityService.name}:Try to invalidate and update #{sharings_to_protect.size} sharings")
    # iterate over sharings
    sharings_to_protect.each do |sharing_with_same_external_entity|
      # skip current sharing
      #if except_current_sharing && sharing_with_same_external_entity == sharing
      #  next
      #end

      if sharing_with_same_external_entity.resume
        resume = sharing_with_same_external_entity.resume
        talent = sharing_with_same_external_entity.resume.talent
        if !sharing_with_same_external_entity.company.nil?
          external_entity =  sharing_with_same_external_entity.company
        elsif !sharing_with_same_external_entity.email_application.nil?
          external_entity =  sharing_with_same_external_entity.email_application
        end
        # remove mapping for external_entity and resource
        if @els.remove_mapping resume, external_entity
          Rails.logger.info("#{AccessSecurityService.name}:Mapping for resume #{resume.id} entity #{external_entity.id} removed, rename resources and update other mappings...")
          # change webdav url of talent and each document
          rename_resource_path talent
          # get old mappings in same folder and refresh with new access_security_part
          # UNNECCESSARY DUE TO LINK TO RESOURCE
          # @els.update_mappings talent.unique_part, talent.asp
          if resume.documents
            resume.documents.each do |doc|
              # change all secret token mappings for talent and documents
              rename_resource_path doc
              # get old mappings in same folder and refresh with new access_security_part
              # UNNECCESSARY DUE TO LINK TO RESOURCE
              # @els.update_mappings doc.unique_part, doc.asp
            end
          end
        end
      elsif sharing_with_same_external_entity.kind_of?(VeetaButtonSharing) && sharing_with_same_external_entity.veeta_button_sharing_connection
        talent = sharing_with_same_external_entity.veeta_button_sharing_connection.talent
        external_entity =  sharing_with_same_external_entity.company
        if talent && @els.remove_talent_mapping(talent, external_entity)
          rename_resource_path talent
          # nothing else then the talent as documents are not shared via veeta button
        end
      end
    end
  end

  private

    def rename_resource_path resource
      # change  token for talent and documents
      if resource.resource_file_available?
        old_dir = resource.store_dir
        old_token = resource.token
        resource.refresh_token
        new_dir = resource.store_dir
        new_token= resource.token
        # change webdav url of talent and each document
        begin
          Rails.logger.info("#{AccessSecurityService.name}:Renaming webdav folder from #{old_dir} to #{new_dir}")
          @webdav_service.rename_folder old_dir, new_dir
          resource.class.where(token: old_token).each do |resource_with_same_token|
            resource_with_same_token.update_column(:token, new_token)
          end
        rescue
          Rails.logger.fatal("#{AccessSecurityService.name}:Failed to rename webdav folder from #{old_dir} to #{new_dir}")
        end
      end
    end

end
