class CopyService
  # Original company is comapny from which sharing is to be copied.  
  # New company is company to which sharing is to be copied.
  def initialize original_company_short_name, new_company_short_name
    @company = Company.find_by_short_name(original_company_short_name)
    @new_company = Company.find_by_short_name(new_company_short_name)
  end

  # copy sharing from one company to another company
  def resume_sharings
    sharings = []
    if @company.present? && @new_company.present?
      # gives all active sharings @company.sharings.nonrevoked
      original_resume_ids = @company.sharings.nonrevoked.pluck(:resume_id)
      new_resume_ids = @new_company.sharings.pluck(:resume_id)
      resume_ids = original_resume_ids - new_resume_ids

      @company.sharings.where(resume_id: resume_ids).map do |sharing|
        new_sharing = sharing.dup.attributes
        new_sharing["company_id"] = @new_company.id
        sharings << new_sharing
      end
      begin
        ResumeSharing.create(sharings) if sharings.present?
      rescue => e
        Rails.logger.error("copy resume sharings failed: #{e}")
      end

    end
  end

  # copy company talent infos from one company to another company
  def company_talent_infos
    infos = []
    if @company.present? && @new_company.present?
      original_talent_ids = @company.talent_infos.pluck(:talent_id)
      new_talent_ids = @new_company.talent_infos.pluck(:talent_id)
      talent_ids = original_talent_ids - new_talent_ids
      @company.talent_infos.where(talent_id: talent_ids).each do |talent_info|
        new_talent_info = talent_info.dup.attributes
        # removing company specific stuff (rating is copied)
        new_talent_info.delete(:ext_id)
        new_talent_info.delete(:hashed_talent_id_salt)
        new_talent_info.delete(:hashed_talent_id)
        new_talent_info.delete(:sequential_id)
        new_talent_info.delete(:ext_myveeta_events)
        new_talent_info["company_id"] = @new_company.id
        infos << new_talent_info
      end
      begin
        CompanyTalentInfo.create(infos) if infos.present?
      rescue => e
        Rails.logger.error("copy company talent info failed: #{e}")
      end
    end
  end
end
