class JobService::GenericJobReadingService < Service

  attr_reader :ext_job_id, :company

  def initialize ext_job_id, company = nil
    @ext_job_id = ext_job_id
    @company = company
  end

  # Checks if a job needs to be retrieved from another system or not and returns the
  # corresponding myveeta job object
  def call
    return nil if ext_job_id.blank?         # no job id given
    if (ext_job_id =~ /^\w\w\w-.+$/)        # if it starts with 3 characters, get integration and company
      short_name = ext_job_id.split("-").first
      company = Company.find_by(short_name: short_name)
      integration = company.job_fetching_integration if company.present?
    end
    # if it is an erecruiter integration, get jobs with the erecruiter service
    if company.present? && integration && integration.integration_type == 'erecruiter'
      return JobService::ErecruiterJobReadingService.call integration, ext_job_id
    else  # try to get it from system
      if Job.exists?(ext_id: ext_job_id)
        return Job.find_by_ext_id(ext_job_id)
      end
    end
  end


  def call_list
    # if it is an erecruiter integration, get jobs with the erecruiter service
    if company.present? && company.job_fetching_integration.present? && company.job_fetching_integration.integration_type == 'erecruiter'
        JobService::ErecruiterJobReadingService.call_list company.job_fetching_integration, nil, company
    end
  end

  def self.call_list(*args)
    new(nil, *args).call_list
  end

end
