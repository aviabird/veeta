class JobService::ErecruiterJobReadingService < Service

  attr_reader :integration, :ext_job_id, :company

  def initialize integration, ext_job_id, company = nil
    @integration = integration
    @ext_job_id = ext_job_id
    @company = company
    @headers = {'Content-Type': "application/json", 'Accept': "application/json" }
  end


  # retrieve job from API and add/update/disable the myveeta accordingly
  def call
    # ensure that the job request is authenitated
    if do_auth
      #read job
      job = read_job
      # check if the job is already on myveeta
      exists_on_myveeta = Job.exists?(ext_id: ext_job_id)
      process_job job, exists_on_myveeta, ext_job_id
    end
  end

  # bach update all jobs of a company
  def call_list
    # ensure that the job request is authenitated
    if do_auth
      processed_job_ids = []
      #read job
      jobs = read_jobs
      jobs.each do |job|
        exists_on_myveeta = Job.exists?(ext_id: "#{company.short_name}-#{job[:Id]}")
        process_job job, exists_on_myveeta, "#{company.short_name}-#{job[:Id]}"
        processed_job_ids << "#{company.short_name}-#{job[:Id]}"
      end if jobs.present?
      # now diable other jobs
      if jobs.present?
        Job.where(company: company, status: 'new', unsolicited_job: false).where("ext_id NOT IN (?)", processed_job_ids).update_all(status: 'closed')
      end

    end
  end

  def self.call_list(*args)
    new(*args).call_list
  end

  private

    def do_auth
      # first check if a token already exists and is not expired
      begin
        temp_auth = JSON.parse(@integration.temporary_authentication_details)
        if temp_auth && temp_auth['Expires'] && temp_auth['Expires'] > Time.now.utc + 3.hours # add three hours to be safe
          @headers = {'X-ApiToken': temp_auth['Token'], 'Content-Type': "application/json", 'Accept': "application/json" }
          return true
        end
      end
      # else exchange login data with token via API
      begin
        # exchange key with token
        response = RestClient::Request.execute(method: :post, url: "#{@integration.integration_endpoint}/Api", headers: @headers, payload: @integration.authentication_details)
        if response.code == 200 && response.present?
          response = JSON.parse(response)
          # write token and expiry date for consecutive calls
          @integration.update_column(:temporary_authentication_details, response.to_json)
          @headers = {'X-ApiToken': response['Token'], 'Content-Type': "application/json", 'Accept': "application/json" }
          return true
        else
          return false
        end
      rescue Exception => e
        return false
      end
    end

    # get job from API
    def read_job
      # remove company shortname from ext_job_id
      job_id = ext_job_id.sub /^\w\w\w-/, ''
      begin
        # retrieve job from API
        response = RestClient::Request.execute(method: :get, url: "#{@integration.integration_endpoint}/Api/Job/#{job_id}", headers: @headers)
        if response.code == 200 && response.present?
          hash =  JSON.parse(response).deep_symbolize_keys!
          if hash[:PublishedOn].present?    # is this job still published?
            return hash
          else
            return false
          end
        else
          return false
        end
      rescue Exception => e
        return false
      end
    end

    def read_jobs
      # remove company shortname from ext_job_id
      begin
        # retrieve job from API
        response = RestClient::Request.execute(method: :get, url: "#{@integration.integration_endpoint}/Api/PublishedJobs", headers: @headers)
        if response.code == 200 && response.present?
          json_response = JSON.parse(response)
          if json_response['Jobs'].present? && json_response['Jobs'].kind_of?(Array)
            response_hash = []
            json_response['Jobs'].each do |element|
              response_hash << element.deep_symbolize_keys!
            end
            return response_hash
          else
            return false
          end
        else
          return false
        end
      rescue Exception => e
        return false
      end
    end

    # determine existing job, perpare for insert/update/disable
    def process_job job, exists_on_myveeta, ext_job_id
      if job && exists_on_myveeta # update the existing job on mv
        existing_job = Job.find_by(ext_id: ext_job_id)
        return upsert_job existing_job, job, ext_job_id
      elsif job && !exists_on_myveeta # insert the job to mv
        new_job = Job.new
        return upsert_job new_job, job, ext_job_id
      elsif !job && exists_on_myveeta  # disable the job to mv
        existing_job = Job.find_by(ext_id: ext_job_id)
        existing_job.disable
        return existing_job
      end
    end

    # update/add myveet job object with data retrieved from API
    def upsert_job myveeta_job, job, ext_job_id
      myveeta_job.name = CGI.unescapeHTML(job[:Title])
      jobname_addition = []
      jobname_addition << CGI.unescapeHTML(job[:SubTitle]) if job[:SubTitle].present?
      jobname_addition << CGI.unescapeHTML(job[:Location]) if job[:Location].present?
      jobname_addition << Date.parse(CGI.unescapeHTML(job[:UpdateDate])).strftime('%d.%m.%Y')  if job[:UpdateDate].present?
      myveeta_job.jobname_addition = "#{jobname_addition.join(', ')}" unless jobname_addition.empty?
      myveeta_job.company = @integration.company
      myveeta_job.status = 'new'
      myveeta_job.recruiter = @integration.company.owner
      myveeta_job.language = Language.find(job[:Culture]) || Language.find('de')
      if job[:AvailableCultures]
        job[:AvailableCultures].each do |culture|
          myveeta_job.accepted_jobs_languages.build(language: Language.find(culture))
        end
      else
        myveeta_job.accepted_jobs_languages.build(language: myveeta_job.language)
      end
      myveeta_job.ext_id = ext_job_id
      myveeta_job.save
      return myveeta_job
    end
end
