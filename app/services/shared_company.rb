class SharedCompany
  include ActiveModel::Serialization

  attr_reader :company
  attr_accessor :resumes, :applied_at

  # Initialize shared company with company and list of arrays
  def initialize company, resume, applied_at
    @company = company
    @resumes = [ resume ]
    @applied_at = [ applied_at ]
  end

  # Return self if self exist in given array, instead return false
  def is_company_in? companies
    companies.detect {|c| c.company == self.company}
  end
end