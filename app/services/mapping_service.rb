class MappingService
  @@to_ve_json = nil
  @@to_ng_json = nil

  def veeta_industry from, key
    #fromLinkedIn
    #fromXing
    key = key.to_s unless key.nil?
    if from == 'fromXing' && !key.nil? && key.length >= 4
      key = key.to_s[0...-4] + 'xxxx'
      if key.length == 5
        key = "0#{key}"
      end
    end
    unless to_ve_json['Common']['Industry'][from].nil?
      to_ve_json['Common']['Industry'][from][key]
    else
      nil
    end
  end

  def veeta_academic_title_level from, key
    key = key.to_s unless key.nil?
    unless to_ve_json['Talent']['Education']['HighestEducation_to_AcademicTitle'][from].nil?
      to_ve_json['Talent']['Education']['HighestEducation_to_AcademicTitle'][from][key]
    else
      nil
    end
  end

  def veeta_education_level from, key
    key = key.to_s unless key.nil?
    unless to_ve_json['Talent']['Education']['EducationLevel'][from].nil?
      to_ve_json['Talent']['Education']['EducationLevel'][from][key]
    else
      nil
    end
  end

  def veeta_language_level from, key
    key = key.to_s unless key.nil?
    unless to_ve_json['Talent']['LanguageSkill'][from].nil?
      to_ve_json['Talent']['LanguageSkill'][from][key]
    else
      nil
    end
  end

  def veeta_career_level from, key
    key = key.to_s unless key.nil?
    unless to_ve_json['Talent']['WorkExperience']['CareerLevel'][from].nil?
      to_ve_json['Talent']['WorkExperience']['CareerLevel'][from][key]
    else
      nil
    end
  end

  def veeta_employment_type from, key
    key = key.to_s unless key.nil?
    unless to_ve_json['Talent']['WorkExperience']['EmploymentType'][from].nil?
      to_ve_json['Talent']['WorkExperience']['EmploymentType'][from][key]
    else
      nil
    end
  end



###### mapping to xing
  def xing_language_level from, key
    key = key.to_s unless key.nil?
    unless to_ng_json['Talent']['LanguageSkill'][from].nil?
      to_ng_json['Talent']['LanguageSkill'][from][key]
    else
      nil
    end
  end

  def xing_industry from, key
    key = key.to_s unless key.nil?
    unless to_ng_json['Common']['Industry'][from].nil?
      to_ng_json['Common']['Industry'][from][key]
    else
      nil
    end
  end

  def xing_legacy_industry from, key
    key = key.to_s unless key.nil?
    unless to_ng_json['Common']['Industry'][from].nil? || to_ng_json['Common']['Industry'][from]['XingLegacy'].nil?
      to_ng_json['Common']['Industry'][from]['XingLegacy'][key]
    else
      nil
    end
  end

  def xing_industry_name from, key, locale
    key = key.to_s unless key.nil?
    unless to_ng_json['Common']['Industry'][from].nil? || to_ng_json['Common']['Industry'][from]['PlainText'][locale].nil?  || to_ng_json['Common']['Industry'][from]['PlainText'].nil?
      to_ng_json['Common']['Industry'][from]['PlainText'][locale][key]
    else
      nil
    end
  end

  def xing_career_level from, key
    key = key.to_s unless key.nil?
    unless to_ng_json['Talent']['WorkExperience']['CareerLevel'][from].nil?
      to_ng_json['Talent']['WorkExperience']['CareerLevel'][from][key]
    else
      nil
    end
  end

  def xing_employment_type from, key
    key = key.to_s unless key.nil?
    unless to_ng_json['Talent']['WorkExperience']['EmploymentType'][from].nil?
      to_ng_json['Talent']['WorkExperience']['EmploymentType'][from][key]
    else
      nil
    end
  end


  private
    def to_ve_json
      if @@to_ve_json.nil?
        @@to_ve_json = TranslationsService.call('ve').to_hash
      end
      @@to_ve_json
    end

    def to_ng_json
      if @@to_ng_json.nil?
        @@to_ng_json = TranslationsService.call('ng').to_hash
      end
      @@to_ng_json
    end

end
