class IntercomService < Service

  attr_accessor :log_entry

  @@EVENT_NAMES = {}
  @@EVENT_NAMES['api.talent.resumes.download'] = 'intapi-cv-download'
  @@EVENT_NAMES['api.talent.auth.sessions.create'] = 'intapi-sign-in'
  @@EVENT_NAMES['api.talent.resumes.new'] = 'intapi-cv-new'
  @@EVENT_NAMES['api.talent.jobapps.new'] = 'intapi-jobapp-init'
  @@EVENT_NAMES['api.talent.resumes.download'] = 'intapi-cv-download'
  @@EVENT_NAMES['api.talent.talents.update_locale'] = 'intapi-locale-switch'
  @@EVENT_NAMES['api.talent.talents.update'] = 'intapi-update'
  @@EVENT_NAMES['api.talent.auth.sessions.destroy'] = 'intapi-sign-out'
  @@EVENT_NAMES['api.talent.jobapps.create'] = 'intapi-jobapp-email-create'
  @@EVENT_NAMES['api.talent.talents.avatar_upload'] = 'intapi-profile-image-update'
  @@EVENT_NAMES['api.talent.resumes.fillin'] = 'intapi-cv-import-pdf'
  @@EVENT_NAMES['api.talent.resume.pdf_template_settings.update'] = 'intapi-cv-template-change'
  @@EVENT_NAMES['api.talent.jobapps.finish'] = 'intapi-jobapp-send'
  @@EVENT_NAMES['api.talent.resumes.share'] = 'intapi-cv-update-share'
  @@EVENT_NAMES['api.talent.resumes.sharing_request'] = 'intapi-cv-send-mail'
  @@EVENT_NAMES['api.talent.resumes.destroy'] = 'intapi-cv-delete'
  @@EVENT_NAMES['api.talent.resumes.fillin_from_xing'] = 'intapi-cv-import-xing'
  @@EVENT_NAMES['api.talent.resumes.fillin_from_linkedin'] = 'intapi-cv-import-linkedin'
  @@EVENT_NAMES['api.talent.talent_auths.password'] = 'intapi-profile-password-init'
  @@EVENT_NAMES['api.talent.sharing.revoke'] = 'intapi-sharing-revoke'
  @@EVENT_NAMES['api.talent.jobapps.withdrawn'] = 'intapi-jobapp-withdraw'
  @@EVENT_NAMES['api.talent.cv_sharing_responses.new'] = 'intapi-sharing-init'
  @@EVENT_NAMES['api.talent.auth.passwords.update'] = 'intapi-profile-password-change'
  @@EVENT_NAMES['api.talent.talent_auths.resend_confirmation'] = 'intapi-profile-email-confirmation'
  @@EVENT_NAMES['api.talent.cv_sharing_responses.finish'] = 'intapi-sharing-send'
  # CV update events
  @@EVENT_NAMES['api.talent.resume.work_experiences.update'] = 'intapi-resume-module-update'
  @@EVENT_NAMES['api.talent.resume.abouts.update'] = 'intapi-resume-module-update-non-repeatable'
  @@EVENT_NAMES['api.talent.resume.links.update'] = 'intapi-resume-module-update-non-repeatable'
  @@EVENT_NAMES['api.talent.resume.skills.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.educations.update'] = 'intapi-resume-module-update'
  @@EVENT_NAMES['api.talent.resume.skills.update'] = 'intapi-resume-module-update'
  @@EVENT_NAMES['api.talent.resume.languages.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.work_experiences.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.educations.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.documents.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.abouts.create'] = 'intapi-resume-module-create-non-repeatable'
  @@EVENT_NAMES['api.talent.resume.links.create'] = 'intapi-resume-module-create-non-repeatable'
  @@EVENT_NAMES['api.talent.resume.work_experiences.destroy'] = 'intapi-resume-module-delete'
  @@EVENT_NAMES['api.talent.resume.skills.destroy'] = 'intapi-resume-module-delete'
  @@EVENT_NAMES['api.talent.resume.certifications.create'] = 'intapi-resume-module-create'
  @@EVENT_NAMES['api.talent.resume.languages.update'] = 'intapi-resume-module-update'
  @@EVENT_NAMES['api.talent.resume.educations.destroy'] = 'intapi-resume-module-delete'
  @@EVENT_NAMES['api.talent.resume.certifications.update'] = 'intapi-resume-module-update'
  @@EVENT_NAMES['api.talent.resumes.update'] = 'intapi-resume-name-update'
  @@EVENT_NAMES['api.talent.resume.certifications.destroy'] = 'intapi-resume-module-delete'
  @@EVENT_NAMES['api.talent.resume.languages.destroy'] = 'intapi-resume-module-delete'
  @@EVENT_NAMES['api.talent.resume.documents.destroy'] = 'intapi-resume-module-delete'

  @@ACTIVITY_EVENTS = ['api.talent.jobapps.finish', 'api.talent.cv_sharing_responses.finish']
  @@SOURCE_COMPANY_EVENTS = ['api.talent.jobapps.new', 'api.talent.cv_sharing_responses.new']

  def initialize log_entry = nil
    @log_entry = log_entry
  end

  def call
    return if  log_entry.nil? || skip_intercom?(log_entry)
    if trackable?(log_entry) && log_entry.source_user_id.present?
      intercom = intercom_client
      intercom_user_id = Digest::MD5.hexdigest(log_entry.source_user_id)
      begin
        intercom.events.create(:event_name=>lookup_key(log_entry),:user_id=>intercom_user_id,:created_at=>log_entry.created_at.to_i)
      rescue => e
        # e.g. user was not found on Intercom
        Rails.logger.warn(e)
      end

      # custom attribute source company name... company which the talents registers for (through share or jobapp)
      if company_name = first_company(log_entry)
        user = intercom.users.find(:user_id => intercom_user_id)
        if user && user.custom_attributes["source_company"].blank?
          user.custom_attributes = {"source_company": company_name}
          intercom.users.save(user)

        end
      elsif company_name = activity(log_entry)    # custom  attribute, most recent finished jobapp/share company
        user = intercom.users.find(:user_id => intercom_user_id)
        if user
          user.custom_attributes = {"latest_activity_company": company_name}
          intercom.users.save(user)
        end
      end
    end
    if createable?(log_entry)
      intercom = intercom_client
      begin
        talent = Talent.find(log_entry.talent_id)
        user = intercom.users.create(:user_id => talent.intercom_user_id,:email => log_entry.source_user_email, :name => talent.name, :signed_up_at => log_entry.created_at.to_i)
        intercom.users.save(user)
        ## MERGE LEAD HERE
        merge_lead_and_user intercom, user
        event = lookup_create_key(log_entry)
        if event.present?
          intercom.events.create(:event_name=>event,:user_id=>talent.intercom_user_id,:created_at=>log_entry.created_at.to_i)
        end
      rescue => e
        Rails.logger.warn(e)
      end
    end
    if updateable?(log_entry)
      intercom = intercom_client
      begin
        talent = TalentAuth.find(log_entry.source_user_id).talent
        user = intercom.users.find(:user_id => talent.intercom_user_id)
        if user
          user.name = talent.name
          intercom.users.save(user)
          ## MERGE LEAD HERE
          merge_lead_and_user intercom, user
          intercom.events.create(:event_name=>lookup_key(log_entry),:user_hash => talent.intercom_user_hash,:user_id => talent.intercom_user_id,:created_at=>log_entry.created_at.to_i)
        end
      rescue => e
        Rails.logger.warn(e)
      end
    end
  end

  # if it is a test user delete from intercom
  def validate_intercom_user intercom_user_id, email
    return if valid_intercom_email_address?(email)
    intercom = intercom_client
    begin
      user = intercom.users.find(:user_id => intercom_user_id)
      if user
        intercom.users.delete(user)
      end
    rescue => e
      Rails.logger.warn(e)
    end
  end

  private

    # converting Intercom leads to user https://developers.intercom.io/reference#convert-a-lead
    def merge_lead_and_user intercom, user
      contacts = intercom.contacts.find_all(email: user.email)
      contacts.each do |contact|
        intercom.contacts.convert(contact, user)
      end if contacts.present?
    end

    # return company name if user just started first jobapp/cv share
    def first_company log_entry
      if log_entry && @@SOURCE_COMPANY_EVENTS.include?(log_entry.key) && log_entry.source_user_id && log_entry.company_id.present?
        # if talent auth is guest (only set when source_company not yet set)
        talent_auth = TalentAuth.find(log_entry.source_user_id)
        if talent_auth.guest?
          company = Company.find(log_entry.company_id)
          return company.name
        end
      end
    end

    # return company name if user finished jobapp/cv share
    def activity log_entry
      if log_entry && log_entry.company_id.present? && @@ACTIVITY_EVENTS.include?(log_entry.key)
        company = Company.find(log_entry.company_id)
        return company.name
      end
    end

    def trackable? log_entry
      !@@EVENT_NAMES[log_entry.key].blank?
    end

    def createable? log_entry
      !(log_entry.key =~ /^api\.talent\.auth\.registrationscontroller\.create\.(.*)\.success$/i).nil?
    end

    def updateable? log_entry
      log_entry.key == 'api.talent.talents.update'
    end

    def lookup_key log_entry
      @@EVENT_NAMES[log_entry.key]
    end

    def lookup_create_key log_entry
      if !(log_entry.key =~ /^api\.talent\.auth\.registrationscontroller\.create\.user_reg(.*)\.success$/i).nil?
        'intapi-sign-up-user-reg'
      elsif !(log_entry.key =~ /^api\.talent\.auth\.registrationscontroller\.create\.pool_link(.*)\.success$/i).nil?
        'intapi-sign-up-pool-link'
      elsif !(log_entry.key =~ /^api\.talent\.auth\.registrationscontroller\.create\.job_link(.*)\.success$/i).nil?
        'intapi-sign-up-job-link'
      else
        nil
      end
    end

    def skip_intercom? log_entry
      #no blacklist, continue with intercom
      if ENV['INTERCOM_USER_HOST_BLACKLIST'].blank?
        return false
      #blacklist and email avaialibe - check
      elsif log_entry.source_user_email && log_entry.source_user_email.split("@").length > 1
        ENV['INTERCOM_USER_HOST_BLACKLIST'].split(",").include?(log_entry.source_user_email.split("@")[1])
        # if user is found on intercom, delete
      else
        false # continue by default
      end
    end

    def valid_intercom_email_address? email
      if ENV['INTERCOM_USER_HOST_BLACKLIST'].blank?
        true
      elsif !email.blank? #blacklist and email avaialibe - check
        !ENV['INTERCOM_USER_HOST_BLACKLIST'].split(",").include?(email.split("@")[1])
      else
        true # continue by default
      end
    end

    def intercom_client
      Intercom::Client.new(app_id: ENV['INTERCOM_APP_ID'], api_key: ENV['INTERCOM_API_KEY'])
    end

end
