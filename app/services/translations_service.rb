class TranslationsService

  def initialize locale, key = nil
    @locale = locale
    @key = key
  end

  def call
    locale_folder = File.join(Rails.root, 'config/locales')
    locale_json = {}

    Dir[File.join(locale_folder, '*.yml')].sort.each do |locale|
      locale_yml = YAML::load(IO.read(locale))
      locale_hash = locale_yml.to_hash[@locale]
      locale_json.merge! locale_hash unless locale_hash.nil?
    end
    result = if @key
      locale_json[key]
    else
      locale_json
    end
    replace_empty_values(result)
  end

  private
    def replace_empty_values hash
      hash.each_with_object({}) do |(key,value),object|
        if value
          object[key] = value.is_a?(Hash) ? replace_empty_values(value) : value
          object
        end
      end
    end

  def self.call locale, key = nil
    service = new(locale, key)
    service.call
  end
end
