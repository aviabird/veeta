module SharingService
  class SharingService < Service
    attr_reader :talent, :companies

    def initialize talent
      @talent = talent
      @companies = []
      list_companies
    end

    # Get list of jobapplications directly
    delegate :resumes, to: :@talent

    # List of companies shared directly via resume
    def resumes_sharings
      resumes.map{ |r| r.sharings.nonrevoked }.flatten
    end

    # Get only nonrevoked jobapps
    def jobapps
      resumes.map{ |r| r.jobapps.nonrevoked }.flatten
    end

    # Revoke access on jobapps and on company
    def revoke company
      revoke_on_jobapps company
      revoke_on_resumes company
    end

    private
      # Go throw jobapps and revoke access if is necessary
      def revoke_on_jobapps company
        jobapps.each do |jobapp|
          jobapp.revoke if jobapp.applied_to == company
        end
      end

      # Go throw resume sharings and revoke access if is necessary
      def revoke_on_resumes company
        resumes_sharings.each do |sharing|
          sharing.revoke if sharing.company == company
        end
      end

      # Browse companies in jobapps and in resume sharings
      def list_companies
        jobapps.each{ |jobapp| add_company(jobapp.job.company, jobapp.resume, jobapp.created_at) if jobapp.job && jobapp.job.company }
        resumes_sharings.each{ |sharing| add_company(sharing.company, sharing.resume, sharing.created_at) if sharing.company }
      end

      # Add new company into companies if doesn't exist, if exist improve it
      def add_company company, resume, applied_at
        new_company = SharedCompany.new company, resume, applied_at
        shared_company = new_company.is_company_in?(@companies)
        if shared_company
          shared_company.resumes << resume
          shared_company.applied_at << applied_at
        else
          @companies << new_company
        end
      end
  end
end
