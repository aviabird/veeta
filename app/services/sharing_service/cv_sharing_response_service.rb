module SharingService
  class CvSharingResponseService < Service

    attr_accessor :cv_sharing_response

    delegate :cv_sharing_request, to: :cv_sharing_response
    delegate :resume, to: :cv_sharing_response
    delegate :company, to: :cv_sharing_request
    delegate :recruiters, to: :company


    def initialize cv_sharing_response, cv_sharing_response_params
      @cv_sharing_response = cv_sharing_response
      @cv_sharing_response_params = cv_sharing_response_params
      @options = {}

    end

    def call
      is_update_only = cv_sharing_response.currently_shared_cv resume.talent
      if cv_sharing_response.draft && cv_sharing_response.update(cv_sharing_response_params)
        if cv_sharing_request.kind_of?(CvSharingRequest)
          share
          link
          if !is_update_only #new sharing
            perform_cv_sharing_response
          else
            update_cv_sharing_response
          end
        end
      end
      super
    end

    def info
      { meta: @options }
    end

    private

      def share
        Resume::Share.call(resume, [company.id])
      end

      def link
        cv_sharing_response.update(resume: resume.latest)
      end

      def perform_cv_sharing_response
        if !company.nil? && company.setting.talentpool_mail_enabled?
          recruiters.each { |r| Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i,MailSharingResponseJob, 'company',cv_sharing_response.id, r.id)  if r.email_notifications_enabled  }
        end
        ### Disabled, as this will be handled via Intercom
        #if ActivityService.new.activities_count(resume.origin.talent) == 1 # first entry in activity
        #  Resque.enqueue(MailFirstSharingResponseJob, resume.talent.locale, cv_sharing_response.id)
        #end
        cv_sharing_response.update_column(:submitted_at, DateTime.now)
        LogService.call "#{CvSharingResponseService.name}.perform_cv_sharing_response", {resource: cv_sharing_response, talent:resume.talent,company: company, resume: resume, details: 'Talent joined talent pool',priority: :medium}
      end

      def update_cv_sharing_response
        @cv_sharing_response.update_columns(update_only: true, submitted_at: DateTime.now)
        LogService.call "#{CvSharingResponseService.name}.update_cv_sharing_response", {resource: cv_sharing_response, talent:resume.talent,company: company, resume: resume, details: 'Talent updated CV in talent pool',priority: :medium}
      end

      def cv_sharing_response_params
        @cv_sharing_response_params.merge(draft: false)
      end
  end

end
