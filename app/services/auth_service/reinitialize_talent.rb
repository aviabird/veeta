module AuthService
  class ReinitializeTalent < Service
    attr_reader :email

    def initialize email, opts={}
      @email = email
      @opts = opts
    end

    def call
      remove if possible?
      super
      return @reinitialized_auth
    end

    private
      def possible?
        exist? && guest?
      end

      def exist?
        @auth ||= TalentAuth.find_by(email: @email.try(:downcase))
      end

      def guest?
        @auth.guest?
      end

      def remove
        #create a copy of auth,
        #assign differnt email (with uid + random)
        #set locked at
        if @auth.present?
          new_id = SecureRandom.uuid
          old_id = @auth.id
          archived_talent_auth = @auth
          archived_talent_auth.locked_at = DateTime.now.utc
          archived_talent_auth.confirmation_token = nil
          archived_talent_auth.email = "archived-#{@auth.id}-#{SecureRandom.hex(12)}-#{archived_talent_auth.email}"
          archived_talent_auth.id = new_id
          archived_talent_auth.save(validate:false)
          #create new
          @auth = TalentAuth.new(@opts.merge!(id: old_id, email: email.try(:downcase), locked_at: nil, reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 0,
          current_sign_in_at: nil,last_sign_in_at: nil,current_sign_in_ip: nil, last_sign_in_ip: nil, confirmation_token: nil, confirmed_at: nil,
          confirmation_sent_at: nil,unconfirmed_email: nil, tokens: {}, updated_at: DateTime.now.utc,authentication_token: Devise.friendly_token,
          authentication_token_created_at: nil, xing_request_token: nil, xing_request_token_secret: nil, xing_access_token: nil, xing_access_token_secret: nil,
          linkedin_request_token: nil,linkedin_request_token_secret: nil,linkedin_access_token: nil,linkedin_access_token_secret: nil, failed_attempts: 0, unlock_token: nil,
          locked_at: nil,reg_type: nil))
          @auth.resume_type = 50
          @auth.is_guest = true
          #@auth.talent = Talent.init @auth.email, {professional_experience_level: 50}
          @auth.save(validate: false)
          @reinitialized_auth = @auth
        end
        #@auth.destroy
      end
  end
end
