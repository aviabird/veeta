class ActivityService < Service


  def sorted_activities current_talent
    combined = activities_combined current_talent
    combined = (combined.compact.sort_by {|obj| obj.try(:connected_at) ? obj.connected_at : obj.created_at}).reverse
    combined
  end

  def activities_count current_talent
    combined = activities_combined current_talent
    combined.size
  end

  private

    def activities_combined current_talent
      jobapps = Jobapp.includes({job:[:recruiter]}, resume: [:origin]).my_jobapps(current_talent)
      sharing_requests =  ResumeSharingRequest.my_sharings(current_talent)
      third_parties = VeetaButtonSharingConnection.initial_sharing_of_connections(current_talent)
      cv_sharing_responses = CvSharingResponse.my_cv_sharing_requests(current_talent)
      combined = (jobapps + sharing_requests + third_parties + cv_sharing_responses)
      combined
    end
end
