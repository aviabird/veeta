TalentSearch.new.search

pp TalentSearch.new.search.response.aggregations

{
    :index => "veeta-talents-development",
    :type => "talent",
    :body =>
        {
            :sort => [
                {
                    "_score" => {:order => :desc}
                }
            ],
            :query => {
                :bool => {
                    :must =>
                        {
                            :multi_match => {
                                :query => "ca",
                                :fields => ["first_name", "last_name"]
                            },
                            :constant_score =>
                                {
                                    :filter => {
                                        :range => {
                                            "salary_exp.max" => {
                                                :to => 10000
                                            }
                                        }
                                    },
                                    :boost => 1.2}
                        }
                }
            },
            :aggs =>
                {
                    :skills => {
                        :terms => {
                            :field => "skills"
                        }
                    },
                 :languages => {
                     :terms => {
                         :field => "languages"
                     }
                 },
                 :working_locations => {
                     :terms => {
                         :field => "working_locations"
                     }},
                 :working_industries => {
                     :terms => {
                         :field => "working_industries"
                     }
                 }
                }
        }
}
