
namespace :deploy do
  desc 'Copy fallback images to public'
   task :copy_fallback_images do
     @source_files = Dir.glob(Rails.root.join('app', 'assets', 'images', 'fallback', '*'))
     @target_dir = Rails.root.join('public', 'assets', 'fallback')

     @source_files.each do |file|
       dir, filename = File.dirname(file), File.basename(file)
       p file
       p dir
       p filename
       p File.join(@target_dir, filename)
       dest = File.join(@target_dir)
       FileUtils.mkdir_p(dest)
       FileUtils.copy_file(file, File.join(dest, filename))
     end
   end
end
