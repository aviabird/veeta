namespace :temp_uploader do

  desc 'clean up temp uploaders'
  @clear_old_temp = Array.new
  @clear_old_temp << 'temp_uploader:remove_old_db'
  @clear_old_temp << 'temp_uploader:remove_old_files'
  task :clear_up => @clear_old_temp

  desc 'delete temp uploaders file older than one week'
  task :remove_old_files do
    "#{Rails.root}/uploads/*"
    Dir.glob("#{Rails.root}/uploads/temp_files").
        select { |f| File.mtime(f) < (Time.now - 1.week) }.
        each { |f| FileUtils.rm(f) }
  end

  desc 'delete temp uploaders row in db older than  one week'
  task :remove_old_db do
    TempFile.where("created_at < :week", {:week => 1.week.ago}).find_in_batches do |f|
      sleep(50) # Make sure it doesn't get too crowded in there!
      f.destroy!
    end
  end
end
