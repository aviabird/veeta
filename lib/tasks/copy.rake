namespace :copy do
  desc "TODO"
  # to run this script use command  rake copy:sharings["original_company_short_name","new_company_short_name"]
  task :sharings, [:original_company_short_name, :new_company_short_name] => :environment do |t, args|
    begin
      Chewy.strategy(:atomic) do
        cp_service = CopyService.new args[:original_company_short_name], args[:new_company_short_name]
        cp_service.company_talent_infos
        cp_service.resume_sharings
      end
    rescue => e
      puts "Rake failed due to #{es}"
    end  
  end
end
