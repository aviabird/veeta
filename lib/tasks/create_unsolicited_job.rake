namespace :create_unsolicited_job do
  desc 'create at least one unsolicited job for company'
  task create_unsolicited_job_for_old_companies: :environment do
    Company.find_each(:batch_size => 100) do |company|
      begin
        if company.jobs.where(:unsolicited_job => true)
          job = Job.new
          job.unsolicited_job = true
          # unsolicited jobs have no name
          job.company = company
          job.status = 'new'
          job.recruiter = company.owner
          job.language = Language.find('de')
          job.accepted_jobs_languages.build(language: Language.find('en'))
          job.accepted_jobs_languages.build(language: Language.find('de'))
          job.save
        end
      rescue => e
        puts  "ERROR: YourModel: #{company.id} -> #{e.to_s}"
      end
    end
  end
end
