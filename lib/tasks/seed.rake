
namespace :seed do
  desc 'Seeding database with new talents, companies and jobs'
  task :t_c_j, [:talents, :companies, :jobs] => [:environment] do |t, args|
    Resque.enqueue_in(60, SeedDataJob,  args[:talents], args[:companies], args[:jobs])

  end
end
