class AdminSubdomain
  def self.matches?(request)
    (request.subdomain.present? && request.subdomain.start_with?('admin'))
  end
end
