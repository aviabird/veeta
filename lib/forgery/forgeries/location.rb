class Forgery::Location < Forgery
  def self.city(locale='en')
    dictionaries["cities_#{locale}".to_s].random.unextend
  end
end
