class WorkExperienceCalculator
  attr_reader :work_experiences

  def initialize work_experiences
    @work_experiences = work_experiences
  end

  # Calculate difference between two dates, split it in half if it's only part-time job
  def difference from, to, part = false
    if part
      (to - from) / 2
    else
      to - from
    end
  end

  # Generate proper working timeline without overlaps and with higher priority for full-time jobs
  def working_timeline
    timeline = @work_experiences.sort_by { |k| k[:from].nil? ? DateTime.now : k[:from] }
    timeline.each_with_index do |item, index|
      if timeline[index + 1] && item[:to] > (timeline[index+1][:from] || DateTime.now)
        if item[:part]
          item[:to] = timeline[index+1][:from]
        else
          timeline[index+1][:from] = item[:to]
        end
      end
    end
  end

  class << self
    # Generate working duration based on based experiences in seconds
    #
    # Example:
    # we = []
    # we << { from: 6.years.ago, to: 5.years.ago, part: false }
    # we << { from: 9.years.ago, to: 7.years.ago, part: true }
    # we << { from: 10.years.ago, to: 8.years.ago, part: false }
    #
    # working_duration(we) #=> 110376000.001811

    def working_duration work_experiences, opts = {}
      result = 0
      calculator = new(work_experiences)
      calculator.working_timeline.each do |experience|
        unless experience[:from].nil?
          result += calculator.difference(experience[:from], experience[:to], experience[:part])
        end
      end

      case opts[:in]
      when :hours
        result / 3600
      when :days
        result / 3600 / 24
      when :years
        result / 3600 / 24 / 365
      else
        result
      end
    end

    # Generate working duration per country
    def international_duration work_experiences, opts = {}
      result = []
      calculator = new(work_experiences)
      calculator.work_experiences.each do |experience|
        find = result.detect {|k| k[:iso_code] == experience[:iso_code] }
        if find
          find[:duration] += calculator.difference(experience[:from], experience[:to], experience[:part])
        else
          result << {
            iso_code: experience[:iso_code],
            duration: calculator.difference(experience[:from], experience[:to], experience[:part])
          }
        end
      end

      opts[:round] = 0 unless opts[:round]

      case opts[:in]
      when :hours
        result.collect do |i|
          i[:duration] = (i[:duration] / 3600).round(opts[:round])
          i
        end
      when :days
        result.collect do |i|
          i[:duration] = (i[:duration] / 3600 / 24).round(opts[:round])
          i
        end
      when :years
        result.collect do |i|
          i[:duration] = (i[:duration] / 3600 / 24 / 365).round(opts[:round])
          i
        end
      else
        result
      end
    end
  end
end
