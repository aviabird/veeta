require "rails_helper"

RSpec.describe JobService::GenericJobReadingService  do
  describe "Service Call" do
    before(:each) do

    end
    context "jobs of not integrated companies" do
      let(:job) {FactoryGirl.create(:job, :unscoped_ext_id)}
      it "should return the job" do
        result = JobService::GenericJobReadingService.call job.company.short_name+'-123'
        expect(result).to eq(job)
      end
      it "should return nil if job not found" do
        result = JobService::GenericJobReadingService.call job.company.short_name+'-12x'
        expect(result).to be(nil)
      end
    end

    context "jobs of integrated companies" do
      let(:job) {FactoryGirl.create(:job, :unscoped_ext_id)}
      let(:ats_integration) { FactoryGirl.create(:ats_integration, :iss_erecruiter) }
      let(:setting) {FactoryGirl.create(:company_setting)}
      before(:each) do
        job.company.setting = FactoryGirl.create(:company_setting)
        job.company.setting.job_fetching_integration = ats_integration
        job.company.setting.save!

        setting.company = setting.job_fetching_integration.company
        setting.save!
        #company.setting.job_fetching_integration = ats_integration
        #company.save!
      end

      it "should return the already existing job" do

        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_job = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/Job/123").to_return(body: '{"Id": "123", "Title": "API Tester", "Culture": "en", "AvailableCultures": ["en", "de"], "PublishedOn": ["karriere.at"]}')

        result = JobService::GenericJobReadingService.call job.company.short_name+'-123'

        expect(result.name).to eq("API Tester")
        expect(result.status).to eq("new")
        expect(result.language.iso_code).to eq("en")
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_job).to have_been_requested
      end

      it "should return a newly created job" do
        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_job = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/Job/12x").to_return(body: '{"Id": "12x", "Title": "API Tester", "Culture": "de", "PublishedOn": ["karriere.at"]}')

        result = JobService::GenericJobReadingService.call setting.company.short_name+'-12x'
        expect(result.name).to eq("API Tester")
        expect(result.status).to eq("new")
        expect(result.language.iso_code).to eq("de")
        expect(result.ext_id).to eq(setting.company.short_name+'-12x')
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_job).to have_been_requested
      end
      it "should disable the job and return nil" do
        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_job = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/Job/123").to_return(body: '{"Id": "123", "Title": "API Tester", "Culture": "en", "AvailableCultures": ["en", "de"], "PublishedOn": []}')

        result = JobService::GenericJobReadingService.call job.company.short_name+'-123'
        expect(result.name).to eq(job.name)
        expect(result.status).to eq("closed")
        expect(result.ext_id).to eq(job.company.short_name+'-123')
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_job).to have_been_requested
      end
      it "should return a newly created job with job additions" do
        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_job = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/Job/12x").to_return(body: '{"Id": "12x", "Title": "API Tester", "Culture": "de", "PublishedOn": ["karriere.at"], "Location": "Wien", "UpdateDate": "2016-06-29T12:27:01.073"}')

        result = JobService::GenericJobReadingService.call setting.company.short_name+'-12x'
        expect(result.name).to eq("API Tester")
        expect(result.status).to eq("new")
        expect(result.jobname_addition).to include("Wien", "29.06")
        expect(result.language.iso_code).to eq("de")
        expect(result.ext_id).to eq(setting.company.short_name+'-12x')
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_job).to have_been_requested
      end

      it "should return the already existing job with job additions" do

        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_job = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/Job/123").to_return(body: '{"Id": "123", "Title": "API Tester", "Culture": "en", "AvailableCultures": ["en", "de"], "PublishedOn": ["karriere.at"], "Location": "Wien", "SubTitle": "Firing API calls", "UpdateDate": "2016-06-29T12:27:01.073"}')

        result = JobService::GenericJobReadingService.call job.company.short_name+'-123'
        expect(result.name).to eq("API Tester")
        expect(result.status).to eq("new")
        expect(result.jobname_addition).to include("Wien", "Firing API calls", "29.06")
        expect(result.language.iso_code).to eq("en")
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_job).to have_been_requested
      end

    end
  end

  describe "Service Call (List)" do
    before(:each) do

    end
    context "jobs of not integrated companies" do
      let(:company) {FactoryGirl.create(:company, :with_jobs)}
      it "must do nothing" do
        JobService::GenericJobReadingService.call_list company
        expect(company.jobs.length).to be(2)
        expect(company.jobs.first.status).to match('new')
      end
    end

    context "jobs of integrated companies" do
      let(:company) {FactoryGirl.create(:company, :with_jobs)}
      let(:ats_integration) { FactoryGirl.create(:ats_integration, :iss_erecruiter,company: company) }
      let(:setting) {FactoryGirl.create(:company_setting, company: company, job_fetching_integration: ats_integration)}
      before(:each) do
        ats_integration.company =  company
        ats_integration.save!
        company.setting = setting
        company.setting.job_fetching_integration = ats_integration
        company.setting.job_fetching_integration.company = company
        company.setting.job_fetching_integration.save!
        company.setting.save!
        company.setting.job_fetching_integration.reload
        #company.setting.job_fetching_integration = ats_integration
        #company.save!
      end

      it "must fetch all jobs, update them and disable existing jobs" do
        stub_check_applicant = stub_request(:post, "#{ats_integration.integration_endpoint}/Api").to_return(body: '{"Token": "thetoken", "code": "200"}')
        stub_get_jobs = stub_request(:get, "#{ats_integration.integration_endpoint}/Api/PublishedJobs").to_return(status: 200, body: '{"Jobs":[{"Id": "012", "Title": "API Tester", "Culture": "en", "AvailableCultures": ["en", "de"]}, {"Id": "' + company.jobs.active.first.unscoped_ext_id + '", "Title": "Overwriter", "Culture": "de", "AvailableCultures": ["en", "de"]}]}')
        JobService::GenericJobReadingService.call_list company
        company.reload
        expect(company.jobs.length).to be(4)
        expect(company.jobs.active.length).to be(2)
        expect(Job.exists?(name: 'Overwriter')).to be(true)
        expect(stub_check_applicant).to have_been_requested
        expect(stub_get_jobs).to have_been_requested
      end
    end
  end
end
