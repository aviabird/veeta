require "rails_helper"

describe IntercomService do
  before(:each) do
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    @resume = FactoryGirl.create(:resume)
    @resume.update_attribute :origin_id, @resume.id
    @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
    @job = FactoryGirl.build(:job, company_id: Company.first.id, recruiter_id: Company.first.owner.id)
    @job.save(validate: false)
    @accepted_jobs_language = @job.accepted_jobs_languages.create(language_id: Language.all.first.id)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, resume_id: @resume.id, source: "other_source", job_type: "Job")
    @jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @jobapp.id, source: "other_source", job_type: "Job")
    @current_talent =  @resume.talent
    @log_entry = FactoryGirl.create(:log)
    @service =  IntercomService.new(@log_entry)
  end

  it "should trackable" do
    @log_entry.update_attribute :key, "api.talent.auth.sessions.create"
    expect(@service.send(:trackable?, @log_entry)).to be true
  end

  it "should not trackable" do
    expect(@service.send(:trackable?, @log_entry)).to be false
  end

  it "should createable" do
    @log_entry.update_attribute :key, "api.talent.auth.registrationscontroller.create.asd.success"
    expect(@service.send(:createable?, @log_entry)).to be true
  end

  it "should not createable" do
    expect(@service.send(:createable?, @log_entry)).to be false
  end

  it "should updateable" do
    @log_entry.update_attribute :key, "api.talent.talents.update"
    expect(@service.send(:updateable?, @log_entry)).to be true
  end

  it "should not updateable" do
    expect(@service.send(:updateable?, @log_entry)).to be false
  end

  it "should lookup key" do
    @log_entry.update_attribute :key, "api.talent.resumes.new"
    expect(@service.send(:lookup_key, @log_entry)).to eq "intapi-cv-new"
  end

  it "should skip_intercom? return false when user host blacklist" do
    expect(@service.send(:skip_intercom?, @log_entry)).to be false
  end

  it "should skip_intercom? return false when user has email" do
    expect(@service.send(:skip_intercom?, @log_entry)).to be false
  end

  it "should skip_intercom return false" do
    expect(@service.send(:skip_intercom?, @log_entry)).to be false
  end

  it "should validate email address when user host blacklist" do
    email = "test@yopmail.com"
    expect(@service.send(:valid_intercom_email_address?, email)).to be true
  end

  it "should validate email address" do
    email = "test@yopmail.com"
    expect(@service.send(:valid_intercom_email_address?, email)).to be true
  end

  it "should validate email" do
    email = "test@yopmail.com"
    expect(@service.send(:valid_intercom_email_address?, email)).to be true
  end

  it "should return intercom client" do
    email = "test@yopmail.com"
    expect(@service.send(:intercom_client).is_a? Intercom::Client).to be true
  end

  it "should call with trackable" do
    allow_any_instance_of(IntercomService).to receive(:trackable?).and_return(true)
    expect(@service.call).to be nil
  end

  it "should call createable" do
    allow_any_instance_of(IntercomService).to receive(:createable?).and_return(true)
    expect(@service.call).to be nil
  end

  it "should call updateable" do
    allow_any_instance_of(IntercomService).to receive(:updateable?).and_return(true)
    expect(@service.call).to be true
  end

  context "talent create" do
    it "must return the correct event name for user reg" do
      @log_entry.key = 'api.talent.auth.registrationscontroller.create.user_reg.success'
      event_name = @service.send(:lookup_create_key, @log_entry)
      expect(event_name).to match('intapi-sign-up-user-reg')
    end

    it "must return the correct event name for talent pool" do
      @log_entry.key = 'api.talent.auth.registrationscontroller.create.pool_link.linkedin.success'
      event_name = @service.send(:lookup_create_key, @log_entry)
      expect(event_name).to match('intapi-sign-up-pool-link')
    end

    it "must return the correct event name for job" do
      @log_entry.key = 'api.talent.auth.registrationscontroller.create.job_link.file.success'
      event_name = @service.send(:lookup_create_key, @log_entry)
      expect(event_name).to match('intapi-sign-up-job-link')
    end

    it "must not return a value for failed user reg" do
      @log_entry.key = 'api.talent.auth.registrationscontroller.create.create.user_reg.failed'
      event_name = @service.send(:lookup_create_key, @log_entry)
      expect(event_name).to be_nil
    end

  end

end
