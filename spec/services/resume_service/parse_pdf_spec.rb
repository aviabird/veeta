require "rails_helper"

describe ResumeService::ParsePDF do

  let(:path) { Rails.root.join('test', 'data', 'resume', 'cv.pdf') }
  let(:service) { ResumeService::ParsePDF.new(path) }
  let(:response_textkernel) { File.read(Rails.root.join('test', 'data', 'resume', 'response_textkernel.json')) }
  let(:cv_xml) { File.read(Rails.root.join('test', 'data', 'resume', 'cv_textkernel.xml')) }
  let(:lebenslauf_xml) { File.read(Rails.root.join('test', 'data', 'resume', 'lebenslauf_textkernel.xml')) }

  it "should create client" do
    expect(service.send(:client).class).to be Savon::Client
  end

  it "should parse into hash" do
    2.times do |i|
      if i == 1   
        allow_any_instance_of(ResumeService::ParsePDF).to receive(:xml).and_return(cv_xml)
      else
        allow_any_instance_of(ResumeService::ParsePDF).to receive(:xml).and_return(lebenslauf_xml)
      end
      expect(service.hashed[:profile].present?).to be true
      expect(service.hashed[:profile][:personal].present?).to be true
      expect(service.hashed[:profile][:personal].has_key?(:title)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:first_name)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:last_name)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:date_of_birth)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:nationality_code)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:gender_code)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:marital_status_code)).to eq true

      expect(service.hashed[:profile][:personal][:address].present?).to be true
      expect(service.hashed[:profile][:personal][:address].has_key?(:street_name)).to eq true
      expect(service.hashed[:profile][:personal][:address].has_key?(:street_number_base)).to eq true
      expect(service.hashed[:profile][:personal][:address].has_key?(:postal_code)).to eq true
      expect(service.hashed[:profile][:personal][:address].has_key?(:city)).to eq true
      expect(service.hashed[:profile][:personal][:address].has_key?(:country_code)).to eq true
      expect(service.hashed[:profile][:personal].has_key?(:mobile_phone)).to be true
      expect(service.hashed[:profile][:personal][:mobile_phones].present?).to be true
      expect(service.hashed[:profile][:personal][:mobile_phones][:mobile_phone].is_a? Array).to be true
      expect(service.hashed[:profile][:personal].has_key?(:home_phone)).to be true
      expect(service.hashed[:profile][:personal][:home_phones].present?).to be true
      expect(service.hashed[:profile][:personal][:home_phones][:home_phone].present?).to be true
      expect(service.hashed[:profile][:personal][:home_phones][:home_phone].is_a? Array).to be true
      expect(service.hashed[:profile][:personal].has_key?(:email)).to be true
      expect(service.hashed[:profile][:personal][:emails].present?).to be true
      expect(service.hashed[:profile][:personal][:emails][:email].present?).to be true
      expect(service.hashed[:profile][:personal][:emails][:email].is_a? Array).to be true
      expect(service.hashed[:profile][:education_history].present?).to be true
      expect(service.hashed[:profile][:education_history][:education_item].present?).to be true
      expect(service.hashed[:profile][:education_history][:education_item].is_a? Array).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:degree_direction)).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:degree_direction)).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:degree_direction)).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:start_date)).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:end_date)).to be true
      expect(service.hashed[:profile][:education_history][:education_item].first.has_key?(:institute_name)).to be true
      expect(service.hashed[:profile][:employment_history].present?).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].present?).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].present?).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].is_a? Array).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:job_title)).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:start_date)).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:end_date)).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:employer_name)).to be true
      expect(service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:description)).to be true
      expect(service.hashed[:profile][:skills].present?).to be true
      expect(service.hashed[:profile][:skills][:computer_skills][:computer_skill].present?).to be true
      expect(service.hashed[:profile][:skills][:computer_skills][:computer_skill].present?).to be true
      expect(service.hashed[:profile][:skills][:computer_skills][:computer_skill].is_a? Array).to be true
      expect(service.hashed[:profile][:skills][:computer_skills][:computer_skill].first.present?).to be true
      expect(service.hashed[:profile][:skills][:computer_skills][:computer_skill].first.has_key?(:computer_skill_name)).to be true
      expect(service.hashed[:profile][:skills][:language_skills].present?).to be true
      expect(service.hashed[:profile][:skills][:language_skills][:language_skill].present?).to be true
      expect(service.hashed[:profile][:skills][:language_skills][:language_skill].is_a? Array).to be true
      expect(service.hashed[:profile][:skills][:language_skills][:language_skill].first.present?).to be true
      expect(service.hashed[:profile][:skills][:language_skills][:language_skill].first.has_key?(:language_skill_code)).to be true
      expect(service.hashed[:profile][:skills][:language_skills][:language_skill].first.has_key?(:language_proficiency_code)).to be true
      expect(service.hashed[:profile][:skills][:soft_skills].present?).to be true
      expect(service.hashed[:profile][:skills][:soft_skills][:soft_skill].present?).to be true
      expect(service.hashed[:profile][:skills][:soft_skills][:soft_skill].is_a? Array).to be true
      expect(service.hashed[:profile][:skills][:soft_skills][:soft_skill].first.present?).to be true
      expect(service.hashed[:profile][:skills][:soft_skills][:soft_skill].first.has_key?(:soft_skill_name)).to be true
      expect(service.hashed[:profile][:custom_area].present?).to be true
      expect(service.hashed[:profile][:custom_area][:profile_picture].present?).to be true
      expect(service.hashed[:profile][:custom_area][:profile_picture].has_key?(:base64_content)).to be true
    end
  end
end