require "rails_helper"

describe ExternalLinkService do
  before(:each) do
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    @resume = FactoryGirl.create(:resume)
    @resume.update_attribute :origin_id, @resume.id
    @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
    @job = FactoryGirl.build(:job, company_id: Company.first.id, recruiter_id: Company.first.owner.id)
    @job.save(validate: false)
    @accepted_jobs_language = @job.accepted_jobs_languages.create(language_id: Language.all.first.id)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, resume_id: @resume.id, source: "other_source", job_type: "Job")
    @jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @jobapp.id, source: "other_source", job_type: "Job")
    @current_talent =  @resume.talent
    @service =  ExternalLinkService.new
  end

  it "should map resume" do
    document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
    expect(@service.resume_mapping(@resume, @resume.companies.first).is_a? Array).to be true
  end

  it "should not map resume" do
    expect(@service.resume_mapping(@resume, @resume.companies.first).blank?).to be true
  end

  it "should map talent" do
    expect(@service.talent_mapping(@resume.talent, @resume.companies.first)).to be true
  end

  it "should check for none mapping existence" do
    expect(@service.mapping_exists(@resume, @resume.companies.first)).to be false
  end

  it "should check for mapping existence" do
    @service.talent_mapping(@resume.talent, @resume.companies.first)
    expect(@service.mapping_exists(@resume.talent, @resume.companies.first)).to be true
  end

  it "should map document" do
    document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
    expect(@service.document_mapping(document, @resume)).to be true
  end

  it "should remove talent mapping" do
    @service.talent_mapping(@resume.talent, @resume.companies.first)
    expect(@service.remove_talent_mapping(@resume.talent, @resume.companies.first)).to be true
  end

  it "should remove mapping" do
    document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
    @service.resume_mapping(@resume, @resume.companies.first)
    expect(@service.remove_mapping(@resume, @resume.companies.first)).to be true
  end

  # it "should update mapping" do
  #   expect(@service.update_mappings @resume.talent.unique_part, @resume.talent.access_security_part).to be true
  # end

  it "should kickresume mapping" do
    expect(@service.kickresume_mapping @current_talent).to be true
  end

  it "should remove kickresume mapping" do
    @service.kickresume_mapping @current_talent
    expect(@service.remove_kickresume_mapping @current_talent).to be true
  end

  it "should not remove kickresume mapping" do
    expect(@service.remove_kickresume_mapping @current_talent).to be false
  end

  it "should perform mapping" do
    expect(@service.send(:mapping, @resume, @resume.companies.first)).to be true
  end

end