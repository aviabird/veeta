require "rails_helper"

describe ApplyService::Apply do
  before(:each) do
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    @resume = FactoryGirl.create(:resume)
    @resume.update_attribute :origin_id, @resume.id
    @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
    @job = FactoryGirl.build(:job, company_id: Company.first.id, recruiter_id: Company.first.owner.id)
    @job.save(validate: false)
    @accepted_jobs_language = @job.accepted_jobs_languages.create(language_id: Language.all.first.id)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, resume_id: @resume.id, source: "other_source", job_type: "Job")
    @jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @jobapp.id, source: "other_source", job_type: "Job")
    @current_talent =  @resume.talent 
    @service =  ApplyService::Apply.new(@jobapp, @jobapp_params) 
  end

  it "should have empty info" do
    expect(@service.info).to include({meta: {}}) 
  end

  it "should perform job application on job" do
    expect(@service.send(:perform_job_application)).to be true
  end

  it "should perform email job application" do
    expect(@service.send(:perform_email_job_application)).to be true
  end

  it "should share" do
    expect(@service.send(:share).present?).to be true
  end

  it "should share email app" do
    expect(@service.send(:share_email_app).present?).to be true
  end

  it "should update link" do
    expect(@service.send(:link)).to be true
  end

  it "should and_return job params" do
    expect(@service.send(:jobapp_params)).to include(:draft)
  end

  it "should call with job type job" do
    jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @job.id, source: "other_source", job_type: "Job")
    service =  ApplyService::Apply.new(@jobapp, jobapp_params)     
    expect(service.call.present?).to be true
  end

  # it "should call with job type email application" do
  #   email_application = FactoryGirl.create(:email_application, company_id: Company.first.id, recruiter_id: Company.first.owner.id )
  #   jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: email_application.id, source: "other_source", job_type: "EmailApplication")
  #   service =  ApplyService::Apply.new(@jobapp, jobapp_params)     
  #   expect(service.call.present?).to be true
  # end
end