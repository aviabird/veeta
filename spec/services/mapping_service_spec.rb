require "rails_helper"

describe LinkedinClientService do
  before(:each) do
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    @resume = FactoryGirl.create(:resume)
    @resume.update_attribute :origin_id, @resume.id
    @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
    @job = FactoryGirl.build(:job, company_id: Company.first.id, recruiter_id: Company.first.owner.id)
    @job.save(validate: false)
    @accepted_jobs_language = @job.accepted_jobs_languages.create(language_id: Language.all.first.id)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, resume_id: @resume.id, source: "other_source", job_type: "Job")
    @jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @jobapp.id, source: "other_source", job_type: "Job")
    @current_talent =  @resume.talent 
    @service =  MappingService.new
  end

  it "should return ve json" do
    expect(@service.send(:to_ve_json)["Common"]["Industry"]["fromLinkedIn"].present?).to be true
  end

  it "should return ng json" do
    expect(@service.send(:to_ng_json)["Common"]["Industry"]["fromVeeta"].present?).to be true
  end

  it "should return veeta industry" do
    to_ve_json = @service.send(:to_ve_json)
    veeta_industry = @service.veeta_industry "fromXing", "01xxxx"
    expect(veeta_industry).to eq to_ve_json['Common']['Industry']["fromXing"]["01xxxx"]
  end

  it "should veeta_industry return nil" do
    veeta_industry = @service.veeta_industry "fromXing", :test_user
    expect(veeta_industry).to be nil
  end

  it "should return veeta_academic_title_level" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromTextkernel"
    key = "3"
    veeta_academic_title_level = @service.veeta_academic_title_level from, key
    expect(veeta_academic_title_level).to eq to_ve_json['Talent']['Education']['HighestEducation_to_AcademicTitle'][from][key]
  end

  it "should veeta_academic_title_level return nil" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromTextkernel"
    key = "1"
    expect(@service.veeta_academic_title_level from, key).to be nil 
  end

  it "should return veeta_education_level" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromTextkernel"
    key = "1"
    expect(@service.veeta_education_level from, key).to eq to_ve_json['Talent']['Education']['EducationLevel'][from][key]

  end

  it "veeta_education_level should return nil" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromTextkernel"
    key = "10"
    expect(@service.veeta_education_level from, key).to be nil
  end

  it "should return veeta_language_level" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromTextkernel"
    key = "2"
    expect(@service.veeta_language_level from, key).to eq to_ve_json['Talent']['LanguageSkill'][from][key]
  end

  it "veeta_language_level should return nil" do
    from = "test"
    key = "key"
    expect(@service.veeta_language_level from, key).to be nil
  end

  it "should return veeta_career_level" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromXing"
    key = "ENTRY_LEVEL"
    expect(@service.veeta_career_level from, key).to eq to_ve_json['Talent']['WorkExperience']['CareerLevel'][from][key]
  end

  it "veeta_career_level should return nil" do
    from = "test"
    key = "key"
    expect(@service.veeta_career_level from, key).to be nil
  end

  it "should return veeta_employment_type" do
    to_ve_json = @service.send(:to_ve_json)
    from = "fromXing"
    key = "FREELANCER"
    expect(@service.veeta_employment_type from, key).to eq to_ve_json['Talent']['WorkExperience']['EmploymentType'][from][key]
  end

  it "veeta_employment_type should return nil" do
    from = "test"
    key = "key"
    expect(@service.veeta_employment_type from, key).to be nil
  end

  it "should return xing_language_level" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromVeeta"
    key = "1"
    expect(@service.xing_language_level from, key).to eq to_ng_json['Talent']['LanguageSkill'][from][key]
  end

  it "xing_language_level should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_language_level from, key).to be nil
  end

  it "should return xing_industry" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromVeeta"
    key = "AgriFish"
    expect(@service.xing_industry from, key).to eq to_ng_json['Common']['Industry'][from][key]
  end

  it "xing_industry should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_industry from, key).to be nil
  end

  it "should return xing_legacy_industry" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromVeeta"
    key = "AgriFish"
    expect(@service.xing_legacy_industry from, key).to eq to_ng_json['Common']['Industry'][from]['XingLegacy'][key]
  end

  it "xing_legacy_industry should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_legacy_industry from, key).to be nil
  end

  it "should return xing_industry_name" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromXing"
    key = "010000"
    expect(@service.xing_industry_name from, key, "de").to eq to_ng_json['Common']['Industry'][from]['PlainText']["de"][key]
  end

  it "xing_industry_name should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_industry_name from, key, "de").to be nil
  end

  it "should return xing_career_level" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromVeeta"
    key = "executive"
    expect(@service.xing_career_level from, key).to eq to_ng_json['Talent']['WorkExperience']['CareerLevel'][from][key]
  end

  it "xing_career_level should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_career_level from, key).to be nil
  end

  it "should return xing_employment_type" do
    to_ng_json = @service.send(:to_ng_json)
    from = "fromVeeta"
    key = "full_time"
    expect(@service.xing_employment_type from, key).to eq to_ng_json['Talent']['WorkExperience']['EmploymentType'][from][key]
  end

  it "xing_employment_type should return nil" do
    from = "test"
    key = "key"
    expect(@service.xing_employment_type from, key).to be nil
  end
end