require "rails_helper"

describe ActivityService do
  before(:each) do
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    @resume = FactoryGirl.create(:resume)
    @resume.update_attribute :origin_id, @resume.id
    @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
    @job = FactoryGirl.build(:job, company_id: Company.first.id, recruiter_id: Company.first.owner.id)
    @job.save(validate: false)
    @accepted_jobs_language = @job.accepted_jobs_languages.create(language_id: Language.all.first.id)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, resume_id: @resume.id, source: "other_source", job_type: "Job")
    @jobapp_params =  FactoryGirl.attributes_for(:jobapp).merge(job_id: @jobapp.id, source: "other_source", job_type: "Job")
    @current_talent =  @resume.talent
    @service =  ActivityService.new
  end

  it "should sort activities" do
    expect(@service.sorted_activities(@current_talent).is_a? Array).to be true
  end

  it "should count activities" do
    expect(@service.activities_count(@current_talent).is_a? Integer).to be true
  end

  it "should activities_combined" do
    expect(@service.send(:activities_combined, @current_talent).is_a? Array).to be true
  end

end