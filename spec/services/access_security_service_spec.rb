require "rails_helper"

describe AccessSecurityService do
  it "should stabhlish a sharing" do
    allow_any_instance_of(OauthToken).to receive(:generate_keys).and_return(true)
    allow_any_instance_of(ClientApplication).to receive(:generate_keys).and_return(true)
    @talent = FactoryGirl.create(:talent)
    @client_application = FactoryGirl.create(:client_application)
    @oauth_token  = FactoryGirl.create(:oauth_token, client_application_id: @client_application.id)
    vbsc = FactoryGirl.create(:veeta_button_sharing_connection, talent_id: @talent.id, client_application_id: @client_application.id, oauth_token_id: @oauth_token.id)
    vbs = FactoryGirl.create(:veeta_button_sharing,veeta_button_sharing_connection_id: vbsc.id)
    AccessSecurityService.new.establish_sharing vbsc, @talent
  end
end