require "rails_helper"

RSpec.describe KickresumeSerializer, :type => :serializer do
  describe 'Serialization of resume with certifications' do
    before(:each) do
      # Create an instance of the model
      @resume = FactoryGirl.create(:resume)

      # Create a serializer instance
      @serializer = KickresumeSerializer.new(@resume)

      @serialization = @serializer.serializable_hash
      # Create a serialization based on the configured adapter
      #@serialization = ActiveModel::Serializer::Adapter.create(@serializer)
    end
    it 'should have certifications serialized' do
      certs = @serialization[:cv][:sections].select{ |a| a[:type] == "publication" }.first
      expect(certs[:data][:entries].size).to be(@resume.certifications.size)
      expect(certs[:icon]).to match('ion-ribbon-b')
      expect(certs[:data][:entries].first[:name]).to be_truthy
      expect(certs[:data][:entries].first[:date][1]).to be_truthy
      expect(certs[:data][:entries].first[:date][1]).to be > (certs[:data][:entries].last[:date][1])
    end
  end

  describe 'Serialization of resume without certifications' do
    before(:each) do
      # Create an instance of the model
      @resume = FactoryGirl.create(:resume)
      @resume.certifications.destroy_all

      # Create a serializer instance
      @serializer = KickresumeSerializer.new(@resume)

      @serialization = @serializer.serializable_hash
      # Create a serialization based on the configured adapter
      #@serialization = ActiveModel::Serializer::Adapter.create(@serializer)
    end
    it 'should have no certifications serialized' do
      certs = @serialization[:cv][:sections].select{ |a| a[:type] == "publication" }
      expect(certs.size).to be(0)
    end
  end
end
