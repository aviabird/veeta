require "rails_helper"

describe MailWithdrawJobappJob do
  it "should return queue" do
    expect(MailWithdrawJobappJob.queue).to be :mailer
  end

  it "should perform job with email application" do
    talent_auth =  FactoryGirl.create(:talent_auth)
    resume = FactoryGirl.create(:resume, talent_id: talent_auth.talent.id)
    company = FactoryGirl.create(:company)
    job = FactoryGirl.build(:job, company_id: company.id, recruiter_id: company.owner.id)
    job.save(validate: false)
    email_application = FactoryGirl.create(:email_application, id: job.id)
    jobapp = FactoryGirl.create(:jobapp, job_id: job.id, source: "other_source", job_type: "EmailApplication", resume_id: talent_auth.talent.reload.resumes.first.id)
    expect((MailWithdrawJobappJob.perform "en", jobapp.id).is_a? LogService).to be true
  end

  it "should perform job with type job" do
    talent_auth =  FactoryGirl.create(:talent_auth)
    resume = FactoryGirl.create(:resume, talent_id: talent_auth.talent.id)
    company = FactoryGirl.create(:company)
    job = FactoryGirl.build(:job, company_id: company.id, recruiter_id: company.owner.id)
    job.save(validate: false)
    jobapp = FactoryGirl.create(:jobapp, job_id: job.id, source: "other_source", job_type: "Job", resume_id: talent_auth.talent.reload.resumes.first.id)
    expect((MailWithdrawJobappJob.perform "en", jobapp.id).is_a? LogService).to be true
  end

  it "should perform not job" do
    talent_auth =  FactoryGirl.create(:talent_auth)
    resume = FactoryGirl.create(:resume, talent_id: talent_auth.talent.id)
    company = FactoryGirl.create(:company)
    job = FactoryGirl.build(:job, company_id: company.id, recruiter_id: company.owner.id)
    job.save(validate: false)
    jobapp = FactoryGirl.create(:jobapp, job_id: job.id, source: "other_source", job_type: "", resume_id: talent_auth.talent.reload.resumes.first.id)
    expect((MailWithdrawJobappJob.perform "en", jobapp.id).is_a? LogService).to be true
  end
end
