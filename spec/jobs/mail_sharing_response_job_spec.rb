require "rails_helper"

describe MailSharingResponseJob do
  it "should return queue" do
    expect(MailSharingResponseJob.queue).to be :mailer
  end

  it "should perform job with email application" do
    cv_sharing_response = FactoryGirl.create(:cv_sharing_response)
    expect(MailSharingResponseJob.perform 'company', cv_sharing_response.id).to be true
  end

  it "should perform job with pdf" do
    cv_sharing_response = FactoryGirl.create(:cv_sharing_response)
    resume = cv_sharing_response.resume
    resume.update_attribute :pdf, Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))
    allow_any_instance_of(Resume).to receive(:file_exists?).and_return(true)
    expect(MailSharingResponseJob.perform 'company', cv_sharing_response.id, Recruiter.last.id).to be nil
  end

  it "should perform job with pdf with maximum attempts" do
    cv_sharing_response = FactoryGirl.create(:cv_sharing_response)
    resume = cv_sharing_response.resume
    resume.update_attribute :pdf, Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))
    allow_any_instance_of(Resume).to receive(:file_exists?).and_return(true)
    MailShareResumeJob.class_variable_set :@@ATTEMPTS, 4
    expect(MailSharingResponseJob.perform 'company', cv_sharing_response.id).to be true
  end
end
