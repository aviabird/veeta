require "rails_helper"

describe MailRevokedAccessJob do
	it "should return queue" do
		expect(MailRevokedAccessJob.queue).to be :mailer
	end

	it "should perform job with email application" do
		resume = FactoryGirl.create(:resume)
		talent = resume.talent
		resume.update_attribute :origin_id, resume.id
		email_application = FactoryGirl.create(:email_application)
		resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
		expect((MailRevokedAccessJob.perform "en", "emailcompany", email_application.id, talent.id, "Test", resume_sharing.id).is_a? LogService).to be true
	end

	it "should perform job" do
		resume = FactoryGirl.create(:resume)
		talent = resume.talent
		resume.update_attribute :origin_id, resume.id
		email_application = FactoryGirl.create(:email_application)
		resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
		company = Company.find resume_sharing.company_id
		expect((MailRevokedAccessJob.perform "en", "company", company.id, talent.id, "Test", resume_sharing.id).is_a? LogService).to be true
	end

	it "should perform job with recruiters" do
		resume = FactoryGirl.create(:resume)
		talent = resume.talent
		resume.update_attribute :origin_id, resume.id
		email_application = FactoryGirl.create(:email_application)
		resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
		company = Company.find resume_sharing.company_id
		recruiter =  Company.find(company.id).owner
    recruiter.update_attribute :company_id, company.id
		expect((MailRevokedAccessJob.perform "en", "company", company.id, talent.id, "Test", resume_sharing.id).is_a? LogService).to be true
	end
end

