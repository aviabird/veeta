require "rails_helper"

describe MailShareResumeJob do
	it "should return queue" do
		expect(MailShareResumeJob.queue).to be :mailer
	end

	it "should perform job" do
		resume = FactoryGirl.create(:resume)
		resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
		expect(MailShareResumeJob.perform "en", resume.id, "test", "test@yopmail.com", resume_sharing_request.id).to be true
	end

  it "should perform job with pdf" do
    resume = FactoryGirl.create(:resume, pdf: Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf')))
    resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
    allow_any_instance_of(Resume).to receive(:file_exists?).and_return(true)
    expect(MailShareResumeJob.perform "en", resume.id, "test", "test@yopmail.com", resume_sharing_request.id).to be nil
  end

  it "should perform job with pdf with maximum attempts" do
    resume = FactoryGirl.create(:resume, pdf: Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf')))
    resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
    allow_any_instance_of(Resume).to receive(:file_exists?).and_return(true)
    MailShareResumeJob.class_variable_set :@@ATTEMPTS, 4
    expect(MailShareResumeJob.perform "en", resume.id, "test", "test@yopmail.com", resume_sharing_request.id).to be nil
  end
end
