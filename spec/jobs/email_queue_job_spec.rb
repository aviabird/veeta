require "rails_helper"

describe EmailQueueJob do
  it "should check for queue" do
    expect(EmailQueueJob.queue).to be :relaxed_mailer
  end

  it "should perform emailqueuejob" do
    application_setting = FactoryGirl.create(:application_setting)
    resume = FactoryGirl.create(:resume)
    resume.update_attribute :origin_id, resume.id
    resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)

    expect(EmailQueueJob.perform).to be true
  end

  it "should perform emailqueuejob through email_application" do
    application_setting = FactoryGirl.create(:application_setting)
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    resume = FactoryGirl.create(:resume)
    resume.update_attribute :origin_id, resume.id
    company_id = FactoryGirl.create(:company).id
    email_application = FactoryGirl.create(:email_application, id: company_id, company_id: company_id)
    resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id, company_id: nil, email_application_id: email_application.id)
    expect(EmailQueueJob.perform).to be true
  end

  it "should send update" do
    # application_setting = FactoryGirl.create(:application_setting)
    resume = FactoryGirl.create(:resume)
    resume.update_attribute :origin_id, resume.id
    resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    allow_any_instance_of(JobApplicationAndUpdateMailer).to receive(:resume_pdf).and_return("")
    resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
    company_id = resume.companies.first.id
    expect((EmailQueueJob.send_update resume, company_id, resume_sharing.id).is_a? LogService).to be true
  end

  it "should send update with recruiters" do
    # application_setting = FactoryGirl.create(:application_setting)
    resume = FactoryGirl.create(:resume)
    resume.update_attribute :origin_id, resume.id
    resume_sharing_request = FactoryGirl.create(:resume_sharing_request)
    allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
    allow_any_instance_of(JobApplicationAndUpdateMailer).to receive(:resume_pdf).and_return("")
    resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
    company_id = resume.companies.first.id
    recruiter =  Company.find(company_id).owner
    recruiter.update_attribute :company_id, company_id
    expect((EmailQueueJob.send_update resume, company_id, resume_sharing.id).present?).to be true
  end
end