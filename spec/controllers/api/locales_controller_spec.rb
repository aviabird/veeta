require "rails_helper"

RSpec.describe Api::LocalesController, :type => :controller do
  it "GET index should return countries" do
    @headers = {}
    @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    request.headers.merge!(@headers)
    get :index, format: :json, lang: "de"
    body = JSON.parse(response.body)
    expect(response.status).to be 200
  end
end