require "rails_helper"

RSpec.describe Api::Talent::TalentsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it "GET show should show" do
      request.headers.merge!(@headers)
      get :show, format: :json
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["talent"].present?).to be true
      expect(body["talent"]["id"]).to eq @talent_auth.talent.id
    end

    it "PUT update should get update" do
      request.headers.merge!(@headers)
      put :update, {format: :json, id: @talent_auth.talent.id,  talent: { phone_number: '0123456789' }}
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["talent"].present?).to be true
      expect(body["talent"]["id"]).to eq @talent_auth.talent.id
      expect(body["talent"]["phone_number"]).to eq("0123456789")
    end

    it "PUT update should not get update" do
      request.headers.merge!(@headers)
      put :update, {format: :json, id: @talent_auth.talent.id,  talent: { first_name: nil }}
      expect(response.status).to eq 422
      expect(JSON.parse(response.body)["success"]).to be false
    end

    it "PUT validate should just validate the input" do
      request.headers.merge!(@headers)
      put :validate, { format: :json, id: @talent_auth.talent.id, talent: { first_name: nil } }
      expect(response.status).to be 422
    end

    it "PUT validate should return just success" do
      request.headers.merge!(@headers)
      put :validate, { format: :json, id: @talent_auth.talent.id, talent: {}.merge!(FactoryGirl.attributes_for(:talent))}
      expect(response.status).to eq 200
    end

    it "DELETE avatar_remove should try to delete avatar" do
      request.headers.merge!(@headers)
      delete :avatar_remove, format: :json, talent: @talent_auth.talent
      expect(response.status).to eq 200
    end

    it "PUT should update_locale" do
      request.headers.merge!(@headers)
      put :update_locale, {format: :json, talent: {locale: "de"}}
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["talent"]["locale"]).to eq("de")
    end

    # it "PUT should avatar_upload" do
    #   request.headers.merge!(@headers)
    #   put :avatar_upload, {format: :json, talent: {avatar: "spec/data/images/portrait.jpg"}}
    # end
  end
end
