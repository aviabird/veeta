require "rails_helper"

RSpec.describe Api::Talent::TalentSettingsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it "GET show should show" do
      request.headers.merge!(@headers)
      get :show, format: :json, id: @talent_auth.talent.setting.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["talent_setting"].present?).to be true
      expect(body["talent_setting"]["id"]).to eq @talent_auth.talent.setting.id
    end

    it "put update should update" do
      request.headers.merge!(@headers)
      put :update, format: :json, id: @talent_auth.talent.setting.id, talent_setting: { professional_experience_level: 'starter' }
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["talent_setting"]["professional_experience_level"]).to eq 0
    end

    it "should just validate the input" do
      request.headers.merge!(@headers)
      put :validate, format: :json, talent_setting: { professional_experience_level: 200 }
      expect(response.status).to eq 422
    end

    it "should return just success" do
      request.headers.merge!(@headers)
      put :validate, format: :json, talent_setting: FactoryGirl.attributes_for(:talent_setting)
      expect(response.status).to eq 200
    end
  end
end
