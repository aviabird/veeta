require "rails_helper"

RSpec.describe Api::Talent::Auth::RegistrationsController, :type => :controller do

  describe "Create a talent account via xing" do
    before(:each) do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    end

    it "should init xing successfully" do
      allow_any_instance_of(XingClientService).to receive(:trigger_oauth_handshake).and_return('http://example.com')
      post :xing_init, {callback: 'http://t13s.at'}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["xing_auth_url"]).to match('http://example.com')
    end

    it "should fail when init xing" do
      allow_any_instance_of(XingClientService).to receive(:trigger_oauth_handshake).and_raise(XingApi::Error.new('error'))
      post :xing_init, {callback: 'http://t13s.at'}
      body = JSON.parse(response.body)
      expect(response.status).to be(400)
      expect(body["error"]).to be_truthy
    end

    describe "Xing callback handling" do
      let(:temp_cv) {FactoryGirl.create(:temp_cv_import)}

      it "should handle a xing callback successfully" do

        allow_any_instance_of(XingClientService).to receive(:handle_xing_callback_without_talent).and_return(temp_cv)
        post :xing_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(200)
        expect(body["status"]).to match('email_not_existing')
        expect(body["data"]["email"]).to match(temp_cv.email)
        expect(body["data"]["token"]).to match(temp_cv.token)
      end

      it "should handle a xing callback successfully" do
        FactoryGirl.create(:talent_auth, email: temp_cv.email, password: 12345678, password_confirmation: 12345678)
        allow_any_instance_of(XingClientService).to receive(:handle_xing_callback_without_talent).and_return(temp_cv)
        post :xing_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(422)
        expect(body["status"]).to match('email_existing')
        expect(body["data"]["email"]).to match(temp_cv.email)
        expect(body["data"]["token"]).to match(temp_cv.token)
      end

      it "should fail handling a xing callback" do
        FactoryGirl.create(:talent_auth, email: temp_cv.email, password: 12345678, password_confirmation: 12345678)
        allow_any_instance_of(XingClientService).to receive(:handle_xing_callback_without_talent).and_raise(StandardError.new)
        post :xing_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(400)
        expect(body["error"]).to be_truthy
      end

    end


  end


  describe "Create a talent account via LinkedIn" do
    before(:each) do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    end

    it "should init LinkedIn successfully" do
      allow_any_instance_of(LinkedinClientService).to receive(:trigger_oauth_handshake).and_return('http://example.com')
      post :linkedin_init, {callback: 'http://t13s.at'}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["linkedin_auth_url"]).to match('http://example.com')
    end

    it "should fail when init LinkedIn" do
      allow_any_instance_of(LinkedinClientService).to receive(:trigger_oauth_handshake).and_raise(StandardError.new('error'))
      post :linkedin_init, {callback: 'http://t13s.at'}
      body = JSON.parse(response.body)
      expect(response.status).to be(400)
      expect(body["error"]).to be_truthy
    end

    describe "LinkedIn callback handling" do
      let(:temp_cv) {FactoryGirl.create(:temp_cv_import, :linkedin)}

      it "should handle a LinkedIn callback successfully" do

        allow_any_instance_of(LinkedinClientService).to receive(:handle_linkedin_callback_without_talent).and_return(temp_cv)
        post :linkedin_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(200)
        expect(body["status"]).to match('email_not_existing')
        expect(body["data"]["email"]).to match(temp_cv.email)
        expect(body["data"]["token"]).to match(temp_cv.token)
      end

      it "should handle a LinkedIn callback successfully" do
        FactoryGirl.create(:talent_auth, email: temp_cv.email, password: 12345678, password_confirmation: 12345678)
        allow_any_instance_of(LinkedinClientService).to receive(:handle_linkedin_callback_without_talent).and_return(temp_cv)
        post :linkedin_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(422)
        expect(body["status"]).to match('email_existing')
        expect(body["data"]["email"]).to match(temp_cv.email)
        expect(body["data"]["token"]).to match(temp_cv.token)
      end

      it "should fail handling a LinkedIn callback" do
        FactoryGirl.create(:talent_auth, email: temp_cv.email, password: 12345678, password_confirmation: 12345678)
        allow_any_instance_of(LinkedinClientService).to receive(:handle_linkedin_callback_without_talent).and_raise(StandardError.new)
        post :linkedin_callback, {callback: 'http://t13s.at'}
        body = JSON.parse(response.body)
        expect(response.status).to be(400)
        expect(body["error"]).to be_truthy
      end

    end

  end


  describe "Create a talent account via file" do
    before(:each) do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    end

    it "should init a temp cv with not existing email" do
      allow_any_instance_of(ResumeService::ParsePDF).to receive(:hashed).and_return(JSON.parse('{"profile": {"personal": {"email":"test@t13s.at"}}}').deep_symbolize_keys!)
      post :create_via_file, resume_pdf: {file: Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))) }
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["status"]).to match('email_not_existing')
      expect(body["data"]["email"]).to match('test@t13s.at')
      expect(body["data"]["token"]).to_not be_blank
    end

    it "should init a temp cv with fake email" do
      allow_any_instance_of(ResumeService::ParsePDF).to receive(:hashed).and_return(JSON.parse('{"profile": {"personal": {"email":""}}}').deep_symbolize_keys!)
      post :create_via_file, resume_pdf: {file: Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))) }
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["status"]).to match('email_not_existing')
      expect(body["data"]["email"]).to_not be_blank
      expect(body["data"]["token"]).to_not be_blank
    end

    it "should init a temp cv with existing email" do
      talent_auth = FactoryGirl.create(:talent_auth, password: 12345678, password_confirmation: 12345678)
      allow_any_instance_of(ResumeService::ParsePDF).to receive(:hashed).and_return(JSON.parse('{"profile": {"personal": {"email":"'+talent_auth.email+'"}}}').deep_symbolize_keys!)
      post :create_via_file, resume_pdf: {file: Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))) }
      body = JSON.parse(response.body)
      expect(response.status).to be(422)
      expect(body["status"]).to match('email_existing')
      expect(body["data"]["email"]).to match(talent_auth.email)
      expect(body["data"]["token"]).to_not be_blank
    end
  end


  describe "Create a talent account" do
    let(:temp_cv) {FactoryGirl.create(:temp_cv_import, :file)}
    before(:each) do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    end

    it "should create a new account with random fake email" do
      post :create, {email: '', confirm_success_url: '', resume_type: 50, is_guest: true}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["status"]).to match('success')
      expect(body["resumeId"]).to be(nil)
      expect(body["data"]["guest"]).to be(true)
    end

    it "should create a new account with token" do
      post :create, {email: '', confirm_success_url: '', token: temp_cv.token}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["status"]).to match('success')
      expect(body["resumeId"]).to be_truthy
    end

    it "should create a new account with token" do
      post :create, {email: 'test@t13s.at', confirm_success_url: '', token: temp_cv.token}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["status"]).to match('success')
      expect(body["resumeId"]).to be_truthy
      expect(body["data"]["email"]).to match('test@t13s.at')
    end

    it "should create a new account with token" do
      talent_auth = FactoryGirl.create(:talent_auth, password: 12345678, password_confirmation: 12345678)
      post :create, {email: talent_auth.email, confirm_success_url: '', token: temp_cv.token}
      body = JSON.parse(response.body)
      expect(body["status"]).to match('error')
    end

  end

end
