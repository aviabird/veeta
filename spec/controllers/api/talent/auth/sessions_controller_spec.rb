require "rails_helper"

RSpec.describe Api::Talent::Auth::SessionsController, :type => :controller do
  let(:talent_auth) {FactoryGirl.create(:talent_auth, password: 12345678, password_confirmation: 12345678)}

  describe "Session Creation" do
    before(:each) do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    end

    it "should sign_in successfully" do
      post :create, {email: talent_auth.email, password: talent_auth.password}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["data"]["id"]).to be_truthy
      expect(body["data"]["email"]).to eq(talent_auth.email)
      expect(body["data"]["guest"]).to be false
    end

    it "should not sign_in without email" do
      post :create, {email: "", password: talent_auth.password}
      body = JSON.parse(response.body)
      expect(response.status).to be(422)
      expect(body["errors"][0]).to eq("talent.ui.sign_in.invalid_credentials")
    end

    it "should not sign_in without password" do
      post :create, {email: talent_auth.email, password: ""}
      body = JSON.parse(response.body)
      expect(response.status).to be(422)
      expect(body["errors"][0]).to eq("talent.ui.sign_in.invalid_credentials")
    end

    it "should not sign_in when locked" do
      talent_auth.update_attribute :locked_at, Time.now
      post :create, {email: talent_auth.email, password: talent_auth.password}
      body = JSON.parse(response.body)
      expect(response.status).to be(422)
      expect(body["errors"][0]).to eq("talent.ui.sign_in.locked_account")
    end

    it "should not login when too many attempt" do
      talent_auth.update_attribute :failed_attempts, 6
      post :create, {email: talent_auth.email, password: ""}
      body = JSON.parse(response.body)
      expect(response.status).to be(422)
      expect(body["errors"][0]).to eq("talent.ui.sign_in.locked_account")
    end
  end

  it "should destroy session" do
    request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    headers = talent_auth.create_new_auth_token
    delete :destroy, {}.merge(headers)
    expect(response.status).to be(200)
    expect(JSON.parse(response.body)["success"]).to be_truthy
  end

  it "should unauthorize the request" do
    request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    get :new
    body = JSON.parse(response.body)
    expect(response.status).to be(406)
    expect(body["status"]).to eq("error")
    expect(body["errors"][0]).to eq("Not allowed to perform this operation.")
  end
end