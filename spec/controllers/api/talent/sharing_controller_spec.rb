require "rails_helper"

RSpec.describe Api::Talent::SharingController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it "GET index should return all sharings" do
      request.headers.merge!(@headers)
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume.update_attribute :origin_id, @resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      get :index, {format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["sharings"].present?).to be true
      expect(body["sharings"].first["id"].present?).to be true
    end

    it "PUT revoke should revoke the sharing" do
      request.headers.merge!(@headers)
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume.update_attribute :origin_id, @resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      put :revoke , {format: :json, company_id: @resume.talent.companies.first.id, sharing: {is_company_revocation: true} }
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["meta"]["notice"]).to eq "Access revoked"
      expect(body["meta"]["message"]).to eq "notice"
    end

    it "PUT revoke should revoke sharing according to email application" do
      request.headers.merge!(@headers)
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume.update_attribute :origin_id, @resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      company_id = @resume.talent.companies.first.id
      email_application = FactoryGirl.create(:email_application,id: company_id, company_id: company_id)
      put :revoke , {format: :json, company_id: company_id, sharing: {is_company_revocation: false} }
      expect(response.status).to be 403
      expect(JSON.parse(response.body)["error"]).to eq "You are not authorized to access this page."
    end
  end
end