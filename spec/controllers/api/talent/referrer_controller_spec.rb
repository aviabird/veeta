require 'rails_helper'

RSpec.describe Api::Talent::ReferrersController, type: :controller do
=begin
  before(:each) do
    @talent_auth = FactoryGirl.create(:talent_auth)
    @company = FactoryGirl.create(:company)
    @referrer= FactoryGirl.create(:referrer)
    @job = FactoryGirl.build(:job, company_id: @company.id, recruiter_id: @company.owner.id)
    @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
    @job.save(validate: false)
    @jobapp = FactoryGirl.create(:jobapp, job_id: @job.id, referrer_id: @referrer.id, source: "other_source", job_type: "Job", resume_id: @talent_auth.talent.reload.resumes.first.id)
    @headers = @talent_auth.create_new_auth_token
    @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
  end


  it " GET get_referrers should return list of referrers" do
    request.headers.merge!(@headers)
    @company.referrer_ids = @referrer.id
    get :get_referrers, format: :json, job_id: @job.id
    expect(JSON.parse(response.body)["referrers"].first["id"]).to eq @referrer.id
  end

  it " GET get_referrers should raise error if job_id not presnet" do
    request.headers.merge!(@headers)
    get :get_referrers, format: :json, job_id: nil
    expect(JSON.parse(response.body)["errors"]).to eq "Can't find job_id."
  end

  it " GET get_referrers should return blank error" do
    request.headers.merge!(@headers)
    @company.referrer_ids = nil
    get :get_referrers, format: :json, job_id: @job.id
    expect(JSON.parse(response.body)["referrers"]).to eq []
  end
=end


end
