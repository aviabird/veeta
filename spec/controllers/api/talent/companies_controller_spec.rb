require "rails_helper"

RSpec.describe Api::Talent::CompaniesController, :type => :controller do
  describe "GET index" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end
    it "should retrive all companies" do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
      request.headers.merge!(@headers)
      get :index, {format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["companies"].present?).to be false
    end

    it "all companies" do
      request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      resume.update_attribute :origin_id, resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: resume.id)
      request.headers.merge!(@headers)
      get :index, {format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be(200)
      expect(body["companies"].present?).to be true
    end
  end
end