require "rails_helper"

RSpec.describe Api::Talent::CvSharingResponsesController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end
    context "GET show" do
      it "should show" do
        talent = FactoryGirl.create(:talent)
        cv_sharing_response = FactoryGirl.create(:cv_sharing_response)
        resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
        request.headers.merge!(@headers)
        get :show, {id: cv_sharing_response.id, format: :json}
      end
    end
  end
end
