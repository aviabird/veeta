require "rails_helper"

RSpec.describe Api::Talent::JobappsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    context "GET index" do
      it "should return blank Array" do
        request.headers.merge!(@headers)
        get :index, {format: :json}
        expect(response.status).to be 200
        expect(JSON.parse(response.body)["jobapps"].blank?).to be true
      end

      it "Should return array of jobaaps" do
        allow_any_instance_of(ActivityService).to receive(:sorted_activities).and_return([] << FactoryGirl.build_stubbed(:jobapp))
        request.headers.merge!(@headers)
        get :index, {format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body.has_key?("jobapps")).to be true
        expect(body["jobapps"].kind_of?(Array)).to be true
      end
    end


    it "GET show should return jobapp" do
      request.headers.merge!(@headers)
      resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      company = FactoryGirl.create(:company)
      job = FactoryGirl.build(:job, company_id: company.id, recruiter_id: company.owner.id)
      job.save(validate: false)
      jobapp = FactoryGirl.create(:jobapp, job_id: job.id, source: "other_source", job_type: "Job", resume_id: @talent_auth.talent.reload.resumes.first.id)
      get :show, {id: jobapp.id, format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["jobapp"].present?).to be true
      expect(body["jobapp"]["id"].present?).to be true
      expect(body["jobapp"]["id"]).to eq jobapp.id
    end

    it "GET new should instantiate the jobapp" do
      request.headers.merge!(@headers)
      get :new, {format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["jobapp"].present?).to be true
      expect(body["jobapp"]["id"]).to be nil
    end

    it "POST create should create the jobapp" do
      resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      versioned_resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id,version:2, origin_id: resume.id)
      company = FactoryGirl.create(:company)
      job = FactoryGirl.build(:job, company_id: company.id, recruiter_id: company.owner.id)
      job.save(validate: false)
      job_params = { jobapp: { source: "other_source", job_type: "Job", resume_id: resume.id, job_id: job.id, job_attributes: {id: job.id} } }
      request.headers.merge!(@headers)
      post :create, {format: :json}.merge!(job_params)
    end
  end
end
