require "rails_helper"

RSpec.describe Api::Talent::JobsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @company = FactoryGirl.create(:company)
      @job = FactoryGirl.build(:job, company_id: @company.id, recruiter_id: @company.owner.id)
      @job.save(validate: false)
    end
    context "GET show" do
      it "should return job with status new" do
        request.headers.merge!(@headers)
        get :show, {id: @job.id, format: :json}
        body = JSON.parse(response.body)
        expect(body["job"].present?).to be true
        expect(response.status).to be(200)
        expect(body["job"]["id"]).to eq(@job.id)
      end

      it "should not show job without status new" do
        request.headers.merge!(@headers)
        @job.update_attribute :status, nil
        get :show, {id: @job.id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end

      it "should not show job without valid_id" do
        request.headers.merge!(@headers)
        get :show, {id: "11212", format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["error"]).to eq "Record not found"
      end
    end
    context "GET find_by_ext_id" do
      it "should find record by ext_id with status new" do
        @job.update_attribute :ext_id, "121"
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: @job.ext_id, format: :json}
        body = JSON.parse(response.body)
        expect(body["job"].present?).to be true
        expect(response.status).to be(200)
        expect(body["job"]["id"]).to eq(@job.id)
      end

      it "should not find record by ext_id without status new" do
        @job.update_attributes(ext_id: "121", status: nil)
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: @job.ext_id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end

      it "should not find record without valid ext_id" do
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: "11212", format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end
    end

    context "GET get_current_openings" do
      it "should not return current active opening of a company without email" do
        request.headers.merge!(@headers)
        get :get_current_openings, format: :json
        body = JSON.parse(response.body)
        expect(body["jobs"].is_a? Array).to be true
        expect(body["jobs"].blank?).to be true
      end

      it "should not return current active opening of a company without email and domain match" do
        @job.update_attribute :status, "active"
        recruiter = Recruiter.first
        recruiter.update_attribute :company_id, @company.id
        request.headers.merge!(@headers)
        get :get_current_openings, {format: :json, email: "my@yopmail.com"}
        body = JSON.parse(response.body)
        expect(body["jobs"].is_a? Array).to be true
        expect(body["jobs"].blank?).to be true
      end

      it "should return current active opening of a company with email" do
        @job.update_attribute :status, "new"
        recruiter = Recruiter.first
        recruiter.update_attribute :company_id, @company.id
        request.headers.merge!(@headers)
        get :get_current_openings, {format: :json, email: recruiter.email}
        body = JSON.parse(response.body)
        expect(body["jobs"].is_a? Array).to be true
        expect(body["jobs"][0]["id"]).to eq @job.id
      end

      it "should return current active opening of a company by using domain" do
        @job.update_attribute :status, "new"
        recruiter = Recruiter.first
        recruiter.update_attribute :company_id, @company.id
        request.headers.merge!(@headers)
        email = '@' + recruiter.email.split("@").last
        get :get_current_openings, {format: :json, email: email}
        body = JSON.parse(response.body)
        expect(body["jobs"].is_a? Array).to be true
        expect(body["jobs"][0]["id"]).to eq @job.id
      end
    end
  end
end
