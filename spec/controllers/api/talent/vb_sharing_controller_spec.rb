require "rails_helper"

RSpec.describe Api::Talent::VbSharingController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end
    it "GET index return vb_sharings" do
      request.headers.merge!(@headers)
      get :index, format: :json
      expect(response.status).to be 200
      expect(JSON.parse(response.body).has_key? "sharings").to be true
    end
  end
end