require "rails_helper"

RSpec.describe Api::Talent::CvSharingRequestsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @headers = {}
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    context "GET show" do
      it "should show cv sharing request with status new" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request, status: 'new')
        request.headers.merge!(@headers)
        get :show, {id: cv_sharing_request.id, format: :json}
        body = JSON.parse(response.body)
        expect(body["cv_sharing_request"].present?).to be true
        expect(response.status).to be(200)
        expect(body["cv_sharing_request"]["id"]).to eq(cv_sharing_request.id)
      end

      it "should not show cv sharing request without status new" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request, status: 'closed')
        request.headers.merge!(@headers)
        get :show, {id: cv_sharing_request.id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end

      it "should not show cv sharing request without valid_id" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request)
        request.headers.merge!(@headers)
        get :show, {id: "11212", format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["error"]).to eq "Record not found"
      end
    end

    context "GET find_by_ext_id" do
      it "should find record by ext_id with status new" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request, ext_id: 'gBZs8', status: 'new')
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: cv_sharing_request.ext_id, format: :json}
        body = JSON.parse(response.body)
        expect(body["cv_sharing_request"].present?).to be true
        expect(response.status).to be(200)
        expect(body["cv_sharing_request"]["id"]).to eq(cv_sharing_request.id)
      end

      it "should not find record by ext_id without status new" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request, status: 'closed', ext_id: 'gBZs8')
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: cv_sharing_request.ext_id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end

      it "should not find record without valid ext_id" do
        cv_sharing_request = FactoryGirl.create(:cv_sharing_request)
        request.headers.merge!(@headers)
        get :find_by_ext_id, {ext_id: "11212", format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 404
        expect(body["success"]).to be false
        expect(body["errors"]).to eq "not found"
      end
    end
  end
end
