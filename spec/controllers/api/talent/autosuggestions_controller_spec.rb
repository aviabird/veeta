require "rails_helper"

RSpec.describe Api::Talent::AutosuggestionsController, :type => :controller do
  before(:each) do
    @talent_auth =  FactoryGirl.create(:talent_auth)
    @headers = @talent_auth.create_new_auth_token
    @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
  end

  it "should response with company name without language" do
    request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    request.headers.merge!(@headers)
    get :index, {format: :json, key: 'talent.work_experience.company', text: 'micr'}
    body = JSON.parse(response.body)
    expect(response.status).to be 200
    expect(body["autosuggestions"].is_a? Array).to be true
  end

  it "should response with company name with language" do
    request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    request.headers.merge!(@headers)
    get :index, {format: :json, key: 'talent.work_experience.company', text: 'micr', language: 'en'}
    body = JSON.parse(response.body)
    expect(response.status).to be 200
    expect(body["autosuggestions"].is_a? Array).to be true
  end

  it "should response with out company name" do
    request.env['devise.mapping'] = Devise.mappings[:api_talent_talent_auth]
    request.headers.merge!(@headers)
    get :index, {format: :json, key: 'talent.work_experience.company', text: 'test'}
    body = JSON.parse(response.body)
    expect(response.status).to be 200
    expect(body["autosuggestions"].is_a? Array).to be true
  end
end
