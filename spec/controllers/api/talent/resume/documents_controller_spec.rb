require "rails_helper"

RSpec.describe Api::Talent::Resume::DocumentsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/documents should return documents ' do
      request.headers.merge!(@headers)
      document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["documents"][0]["id"]).to eq document.id
    end

    it 'GET /api/talent/resume/{:resume_id}/document/{:id} should document' do
      request.headers.merge!(@headers)
      document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
      get :show, format: :json, id: document, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["document"]["id"]).to eq document.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/documents/new should get new document' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["document"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/documents' do
      it 'should not create document' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::Document).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, document: { file: nil }
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["document"]["id"]).to be nil
      end

      it 'should create document' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, document: FactoryGirl.attributes_for(:rs_document, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["document"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/documents/{:id} should update document' do
      request.headers.merge!(@headers)
      document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
      put :update, format: :json, resume_id: @resume.id, id: document.id, document: { file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF1.pdf')))}
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
    end

    it 'DELETE /api/talent/resume/{:resume_id}/documents/{:id} should delete document' do
      request.headers.merge!(@headers)
      document = FactoryGirl.create(:rs_document, resume_id: @resume.id, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
      delete :destroy, format: :json, id: document, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Document was successfully destroyed.")
    end

    describe 'POST /api/talent/resumes/{:resume_id}/document/validate' do
      it 'should validate the input' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, document: FactoryGirl.attributes_for(:rs_document, file: (Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'data', 'files', 'DemoPDF.pdf'))))
        body = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(body["document"]["id"]).to be nil
      end

      it 'should validate the input with invalid resources' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, document: FactoryGirl.attributes_for(:rs_document, file: nil)
        body = JSON.parse(response.body)
        expect(response.status).to eq 422
      end
    end
  end
end