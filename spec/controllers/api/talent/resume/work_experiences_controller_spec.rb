require "rails_helper"

RSpec.describe Api::Talent::Resume::WorkExperiencesController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/work_experiences should return work_experiences ' do
      request.headers.merge!(@headers)
      work_experience = FactoryGirl.create(:rs_work_experience, resume_id: @resume.id)
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["work_experiences"].select{|we| we["id"] == work_experience.id}).to be_present
    end

    it 'GET /api/talent/resume/{:resume_id}/work_experience/{:id} should work_experience' do
      request.headers.merge!(@headers)
      work_experience = FactoryGirl.create(:rs_work_experience, resume_id: @resume.id)
      get :show, format: :json, id: work_experience, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["work_experience"]["id"]).to eq work_experience.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/work_experiences/new should get new work_experience' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["work_experience"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/work_experiences' do
      it 'should not create work_experience' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::WorkExperience).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, work_experience: { position: 'Sales Associate' }
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["work_experience"]["id"]).to be nil
      end

      it 'should create work_experience' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, work_experience: FactoryGirl.attributes_for(:rs_work_experience, organization: 'Academy', subject: 'Comp Sci', year: 2016, work_experience_type: 'continued_education')
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["work_experience"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/work_experience/{:id} should update work_experience' do
      request.headers.merge!(@headers)
      work_experience = FactoryGirl.create(:rs_work_experience, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: work_experience.id, work_experience: { position: 'Sales Associate' }
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["work_experience"]["position"]).to eq "Sales Associate"
    end

    it 'DELETE /api/talent/resume/{:resume_id}/work_experience/{:id} should delete work_experience' do
      request.headers.merge!(@headers)
      work_experience = FactoryGirl.create(:rs_work_experience, resume_id: @resume.id)
      delete :destroy, format: :json, id: work_experience, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Work experience was successfully destroyed.")
    end

    describe 'POST /api/talent/resumes/{:resume_id}/work_experience/validate' do
      it 'should validate the input' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, work_experience: FactoryGirl.attributes_for(:rs_work_experience)
        body = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(body["work_experience"]["id"]).to be nil
      end

      it 'should validate the input with invalid resources' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, work_experience: FactoryGirl.attributes_for(:rs_work_experience, company_name: nil, position: nil)
        body = JSON.parse(response.body)
        expect(response.status).to eq 200 # this will be valid as presence validation only upon update
      end
    end
  end
end
