require "rails_helper"

RSpec.describe Api::Talent::Resume::CertificationsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/certifications should return certifications ' do
      request.headers.merge!(@headers)
      certification = FactoryGirl.create(:rs_certification, resume_id: @resume.id)
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["certifications"].last["id"]).to eq certification.id
    end

    it 'GET /api/talent/resume/{:resume_id}/certification/{:id} should certification' do
      request.headers.merge!(@headers)
      certification = FactoryGirl.create(:rs_certification, resume_id: @resume.id)
      get :show, format: :json, id: certification, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["certification"]["id"]).to eq certification.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/certifications/new should get new certification' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["certification"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/certifications' do
      let(:past_date) { 3.years.ago }

      it 'should not create certification' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::Certification).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, certification: { organization: 'Universitat at Wien', subject: nil, certification_type: 'wrong type', year: nil }
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["certification"]["id"]).to be nil
      end

      it 'should create certification' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, certification: FactoryGirl.attributes_for(:rs_certification, organization: 'Academy', subject: 'Comp Sci', year: 2016, certification_type: 'continued_education')
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["certification"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/certification/{:id} should update certification' do
      request.headers.merge!(@headers)
      certification = FactoryGirl.create(:rs_certification, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: certification.id, certification: { organization: 'Universitat at Wien' }
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["certification"]["organization"]).to eq "Universitat at Wien"
    end

    it 'DELETE /api/talent/resume/{:resume_id}/certification/{:id} should delete certification' do
      request.headers.merge!(@headers)
      certification = FactoryGirl.create(:rs_certification, resume_id: @resume.id)
      delete :destroy, format: :json, id: certification, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Certification was successfully destroyed.")
    end

    describe 'POST /api/talent/resumes/{:resume_id}/certification/validate' do
      it 'should validate the input' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, certification: FactoryGirl.attributes_for(:rs_certification)
        body = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(body["certification"]["id"]).to be nil
      end

      it 'should validate the input with invalid resources' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, certification: FactoryGirl.attributes_for(:rs_certification, organization: nil, subject: nil, year: nil)
        body = JSON.parse(response.body)
        expect(response.status).to eq 422
      end
    end
  end
end
