require "rails_helper"

RSpec.describe Api::Talent::Resume::EducationsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/educations should return educations ' do
      request.headers.merge!(@headers)
      education = FactoryGirl.create(:rs_education, resume_id: @resume.id)
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["educations"][0]["id"]).to eq education.id
    end

    it 'GET /api/talent/resume/{:resume_id}/education/{:id} should education' do
      request.headers.merge!(@headers)
      education = FactoryGirl.create(:rs_education, resume_id: @resume.id)
      get :show, format: :json, id: education, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["education"]["id"]).to eq education.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/educations/new should get new education' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["education"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/educations' do
      it 'should not create education' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::Education).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, education: { school_name: 'Universitat at Wien', from: 'now'}
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["education"]["id"]).to be nil
      end

      it 'should create education' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, education: FactoryGirl.attributes_for(:rs_education)
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["education"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/education/{:id} should update education' do
      request.headers.merge!(@headers)
      education = FactoryGirl.create(:rs_education, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: education.id, education: { school_name: "test school"}
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["education"]["school_name"]).to eq "test school"
    end

    it 'DELETE /api/talent/resume/{:resume_id}/education/{:id} should delete education' do
      request.headers.merge!(@headers)
      education = FactoryGirl.create(:rs_education, resume_id: @resume.id)
      delete :destroy, format: :json, id: education, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Education was successfully destroyed.")
    end

    it 'POST /api/talent/resumes/{:resume_id}/education/validate should validate the input' do
      request.headers.merge!(@headers)
      post :validate, format: :json, resume_id: @resume.id, education: FactoryGirl.attributes_for(:rs_education)
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["education"]["id"]).to be nil
    end
  end
end