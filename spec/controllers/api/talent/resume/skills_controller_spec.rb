require "rails_helper"

RSpec.describe Api::Talent::Resume::SkillsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/skills should return Skills ' do
      request.headers.merge!(@headers)
      skill = FactoryGirl.create(:rs_skill, resume_id: @resume.id)
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["skills"][0]["id"]).to eq skill.id
    end

    it 'GET /api/talent/resume/{:resume_id}/skills/{:id} should skill' do
      request.headers.merge!(@headers)
      skill = FactoryGirl.create(:rs_skill, resume_id: @resume.id)
      get :show, format: :json, id: skill, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["skill"]["id"]).to eq skill.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/Skills/new should get new skill' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["skill"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/Skills' do
      it 'should not create skill' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::Skill).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, skill: { name: nil}
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["skill"]["id"]).to be nil
      end

      it 'should create skill' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, skill: FactoryGirl.attributes_for(:rs_skill)
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["skill"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/skill/{:id} should update skill' do
      request.headers.merge!(@headers)
      skill = FactoryGirl.create(:rs_skill, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: skill.id, skill: { name: "test" }
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["skill"]["name"]).to eq "test"
    end

    it 'DELETE /api/talent/resume/{:resume_id}/skill/{:id} should delete skill' do
      request.headers.merge!(@headers)
      skill = FactoryGirl.create(:rs_skill, resume_id: @resume.id)
      delete :destroy, format: :json, id: skill, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Skill was successfully destroyed.")
    end

    describe 'POST /api/talent/resumes/{:resume_id}/skill/validate' do
      it 'should validate the input' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, skill: FactoryGirl.attributes_for(:rs_skill)
        body = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(body["skill"]["id"]).to be nil
      end
    end
  end
end