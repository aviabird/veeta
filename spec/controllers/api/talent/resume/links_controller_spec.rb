require "rails_helper"

RSpec.describe Api::Talent::Resume::LinksController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resume/{:resume_id}/link/{:id} should link' do
      request.headers.merge!(@headers)
      link = FactoryGirl.create(:rs_link, resume_id: @resume.id)
      get :show, format: :json, id: link, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["link"]["id"]).to eq link.id
    end

    it 'GET /api/talent/resumes/{:resume_id}/links/new should get new link' do
      request.headers.merge!(@headers)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["link"]["id"]).to be nil
    end

    describe 'POST /api/talent/resumes/{:resume_id}/links' do
      let(:past_date) { 3.years.ago }

      it 'should not create link' do
        request.headers.merge!(@headers)
        allow_any_instance_of(Rs::Link).to receive(:save).and_return(false)
        post :create, format: :json, resume_id: @resume.id, link: { organization: 'Universitat at Wien', subject: nil, link_type: 'wrong type', year: nil }
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["link"]["id"]).to be nil
      end

      it 'should create link' do
        request.headers.merge!(@headers)
        post :create, format: :json, resume_id: @resume.id, link: FactoryGirl.attributes_for(:rs_link, organization: 'Academy', subject: 'Comp Sci', year: 2016, link_type: 'continued_education')
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["link"]["id"].present?).to be true
      end
    end

    it 'PUT /api/talent/resume/{:resume_id}/link/{:id} should update link' do
      request.headers.merge!(@headers)
      link = FactoryGirl.create(:rs_link, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: link.id, link: { website: 'npath.net' }
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["link"]["website"]).to eq "npath.net"
    end

    describe 'POST /api/talent/resumes/{:resume_id}/link/validate' do
      it 'should validate the input' do
        request.headers.merge!(@headers)
        post :validate, format: :json, resume_id: @resume.id, link: FactoryGirl.attributes_for(:rs_link)
        body = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(body["link"]["id"]).to be nil
      end
    end
  end
end