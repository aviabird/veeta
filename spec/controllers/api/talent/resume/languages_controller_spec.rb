require "rails_helper"

RSpec.describe Api::Talent::Resume::LanguagesController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resumes/{:resume_id}/languages should return languages ' do
      request.headers.merge!(@headers)
      language = FactoryGirl.create(:rs_language, resume_id: @resume.id)
      get :index, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["languages"][0]["id"]).to eq language.id
    end

    it 'GET /api/talent/resume/{:resume_id}/language/{:id} should language' do
      request.headers.merge!(@headers)
      language = FactoryGirl.create(:rs_language, resume_id: @resume.id)
      get :show, format: :json, id: language, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["language"]["id"]).to eq language.id
    end

    # it 'GET /api/talent/resumes/{:resume_id}/languages/new should get new language' do
    #   request.headers.merge!(@headers)
    #    allow_any_instance_of(Rs::Language).to (:iso_code).and_return("en")
    #    allow_any_instance_of(Language::Hello).to (:by_iso).and_return("hello")
    #   binding.pry
    #   get :new, format: :json, resume_id: @resume.id
    #   binding.pry
    #   body = JSON.parse(response.body)
    #   expect(response.status).to be 200
    #   expect(body["language"]["id"]).to be nil
    # end

    # describe 'POST /api/talent/resumes/{:resume_id}/languages' do
    #   it 'should not create language' do
    #     request.headers.merge!(@headers)
    #     allow_any_instance_of(Rs::language).to receive(:save).and_return(false)
    #     post :create, format: :json, resume_id: @resume.id, language: { school_name: 'Universitat at Wien', from: 'now'}
    #     expect(response.status).to eq 200
    #     body = JSON.parse(response.body)
    #     expect(response.status).to be 200
    #     expect(body["language"]["id"]).to be nil
    #   end

    #   it 'should create language' do
    #     request.headers.merge!(@headers)
    #     post :create, format: :json, resume_id: @resume.id, language: FactoryGirl.attributes_for(:rs_language)
    #     body = JSON.parse(response.body)
    #     expect(response.status).to be 200
    #     expect(body["language"]["id"].present?).to be true
    #   end
    # end

    it 'PUT /api/talent/resume/{:resume_id}/language/{:id} should update language' do
      request.headers.merge!(@headers)
      language = FactoryGirl.create(:rs_language, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, id: language.id, language: { level: 3}
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["language"]["level"]).to be 3
    end

    it 'DELETE /api/talent/resume/{:resume_id}/language/{:id} should delete language' do
      request.headers.merge!(@headers)
      language = FactoryGirl.create(:rs_language, resume_id: @resume.id)
      delete :destroy, format: :json, id: language, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["meta"]["notice"]).to eq("Language was successfully destroyed.")
    end

    # describe 'POST /api/talent/resumes/{:resume_id}/language/validate' do
    #   it 'should validate the input' do
    #     request.headers.merge!(@headers)
    #     post :validate, format: :json, resume_id: @resume.id, language: FactoryGirl.attributes_for(:rs_language)
    #     body = JSON.parse(response.body)
    #     expect(response.status).to eq 200
    #     expect(body["language"]["id"]).to be nil
    #   end

    #   it 'should validate the input with invalid resources' do
    #     request.headers.merge!(@headers)
    #     post :validate, format: :json, resume_id: @resume.id, language: FactoryGirl.attributes_for(:rs_language, school_name: nil)
    #     body = JSON.parse(response.body)
    #     expect(response.status).to eq 422
    #   end
    # end
  end
end