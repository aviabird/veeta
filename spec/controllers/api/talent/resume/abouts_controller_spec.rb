require "rails_helper"

RSpec.describe Api::Talent::Resume::AboutsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it 'GET /api/talent/resume/about/{:id}' do
      request.headers.merge!(@headers)
      about = FactoryGirl.create(:rs_about, resume_id: @resume.id)
      get :show, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["about"]["id"]).to eq about.id
      expect(body["about"]["text"]).to eq "MyText"
    end

    it 'GET /api/talent/resumes/{:resume_id}/new' do
      request.headers.merge!(@headers)
      about = FactoryGirl.create(:rs_about, resume_id: @resume.id)
      get :new, format: :json, resume_id: @resume.id
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["about"]["id"]).to be nil
      expect(body["about"]["text"]).to be nil
    end

    it "POST /api/talent/resumes/{:resume_id}/about" do
      request.headers.merge!(@headers)
      about = FactoryGirl.create(:rs_about, resume_id: @resume.id)
      allow_any_instance_of(Rs::About).to receive(:save).and_return(true)
      post :create, format: :json, resume_id: @resume.id, about: FactoryGirl.attributes_for(:rs_about, text: "I'm so cool")
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["meta"]["notice"]).to eq "About was successfully created."
    end

    it "PUT /api/talent/resume/about/{:id}" do
      request.headers.merge!(@headers)
      about = FactoryGirl.create(:rs_about, resume_id: @resume.id)
      put :update, format: :json, resume_id: @resume.id, about: { text: "I'm so cool" }
      expect(response.status).to eq 200
    end

    # here is no case for invalid because we dont have any validation on about model
    it 'POST /api/talent/resumes/{:resume_id}/about/validate should validate the input' do
      request.headers.merge!(@headers)
      post :validate, format: :json, resume_id: @resume.id, about: FactoryGirl.attributes_for(:rs_about, text: "I'm so cool")
      body = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(body["about"]["id"]).to be nil
      expect(body["about"]["text"]).to eq "I'm so cool"
    end
  end
end