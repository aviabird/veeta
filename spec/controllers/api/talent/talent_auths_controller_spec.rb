require "rails_helper"

RSpec.describe Api::Talent::TalentAuthsController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    it "PUT email should set email" do
      request.headers.merge!(@headers)
      @talent_auth.talent.update_attribute :phone_number, "0123456789"
      put :email, {format: :json, id: @talent_auth.id, talent_auth: {email: @talent_auth.email}}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["talent_auth"]["id"]).to eq @talent_auth.id
      expect(body["talent_auth"]["email"]).to eq @talent_auth.email
    end

    it "PUT email should set email" do
      request.headers.merge!(@headers)
      put :email, {format: :json, id: @talent_auth.id, talent_auth: {email: @talent_auth.email}}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["talent_auth"]["id"]).to eq @talent_auth.id
      expect(body["talent_auth"]["email"]).to eq @talent_auth.email
    end

    it "PUT password set password for talent" do
      request.headers.merge!(@headers)
      @talent_auth.talent.update_attribute :phone_number, "0123456789"
      put :password, {format: :json, id: @talent_auth.id, talent_auth: {email: @talent_auth.email, password: '12345678'}}
      body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(body["talent_auth"]["id"]).to eq @talent_auth.id
      expect(body["talent_auth"]["email"]).to eq @talent_auth.email
      expect(body["meta"]["error"]["errors"]["guest"][0]).to eq "only can update password"
    end

    it "PUT delete should not delete account with delete_reason" do
      request.headers.merge!(@headers)
      @talent_auth.talent.update_attribute :phone_number, "0123456789"
      put :delete, {format: :json, id: @talent_auth.id, talent_auth: {email: @talent_auth.email, password: '12345678'}}
      body = JSON.parse(response.body)
      expect(response.status).to be 422
      expect(body["meta"]["error"]["errors"]["delete_reason"][0]).to eq "wasn't filled in"
    end

    it "PUT delete should delete account" do
      request.headers.merge!(@headers)
      @talent_auth.talent.update_attribute :phone_number, "0123456789"
      allow_any_instance_of(Intercom::Service::User).to receive(:find).and_return(nil)
      put :delete, {format: :json, id: @talent_auth.id, talent_auth: {email: @talent_auth.email, password: '12345678', delete_reason: "test deleted"} }
      body = JSON.parse(response.body)
      @talent_auth.talent.setting.reload
      expect(response.status).to be 200
      expect(@talent_auth.talent.setting.newsletter_updates).to eq('disabled')
      expect(body["talent_auth"]["id"]).to eq @talent_auth.id
    end

    it "PUT resend_confirmation" do
      request.headers.merge!(@headers)
      @talent_auth.talent.update_attribute :phone_number, "0123456789"
      put :resend_confirmation, {format: :json, id: @talent_auth.id}
      expect(response.status).to be 200
      body = JSON.parse(response.body)
      expect(body["talent_auth"]["id"]).to eq @talent_auth.id
      expect(body["talent_auth"]["email"]).to eq @talent_auth.email
    end
  end
end
