require "rails_helper"

RSpec.describe Api::Talent::ResumesController, :type => :controller do
  describe "Controller Action" do
    before(:each) do
      @talent_auth =  FactoryGirl.create(:talent_auth)
      @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
      @headers = @talent_auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    end

    context "GET index" do
      it "should return array of resumes" do
        request.headers.merge!(@headers)
        get :index, {format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body.has_key?("resumes")).to be true
        expect(body["resumes"].kind_of?(Array)).to be true
      end

      it "should return blank Array" do
        @resume.update_attribute :origin_id, @resume.id
        request.headers.merge!(@headers)
        get :index, {format: :json}
        expect(response.status).to be 200
        expect(JSON.parse(response.body)["resumes"].blank?).to be true
      end
    end

    context "GET show" do
      it "should return resume" do
        pdf_template_setting = FactoryGirl.build(:pdf_template_setting, resume_id: @resume.id)
        pdf_template_setting.save(validate: false)
        request.headers.merge!(@headers)
        get :show, {id: @resume.id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["resume"].present?).to be true
        expect(body["resume"]["id"]).to eq @resume.id
      end

      it "should return default resume" do
        pdf_template_setting = FactoryGirl.build(:pdf_template_setting, resume_id: 1234, pdf_template_type: "myveeta_base", font_face: "Arial")
        pdf_template_setting.save(validate: false)
        request.headers.merge!(@headers)
        get :show, {id: @resume.id, format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body["resume"].present?).to be true
        expect(body["resume"]["id"]).to eq @resume.id
      end
    end

    context "GET new" do
      it "should return instance of resume" do
        allow(Resume).to receive(:prepare).and_return(@resume)
        request.headers.merge!(@headers)
        get :new, {format: :json}
        body = JSON.parse(response.body)
        expect(response.status).to be 200
        expect(body.has_key?("resume")).to be true
      end
    end

    it "PUT update should update resume" do
      request.headers.merge!(@headers)
      put :update, {id: @resume.id, resume: {name: "test"}, format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["resume"]["id"]).to eq @resume.id
      expect(body["resume"]["name"]).to eq "test"
    end

    it "DELETE destroy should destroy resume" do
      request.headers.merge!(@headers)
      delete :destroy, {id: @resume.id, format: :json}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["resume"]["id"]).to eq @resume.id
    end

    it "GET validate should return valid resume" do
      request.headers.merge!(@headers)
      @resume.talent.update_attribute :phone_number, "0123456789"
      get :validate, {format: :json, resume: {talent_id: "49da1bc3-f4c7-4125-91b1-f56019ee9177", language_id: "101110", name: "My Resume", work_experience_duration: nil, origin_id: nil, version: 1, origin_updated_at: nil, pdf: nil, token: nil }}
      body = JSON.parse(response.body)
      expect(response.status).to be 200
      expect(body["resume"]["id"]).to be nil
      expect(body["resume"]["name"]).to eq "My Resume"
    end

    it "PUT share should share resume" do
      request.headers.merge!(@headers)
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume.update_attribute :origin_id, @resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      allow_any_instance_of(Resume).to receive(:share).and_return(@resume)
      # resume.expects(:share).with(company_ids: [1,2])
      put :share, format: :json, id: @resume.id, resume: { company_ids: [1, 2] }
      # response.status.must_equal 200
    end

    it "PUT sharing_request should share request" do
      request.headers.merge!(@headers)
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume.update_attribute :origin_id, @resume.id
      resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      allow_any_instance_of(Resume).to receive(:share).and_return(@resume)
      put :sharing_request, {id: @resume.id, format: :json, resume_sharing: {email: 'sharing_request_foo@t13s.at', contact_name: 'Foo'}}
    end
  end
end
