require "rails_helper"

RSpec.describe Api::LanguagesController, :type => :controller do
  it "GET index should return countries" do
    @headers = {}
    @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
    request.headers.merge!(@headers)
    get :index, format: :json
    body = JSON.parse(response.body)
    expect(response.status).to be 200
    expect(body["languages"].is_a? Array).to be true
    expect(body["languages"].present?).to be true
  end
end