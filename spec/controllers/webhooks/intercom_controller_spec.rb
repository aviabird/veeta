require "rails_helper"

RSpec.describe Webhooks::IntercomController, :type => :controller do
  before(:each) do
    @talent_auth =  FactoryGirl.create(:talent_auth)
    @resume = FactoryGirl.create(:resume, talent_id: @talent_auth.talent.id)
    @headers = @talent_auth.create_new_auth_token
    @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
  end

  describe "intercom unsubscribe webhook" do
    it "POST unsubscription without available intercom user id " do
      request.headers.merge!(@headers)
      jsondata =
       {
         "data" => {
           "item" => {
              "email" => "#{@talent_auth.email}",
              "unsubscribed_from_emails" => false,
              "user_id" => "123",
           }
         }
       }
      post :unsubscribe, jsondata.to_json
      body = JSON.parse(response.body)
      expect(@talent_auth.talent.setting.newsletter_updates).to eq 'enabled'
      expect(response.status).to be 200
    end

    it "POST intercom unsubscribe webhook " do
      request.headers.merge!(@headers)
      allow_any_instance_of(Talent).to receive(:intercom_user_id).and_return('123')
      jsondata =
       {
         "data" => {
           "item" => {
              "email" => "#{@talent_auth.email}",
              "unsubscribed_from_emails" => true,
              "user_id" => "123",
           }
         }
       }
      post :unsubscribe, jsondata.to_json
      body = JSON.parse(response.body)
      @talent_auth.reload
      expect(@talent_auth.talent.setting.newsletter_updates).to eq 'disabled'
      expect(response.status).to be 200
    end

    it "POST intercom unsubscribe webhook " do
      @talent_auth.talent.setting.update_columns(newsletter_updates: 'disabled')
      allow_any_instance_of(Talent).to receive(:intercom_user_id).and_return('123')
      request.headers.merge!(@headers)
      jsondata =
       {
         "data" => {
           "item" => {
              "email" => "#{@talent_auth.email}",
              "unsubscribed_from_emails" => false,
              "user_id" => "123",
           }
         }
       }
      post :unsubscribe, jsondata.to_json
      body = JSON.parse(response.body)
      @talent_auth.reload
      expect(@talent_auth.talent.setting.newsletter_updates).to eq 'enabled'
      expect(response.status).to be 200
    end


  end
end
